<?php

/**
 * Licentia Crypta - Attributes ACL
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Attributes ACL
 * @category   Easy of Use
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Crypta_Block_Adminhtml_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    public function getTabLabel() {
        return $this->__('Product Tabs');
    }

    public function getTabTitle() {
        return $this->__('Product Tabs');
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }

    protected function _prepareForm() {

        $role = Mage::registry("current_role");
        $current = false;
        if ($role->getId()) {
            $current = Mage::getModel('crypta/tabs')->loadAcl($role->getId());
        }

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("crypta_tabs", array("legend" => $this->__("Tabs Access")));

        $options = Mage::getModel('crypta/tabs')->toOptionArray();
        foreach ($options as $tab) {
            $fieldset->addField($tab['value'], "select", array(
                "label" => $this->__('Hide') . ' ' . $tab['label'],
                "options" => array('1' => $this->__('Yes'), '0' => $this->__('No')),
                "name" => "crypta_tabs[{$tab['value']}]",
                "value" => in_array($tab['value'], $current) ? 1 : 0,
            ));
        }

        return parent::_prepareForm();
    }

}
