<?php

/**
 * Licentia Crypta - Attributes ACL
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Attributes ACL
 * @category   Easy of Use
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Crypta_Model_Attributes extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('crypta/attributes');
    }

    public function savePost($params, $roleId) {

        $collection = $this->getCollection()
                ->addFieldToFilter('role_id', $roleId);

        foreach ($collection as $item) {
            $item->delete();
        }

        foreach ($params as $key => $param) {
            if ($param != 1) {
                continue;
            }
            $data = array();
            $data['role_id'] = $roleId;
            $data['attribute_code'] = $key;
            $this->setData($data)->save();
        }
    }

    public function loadAcl($roleId) {

        $collection = $this->getCollection()
                ->addFieldToFilter('role_id', $roleId);

        $return = array();

        foreach ($collection as $item) {
            $return[$item->getAttributeCode()] = $item->getAttributeCode();
        }

        return $return;
    }

    public function toOptionArray() {

        $attributes = Mage::getResourceModel('catalog/product_attribute_collection')
                ->addVisibleFilter()
                ->getItems();

        $attributeArray = array();
        foreach ($attributes as $attribute) {
            $attributeArray[] = array(
                'label' => $attribute->getFrontendLabel(),
                'value' => $attribute->getAttributecode()
            );
        }
        return $attributeArray;
    }

}
