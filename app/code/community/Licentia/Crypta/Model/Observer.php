<?php

/**
 * Licentia Crypts - Attributes ACL
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Attributes ACL
 * @category   Easy of Use
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Crypta_Model_Observer {

    public function saveVars($event) {
        $request = Mage::app()->getRequest();

        $role = $event->getObject();

        $role->setData('crypta_add', $request->getParam('crypta_add'));
        $role->setData('crypta_delete', $request->getParam('crypta_delete'));


        Mage::getModel('crypta/tabs')->savePost($request->getParam('crypta_tabs'), $request->getParam('role_id'));
        Mage::getModel('crypta/attributes')->savePost($request->getParam('crypta_attributes'), $request->getParam('role_id'));
    }

    public function addFormFields($event) {
        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin)
            return;

        $class = get_class($event->getBlock());

        if ($class == 'Mage_Adminhtml_Block_Permissions_Tab_Roleinfo') {
            $form = $event->getBlock()->getForm()->getElement('base_fieldset');
            $role = Mage::registry('current_role');

            $form->addField('crypta_add', 'select', array(
                'name' => 'crypta_add',
                'label' => Mage::helper('crypta')->__('Allow Add Products'),
                'title' => Mage::helper('crypta')->__('Allow Add Products'),
                'note' => Mage::helper('crypta')->__('When adding products ALL REQUIRED attributes will be enabled, even if the user has no permissions'),
                'options' => array('0' => Mage::helper('adminhtml')->__('No'), '1' => Mage::helper('adminhtml')->__('Yes')),
            ));

            $form->addField('crypta_delete', 'select', array(
                'name' => 'crypta_delete',
                'label' => Mage::helper('crypta')->__('Allow Delete Products'),
                'title' => Mage::helper('crypta')->__('Allow Delete Products'),
                'options' => array('0' => Mage::helper('adminhtml')->__('No'), '1' => Mage::helper('adminhtml')->__('Yes')),
            ));

            $event->getBlock()->getForm()->setValues($role->getData());
        }
    }

    public function checkAclTabs($event) {
        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin)
            return;

        $class = get_class($event->getBlock());
        $block = $event->getBlock();

        if ($class == 'Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs') {
            $remove = Mage::getModel('crypta/tabs')->loadAcl($admin->getRole()->getId());

            foreach ($remove as $tab) {
                if ($block == 'websites') {
                    if (Mage::registry('product')->getId()) {
                        Mage::register('crypta_hide_websites', true);
                    }
                } else {
                    $block->removeTab($tab);
                }
            }

            return;
        }
    }

    public function checkAclMassActions($event) {

        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin) {
            return;
        }

        $block = $event->getBlock();

        $role = Mage::getModel('admin/role')->load($admin->getRole()->getId());
        if ($role->getData('crypta_delete') == 0) {

            $block->getMassactionBlock()->removeItem('delete');
        }
    }

    public function checkDelete($event) {
        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin) {
            return;
        }

        $role = Mage::getModel('admin/role')->load($admin->getRole()->getId());
        if ($role->getData('crypta_delete') == 0) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('crypta')->__('Access Denied. Can not delete products'));

            $url = Mage::getModel('adminhtml/url');
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($url->getUrl('*/*'))
                    ->sendResponse();
            die();
        }
    }

    public function checkAdd($event) {
        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin) {
            return;
        }

        $role = Mage::getModel('admin/role')->load($admin->getRole()->getId());
        if ($role->getData('crypta_add') == 0) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('crypta')->__('Access Denied. Can not add products'));

            $url = Mage::getModel('adminhtml/url');
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($url->getUrl('*/*'))
                    ->sendResponse();
            die();
        }
    }

    public function checkAcl($event) {

        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin) {
            return;
        }

        $role = Mage::getModel('admin/role')->load($admin->getRole()->getId());
        if (!Mage::registry('product')->getId() && $role->getData('crypta_add') == 0) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('crypta')->__('Access Denied. Can not add products'));

            $url = Mage::getModel('adminhtml/url');
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect($url->getUrl('*/*'))
                    ->sendResponse();
            die();
        }

        $renderer = Mage::getBlockSingleton('crypta/adminhtml_render');
        $remove = Mage::getModel('crypta/attributes')->loadAcl($admin->getRole()->getId());

        $form = $event->getForm();

        foreach ($remove as $attribute) {

            $el = $form->getElement($attribute);

            if ($el && ($el->getRequired() == false || Mage::registry('product')->getId())) {
                $el->setReadOnly(true, true)->setRenderer($renderer);
            }
        }
    }

}
