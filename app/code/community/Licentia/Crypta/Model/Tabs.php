<?php


/**
 * Licentia Crypta - Attributes ACL
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Attributes ACL
 * @category   Easy of Use
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Crypta_Model_Tabs extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('crypta/tabs');
    }

    public function savePost($params, $roleId) {

        $collection = $this->getCollection()
                ->addFieldToFilter('role_id', $roleId);

        foreach ($collection as $item) {
            $item->delete();
        }

        foreach ($params as $key => $param) {
            if ($param != 1) {
                continue;
            }
            $data = array();
            $data['role_id'] = $roleId;
            $data['tab'] = $key;
            $this->setData($data)->save();
        }
    }

    public function loadAcl($roleId) {

        $collection = $this->getCollection()
                ->addFieldToFilter('role_id', $roleId);

        $return = array();

        foreach ($collection as $item) {
            $return[$item->getTab()] = $item->getTab();
        }

        return $return;
    }

    public function toOptionArray() {

        $return = array();
        $return[] = array('label' => Mage::helper('adminhtml')->__('Inventory'), 'value' => 'inventory');
        $return[] = array('label' => Mage::helper('adminhtml')->__('websites'), 'value' => 'websites');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Categories'), 'value' => 'categories');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Related'), 'value' => 'related');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Up-sells'), 'value' => 'upsell');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Cross-sells'), 'value' => 'crosssell');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Product Alerts'), 'value' => 'productalert');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Product Reviews'), 'value' => 'reviews');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Product Tags'), 'value' => 'tags');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Customers Tagged Product'), 'value' => 'customers_tags');
        $return[] = array('label' => Mage::helper('adminhtml')->__('Custom Options'), 'value' => 'customer_options');

        return $return;
    }

}
