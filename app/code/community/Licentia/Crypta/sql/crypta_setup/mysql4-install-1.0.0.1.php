<?php

/**
 * Licentia Crypta - Attributes ACL
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Attributes ACL
 * @category   Easy of Use
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
$installer = $this;
$installer->startSetup();

$installer->run("ALTER TABLE `{$this->getTable('admin_role')}` ADD COLUMN `crypta_add` enum('0','1') DEFAULT '1'");
$installer->run("ALTER TABLE `{$this->getTable('admin_role')}` ADD COLUMN `crypta_delete` enum('0','1') DEFAULT '1'");

$installer->run("

-- ----------------------------
--  Table structure for `crypta_attributes`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('crypta_attributes')}`;
CREATE TABLE `{$this->getTable('crypta_attributes')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `attribute_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_CRYPTA_ATTRIBUTES_ROLEID` FOREIGN KEY (`role_id`) REFERENCES `{$this->getTable('admin_role')}` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Crypta - ACL Product Attributes';

-- ----------------------------
--  Table structure for `crypta_tabs`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('crypta_tabs')}`;
CREATE TABLE `{$this->getTable('crypta_tabs')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `tab` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_CRYPTA_TABS_ROLEID` FOREIGN KEY (`role_id`) REFERENCES `{$this->getTable('admin_role')}` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Crypta - ACL Product Tabs';

");

$installer->endSetup();
