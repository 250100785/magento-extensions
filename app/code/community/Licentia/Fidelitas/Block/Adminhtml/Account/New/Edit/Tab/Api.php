<?php

/**
 * Licentia Fidelitas - Advanced Email and SMS Marketing Automation for E-Goi
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International  
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International 
 */


class Licentia_Fidelitas_Block_Adminhtml_Account_New_Edit_Tab_Api extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("fidelitas_form", array("legend" => $this->__("New E-Goi Customers")));

        $fieldset->addField('apikey', "text", array(
            "name" => 'apikey',
            "label" => $this->__('Your APi Key'),
            "note" => $this->__('To get your API Key, login into your E-goi.com panel, go to your user menu (upper right corner), select "Integrations" and copy the account API key.'),
            "class" => "required-entry",
            "required" => true,
        ));


        return parent::_prepareForm();
    }

}
