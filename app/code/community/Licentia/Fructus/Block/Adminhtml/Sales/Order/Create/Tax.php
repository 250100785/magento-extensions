<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Block_Adminhtml_Sales_Order_Create_Tax extends Mage_Core_Block_Template {

    public function getItem() {
        return $this->getParentBlock()->getData('item');
    }

}
