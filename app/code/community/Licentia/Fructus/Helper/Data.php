<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getExemptionsOptions() {
        return array(
            'M01' => 'Artigo 16.º n.º 6 alínea c) do CIVA',
            'M02' => 'Artigo 6.º do Decreto‐Lei n.º 198/90, de 19 de Junho',
            'M03' => 'Exigibilidade de caixa',
            'M04' => 'Isento Artigo 13.º do CIVA',
            'M05' => 'Isento Artigo 14.º do CIVA',
            'M06' => 'Isento Artigo 15.º do CIVA',
            'M07' => 'Isento Artigo 9.º do CIVA',
            'M08' => 'IVA – Autoliquidação',
            'M09' => 'IVA ‐ não confere direito a dedução',
            'M10' => 'IVA – Regime de isenção',
            'M11' => 'Não tributado',
            'M12' => 'Regime da margem de lucro – Agências de Viagens',
            'M13' => 'Regime da margem de lucro – Bens em segunda mão',
            'M14' => 'Regime da margem de lucro – Objetos de arte',
            'M15' => 'Regime da margem de lucro – Objetos de coleção e antiguidades',
            'M16' => 'Isento Artigo 14.º do RITI',
            'M99' => 'Não sujeito; não tributado (ou similar)',
        );
    }

}
