<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Model_Fructus extends Mage_Core_Model_Abstract {

    public function getTaxList() {
        return $this->client('taxes');
    }

    public function getTaxListSimple() {
        $list = $this->client('taxes');
        $return = array();

        foreach ($list['tax'] as $item) {
            $return[$item['value']] = $item['name'];
        }

        return $return;
    }

    public function getSequences() {
        $list = $this->client('sequences');

        if (isset($list['sequence']['id'])) {
            $list[0] = $list;
        }

        return $list;
    }

    public function updateClient($id, $data) {
        return $this->client('clients/' . $id, $data, 'PUT', 'client');
    }

    public function getClient($id) {
        return $this->client('clients/find-by-code.xml?client_code=' . $id);
    }

    public function getTaxName($percent) {
        $list = $this->getTaxList();

        foreach ($list['tax'] as $tax) {
            if ($tax['value'] == $percent) {
                return $tax['name'];
            }

            if ($tax['default_tax'] == 1) {
                $tmp = $tax['name'];
            }
        }
        return $tmp;
    }

    public function getInvoice($id) {
        return $this->client('invoices/' . $id);
    }

    public function getCreditmemo($id) {
        return $this->client('credit_notes/' . $id);
    }

    public function closeInvoice($id) {
        $data = array('state' => 'finalized');
        return $this->client('invoice/' . $id . '/change-state', $data, 'PUT', 'invoice');
    }

    public function createInvoice($data) {
        return $this->client('invoices', $data, 'POST', 'invoice');
    }

    public function closeCreditmemo($id) {
        $data = array('state' => 'finalized');
        return $this->client('credit_notes/' . $id . '/change-state', $data, 'PUT', 'invoice');
    }

    public function createCreditmemo($data) {
        return $this->client('credit_notes', $data, 'POST', 'credit_note');
    }

    public function getClients() {
        return $this->client('clients');
    }

    public function newClient($data) {
        return $this->client('clients', $data, 'POST', 'client');
    }

    public function newItem($data) {
        return $this->client('items', $data, 'POST');
    }

    public function getItems() {
        return $this->client('items');
    }

    public function deleteItem($id) {
        return $this->client('items/' . $id, null, 'DELETE');
    }

    public function updateItem($id, $data) {
        return $this->client('items/' . $id, $data, 'PUT');
    }

    public function client($url, $data = null, $method = 'GET', $element = 'item') {

        $storeId = Mage::registry('fructus_current_store');

        $domain = Mage::getStoreConfig('fructus/config/domain', $storeId);
        $api = Mage::getStoreConfig('fructus/config/api_key', $storeId);

        if (stripos($url, '?') === false) {
            $url .= '.xml?';
        } else {
            $url .= '&';
        }

        $url = 'https://' . $domain . '.invoicexpress.net/' . $url . 'api_key=' . $api;
        if ($data) {
            $data = $this->arrayToXML($data, $element);
            if (stripos($data, '<items>') !== false) {
                $data = str_replace("<items>", '<items type="array">', $data);
            }

        }

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_MUTE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if ($data) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, "$data");
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (substr($http_status, 0, 1) != 2 && $http_status != 422) {
            if ($http_status == 401) {
                return array('error' => Mage::helper('fructus')->__('InvoiceXpress: Access Denied. Please check your auth data. Error code:') . $http_status);
            } elseif ($http_status == 404) {
                return array('error' => Mage::helper('fructus')->__('InvoiceXpress: Document/Endpoint not found. Error code:') . $http_status);
            } else {
                return array('error' => Mage::helper('fructus')->__('InvoiceXpress: General Error. Code:') . $http_status);
            }
        }
        return $this->_object2array(simplexml_load_string($output));
    }

    protected function _object2array($data) {
        if (!is_object($data) && !is_array($data))
            return $data;

        if (is_object($data))
            $data = get_object_vars($data);

        return array_map(array($this, '_object2array'), $data);
    }

    public function arrayToXML($data, $startElement, $xml_version = '1.0', $xml_encoding = 'UTF-8') {
        if (!is_array($data)) {
            $err = 'Invalid variable type supplied, expected array not found on line ' . __LINE__ . " in Class: " . __CLASS__ . " Method: " . __METHOD__;
            trigger_error($err);
            return false;
        }
        $xml = new XmlWriter();
        $xml->openMemory();
        $xml->startDocument($xml_version, $xml_encoding);
        $xml->startElement($startElement);

        $this->write($xml, $data);

        $xml->endElement();
        return $xml->outputMemory(true);
    }

    public function write(XMLWriter $xml, $data) {
        foreach ($data as $key => $value) {
            if (is_array($value) && isset($value[0])) {
                foreach ($value as $itemValue) {
                    if (is_array($itemValue)) {
                        $xml->startElement($key);
                        $this->write($xml, $itemValue);
                        $xml->endElement();
                        continue;
                    }

                    if (!is_array($itemValue)) {
                        $xml->writeElement($key, $itemValue . "");
                    }
                }
            } else if (is_array($value)) {
                $xml->startElement($key);
                $this->write($xml, $value);
                $xml->endElement();
                continue;
            }

            if (!is_array($value)) {
                $xml->writeElement($key, $value . "");
            }
        }
    }

    public function test($storeId) {
        $domain = Mage::getStoreConfig('fructus/config/domain', $storeId);
        $api = Mage::getStoreConfig('fructus/config/api_key', $storeId);

        $url = 'https://' . $domain . '.invoicexpress.net/sequences?api_key=' . $api;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_MUTE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_status != 200) {
            return false;
        }

        return true;
    }

}
