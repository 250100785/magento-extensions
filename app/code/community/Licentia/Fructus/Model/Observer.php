<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Model_Observer extends Mage_Core_Model_Abstract {

    public function validateDocumentTaxes() {
        $orderId = Mage::app()->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);

        if (!$order->getId()) {
            return;
        }

        if ($order->getData('base_currency_code') != 'EUR') {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fructus')->__('This order will not be processed by InvoiceXpress because your base currency must be Euro (EUR)'));

            return;
        }

        $taxes = Mage::getModel('fructus/fructus')->getTaxListSimple();
        $items = $order->getAllItems();

        foreach ($items as $item) {
            if (!array_key_exists(number_format($item->getData('tax_percent'), 1), $taxes)) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fructus')->__('The item %s has a tax percent value (%d) that is not in your InvoiceXpress configured Tax. The default Tax value will be used', $item->getName(), $item->getData('tax_percent')));
            }
        }
    }

    public function validateEnvironment() {

        $request = Mage::app()->getRequest();
        $store = Mage::app()->getRequest()->getParam('store');
        $section = $request->getParam('section');
        if ($section != 'fructus') {
            return;
        }
        if ($store) {
            $c = Mage::getModel('core/store')->getCollection()
                    ->addFieldToFilter('code', $store);
            $storeId = $c->getFirstItem()->getStoreId();
        } else {
            $storeId = null;
        }

        $test = Mage::getModel('fructus/fructus')->test($storeId);
        if ($test) {
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('fructus')->__('Everyting seams ok with your configuration'));
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('fructus')->__('Something is wrong with your configuration. Please double check your info.'));
        }
    }

    public function processInvoice($event) {
        $document = $event->getInvoice();
        $type = 'invoice';

        if (!$document) {
            $document = $event->getCreditmemo();
            $type = 'creditmemo';
        }

        if ($document->getViewLink()) {
            return true;
        }

        Mage::register('fructus_current_store', $document->getStoreId(), true);

        $request = Mage::app()->getRequest();
        $taxExemptionReason = $request->getPost('tax-exemption');
        $commentTextPost = $request->getPost('invoice');
        $commentText = $commentTextPost['comment_text'];

        if ($type == 'creditmemo') {
            $commentTextPost = $request->getPost('creditmemo');
            $commentText = $commentTextPost['comment_text'];
            $refundShipping = $commentTextPost['shipping_amount'];
        }

        if (!$taxExemptionReason || !array_key_exists($taxExemptionReason, Mage::helper('fructus')->getExemptionsOptions())) {
            $taxExemptionReason = Mage::getStoreConfig('fructus/tax/exemption', $document->getStoreId());
        }

        $items = $document->getAllItems();
        $taxExemption = false;

        $order = $document->getOrder();

        if ($order->getData('base_currency_code') != 'EUR') {
            return true;
        }

        $data = array();

        $data['date'] = date('d/m/Y', strtotime($document->getCreatedAt()));

        $dueDays = Mage::getStoreConfig('fructus/config/days', $document->getStoreId());
        $data['due_date'] = date('d/m/Y', strtotime($document->getCreatedAt() . ' +' . $dueDays . ' days'));

        if (Mage::getStoreConfig('sales_pdf/invoice/put_order_id', $document->getStoreId()) &&
                $type == 'invoice') {
            $commentText = Mage::helper('fructus')->__('Order') . '# ' . $order->getRealOrderId() . ' | ' . $commentText;
        }
        if (Mage::getStoreConfig('sales_pdf/creditmemo/put_order_id', $document->getStoreId()) &&
                $type == 'creditmemo') {
            $commentText = Mage::helper('fructus')->__('Order') . '# ' . $order->getRealOrderId() . ' | ' . $commentText;
        }

        $sequenceId = Mage::getStoreConfig('fructus/config/sequence_id', $document->getStoreId());

        if ($sequenceId > 0 && $type == 'invoice') {
            $data['sequence_id'] = $sequenceId;
        }
        if ($type == 'creditmemo') {
            if ($sequenceId > 0) {
                #$data['sequence_id'] = $sequenceId;
            }
            $invoiceId = $order->getInvoiceCollection()->getFirstItem()->getData('fructus_id');
            $data['owner_invoice_id'] = $invoiceId;
        }

        if ($order->getData('order_currency_code') != 'EUR') {
            $data['currency_code'] = $order->getData('order_currency_code');
            $data['rate'] = $order->getData('base_to_order_rate');
        }

        $data['observations'] = $commentText;

        $dataItems = array();
        $data['items'] = array();
        foreach ($items as $item) {
            if ($item->getData('base_tax_amount') == 0) {
                $taxExemption = true;
            }

            $discountPercentage = 0;
            if ($item->getData('base_discount_amount') > 0) {
                $discountPercentage = round($item->getData('base_discount_amount') * 100 / $item->getData('base_row_total_incl_tax'), 1);
            }

            $orderItem = Mage::getModel('sales/order_item')->load($item->getOrderItemId());

            $taxName = Mage::getModel('fructus/fructus')->getTaxName($orderItem->getTaxPercent());

            $dataItems['name'] = $item->getSku();
            $dataItems['description'] = $item->getName();
            $dataItems['discount'] = $discountPercentage;
            $dataItems['unit'] = 'unit';
            $dataItems['unit_price'] = $item->getBasePrice();
            $dataItems['quantity'] = $item->getQty();
            $dataItems['tax'] = array('name' => $taxName);

            $data['items']['item'][] = $dataItems;
        }

        ##ENVIO
        $shippingTax = $this->getShippingTax($order);
        $taxName = Mage::getModel('fructus/fructus')->getTaxName($shippingTax);
        if ($type == 'invoice') {
            if (count($document->getOrder()->getInvoiceCollection()) == 1 && $document->getData('base_shipping_amount') > 0) {
                $dataItems['name'] = 'shipping';
                $dataItems['description'] = Mage::helper('fructus')->__('Shipping & Handling');
                $dataItems['unit'] = 'unit';
                $dataItems['discount'] = 0;
                $dataItems['unit_price'] = $document->getData('base_shipping_amount');
                $dataItems['quantity'] = 1;
                $dataItems['tax'] = array('name' => $taxName);
                $data['items']['item'][] = $dataItems;
            }
            if (($taxExemption || (int) $document->getData('base_shipping_tax_amount') == 0) &&
                    count($document->getOrder()->getInvoiceCollection()) == 1) {
                $data['tax_exemption'] = $taxExemptionReason;
            }
        }

        if ($type == 'creditmemo' && $refundShipping > 0) {
            $dataItems['name'] = 'shipping';
            $dataItems['description'] = Mage::helper('fructus')->__('Shipping & Handling');
            $dataItems['unit'] = 'unit';
            $dataItems['discount'] = 0;
            $dataItems['unit_price'] = $refundShipping;
            $dataItems['quantity'] = 1;
            $dataItems['tax'] = array('name' => $taxName);
            $data['items']['item'][] = $dataItems;
            if ($taxExemption || (int) $document->getData('base_shipping_tax_amount') == 0) {
                $data['tax_exemption'] = $taxExemptionReason;
            }
        }

        #FIM ENVIO
        //NO TAX REASON NEEDED IF NOT FROM PORTUGAL
        if (Mage::getStoreConfig('general/store_information/merchant_country', $document->getStoreId()) != 'PT') {
            unset($data['tax_exemption']);
        }

        $dataClient = array();
        $customerData = $document->getBillingAddress();

        $address = $customerData->getStreet();
        $address[] = $customerData->getCity();
        if ($customerData->getRegion()) {
            $address[] = $customerData->getRegion();
        }

        $dataClient['name'] = strlen($customerData->getCompany()) > 0 ? $customerData->getCompany() : $customerData->getFirstname() . ' ' . $customerData->getLastname();
        $dataClient['email'] = $customerData->getEmail();
        $dataClient['code'] = $customerData->getCustomerId() ? $customerData->getCustomerId() : 'DOC' . substr(strtoupper($type), 0, 1) . $document->getId();
        $dataClient['address'] = implode("\n ", $address);
        $dataClient['postal_code'] = $customerData->getPostcode();
        $dataClient['country'] = Mage::getModel('directory/country')->load($customerData->getCountryId())->getName();
        $dataClient['fiscal_id'] = $customerData->getVatId();
        $dataClient['phone'] = $customerData->getTelephone();
        $dataClient['fax'] = $customerData->getFax();

        $data['client'] = $dataClient;

        if ($customerData->getCustomerId()) {
            $exists = Mage::getModel('fructus/fructus')->getClient($customerData->getCustomerId());
            if ($exists) {
                Mage::getModel('fructus/fructus')->updateClient($exists['id'], $dataClient);
            }
        }

        if ($type == 'invoice') {
            $result = Mage::getModel('fructus/fructus')->createInvoice($data);
        }
        if ($type == 'creditmemo') {
            $result = Mage::getModel('fructus/fructus')->createCreditmemo($data);
        }


        if (!isset($result['error'])) {

            if ($type == 'creditmemo') {
                Mage::getModel('fructus/fructus')->closeCreditmemo($result['id']);
                $again = Mage::getModel('fructus/fructus')->getCreditmemo($result['id']);
                $permalink = $again['permalink'];
            }
            if ($type == 'invoice') {
                Mage::getModel('fructus/fructus')->closeInvoice($result['id']);
                $again = Mage::getModel('fructus/fructus')->getInvoice($result['id']);
                $permalink = $again['permalink'];
            }

            $document->setData('increment_id', $again['sequence_number']);
            $document->setData('view_link', $permalink);
            $document->setData('download_link', $permalink . '?format=pdf');
            $document->setData('fructus_id', $result['id'])->save();
        } else {
            throw new Mage_Core_Exception(implode("<br>", (array) $result['error']));
        }
    }

    public function getShippingTax($order) {
        $taxInfo = $order->getFullTaxInfo();
        foreach ($taxInfo as $tax) {
            $percent = '1.' . str_pad($tax['percent'], 2, 0, STR_PAD_LEFT);
            if (round($percent * $order->getBaseShippingAmount(), 2) == round($order->getBaseShippingInclTax(), 2)) {
                return $tax['percent'];
            }
        }
        return 0;
    }

}
