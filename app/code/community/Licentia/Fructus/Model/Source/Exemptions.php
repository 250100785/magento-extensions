<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Model_Source_Exemptions {

    public function toOptionArray() {
        $list = Mage::helper('fructus')->getExemptionsOptions();

        $array = array();
        foreach ($list as $key => $value) {
            $array[] = array('label' => $key . ' - ' . $value, 'value' => $key);
        }

        return $array;
    }

}
