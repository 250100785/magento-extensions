<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Model_Source_Payment {

    public function toOptionArray() {

        $array = array();
        $array[] = array('value' => 0, 'label' => Mage::helper('fructus')->__('Same Day'));
        $array[] = array('value' => 7, 'label' => 7);
        $array[] = array('value' => 15, 'label' => 15);
        $array[] = array('value' => 30, 'label' => 30);
        $array[] = array('value' => 60, 'label' => 60);
        $array[] = array('value' => 90, 'label' => 90);
        $array[] = array('value' => 120, 'label' => 120);

        return $array;
    }

}
