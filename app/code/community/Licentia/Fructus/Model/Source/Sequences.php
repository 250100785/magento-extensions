<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Model_Source_Sequences {

    public function toOptionArray() {
        $list = Mage::getModel('fructus/fructus')->getSequences();
        $array = array();
        $array[] = array('label' => Mage::helper('fructus')->__('Use Default'), 'value' => '0');

        foreach ($list['sequence'] as $value) {
            $default = $value['default_sequence'] == 1 ? ' (' . Mage::helper('fructus')->__('Default') . ')' : '';
            $array[] = array('label' => $value['serie'] . $default, 'value' => $value['id']);
        }

        return $array;
    }

}
