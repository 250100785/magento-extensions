<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Fructus_Adminhtml_Fructus_ItemsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('fructus/items');

        return $this;
    }

    public function indexAction() {

        /*
          $item = array(
          'name' => time(),
          'description' => 'Barcelos sempre',
          'unit_price' => rand(12, 34543),
          'unit' => 'unit',
          );

          $model = Mage::getModel('fructus/fructus')->getItems($item);

          foreach ($model['item'] as $item) {
          Mage::getModel('fructus/fructus')->deleteItem($item['id']);
          }

          $client = array('name' => 'Bento Vilas boas ' . time());

          $model = Mage::getModel('fructus/fructus')->getClients($client);

         */

        #$tax = Mage::getModel('fructus/fructus')->getTaxList();

        $client = Mage::getModel('fructus/fructus')->getSequences();

        echo "<pre>";

        print_r($client);

        echo "</pre>";

        die();


        $data = array(
            'date' => date('d/m/Y'),
            'due_date' => date('d/m/Y'),
            #'tax_exemption' => 'M03',
            'client' => array('name' => 'Bento Vilas Boas'),
            'items' => array(
                array('tax' => array('name' => 'IVA23'), 'name' => time(), 'description' => time() . ' - ois', 'unit_price' => rand(100, 798), 'quantity' => rand(1, 12), 'unit' => 'unit'),
                array('tax' => array('name' => 'IVA23'), 'name' => time(), 'description' => time() . ' - ois', 'unit_price' => rand(100, 798), 'quantity' => rand(1, 12), 'unit' => 'unit')
            ),
        );

        $model = Mage::getModel('fructus/fructus')->createInvoice($data);

        $t = Mage::getModel('fructus/fructus')->closeInvoice($model['id']);
        echo "<pre>";
        print_r($model);
        print_r($t);
        echo "</pre>";

        die();
    }

}
