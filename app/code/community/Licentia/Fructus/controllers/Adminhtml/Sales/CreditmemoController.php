<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
include_once("Mage/Adminhtml/controllers/Sales/CreditmemoController.php");

class Licentia_Fructus_Adminhtml_Sales_CreditmemoController extends Mage_Adminhtml_Sales_CreditmemoController {

    public function pdfcreditmemosAction() {
        $dir = Mage::getBaseDir('var') . '/creditmemo/';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $creditmemosIds = $this->getRequest()->getPost('creditmemo_ids');
        if (!empty($creditmemosIds)) {
            $invoices = Mage::getResourceModel('sales/order_creditmemo_collection')
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('entity_id', array('in' => $creditmemosIds))
                    ->load();


            foreach ($invoices as $invoice) {
                if ($invoice->getData('fructus_id')) {
                    $file = file_get_contents($invoice->getData('download_link'));
                    $fileDir = $dir . $invoice->getId() . '.pdf';
                    $file = file_put_contents($fileDir, $file);
                    if ($file) {
                        $pdf[] = Zend_Pdf::load($fileDir);
                        unlink($fileDir);
                    }
                } else {
                    $this->_getSession()->addError(Mage::helper('fructus')->__('You can not print a mix of creditmemos from InvoiceXpress and locally created using this method'));
                    $this->_redirectReferer();
                    return;
                }
            }

            $pdfMerged = new Zend_Pdf();
            foreach ($pdf as $item) {
                foreach ($item->pages as $page) {
                    $clonedPage = clone $page;
                    $pdfMerged->pages[] = $clonedPage;
                }
                unset($clonedPage);
            }

            return $this->_prepareDownloadResponse('creditmemo-' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') .
                            '.pdf', $pdfMerged->render(), 'application/pdf');
        }
        $this->_redirect('*/*/');
    }

}
