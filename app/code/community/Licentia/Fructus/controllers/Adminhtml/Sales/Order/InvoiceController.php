<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
include_once("Mage/Adminhtml/controllers/Sales/Order/InvoiceController.php");

class Licentia_Fructus_Adminhtml_Sales_Order_InvoiceController extends Mage_Adminhtml_Sales_Order_InvoiceController {

    public function printAction() {
        $this->_initInvoice();
        $invoice = Mage::registry('current_invoice');

        if (!$invoice->getData('fructus_id')) {
            return parent::printAction();
        }

        $this->_prepareDownloadResponse('invoice-' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') .
                '.pdf', file_get_contents($invoice->getData('download_link')), 'application/pdf');
    }

}
