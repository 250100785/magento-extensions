<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
include_once("Mage/Adminhtml/controllers/Sales/OrderController.php");

class Licentia_Fructus_Adminhtml_Sales_OrderController extends Mage_Adminhtml_Sales_OrderController {

    public function pdfinvoicesAction() {
        $dir = Mage::getBaseDir('var') . '/invoice/';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $orderIds = $this->getRequest()->getPost('order_ids');
        $flag = false;
        if (!empty($orderIds)) {
            foreach ($orderIds as $orderId) {
                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
                        ->setOrderFilter($orderId)
                        ->load();
                if ($invoices->getSize() > 0) {
                    $flag = true;
                    foreach ($invoices as $invoice) {
                        if ($invoice->getData('fructus_id')) {
                            $fileExt = file_get_contents($invoice->getData('download_link'));
                            $fileDir = $dir . $invoice->getId() . '.pdf';
                            $file = file_put_contents($fileDir, $fileExt);
                            if ($file) {
                                $pdf[] = Zend_Pdf::load($fileDir);
                                unlink($fileDir);
                            }
                        }
                    }
                }
            }

            if ($flag) {
                $pdfMerged = new Zend_Pdf();
                foreach ($pdf as $item) {
                    foreach ($item->pages as $page) {
                        $clonedPage = clone $page;
                        $pdfMerged->pages[] = $clonedPage;
                    }
                    unset($clonedPage);
                }

                return $this->_prepareDownloadResponse('invoice-' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') .
                                '.pdf', $pdfMerged->render(), 'application/pdf');
            } else {
                $this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
                $this->_redirect('*/*/');
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Print creditmemos for selected orders
     */
    public function pdfcreditmemosAction() {
        $dir = Mage::getBaseDir('var') . '/creditmemo/';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $orderIds = $this->getRequest()->getPost('order_ids');
        $flag = false;
        if (!empty($orderIds)) {
            foreach ($orderIds as $orderId) {
                $creditmemos = Mage::getResourceModel('sales/order_creditmemo_collection')
                        ->setOrderFilter($orderId)
                        ->load();
                if ($creditmemos->getSize()) {
                    $flag = true;
                    foreach ($creditmemos as $creditmemo) {
                        if ($creditmemo->getData('fructus_id')) {
                            $fileExt = file_get_contents($creditmemo->getData('download_link'));
                            $fileDir = $dir . $creditmemo->getId() . '.pdf';
                            $file = file_put_contents($fileDir, $fileExt);
                            if ($file) {
                                $pdf[] = Zend_Pdf::load($fileDir);
                                unlink($fileDir);
                            }
                        }
                    }
                }
            }
            if ($flag) {
                $pdfMerged = new Zend_Pdf();
                foreach ($pdf as $item) {
                    foreach ($item->pages as $page) {
                        $clonedPage = clone $page;
                        $pdfMerged->pages[] = $clonedPage;
                    }
                    unset($clonedPage);
                }

                return $this->_prepareDownloadResponse(
                                'creditmemo-' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') . '.pdf', $pdfMerged->render(), 'application/pdf'
                );
            } else {
                $this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
                $this->_redirect('*/*/');
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Print all documents for selected orders
     */
    public function pdfdocsAction() {
        $orderIds = $this->getRequest()->getPost('order_ids');
        $flag = false;
        if (!empty($orderIds)) {
            foreach ($orderIds as $orderId) {
                $invoices = Mage::getResourceModel('sales/order_invoice_collection')
                        ->setOrderFilter($orderId)
                        ->load();
                if ($invoices->getSize()) {

                    foreach ($invoices as $invoice) {
                        if ($invoice->getData('fructus_id')) {
                            $this->_getSession()->addError(Mage::helper('fructus')->__('You can not print invoices from InvoiceXpress using this method'));
                            $this->_redirectReferer();
                            return;
                        }
                    }

                    $flag = true;
                    if (!isset($pdf)) {
                        $pdf = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);
                    } else {
                        $pages = Mage::getModel('sales/order_pdf_invoice')->getPdf($invoices);
                        $pdf->pages = array_merge($pdf->pages, $pages->pages);
                    }
                }

                $shipments = Mage::getResourceModel('sales/order_shipment_collection')
                        ->setOrderFilter($orderId)
                        ->load();
                if ($shipments->getSize()) {
                    $flag = true;
                    if (!isset($pdf)) {
                        $pdf = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                    } else {
                        $pages = Mage::getModel('sales/order_pdf_shipment')->getPdf($shipments);
                        $pdf->pages = array_merge($pdf->pages, $pages->pages);
                    }
                }

                $creditmemos = Mage::getResourceModel('sales/order_creditmemo_collection')
                        ->setOrderFilter($orderId)
                        ->load();
                if ($creditmemos->getSize()) {

                    foreach ($creditmemos as $creditmemo) {
                        if ($creditmemo->getData('fructus_id')) {
                            $this->_getSession()->addError(Mage::helper('fructus')->__('You can not print creditmemos from InvoiceXpress using this method'));
                            $this->_redirectReferer();
                            return;
                        }
                    }
                    $flag = true;
                    if (!isset($pdf)) {
                        $pdf = Mage::getModel('sales/order_pdf_creditmemo')->getPdf($creditmemos);
                    } else {
                        $pages = Mage::getModel('sales/order_pdf_creditmemo')->getPdf($creditmemos);
                        $pdf->pages = array_merge($pdf->pages, $pages->pages);
                    }
                }
            }
            if ($flag) {
                return $this->_prepareDownloadResponse(
                                'docs' . Mage::getSingleton('core/date')->date('Y-m-d_H-i-s') . '.pdf', $pdf->render(), 'application/pdf'
                );
            } else {
                $this->_getSession()->addError($this->__('There are no printable documents related to selected orders.'));
                $this->_redirect('*/*/');
            }
        }
        $this->_redirect('*/*/');
    }

}
