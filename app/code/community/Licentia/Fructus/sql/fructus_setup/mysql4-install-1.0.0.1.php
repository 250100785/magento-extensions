<?php

/**
 * Licentia Fructus - InvoiceExpress Integration
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license:
 * Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      InvoiceExpress Integration
 * @category   Accounting
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
$installer = $this;
$installer->startSetup();

$installer->run("ALTER  TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `download_link` varchar(255) DEFAULT NULL;");
$installer->run("ALTER  TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `view_link` varchar(255) DEFAULT NULL;");
$installer->run("ALTER  TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `fructus_id` int(11) DEFAULT NULL;");

$installer->run("ALTER  TABLE `{$this->getTable('sales_flat_creditmemo')}` ADD COLUMN `download_link` varchar(255) DEFAULT NULL;");
$installer->run("ALTER  TABLE `{$this->getTable('sales_flat_creditmemo')}` ADD COLUMN `view_link` varchar(255) DEFAULT NULL;");
$installer->run("ALTER  TABLE `{$this->getTable('sales_flat_creditmemo')}` ADD COLUMN `fructus_id` int(11) DEFAULT NULL;");

$installer->run("UPDATE `{$this->getTable('sales_flat_invoice')}` SET view_link='1'");
$installer->run("UPDATE `{$this->getTable('sales_flat_creditmemo')}` SET view_link='1'");

$installer->endSetup();
