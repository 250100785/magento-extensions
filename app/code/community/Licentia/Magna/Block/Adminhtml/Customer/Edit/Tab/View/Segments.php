<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Block_Adminhtml_Customer_Edit_Tab_View_Segments extends Mage_Adminhtml_Block_Template {

    public function getCustomer() {
        return Mage::registry('current_customer');
    }

    public function getCustomerSegments() {
        return Mage::getModel('magna/segments_list')->getCustomerSegments($this->getCustomer()->getId());
    }

    public function getSegmentName($segmentId) {
        return Mage::getModel('magna/segments')->load($segmentId)->getName();
    }

}
