<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Block_Adminhtml_Customer_Rewrite_Grid extends Mage_Adminhtml_Block_Customer_Grid {

    protected function _prepareMassaction() {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('customer');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => Mage::helper('customer')->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('customer')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('newsletter_subscribe', array(
            'label' => Mage::helper('customer')->__('Subscribe to Newsletter'),
            'url' => $this->getUrl('*/*/massSubscribe')
        ));

        $this->getMassactionBlock()->addItem('newsletter_unsubscribe', array(
            'label' => Mage::helper('customer')->__('Unsubscribe from Newsletter'),
            'url' => $this->getUrl('*/*/massUnsubscribe')
        ));

        $groups = $this->helper('customer')->getGroups()->toOptionArray();

        array_unshift($groups, array('label' => '', 'value' => ''));
        $this->getMassactionBlock()->addItem('assign_group', array(
            'label' => Mage::helper('customer')->__('Assign a Customer Group'),
            'url' => $this->getUrl('*/*/massAssignGroup'),
            'additional' => array(
                'visibility' => array(
                    'name' => 'group',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('customer')->__('Group'),
                    'values' => $groups
                )
            )
        ));

        $segments = Mage::getModel('magna/segments')->getOptionArray(false);

        $this->getMassactionBlock()->addItem('massManualAddSegment', array(
            'label' => $this->__('Add To Customer Segment'),
            'url' => $this->getUrl('*/magna_segments/massManualAddCustomer'),
            'confirm' => $this->__('Are you sure?'),
            'additional' => array(
                'visibility' => array(
                    'name' => 'segment',
                    'type' => 'select',
                    'class' => 'required-entry',
                    'label' => Mage::helper('magna')->__('Segment'),
                    'values' => $segments
                )
            )
        ));

        return $this;
    }

}
