<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Block_Adminhtml_Segments_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry("current_segment_rule");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("magna_form", array("legend" => $this->__("Segment Information")));

        $fieldset->addField("name", "text", array(
            "label" => $this->__("Segment Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        $fieldset->addField("is_active", "select", array(
            "label" => $this->__("Is Active"),
            "options" => array('1' => $this->__('Yes'), '0' => $this->__('No')),
            "required" => true,
            "name" => "is_active",
        ));

        $fieldset->addField("description", "textarea", array(
            "label" => $this->__("Description"),
            "name" => "description",
        ));

        $fieldset->addField("cron", "select", array(
            "label" => $this->__("Auto Update List"),
            "options" => array(
                '0' => $this->__('No'),
                'd' => $this->__('Daily'),
                'w' => $this->__('Weekly'),
                'm' => $this->__('Monthly')),
            "name" => "cron",
        ));

        $fieldset->addField("priority", "text", array(
            "label" => $this->__("Priority"),
            "required" => true,
            "note" => $this->__('Priority is very important because one customer can be in more than one segment. If you use segment prices, the price that prevails is the one from the segment with higher priority (lower number)'),
            "class" => 'validate-digits',
            "name" => "priority",
        ));

        if ($current) {
            $currentValues = $current->getData();
            $form->setValues($currentValues);
        }
        return parent::_prepareForm();
    }

}
