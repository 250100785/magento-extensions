<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Block_Adminhtml_Segments_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("magna_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle($this->__("Segment Information"));
    }

    protected function _beforeToHtml() {

        $this->addTab("main_section", array(
            "label" => $this->__("General"),
            "title" => $this->__("General"),
            "content" => $this->getLayout()->createBlock("magna/adminhtml_segments_edit_tab_main")->toHtml(),
        ));

        $this->addTab("conditions_section", array(
            "label" => $this->__("Conditions"),
            "title" => $this->__("Conditions"),
            "content" => $this->getLayout()->createBlock("magna/adminhtml_segments_edit_tab_conditions")->toHtml(),
        ));

        if ($this->getRequest()->getParam('tab_id')) {
            $this->setActiveTab($this->getRequest()->getParam('tab_id'));
        }

        return parent::_beforeToHtml();
    }

}
