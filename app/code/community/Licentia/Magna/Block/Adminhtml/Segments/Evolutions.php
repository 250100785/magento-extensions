<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Block_Adminhtml_Segments_Evolutions extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_segments_evolutions';
        $this->_blockGroup = 'magna';
        $this->_headerText = $this->__('Segments Evolutions');
        if ($segment = Mage::registry('current_segment')) {

            $this->_headerText = $this->__('Segments Evolutions') . ' / ' . $segment->getName();

            $cancelUrl = $this->getUrl('*/magna_segments', array('id' => $segment->getId()));

            $this->addButton('cancel_campaign', array('label' => $this->__('Back'),
                'onclick' => "window.location='$cancelUrl';", 'class' => 'back'));
        }
        parent::__construct();

        $recordsUrl = $this->getUrl('*/magna_segments/records', array('id' => $segment->getId()));
        $this->addButton('records_button', array('label' => $this->__('Records'),
            'onclick' => "window.location='$recordsUrl';"));

        $this->_removeButton('add');
    }

}
