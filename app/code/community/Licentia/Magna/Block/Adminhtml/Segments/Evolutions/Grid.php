<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Block_Adminhtml_Segments_Evolutions_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('campaign_grid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('magna/evolutions')
                ->getResourceCollection();

        if ($segment = Mage::registry('current_segment')) {
            $collection->addFieldToFilter('segment_id', $segment->getId());
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        if (!Mage::registry('current_segment')) {
            $this->addColumn('segment_id', array(
                'header' => $this->__('Segment Name'),
                'align' => 'left',
                'index' => 'segment_id',
                'type' => 'options',
                'options' => Mage::getModel('magna/segments')->toFormValues(),
            ));
        }

        $this->addColumn('created_at', array(
            'header' => $this->__('Created at'),
            'align' => 'left',
            'type' => 'date',
            'index' => 'created_at',
        ));

        $this->addColumn('records', array(
            'header' => $this->__('Total Records'),
            'align' => 'left',
            'type' => 'number',
            'index' => 'records',
        ));

        $this->addColumn('change', array(
            'header' => $this->__('Change'),
            'align' => 'left',
            'type' => 'number',
            'index' => 'change',
        ));

        $this->addExportType('*/*/exportevoCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportevoXml', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/evolutiongrid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return false;
    }

}
