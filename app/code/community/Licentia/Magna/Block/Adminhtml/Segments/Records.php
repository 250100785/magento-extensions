<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Block_Adminhtml_Segments_Records extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_segments_records';
        $this->_blockGroup = 'magna';
        parent::__construct();

        $this->_removeButton('add');

        if ($segment = Mage::registry('current_segment')) {

            $this->_headerText = $segment->getname() . ' / ' . $this->__('Segments');

            $urlBack = $this->getUrl('*/*/');
            $this->addButton('back', array('label' => $this->__('Back'),
                'class' => 'back',
                'onclick' => "window.location='$urlBack'"));

            $evolutionsUrl = $this->getUrl('*/magna_segments/evolution', array('id' => $segment->getId()));
            $this->addButton('evolutions_button', array('label' => $this->__('Evolutions'),
                'onclick' => "window.location='$evolutionsUrl';"));

            $url = $this->getUrl('*/magna_segments/records', array('refresh' => 1, 'id' => $segment->getId()));
            $text = $this->__('This will refresh your segment next time your cron runs. Continue?');
            $this->addButton('background_refresh', array('label' => $this->__('Refresh Segment'),
                'class' => 'save',
                'onclick' => "if(!confirm('$text')){return false;}; window.location='$url'"));
        }
    }

}
