<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Block_Adminhtml_Segments_Records_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        $id = $this->getRequest()->getParam('id');

        $collection = Mage::getModel('magna/segments_list')
                ->getCollection()
                ->addFieldToFilter('segment_id', $id);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $segment = Mage::registry('current_segment');
        $extraData = (array) unserialize($segment->getExtraData());

        $this->addColumn('customer_id', array(
            'header' => $this->__('Cust. ID'),
            'align' => 'left',
            'type' => 'number',
            'index' => 'customer_id',
        ));

        $this->addColumn('firstname', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => array('firstname', 'lastname'),
            'type' => 'concat',
            'separator' => ' ',
            'filter_index' => "CONCAT(firstname, ' ',lastname)",
        ));

        $this->addColumn('email', array(
            'header' => $this->__('Email'),
            'align' => 'left',
            'index' => 'email',
        ));

        $this->addColumn('manual', array(
            'header' => $this->__('Man. Assigned'),
            'align' => 'left',
            'index' => 'manual',
            'type' => 'options',
            'options' => array('0' => $this->__('No'), '1' => $this->__('Yes')),
        ));


        $fields = isset($extraData['fields']) ? $extraData['fields'] : array();
        $type = isset($extraData['type']) ? $extraData['type'] : array();
        if (count($fields) < 10) {

            for ($i = 0; $i < count($fields); $i++) {
                $a = $i + 1;

                $options = array(
                    'header' => $this->__($fields[$i]),
                    'align' => 'left',
                    'index' => 'data_' . $a,
                );

                if (isset($type['type_' . $fields[$i]])) {
                    $infoType = $type['type_' . $fields[$i]];

                    if (stripos($infoType, 'number') !== false) {
                        $options['type'] = 'number';
                        $options['filter_index'] = new Zend_Db_Expr('CAST(`data_' . $a . '` AS SIGNED)');
                    }
                    if (stripos($infoType, 'currency') !== false) {
                        $options['type'] = 'currency';
                        $options['filter_index'] = new Zend_Db_Expr('CAST(`data_' . $a . '` AS SIGNED)');
                        $options['currency_code'] = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
                    }
                    if (stripos($infoType, 'options') !== false) {
                        $options['type'] = 'options';

                        if (stripos($fields[$i], $this->__('Country')) !== false) {
                            $cList = Mage::getResourceModel('directory/country_collection')
                                    ->loadData()
                                    ->toOptionArray(false);
                            $values = array();
                            foreach ($cList as $key => $value) {
                                $values[$value['value']] = $value['label'];
                            }
                        } elseif (stripos($fields[$i], $this->__('Region')) !== false) {

                            $cList = Mage::getModel('adminhtml/system_config_source_allregion')
                                    ->toOptionArray();

                            $values = array();
                            foreach ($cList as $key => $value) {
                                $values[$value['value']] = $value['label'];
                            }
                        } elseif (stripos($fields[$i], $this->__('Payment')) !== false) {

                            $cList = Mage::getModel('adminhtml/system_config_source_payment_allmethods')
                                    ->toOptionArray();

                            $values = array();
                            foreach ($cList as $key => $value) {
                                if (is_array($value['value'])) {
                                    foreach ($value['value'] as $value1) {
                                        $values[$value1['value']] = $value1['label'];
                                    }
                                } else {
                                    $values[$value['value']] = $value['label'];
                                }
                            }
                        } elseif (stripos($fields[$i], $this->__('Shipping')) !== false) {

                            $cList = Mage::getModel('adminhtml/system_config_source_shipping_allmethods')
                                    ->toOptionArray();

                            $values = array();
                            foreach ($cList as $key => $value) {
                                $values[$value['value']] = $value['label'];
                            }
                        }

                        if (count($values) > 0) {
                            $options['options'] = $values;
                            $options['width'] = 200;
                        } else {
                            $options['type'] = 'text';
                        }
                    }
                }

                $this->addColumn('data_' . $a, $options);
            }
        }

        $this->addColumn('action', array(
            'header' => $this->__('View'),
            'type' => 'action',
            'align' => 'center',
            'width' => '100px',
            'filter' => false,
            'sortable' => false,
            'actions' => array(array(
                    'url' => $this->getUrl('*/customer/edit', array('id' => '$customer_id')),
                    'caption' => $this->__('Customer'),
                )),
            'index' => 'type',
            'is_system' => true,
            'sortable' => false
        ));
        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportXml', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('record_id');
        $this->getMassactionBlock()->setFormFieldName('ids');

        $this->getMassactionBlock()->addItem('massManualDelete', array(
            'label' => $this->__('Mark as Auto Added'),
            'url' => $this->getUrl('*/*/massManualDelete'),
            'confirm' => $this->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('massManualAdd', array(
            'label' => $this->__('Mark as Manually Added'),
            'url' => $this->getUrl('*/*/massManualAdd'),
            'confirm' => $this->__('Are you sure?')
        ));

        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/recordsgrid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return false;
    }

}
