<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Block_Adminhtml_System_Config_Form_Field_Clear extends Mage_Adminhtml_Block_System_Config_Form_Field {

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {

        $url = $this->getUrl('*/magna_segments/clear');
        $message = Mage::helper('magna')->__('Are you sure?');
        return '<style type="text/css">#row_magna_uninstall_clear td.scope-label {display:none;}</style><br><br><button  onclick="if(!confirm(\'' . $message . '\')){return false;}window.location=\'' . $url . '\'" class="scalable" type="button" ><span><span><span>' . Mage::helper('magna')->__('Delete Attributes') . '</span></span></span></button>';
    }

}
