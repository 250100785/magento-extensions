<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * Checks for Licentia_Magna
     * @return type
     */
    public function nuntius() {
        return (bool) Mage::getConfig()->getModuleConfig('Licentia_Nuntius');
    }

    /**
     * Checks for Licentia_Stella
     * @return type
     */
    public function stella() {
        return (bool) Mage::getConfig()->getModuleConfig('Licentia_Stella');
    }

    /**
     * Returns current customer Id
     * @return type
     */
    public function getCustomerId() {

        $customerId = Mage::getSingleton('customer/session')->getId();

        if (Mage::getDesign()->getArea() == 'adminhtml' &&
                Mage::getSingleton('adminhtml/session_quote') &&
                Mage::getSingleton('adminhtml/session_quote')->getCustomer() &&
                Mage::getSingleton('adminhtml/session_quote')->getCustomer()->getId() > 0) {

            $customerId = Mage::getSingleton('adminhtml/session_quote')->getCustomer()->getId();
        }

        return $customerId;
    }

     /**
      * Checks if a given customer is in a segment
      * @param type $segmentId
      * @return boolean
      */
    public function isCustomerInSegment($segmentId) {

        $customerId = $this->getCustomerId();

        if (!$customerId) {
            return false;
        }

        $seg = Mage::getModel('magna/segments_list')->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('segment_id', $segmentId)
                ->setPageSize(1);

        if ($seg->count() != 1) {
            return false;
        }

        return ($segmentId == $seg->getFirstItem()->getSegmentId()) ? $segmentId : false;
    }

    /**
     * Returns the segment from customer with higher priority
     * @param type $customerId
     * @return boolean
     */
    public function getCustomerSegmentId($customerId = false) {

        if ($customerId === false) {
            $customerId = $this->getCustomerId();
        }

        if (!$customerId) {
            return false;
        }

        if (Mage::getSingleton('customer/session')->getCustomer()->getSegmentId()) {
            return (int) Mage::getSingleton('customer/session')->getCustomer()->getSegmentId();
        }

        $seg = Mage::getModel('magna/segments_list')->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->setOrder('priority', 'ASC')
                ->setPageSize(1);

        if ($seg->count() != 1) {
            return false;
        }

        $segmentId = $seg->getFirstItem()->getSegmentId();

        Mage::getSingleton('customer/session')->getCustomer()->setSegmentId($segmentId);

        return $segmentId;
    }

    /**
     * Returns an array of customer segments
     * @param type $customerId
     * @return array
     */
    public function getCustomerSegmentsIds($customerId = false) {

        if ($customerId === false) {
            $customerId = $this->getCustomerId();
        }

        if (!$customerId) {
            return array();
        }

        if (Mage::getSingleton('customer/session')->getCustomer()->getSegmentsIds()) {
            return (array) Mage::getSingleton('customer/session')->getCustomer()->getSegmentsIds();
        }

        $segmentsIds = Mage::getModel('magna/segments_list')
                ->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->getAllIds('segment_id');

        Mage::getSingleton('customer/session')->getCustomer()->setSegmentsIds($segmentsIds);

        return $segmentsIds;
    }

    /**
     * Returns Product prce for segment
     *
     * @param type $segmentId
     * @param type $product
     * @return boolean
     */
    public function getSegmentPrice($segmentId, $product) {

        $websiteId = Mage::app()->getWebsite()->getId();

        $model = Mage::getModel('magna/index')->getCollection()
                ->addFieldToFilter('product_id', $product->getId())
                ->addFieldToFilter('segment_id', $segmentId)
                ->addFieldToFilter('website_id', $websiteId)
                ->setOrder('website_id', 'DESC');

        if ($model->count() != 1) {
            return false;
        }

        $price = $model->getFirstItem()->getPrice();

        if ($product->getFinalPrice() > $price) {
            return $price;
        }

        return false;
    }

}
