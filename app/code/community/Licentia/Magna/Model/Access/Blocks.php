<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Model_Access_Blocks extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('magna/access_blocks');
    }

    public function getValuesForForm($blockId) {
        return $this->getCollection()
                        ->addFieldToFilter('block_id', $blockId)
                        ->getAllIds('segment_id');
    }

    public function updateData($id, $segments) {
        $delete = $this->getCollection()
                ->addFieldToFilter('block_id', $id);
        foreach ($delete as $item) {
            $item->delete();
        }

        if (in_array(0, $segments)) {
            return;
        }

        foreach ($segments as $segment) {
            if ((int) $segment == 0) {
                continue;
            }
            $data = array();
            $data['block_id'] = $id;
            $data['segment_id'] = $segment;
            $this->setData($data)->save();
        }
    }

    public function checkAccess($blockId) {

        $model = $this->getCollection()
                ->addFieldToFilter('block_id', $blockId)
                ->getAllIds('segment_id');

        if (count($model) == 0) {
            return true;
        }

        $customerSegments = Mage::helper('magna')->getCustomerSegmentsIds();

        if (count(array_intersect($model, $customerSegments)) > 0) {
            return true;
        }

        return false;
    }

}
