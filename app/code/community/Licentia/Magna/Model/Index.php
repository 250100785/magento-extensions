<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Model_Index extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('magna/index');
    }

    public function buildIndexForProduct($event) {
        $product = $event->getDataObject();
        $this->_getResource()->reindexProduct($product->getId());
    }

    public function buildIndexAll() {
        $collection = Mage::getModel('magna/prices')
                ->getCollection();

        foreach ($collection as $item) {
            $this->_getResource()->reindexProduct($item->getProductId());
        }
    }

}
