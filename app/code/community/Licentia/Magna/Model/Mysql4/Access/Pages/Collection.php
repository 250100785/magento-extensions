<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Model_Mysql4_Access_Pages_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('magna/access_pages');
    }

    public function getAllIds($field = false) {
        if (!$field) {
            return parent::getAllIds();
        }

        $idsSelect = clone $this->getSelect();
        $idsSelect->reset(Zend_Db_Select::ORDER);
        $idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns($field, 'main_table');
        return $this->getConnection()->fetchCol($idsSelect);
    }

}
