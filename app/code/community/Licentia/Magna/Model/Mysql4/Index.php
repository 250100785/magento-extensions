<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Model_Mysql4_Index extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('magna/index', 'index_id');
    }

    public function reindexProduct($productId) {
        $adapter = $this->_getWriteAdapter();
        $websiteIds = array_keys(Mage::app()->getWebsites());

        $where = array(
            $adapter->quoteInto('product_id = ?', $productId)
        );

        $adapter->delete($this->getMainTable(), $where);

        $collection = Mage::getModel('magna/prices')
                ->getCollection()
                ->addFieldToFilter('product_id', $productId);

        foreach ($collection as $item) {
            if ($item->getWebsiteId() == 0) {
                foreach ($websiteIds as $websiteId) {
                    $item->setData('website_id', $websiteId);
                    $data = $this->_prepareDataForSave($item, $this->getMainTable());
                    $adapter->insert($this->getMainTable(), $data);
                }
            } else {
                $data = $this->_prepareDataForSave($item, $this->getMainTable());
                $adapter->insert($this->getMainTable(), $data);
            }
        }

        return $this;
    }

}
