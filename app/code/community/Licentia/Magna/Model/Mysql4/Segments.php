<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Model_Mysql4_Segments extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('magna/segments', 'segment_id');
    }

    public function updateRuleProductData(Mage_CatalogRule_Model_Rule $rule) {
        return $this;
    }

}
