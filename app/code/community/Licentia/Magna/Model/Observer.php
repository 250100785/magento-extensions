<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Model_Observer {

    public function extraConditions($event) {

        $conditions = $event->getAdditional();
        $attributes = array();
        $attributes[] = array('value' => 'magna/rule_condition_segment|exists', 'label' => Mage::helper('magna')->__('Customer Segment'));

        $conditions1 = array(array('label' => Mage::helper('magna')->__('Customer Segment'), 'value' => $attributes));

        $conditions->setConditions($conditions1);
    }

    public function updateCustomerData($event) {

    }

    public function newCustomer($event) {
        $customer = $event->getEvent()->getCustomer();
    }

    /**
     *
     * @param type $eventSegments Price Product Form Edit
     */
    public function formSegments($event) {
        $form = $event->getForm();
        $segmentsAccess = $form->getElement('segments_access');
        if ($segmentsAccess) {
            $enable = Mage::getStoreConfig('magna/config/acl');
            if (!$enable) {
                $segmentsAccess->setReadOnly(true, true)->setAfterElementHtml("<script>$('segments_access').up().up().hide();</script>");
            }
        }
        $groupPrice = $form->getElement('magna_segments');
        if ($groupPrice) {
            $enable = Mage::getStoreConfig('magna/config/prices');
            if (!$enable) {
                $groupPrice->setReadOnly(true, true);
            }
            $groupPrice->setRenderer(
                    Mage::app()->getLayout()->createBlock('magna/adminhtml_catalog_product_form_segments')
            );
        }
    }

    /**
     *
     * Loads segments access product edit form
     * @param type $event
     */
    public function editProduct($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }

        $product = $event->getProduct();
        $values = Mage::getModel('magna/access_products')->getValuesForForm($product->getId());
        if (count($values) == 0) {
            $values = array('0');
        }
        $product->setData('segments_access', $values);
    }

    /**
     *
     * @param type $event
     */
    public function newProduct($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }

        $product = $event->getProduct();
        $product->setData('segments_access', array('0'));
    }

    /**
     *
     * Saves Product ASegments Access
     * @param type $event
     */
    public function saveProductAfter($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }

        $product = $event->getProduct();
        Mage::getModel('magna/access_products')->updateData($product->getId(), $product->getData('segments_access'));
    }

    public function loadAfter($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }

        $model = $event->getObject();
        $class = get_class($model);

        if (!in_array($class, array(
                    'Mage_Cms_Model_Page',
                    'Mage_Catalog_Model_Product',
                    'Mage_Cms_Model_Block',
                ))) {
            return;
        }
        if (stripos($class, 'page') !== false) {
            $type = 'page_id';
            $m = 'pages';
        } elseif (stripos($class, 'product') !== false) {
            $type = 'product_id';
            $m = 'products';
        } else {
            $m = 'blocks';
            $type = 'block_id';
        }

        $ok = Mage::getModel('magna/access_' . $m)->checkAccess($model->getId());

        if (!$ok && $type != 'block_id') {
            Mage::app()->getFrontController()->getResponse()
                    ->setRedirect(Mage::getUrl('cms/index/noRoute'))
                    ->sendResponse();
            die();
        } elseif (!$ok) {
            $model->setData(array());
        }
    }

    public function saveBefore($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }


        $model = $event->getObject();
        $class = get_class($model);

        if (!in_array($class, array(
                    'Mage_Cms_Model_Page',
                    'Mage_Cms_Model_Block',
                ))) {
            return;
        }
        Mage::register('magna_model_save_before_' . $class, $model->getData('customer_segments'), true);
    }

    public function saveAfter($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }

        $model = $event->getObject();
        $class = get_class($model);
        if (!in_array($class, array(
                    'Mage_Cms_Model_Page',
                    'Mage_Cms_Model_Block',
                ))) {
            return;
        }

        $customerSegments = Mage::registry('magna_model_save_before_' . $class);
        if (stripos($class, 'page') !== false) {
            $type = 'page_id';
            $m = 'pages';
        } else {
            $m = 'blocks';
            $type = 'block_id';
        }
        Mage::getModel('magna/access_' . $m)->updateData($model->getId(), $customerSegments);
    }

    public function formSegmentsForm($event) {

        $enable = Mage::getStoreConfig('magna/config/acl');

        if (!$enable) {
            return;
        }


        $class = get_class($event->getBlock());

        if (!in_array($class, array(
                    'Mage_Adminhtml_Block_Cms_Block_Edit_Form',
                    'Mage_Adminhtml_Block_Cms_Page_Edit_Tab_Main',
                ))) {
            return;
        }

        if (stripos($class, 'page') !== false) {
            $type = 'page_id';
            $name = 'Page';
            $m = 'pages';
            $typeId = Mage::app()->getRequest()->getParam('page_id');
        } else {
            $m = 'blocks';
            $name = 'Block';
            $type = 'block_id';
            $typeId = Mage::app()->getRequest()->getParam('block_id');
        }

        $form = $event->getBlock()->getForm()->getElement('base_fieldset');

        $form->addField('customer_segments', 'multiselect', array(
            'name' => 'customer_segments[]',
            'note' => Mage::helper('magna')->__('Allow customers from selected  segments to view this ' . $name),
            'label' => Mage::helper('magna')->__('Segments Access'),
            'title' => Mage::helper('magna')->__('Segments Access'),
            'values' => Mage::getModel('magna/segments')->getOptionArray('Any Customer'),
                ), 'is_active');

        $values = Mage::getModel('magna/access_' . $m)->getValuesForForm($typeId);

        if (count($values) == 0) {
            $values = array('0');
        }

        $event->getBlock()->getForm()->addValues(array('customer_segments' => $values));
    }

    public function priceCollection($event) {

        $enable = Mage::getStoreConfig('magna/config/prices');
        $acl = Mage::getStoreConfig('magna/config/acl');

        if (!$enable && !$acl) {
            return;
        }

        $segmentId = Mage::helper('magna')->getCustomerSegmentId();
        $segmentsIds = Mage::helper('magna')->getCustomerSegmentsIds();

        $collection = $event->getCollection();
        $table = Mage::getSingleton('core/resource')->getTableName('magna/index');
        $tableAccess = Mage::getSingleton('core/resource')->getTableName('magna/access_products');

        if ($acl) {
            $collection->getSelect()
                    ->where('e.entity_id NOT IN (SELECT product_id FROM ' . $tableAccess . ' )');
        }

        if (count($segmentsIds) == 0) {
            return;
        }

        if ($acl) {
            $collection->getSelect()
                    ->orWhere('e.entity_id IN (SELECT product_id FROM ' . $tableAccess . ' WHERE ' . $tableAccess . '.segment_id IN (?) )', $segmentsIds);
        }

        if ($segmentId && $enable) {
            $parts = $collection->getSelect()->getPart('from');

            $ok = false;
            foreach (array_keys($parts) as $part) {
                if ($part == 'price_index') {
                    $ok = true;
                    break;
                }
            }

            if (!$ok) {
                return;
            }

            $websiteId = (int) Mage::app()->getWebsite()->getId();

            $collection->getSelect()
                    ->joinLeft(array('segs' => $table), "e.entity_id=segs.product_id AND segs.segment_id=" . $segmentId . " AND segs.website_id=$websiteId ", array('final_price' => new Zend_Db_Expr("IF(price_index.final_price>segs.price,segs.price,price_index.final_price)")));
        }
    }

    /**
     * Loads Final product Price
     *
     * @param type $event
     * @return type
     */
    public function priceProduct($event) {

        $enable = Mage::getStoreConfig('magna/config/prices');

        if (!$enable) {
            return;
        }

        $segmentId = Mage::helper('magna')->getCustomerSegmentId();

        if (!$segmentId) {
            return;
        }

        $price = Mage::helper('magna')->getSegmentPrice($segmentId, $event->getProduct());
        if (!$price) {
            return;
        }

        $event->getProduct()->setFinalPrice($price);
    }

    public function notifyBuild() {
        $admin = Mage::getSingleton('admin/session')->getUser();

        if (!$admin)
            return;

        $user = $admin->getId();

        $segments = Mage::getModel('magna/segments')
                ->getCollection()
                ->addFieldToSelect('notify_user')
                ->addFieldToSelect('segment_id')
                ->addFieldToFilter('notify_user', $user)
                ->addFieldToFilter('build', 0);

        if ($segments->count() == 0)
            return;

        foreach ($segments as $segment) {
            $segment->setData('notify_user', 0)->save();
        }

        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('magna')->__('Your background segment updates have finished.'));
    }

}
