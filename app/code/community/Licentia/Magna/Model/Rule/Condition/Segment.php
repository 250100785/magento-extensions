<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Model_Rule_Condition_Segment extends Mage_Rule_Model_Condition_Abstract {

    public function getInputType() {
        return 'select';
    }

    public function getValueElementType() {
        return 'select';
    }

    public function getAttributeName() {
        return 'Customer Segment';
    }

    public function getAttributeElement() {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    public function getValueSelectOptions() {

        if (!$this->hasData('value_select_options')) {
            $options = Mage::getModel('magna/segments')->getOptionArray(false);
            $this->setData('value_select_options', $options);
        }

        return $this->getData('value_select_options');
    }

    public function validate(Varien_Object $object) {

        $object->setData($this->getAttribute(), Mage::helper('magna')->isCustomerInSegment($this->getValueParsed()));

        return parent::validate($object);
    }

}
