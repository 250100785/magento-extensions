<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Model_Segments extends Mage_Rule_Model_Rule {

    const MYSQL_DATE = 'yyyy-MM-dd';
    const MYSQL_DATETIME = 'yyyy-MM-dd HH:mm:ss';

    protected $_eventPrefix = 'magna_segments';
    protected $_eventObject = 'rule';
    protected $_customersIds;
    protected $_customersData;

    protected function _construct() {

        $this->_init('magna/segments');
    }

    public function getOptionArray($includeNone = 'None') {

        $lists = Mage::getModel('magna/segments')
                ->getCollection()
                ->addFieldToSelect('segment_id')
                ->addFieldToSelect('name')
                ->addFieldToFilter('is_active', 1);

        $return = array();

        if ($includeNone) {
            $return[] = array('value' => '0', 'label' => Mage::helper('magna')->__("-- $includeNone --"));
        }

        foreach ($lists as $list) {
            $return[] = array('value' => $list->getId(), 'label' => $list->getName());
        }

        return $return;
    }

    public function getConditionsInstance() {
        return Mage::getModel('magna/segments_condition_combine');
    }

    public function getActionsInstance() {
        return Mage::getModel('magna/segments_action_collection');
    }

    /**
     * Get array of product ids which are matched by rule
     *
     * @return array
     */
    public function getMatchingCustomersIds() {


        if (is_null($this->_customersIds)) {

            $dateObj = Mage::app()->getLocale()->date();
            $day = $dateObj->get(Licentia_Magna_Model_Segments::MYSQL_DATE);
            $now = $dateObj->get(Licentia_Magna_Model_Segments::MYSQL_DATETIME);

            $this->setData('run', $now)->save();

            $this->_customersIds = array();
            $this->setCollectedAttributes(array());

            $customerCollection = Mage::getResourceModel('customer/customer_collection');

            $this->getConditions()->collectValidatedAttributes($customerCollection);

            Mage::getSingleton('core/resource_iterator')->walk(
                    $customerCollection->getSelect(), array(array($this, 'callbackValidateCustomer')), array(
                'attributes' => $this->getCollectedAttributes(),
                'customer' => Mage::getModel('customer/customer'),
                    )
            );

            foreach ($this->_customersIds as $customerId) {

                $data = Mage::getModel('customer/customer')->load($customerId)->getData();
                $segment = $this->_customersData[$customerId]->getData();

                $type = array();
                foreach ($segment as $key => $value) {
                    if (stripos($key, 'type_') !== false) {
                        $type[$key] = $value;
                        unset($segment[$key]);
                    }
                }

                if (count($segment) <= 10) {
                    $extraData = array_keys($segment);
                    $segment = array_values($segment);
                    for ($i = 0; $i < count($segment); $i++) {
                        $data['data_' . ($i + 1)] = $segment[$i];
                    }
                }
                $data['segment_id'] = $this->getId();
                $data['customer_id'] = $customerId;
                $data['priority'] = $this->getPriority();
                Mage::getModel('magna/segments_list')->saveRecord($data);
            }

            $previousRecords = Mage::getModel('magna/evolutions')->getCollection()
                    ->addFieldToFilter('segment_id', $this->getId())
                    ->addFieldToFilter('created_at', array('lt' => $day))
                    ->setOrder('created_at', 'DESC')
                    ->setPageSize(1);

            $collection = Mage::getModel('magna/segments_list')->getCollection()
                    ->addFieldToFilter('segment_id', $this->getId());

            $evolution = array();
            $evolution['segment_id'] = $this->getId();
            $evolution['created_at'] = $day;
            $evolution['records'] = $collection->count();

            if ($previousRecords->count() != 1) {
                $previousRecordsNumber = 0;
            } else {
                $pRecords = $previousRecords->getFirstItem();
                $previousRecordsNumber = $pRecords->getData('records');
            }

            $evolution['change'] = $evolution['records'] - $previousRecordsNumber;

            Mage::getModel('magna/evolutions')->setData($evolution)->save();

            $i = count($this->_customersIds);
            $this->setData('last_update', $now)
                    ->setData('extra_data', serialize(array('type' => $type, 'fields' => $extraData)))
                    ->setData('records', $i)
                    ->setData('run', 0)
                    ->save();
        }

        return $this->_customersIds;
    }

    /**
     * Callback function for product matching
     *
     * @param $args
     * @return void
     */
    public function callbackValidateCustomer($args) {
        $customer = clone $args['customer'];
        $customer->setData($args['row']);

        Mage::unregister('magna_segments_data');
        Mage::register('magna_segments_data', new Varien_Object);

        if ($this->getConditions()->validate($customer)) {
            $data = Mage::registry('magna_segments_data');
            $this->_customersIds[] = $customer->getId();
            $this->_customersData[$customer->getId()] = $data;
        }
    }

    /**
     * Get array of assigned customer group ids
     *
     * @return array
     */
    public function getCustomerGroupIds() {
        $ids = $this->getData('customer_group_ids');
        if (($ids && !$this->getCustomerGroupChecked()) || is_string($ids)) {
            if (is_string($ids)) {
                $ids = explode(',', $ids);
            }

            $groupIds = Mage::getModel('customer/group')->getCollection()->getAllIds();
            $ids = array_intersect($ids, $groupIds);
            $this->setData('customer_group_ids', $ids);
            $this->setCustomerGroupChecked(true);
        }
        return $ids;
    }

    /**
     * Returns a list of segments IDS and internal name
     * @return type
     */
    public function toFormValues() {
        $return = array();
        $collection = $this->getCollection()
                ->addFieldToSelect('segment_id')
                ->addFieldToSelect('name');

        foreach ($collection as $segment) {
            $return[$segment->getId()] = $segment->getName();
        }

        return $return;
    }

    public function buildUser() {
        $segments = $this->getCollection()
                ->addFieldToFilter('build', 1);

        foreach ($segments as $segment) {
            Mage::getModel('magna/segments')->load($segment->getId())
                    ->setData('build', 2)
                    ->save();
            Mage::getModel('magna/segments_list')->loadList($segment->getId());
            Mage::getModel('magna/segments')->load($segment->getId())
                    ->setData('build', 0)
                    ->save();
        }
    }

    public function cron() {

        $date = Mage::app()->getLocale()->date()->get(Licentia_Magna_Model_Segments::MYSQL_DATE);

        $segments = $this->getCollection()
                ->addFieldToFilter('cron', array('neq' => '0'));

        //Version Compatability
        $segments->getSelect()
                ->where(" cron_last_run <? or cron_last_run IS NULL ", $date);


        foreach ($segments as $segment) {

            if ($segment->getCron() == 'd') {
                Mage::getModel('magna/segments_list')->loadList($segment->getId());
            }

            if ($segment->getCron() == 'w' && $date->get('e') == 1) {
                Mage::getModel('magna/segments_list')->loadList($segment->getId());
            }

            if ($segment->getCron() == 'm' && $date->get('d') == 1) {
                Mage::getModel('magna/segments_list')->loadList($segment->getId());
            }
        }
    }

    public function _beforeSave() {

        if (!$this->getData('controller')) {
            return;
        }
        return parent::_beforeSave();
    }

}
