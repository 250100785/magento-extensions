<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Model_Segments_Condition_Combine extends Mage_Rule_Model_Condition_Combine {

    public function __construct() {
        parent::__construct();
        $this->setType('magna/segments_condition_combine');
    }

    public function getNewChildSelectOptions() {
        $customerCondition = Mage::getModel('magna/segments_condition_customer');
        $customerAttributes = $customerCondition->loadAttributeOptions()->getAttributeOption();
        $attributes = array();
        foreach ($customerAttributes as $code => $label) {
            $attributes[] = array('value' => 'magna/segments_condition_customer|' . $code, 'label' => $label);
        }

        $conditions = parent::getNewChildSelectOptions();

        $addressCondition = Mage::getModel('magna/segments_condition_address');
        $addressAttributes = $addressCondition->loadAttributeOptions()->getAttributeOption();
        $attributesCart = array();
        foreach ($addressAttributes as $code => $label) {
            $attributesCart[] = array('value' => 'magna/segments_condition_address|' . $code, 'label' => $label);
        }

        $addressActivity = Mage::getModel('magna/segments_condition_activity');
        $activityAttributes = $addressActivity->loadAttributeOptions()->getAttributeOption();
        $attributesActivity = array();
        foreach ($activityAttributes as $code => $label) {
            $attributesActivity[] = array('value' => 'magna/segments_condition_activity|' . $code, 'label' => $label);
        }

        $productCondition = Mage::getModel('magna/segments_condition_product');
        $productAttributes = $productCondition->loadAttributeOptions()->getAttributeOption();
        $pAttributes = array();
        foreach ($productAttributes as $code => $label) {
            $pAttributes[] = array('value' => 'magna/segments_condition_product|' . $code, 'label' => $label);
        }

        $conditions = array_merge_recursive($conditions, array(
            array('label' => Mage::helper('magna')->__('Conditions Combination'), 'value' => 'magna/segments_condition_combine'),
            array('label' => Mage::helper('magna')->__('Customer Attribute'), 'value' => $attributes),
            array('label' => Mage::helper('magna')->__('Customer Activity'), 'value' => $attributesActivity),
            array('label' => Mage::helper('magna')->__('Previous Order - Cart Attribute'), 'value' => $attributesCart),
            array('label' => Mage::helper('magna')->__('Previous Order - Product Attribute'), 'value' => $pAttributes),
        ));

        if (Mage::helper('magna')->stella()) {
            $ltvCondition = Mage::getModel('magna/segments_condition_ltv');
            $lAttributes = $ltvCondition->loadAttributeOptions()->getAttributeOption();
            $ltvAttributes = array();
            foreach ($lAttributes as $code => $label) {
                $ltvAttributes[] = array('value' => 'magna/segments_condition_ltv|' . $code, 'label' => $label);
            }
            $conditions = array_merge_recursive($conditions, array(
                array('label' => Mage::helper('magna')->__('Customer Lifetime Value'), 'value' => $ltvAttributes),
            ));
        }

        if (Mage::helper('magna')->nuntius()) {
            $newsletterCondition = Mage::getModel('magna/segments_condition_newsletter');
            $newsletterAttributes = $newsletterCondition->loadAttributeOptions()->getAttributeOption();
            $attributesNewsletter = array();
            foreach ($newsletterAttributes as $code => $label) {
                $attributesNewsletter[] = array('value' => 'magna/segments_condition_newsletter|' . $code, 'label' => $label);
            }
            $conditions = array_merge_recursive($conditions, array(
                array('label' => Mage::helper('magna')->__('Newsletter Value'), 'value' => $attributesNewsletter),
            ));
        }

        return $conditions;
    }

    public function collectValidatedAttributes($productCollection) {
        foreach ($this->getConditions() as $condition) {
            $condition->collectValidatedAttributes($productCollection);
        }
        return $this;
    }

}
