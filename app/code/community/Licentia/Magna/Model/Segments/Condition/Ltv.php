<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Model_Segments_Condition_Ltv extends Mage_Rule_Model_Condition_Abstract {

    public function loadAttributeOptions() {

        $name = Mage::getModel('stella/equations')->load(1);

        $attributes = array(
            'equation_1' => $name->getData('name_1'),
            'equation_2' => $name->getData('name_2'),
            'equation_3' => $name->getData('name_3'),
        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getAttributeElement() {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    public function getInputType() {
        return 'numeric';
    }

    public function getValueElementType() {
        return 'text';
    }

    /**
     * Validate Address Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object) {


        $model = Mage::getModel('stella/reports_customer')
                ->getCollection()
                ->addFieldToFilter($this->getAttribute(), array($this->translateOperator() => $this->getValueParsed()))
                ->addFieldToFilter('customer_id', $object->getId())
                ->setPageSize(1);

        if ($model->count() != 1)
            return false;

        $object->setData($this->getAttribute(), $model->getFirstItem()->getData($this->getAttribute()));

        return parent::validate($object);
    }

    public function collectValidatedAttributes($customerCollection) {

        $attribute = $this->getAttribute();

        $attributes = $this->getRule()->getCollectedAttributes();
        $attributes[$attribute] = true;
        $this->getRule()->setCollectedAttributes($attributes);

        return $this;
    }

    public function translateOperator() {

        $operator = $this->getOperator();

        $newValue = array('==' => 'eq', '!=' => 'neq', '>=' => 'gteq', '<=' => 'lteq', '<' => 'lt', '>' => 'gt', '{}' => 'like', '!{}' => 'nlike', '()' => 'in', '!()' => 'nin');

        if (isset($newValue[$operator]))
            return $newValue[$operator];

        return 'eq';
    }

}
