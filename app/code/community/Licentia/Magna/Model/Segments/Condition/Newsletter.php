<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Magna_Model_Segments_Condition_Newsletter extends Mage_Rule_Model_Condition_Abstract {

    public function loadAttributeOptions() {
        $attributes = array(
            'factivity_percentage_clicks_newsletter' => Mage::helper('nuntius')->__('Percentage of Clicks Vs Opens'),
            'factivity_percentage_sent_newsletter' => Mage::helper('nuntius')->__('Percentage of Clicks Vs Sent'),
            'factivity_percentage_opens_newsletter' => Mage::helper('nuntius')->__('Percentage of Opens Vs Sent'),
            'factivity_percentage_conversions' => Mage::helper('nuntius')->__('Percentage Conversions Vs Sent'),
            'factivity_amount_conversions' => Mage::helper('nuntius')->__('Conversions Amount'),
            'factivity_number_conversions' => Mage::helper('nuntius')->__('Conversions Number'),
        );

        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getAttributeElement() {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    public function getInputType() {

        switch ($this->getAttribute()) {
            case 'factivity_lists':
                return 'select';
        }

        return 'numeric';
    }

    public function getValueElementType() {

        switch ($this->getAttribute()) {
            case 'factivity_lists':
                return 'select';
        }

        return 'text';
    }

    /**
     * Validate Address Rule Condition
     *
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $object) {

        $resultData = Mage::registry('magna_segments_data');
        $dbAttrName = str_replace('factivity_', '', $this->getAttribute());

        if ($dbAttrName == 'percentage_clicks_newsletter') {

            $model = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('clicks')
                    ->addFieldToSelect('views')
                    ->addFieldToFilter('customer_id', $object->getId());

            if ($model->count() != 1)
                return false;

            $perc = round($model->getFirstItem()->getData('clicks') * 100 / $model->getFirstItem()->getData('views'));

            $object->setData($this->getAttribute(), $perc);

            $resultData->setData($this->getAttributeName(), $object->getData($this->getAttribute()));
            $resultData->setData('type_' . $this->getAttributeName(), 'number');
            return parent::validate($object);
        } elseif ($dbAttrName == 'percentage_opens_newsletter') {

            $model = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('views')
                    ->addFieldToSelect('sent')
                    ->addFieldToSelect('customer_id')
                    ->addFieldToFilter('customer_id', $object->getId());

            if ($model->count() != 1)
                return false;

            $perc = round($model->getFirstItem()->getData('views') * 100 / $model->getFirstItem()->getData('sent'));

            $object->setData($this->getAttribute(), $perc);

            $resultData->setData($this->getAttributeName(), $object->getData($this->getAttribute()));
            $resultData->setData('type_' . $this->getAttributeName(), 'number');
            return parent::validate($object);
        } elseif ($dbAttrName == 'percentage_sent_newsletter') {

            $model = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('clicks')
                    ->addFieldToSelect('sent')
                    ->addFieldToFilter('customer_id', $object->getId());

            if ($model->count() != 1)
                return false;

            $perc = round($model->getFirstItem()->getData('clicks') * 100 / $model->getFirstItem()->getData('sent'));

            $object->setData($this->getAttribute(), $perc);

            $resultData->setData($this->getAttributeName(), $object->getData($this->getAttribute()));
            $resultData->setData('type_' . $this->getAttributeName(), 'number');
            return parent::validate($object);
        } elseif ($dbAttrName == 'percentage_conversions') {

            $model = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('conversions_number')
                    ->addFieldToSelect('sent')
                    ->addFieldToFilter('customer_id', $object->getId());

            if ($model->count() != 1)
                return false;

            $perc = round($model->getFirstItem()->getData('conversions_number') * 100 / $model->getFirstItem()->getData('sent'));

            $object->setData($this->getAttribute(), $perc);

            $resultData->setData($this->getAttributeName(), $object->getData($this->getAttribute()));
            $resultData->setData('type_' . $this->getAttributeName(), 'number');
            return parent::validate($object);
        } elseif ($dbAttrName == 'amount_conversions') {

            $model = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('conversions_amount')
                    ->addFieldToFilter('customer_id', $object->getId());

            if ($model->count() != 1)
                return false;

            $object->setData($this->getAttribute(), $model->getFirstItem()->getData('conversions_amount'));

            $resultData->setData($this->getAttributeName(), $object->getData($this->getAttribute()));
            $resultData->setData('type_' . $this->getAttributeName(), 'currency');
            return parent::validate($object);
        } elseif ($dbAttrName == 'number_conversions') {

            $model = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('conversions_number')
                    ->addFieldToFilter('customer_id', $object->getId());

            if ($model->count() != 1)
                return false;

            $object->setData($this->getAttribute(), $model->getFirstItem()->getData('conversions_number'));

            $resultData->setData($this->getAttributeName(), $object->getData($this->getAttribute()));
            $resultData->setData('type_' . $this->getAttributeName(), 'number');
            return parent::validate($object);
        }


        return false;
    }

    public function collectValidatedAttributes($customerCollection) {

        $attribute = $this->getAttribute();

        $attributes = $this->getRule()->getCollectedAttributes();
        $attributes[$attribute] = true;
        $this->getRule()->setCollectedAttributes($attributes);

        return $this;
    }

    public function translateOperator() {

        $operator = $this->getOperator();

        $newValue = array('==' => 'eq', '!=' => 'neq', '>=' => 'gteq', '<=' => 'lteq', '<' => 'lt', '>' => 'gt', '{}' => 'like', '!{}' => 'nlike', '()' => 'in', '!()' => 'nin');

        if (isset($newValue[$operator]))
            return $newValue[$operator];

        return 'eq';
    }

    public function getValueSelectOptions() {
        if (!$this->hasData('value_select_options')) {
            switch ($this->getAttribute()) {
                case 'factivity_lists':
                    $options = Mage::getModel('nuntius/lists')
                            ->getOptionArray();
                    break;
                default:
                    $options = array();
            }
            $this->setData('value_select_options', $options);
        }
        return $this->getData('value_select_options');
    }

}
