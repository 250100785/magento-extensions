<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Model_Segments_List extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('magna/segments_list');
    }

    public function getCustomerSegments($customerId) {

        return $this->getCollection()->addFieldToFilter('customer_id', $customerId);
    }

    public function loadList($segmentId) {

        $segment = Mage::getModel('magna/segments')->load($segmentId);
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');
        $table = Mage::getSingleton('core/resource')->getTableName('magna/segments_list');
        $write->delete($table, array('segment_id = ?' => $segmentId, 'manual=?' => '0'));

        $tableEvolutions = Mage::getSingleton('core/resource')->getTableName('magna/evolutions');
        $write->delete($tableEvolutions, array('segment_id = ?' => $segmentId));

        $segment->getMatchingCustomersIds();
    }

    public function addCustomerToSegment($customerId, $segmentId) {

        $item = $this->getCollection()
                ->addFieldToFilter('customer_id', $customerId)
                ->addFieldToFilter('segment_id', $segmentId);

        if ($item->count() == 0) {

            $customer = Mage::getModel('customer/customer')->load($customerId);
            if (!$customer->getId()) {
                return false;
            }

            $segment = Mage::getModel('magna/segments')->load($segmentId);
            $data = array();
            $data['manual'] = '1';
            $data['customer_id'] = $customerId;
            $data['segment_id'] = $segmentId;
            $data['firstname'] = $customer->getFirstname();
            $data['lastname'] = $customer->getLastname();
            $data['email'] = $customer->getEmail();
            $data['priority'] = $segment->getData('priority');

            try {
                $record = $this->setData($data)->save();
                $segment->setData('records', $segment->getData('records') + 1)
                        ->setData('manual', $segment->getData('manual') + 1)
                        ->save();
            } catch (Exception $e) {
                $record = false;
                Mage::logException($e);
            }
            return $record;
        } else {

            if ($item->getFirstItem()->getData('manual') == 0) {
                $segment = Mage::getModel('magna/segments')->load($segmentId);
                $segment->setData('manual', $segment->getData('manual') + 1)->save();
                return $item->getFirstItem()->setData('manual', 1)->save();
            }

            return false;
        }
    }

    public function changeToManual($id) {

        $item = $this->load($id);

        if (!$item->getId()) {
            return false;
        }

        if ($item->getData('manual') == 0) {
            $segment = Mage::getModel('magna/segments')->load($item->getSegmentId());
            $segment->setData('manual', $segment->getData('manual') + 1)->save();
            return $item->getFirstItem()->setData('manual', 1)->save();
        }

        return false;
    }

    public function saveRecord($data) {

        $item = $this->getCollection()
                ->addFieldToFilter('customer_id', $data['customer_id'])
                ->addFieldToFilter('segment_id', $data['segment_id']);

        if ($item->count() == 0) {
            return $this->setData($data)->save();
        } else {
            return $item->getFirstItem()->addData($data)->save();
        }
    }

    public function changeToAuto($id) {

        $item = $this->load($id);

        if (!$item->getId()) {
            return false;
        }

        if ($item->getData('manual') == 1) {
            $segment = Mage::getModel('magna/segments')->load($item->getSegmentId());
            $segment->setData('manual', $segment->getData('manual') - 1)->save();
            return $item->getFirstItem()->setData('manual', 0)->save();
        }

        return false;
    }

}
