<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Magna_Adminhtml_Magna_SegmentsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('customer/segments');

        return $this;
    }

    public function indexAction() {

        $this->_title($this->__('Magna'))->_title($this->__('Segments'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('magna/adminhtml_segments'));
        $this->renderLayout();
    }

    public function recordsgridAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('magna/segments');

        $model->load($id);
        if (!$model->getId()) {
            throw new Exception($this->__('This segment no longer exists.'));
        }

        Mage::register('current_segment', $model);
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__('Magna'))->_title($this->__('Segments'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('magna/segments');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('This segment no longer exists.'));
                $this->_redirect('*/*');
                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Segment'));

        // set entered data if was error when we do save
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $model->getConditions()->setJsFormObject('rule_conditions_fieldset');

        Mage::register('current_segment_rule', $model);

        $this->_initAction();

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
        $this->getLayout()->getBlock('head')->setCanLoadRulesJs(true);

        $this->_addContent($this->getLayout()->createBlock('magna/adminhtml_segments_edit'))
                ->_addLeft($this->getLayout()->createBlock('magna/adminhtml_segments_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {

        if ($this->getRequest()->getPost()) {

            $data = $this->getRequest()->getPost();

            try {
                $model = Mage::getModel('magna/segments');
                $data = $this->_filterDates($data, array('deploy_at'));

                if ($id = $this->getRequest()->getParam('id')) {
                    $model->load($id);
                    if ($id != $model->getId()) {
                        Mage::throwException($this->__('Wrong Segment specified.'));
                    }
                }

                $validateResult = $model->validateData(new Varien_Object($data));
                if ($validateResult !== true) {
                    foreach ($validateResult as $errorMessage) {
                        $this->_getSession()->addError($errorMessage);
                    }
                    $this->_getSession()->setFormData($data);
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                $data['conditions'] = $data['rule']['conditions'];
                unset($data['rule']);

                $model->loadPost($data);
                $this->_getSession()->setFormData($model->getData());

                $model->setData('controller', true);
                $model->save();

                $this->_getSession()->addSuccess($this->__('The segment has been saved.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function newConditionHtmlAction() {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];


        $model = Mage::getModel($type)
                ->setId($id)
                ->setType($type)
                ->setRule(Mage::getModel('magna/segments'))
                ->setPrefix('conditions');
        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }

        if ($model instanceof Mage_Rule_Model_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }

    public function deleteAction() {

        if ($this->getRequest()->getParam('id')) {

            $id = $this->getRequest()->getParam('id');

            try {
                $model = Mage::getModel('magna/segments');
                $model->setId($id)->delete();

                $this->_getSession()->addSuccess($this->__('Segment was successfully deleted'));

                $this->_redirect('*/*/index');
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/index');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function recordsAction() {

        $this->_title($this->__('Magna'))->_title($this->__('Segments'))->_title($this->__('Records'));
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('magna/segments');

        $model->load($id);
        if (!$model->getId()) {
            $this->_getSession()->addError($this->__('This segment no longer exists.'));
            $this->_redirect('*/*');
            return;
        }

        Mage::register('current_segment', $model);

        $this->_title($model->getName());

        if ($this->getRequest()->getParam('refresh')) {
            $cron = Mage::getModel('cron/schedule')->load('magna_build_segments_user', 'job_code');
            if ($cron->getId() && $cron->getStatus() == 'pending') {
                $this->_getSession()->addError($this->__('Please wait until previous cron ends'));
            } else {
                $data['status'] = 'pending';
                $data['job_code'] = 'magna_build_segments_user';
                $data['scheduled_at'] = now();
                $data['created_at'] = now();
                $cron->setData($data)->save();

                $userId = Mage::getSingleton('admin/session')->getUser()->getId();
                $model->setData('build', 1)->setData('notify_user', $userId)->save();
                $this->_getSession()->addSuccess($this->__('Report will be built next time your cron runs'));
            }
            $this->_redirect('*/*/*', array('id' => $id));
            return;
        }

        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('magna/adminhtml_segments_records'));
        $this->renderLayout();
    }

    public function evolutionAction() {

        $this->_title($this->__('Magna'))->_title($this->__('Segments'))->_title($this->__('Evolution'));
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('magna/segments');
        $model->load($id);
        if (!$model->getId()) {
            $this->_getSession()->addError($this->__('This segment no longer exists.'));
            $this->_redirect('*/*');
            return;
        }

        Mage::register('current_segment', $model);
        $this->_title($model->getName());
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('magna/adminhtml_segments_evolutions'));
        $this->renderLayout();
    }

    public function evolutiongridAction() {

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('magna/segments')->load($id);
        Mage::register('current_segment', $model);
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('magna/adminhtml_segments_evolutions'));
        $this->renderLayout();
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportCsvAction() {
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('magna/segments');

        $model->load($id);
        if (!$model->getId()) {
            $this->_getSession()->addError($this->__('This segment no longer exists.'));
            $this->_redirect('*/*');
            return;
        }

        Mage::register('current_segment', $model);

        $fileName = 'segments.csv';
        $content = $this->getLayout()->createBlock('magna/adminhtml_segments_records_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportXmlAction() {
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('magna/segments');

        $model->load($id);
        if (!$model->getId()) {
            $this->_getSession()->addError($this->__('This segment no longer exists.'));
            $this->_redirect('*/*');
            return;
        }

        Mage::register('current_segment', $model);

        $fileName = 'segments.xml';
        $content = $this->getLayout()->createBlock('magna/adminhtml_segments_records_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportevoCsvAction() {
        $fileName = 'segments.csv';
        $content = $this->getLayout()->createBlock('magna/adminhtml_segments_evolutions_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportevoXmlAction() {
        $fileName = 'segments.xml';
        $content = $this->getLayout()->createBlock('magna/adminhtml_segments_evolutions_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massManualDeleteAction() {
        $ids = $this->getRequest()->getParam('ids');
        $changesNum = 0;

        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Customers.'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('magna/segments_list')->changeToAuto($id);
                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were marked as auto-added.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function massManualAddAction() {
        $ids = $this->getRequest()->getParam('ids');
        $changesNum = 0;

        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Customers.'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('magna/segments_list')->changeToManual($id);
                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were marked as manually-added.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function massManualAddCustomerAction() {
        $ids = $this->getRequest()->getParam('customer');
        $segmentId = $this->getRequest()->getParam('segment');
        $changesNum = 0;

        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Customers.'));
        } else {
            try {
                foreach ($ids as $id) {
                    Mage::getModel('magna/segments_list')->addCustomerToSegment($id, $segmentId);
                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were marked as manually-added.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function massManualAddInvoiceAction() {
        $ids = $this->getRequest()->getParam('ids');
        $segmentId = $this->getRequest()->getParam('segment');
        $changesNum = 0;

        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Records.'));
        } else {
            try {
                foreach ($ids as $id) {

                    $customer = Mage::getModel('sales/order_invoice')
                            ->getCollection()
                            ->addFieldToSelect('stella_customer_id')
                            ->addFieldToFilter('entity_id', $id)
                            ->getFirstItem();

                    $insert = Mage::getModel('magna/segments_list')->addCustomerToSegment($customer->getData('stella_customer_id'), $segmentId);
                    if ($insert) {
                        $changesNum++;
                    }
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were marked as manually-added and will not be removed automatically.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function clearAction() {
        try {
            $col = Mage::getModel('eav/entity_attribute')
                    ->getCollection()
                    ->addFieldToFilter('attribute_code', array('in' => array('segments_access', 'magna_segments')));
            foreach ($col as $item) {
                $item->delete();
            }

            $this->_getSession()->addSuccess($this->__('Attributes Removed. You can now uninstall the extension'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirectReferer();
    }

}
