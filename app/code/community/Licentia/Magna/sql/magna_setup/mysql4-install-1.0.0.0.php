<?php

/**
 * Licentia Magna - Customer Segmentation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Segmentation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'magna_segments', array(
    'group' => 'Prices',
    'type' => 'text',
    'backend' => 'magna/product_attribute_backend_segments',
    'label' => 'Segment Prices',
    'class' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'searchable' => false,
    'filterable' => false,
    'is_user_defined' => true,
    'comparable' => false,
    'visible_on_front' => false,
    'unique' => false,
    'is_configurable' => false,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'segments_access', array(
    'type' => 'int',
    'group' => 'General',
    'backend' => '',
    'frontend' => '',
    'label' => 'Segments Access',
    'input' => 'multiselect',
    'class' => '',
    'source' => 'magna/source_segments',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => false,
    'required' => true,
    'user_defined' => false,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => false,
    'used_in_product_listing' => false,
    'unique' => false,
    'note' => 'Allows customers from selected segments to view this product',
));

$installer->run("

-- ----------------------------
--  Table structure for `magna_segments`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_segments')}`;
CREATE TABLE `{$this->getTable('magna_segments')}` (
  `segment_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `conditions_serialized` text,
  `is_active` enum('0','1') DEFAULT '1',
  `records` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `run` varchar(25) DEFAULT NULL,
  `cron` enum('0','d','w','m') DEFAULT '0',
  `cron_last_run` date DEFAULT NULL,
  `build` tinyint(2) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `notify_user` int(11) DEFAULT NULL,
  `priority` smallint(5) NOT NULL,
  `extra_data` text,
  PRIMARY KEY (`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments List';

-- ----------------------------
--  Table structure for `magna_customers`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_customers')}`;
CREATE TABLE `{$this->getTable('magna_customers')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `priority` smallint(5) DEFAULT NULL,
  `data_1` varchar(255) DEFAULT NULL,
  `data_2` varchar(255) DEFAULT NULL,
  `data_3` varchar(255) DEFAULT NULL,
  `data_4` varchar(255) DEFAULT NULL,
  `data_5` varchar(255) DEFAULT NULL,
  `data_6` varchar(255) DEFAULT NULL,
  `data_7` varchar(255) DEFAULT NULL,
  `data_8` varchar(255) DEFAULT NULL,
  `data_9` varchar(255) DEFAULT NULL,
  `data_10` varchar(255) DEFAULT NULL,
  `manual` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`record_id`),
  KEY `email` (`email`),
  KEY `segment_id` (`segment_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `FK_MAGNA_CUST_CUSTID` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_CUST_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Customers List';

-- ----------------------------
--  Table structure for `magna_evolutions`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_evolutions')}`;
CREATE TABLE `{$this->getTable('magna_evolutions')}` (
  `evolution_id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_id` int(11) DEFAULT NULL,
  `records` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `change` int(11) DEFAULT NULL,
  PRIMARY KEY (`evolution_id`),
  KEY `segment_id` (`segment_id`),
  CONSTRAINT `FK_MAGNA_EVO_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Evolutions';

-- ----------------------------
--  Table structure for `magna_segments_prices`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_segments_prices')}`;
CREATE TABLE `{$this->getTable('magna_segments_prices')}` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `website_id` smallint(5) unsigned DEFAULT NULL,
  `price` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`price_id`),
  KEY `product_id` (`product_id`),
  KEY `segment_id` (`segment_id`),
  KEY `website_id` (`website_id`),
  CONSTRAINT `FK_MAGNA_SEG_PRICES_PRODID` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog_product_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEG_PRICES_SEG` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Prices';

-- ----------------------------
--  Table structure for `magna_segments_prices_idx`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_segments_prices_idx')}`;
CREATE TABLE `{$this->getTable('magna_segments_prices_idx')}` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `website_id` smallint(5) unsigned DEFAULT NULL,
  `price` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`index_id`),
  KEY `product_id` (`product_id`),
  KEY `segment_id` (`segment_id`),
  KEY `website_id` (`website_id`),
  CONSTRAINT `FK_MAGNA_IDX_PROD` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog_product_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_IDX_SEG` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_IDX_WS` FOREIGN KEY (`website_id`) REFERENCES `{$this->getTable('core_website')}` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Prices Index';



-- ----------------------------
--  Table structure for `magna_segments_access_blocks`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_segments_access_blocks')}`;
CREATE TABLE `{$this->getTable('magna_segments_access_blocks')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` smallint(6) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `segment_id` (`segment_id`),
  KEY `block_id` (`block_id`) USING BTREE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_BLOCK_BLOCKID` FOREIGN KEY (`block_id`) REFERENCES `{$this->getTable('cms_block')}` (`block_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_BLOCK_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Access Blocks';

-- ----------------------------
--  Table structure for `magna_segments_access_pages`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_segments_access_pages')}`;
CREATE TABLE `{$this->getTable('magna_segments_access_pages')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` smallint(6) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `segment_id` (`segment_id`),
  KEY `page_id` (`page_id`) USING BTREE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PAG_PAGEID` FOREIGN KEY (`page_id`) REFERENCES `{$this->getTable('cms_page')}` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PAG_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Access Pages';

-- ----------------------------
--  Table structure for `magna_segments_access_products`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('magna_segments_access_products')}`;
CREATE TABLE `{$this->getTable('magna_segments_access_products')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `segment_id` (`segment_id`),
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PROD_PRODID` FOREIGN KEY (`product_id`) REFERENCES `{$this->getTable('catalog_product_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PROD_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `{$this->getTable('magna_segments')}` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Magna - Segments Access Products';



");

$installer->endSetup();
