<?php

/**
 * Licentia Monitus - SMS Notifications for E-Goi
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      SMS Notifications
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Monitus_Model_Autoresponders extends Mage_Core_Model_Abstract {

    const MYSQL_DATE = 'yyyy-MM-dd';
    const MYSQL_DATETIME = 'yyyy-MM-dd HH:mm:ss';

    protected function _construct() {

        $this->_init('monitus/autoresponders');
    }

    public function toOptionArray() {

        $return = array(
            'new_search' => Mage::helper('monitus')->__('Search - New'),
            'order_new' => Mage::helper('monitus')->__('Order - New Order'),
            'order_product' => Mage::helper('monitus')->__('Order - Bought Specific Product'),
            'order_status' => Mage::helper('monitus')->__('Order - Order Status Changes'),
            'new_review' => Mage::helper('monitus')->__('Product - New Review'),
            'new_review_self' => Mage::helper('monitus')->__('Product - New Review on a Bought Product'),
        );


        return $return;
    }

    public function changeStatus($event) {

        $order = $event->getEvent()->getOrder();
        $newStatus = $order->getData('status');
        $olderStatus = $order->getOrigData('status');

        if ($newStatus == $olderStatus) {
            return;
        }

        $phone = Mage::getModel('monitus/egoi')->getPhone($order);

        if (!$phone) {
            return false;
        }

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'order_status')
                ->addFieldToFilter('order_status', $newStatus);


        foreach ($autoresponders as $autoresponder) {
            $this->_insertData($autoresponder, $phone, $order->getStoreId());
        }
    }

    public function newSearch($event) {

        $query = Mage::helper('catalogsearch')->getQueryText();

        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
        } else {
            return;
        }

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_search');


        foreach ($autoresponders as $autoresponder) {
            $phone = Mage::getModel('monitus/egoi')->getPhone($customer);

            if (!$phone) {
                return false;
            }

            $search = explode(',', $autoresponder->getSearch());

            foreach ($search as $string) {

                if ($autoresponder->getSearchOption() == 'eq' && strtolower($query) == strtolower($string)) {
                    $this->_insertData($autoresponder, $phone, Mage::app()->getStore()->getId());
                }

                if ($autoresponder->getSearchOption() == 'like' && stripos($query, $string) !== false) {
                    $this->_insertData($autoresponder, $phone, Mage::app()->getStore()->getId());
                }
            }
        }
    }

    public function newReviewSelf($event) {
        $review = $event->getObject();
        $productId = $review->getProductId();
        $customerId = $review->getCustomerId();

        if (!$customerId) {
            return false;
        }

        $orders = Mage::getResourceModel('sales/order_collection')
                ->addFieldToFilter('customer_id', $customerId);

        $return = true;
        foreach ($orders as $order) {
            $items = $order->getAllItems();
            foreach ($items as $item) {
                if ($item->getProductId() == $productId) {
                    $return = false;
                    break 2;
                }
            }
        }

        if ($return) {
            return;
        }

        $customer = Mage::getModel('customer/customer')->load($customerId);

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_review_self');

        foreach ($autoresponders as $autoresponder) {
            $phone = Mage::getModel('monitus/egoi')->getPhone($customer);

            if (!$phone) {
                return false;
            }
            $this->_insertData($autoresponder, $phone, Mage::app()->getStore()->getId());
        }
    }

    public function newReview($event) {
        $review = $event->getObject();

        $customerId = $review->getCustomerId();

        if (!$customerId) {
            return false;
        }

        $customer = Mage::getModel('customer/customer')->load($customerId);

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_review');


        foreach ($autoresponders as $autoresponder) {
            $phone = Mage::getModel('monitus/egoi')->getPhone($customer);

            if (!$phone) {
                return false;
            }
            $this->_insertData($autoresponder, $phone, Mage::app()->getStore()->getId());
        }
    }

    public function newOrder($event) {

        $order = $event->getEvent()->getOrder();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', array('in' => array('order_product', 'order_new')));


        foreach ($autoresponders as $autoresponder) {

            if ($autoresponder->getEvent() == 'order_product') {
                $items = $order->getAllItems();
                $ok = false;
                foreach ($items as $item) {
                    if ($item->getProductId() == $autoresponder->getProduct()) {
                        $ok = true;
                        break;
                    }
                }
                if ($ok === false) {
                    break;
                }
            }

            $phone = Mage::getModel('monitus/egoi')->getPhone($order);

            if (!$phone) {
                return false;
            }


            $this->_insertData($autoresponder, $phone, $order->getStoreId());
        }
    }

    public function calculateSendDate($autoresponder) {
        if ($autoresponder->getSendMoment() == 'occurs') {
            $date = Mage::app()->getLocale()->date()
                    ->get(self::MYSQL_DATETIME);
        }

        if ($autoresponder->getSendMoment() == 'after') {
            $date = Mage::app()->getLocale()->date();

            if ($autoresponder->getAfterHours() > 0) {
                $date->addHour($autoresponder->getAfterHours());
            }
            if ($autoresponder->getAfterDays() > 0) {
                $date->addDay($autoresponder->getAfterDays());
            }
            $date->get(self::MYSQL_DATETIME);
        }

        return $date;
    }

    public function send() {
        $date = Mage::app()->getLocale()->date()->get(self::MYSQL_DATETIME);

        $smsCollection = Mage::getModel('monitus/events')->getCollection()
                ->addFieldToFilter('sent', 0)
                ->addFieldToFilter('send_at', array('lteq' => $date));

        foreach ($smsCollection as $cron) {

            $autoresponder = Mage::getModel('monitus/autoresponders')->load($cron->getAutoresponderId());

            $message = Mage::helper('cms')->getBlockTemplateProcessor()->filter($autoresponder->getMessage());
            #$message = $autoresponder->getMessage();

            $result = Mage::getModel('monitus/egoi')->send($cron->getCellphone(), $message);

            if ($result) {
                $cron->setSent(1)->setSentAt($date)->save();
            }
        }
    }

    protected function _insertData($autoresponder, $number, $storeId) {

        $storeIds = explode(',', $autoresponder->getStoreIds());

        if (!in_array($storeId, $storeIds)) {
            return false;
        }

        if ($autoresponder->getSendOnce() == 1) {
            $exists = Mage::getModel('monitus/events')->getCollection()
                    ->addFieldToFilter('autoresponder_id', $autoresponder->getId())
                    ->addFieldToFilter('cellphone', $number);

            if ($exists->count() != 0) {
                return;
            }
        }

        $data = array();
        $data['send_at'] = $this->calculateSendDate($autoresponder);
        $data['autoresponder_id'] = $autoresponder->getId();
        $data['cellphone'] = $number;
        $data['event'] = $autoresponder->getEvent();
        $data['created_at'] = new Zend_Db_Expr('NOW()');
        $data['sent'] = 0;

        Mage::getModel('monitus/events')->setData($data)->save();
        $autoresponder->setData('number_subscribers', $autoresponder->getData('number_subscribers') + 1)->save();
    }

    public function toFormValues() {
        $return = array();
        $collection = $this->getCollection()
                ->addFieldToSelect('name')
                ->addFieldToSelect('autoresponder_id')
                ->setOrder('name', 'ASC');
        foreach ($collection as $autoresponder) {
            $return[$autoresponder->getId()] = $autoresponder->getName() . ' (ID:' . $autoresponder->getId() . ')';
        }

        return $return;
    }

    protected function _getCollection() {

        $date = Mage::app()->getLocale()->date()->get(self::MYSQL_DATE);
        //Version Compatability
        $return = $this->getCollection()->addFieldToFilter('active', 1);
        $return->getSelect()
                ->where(" from_date <=? or from_date IS NULL ", $date)
                ->where(" to_date >=? or to_date IS NULL ", $date);

        return $return;
    }

}
