<?php

/**
 * Licentia Monitus - SMS Notifications for E-Goi
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      SMS Notifications
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Monitus_Model_Egoi extends Varien_Object {

    public function send($number, $message, $storeId = null) {

        $url = 'https://www51.e-goi.com/api/public/sms/send';

        #$number = $this->getPhone($document);

        if (!$number) {
            return false;
        }

        $data = array(
            "apikey" => Mage::getStoreConfig('monitus/config/apikey', $storeId),
            "mobile" => $number,
            "senderHash" => Mage::getStoreConfig('monitus/config/sender', $storeId),
            "message" => $message
        );

        $data = Zend_Json::encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_MUTE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$data");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return ($http_status == 200) ? true : false;
        #return Zend_Json::decode($output);
    }

    public function getPhone($document) {

        if (!is_object($document) && stripos($document, '-') === false) {
            return false;
        }

        if (!is_object($document) && stripos($document, '-')) {
            return $this->validateNumber($document);
        }


        if ($document instanceof Mage_Sales_Model_Order) {
            $billing = $document->getBillingAddress();
        } else if ($document instanceof Mage_Customer_Model_Customer) {
            $billing = $document->getPrimaryBillingAddress();
        } elseif (is_object($document->getOrder())) {
            $billing = $document->getOrder()->getBillingAddress();
        }

        $prefix = Mage::helper('monitus')->getPrefixForCountry($billing->getCountryId());

        $number = preg_replace('/\D/', '', $billing->getTelephone());
        $number = ltrim($number, $prefix);
        $number = ltrim($number, 0);

        return $this->validateNumber($prefix . '-' . $number);
    }

    public function validateNumber($number) {
        $url = 'https://www51.e-goi.com/api/public/sms/validatePhone/' . $number;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_MUTE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        $final = Zend_Json::decode($result);

        if (array_key_exists('errorCode', $final)) {
            return false;
        }

        return $number;
    }

}
