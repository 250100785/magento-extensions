<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Autoresponders_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $this->_objectId = "autoresponder_id";
        $this->_blockGroup = "nuntius";
        $this->_controller = "adminhtml_autoresponders";

        $this->_addButton("saveandcontinue", array(
            "label" => $this->__("Save and Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);

        if (!Mage::registry("current_autoresponder")->getId() && !$this->getRequest()->getParam('send_moment')) {
            $this->_removeButton('saveandcontinue');
            $this->_removeButton('save');
            $this->_removeButton('reset');
        }


        $this->_formScripts[] = " function saveAndContinueEdit(){
                    tabID = '';
                    $$('ul.tabs li a.active').each(function(item){
                         tabID = item.readAttribute('name');
                    })
                editForm.submit($('edit_form').action+'back/edit/tab_id/'+tabID);
            }";
    }

    public function getHeaderText() {

        if (Mage::registry("current_autoresponder")->getId()) {

            return $this->__("Edit Autoresponder: " . Mage::registry("current_autoresponder")->getName());
        } else {

            return $this->__("Add Autoresponder");
        }
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

}
