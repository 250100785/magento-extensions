<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Autoresponders_Edit_Tab_Data extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry('current_autoresponder');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('page_');

        $fieldset2 = $form->addFieldset('content_fieldset', array('legend' => $this->__('Content')));

        $fieldset2->addField('name', 'text', array(
            'name' => 'name',
            'label' => $this->__('Name'),
            'title' => $this->__('Name'),
            "required" => true,
        ));

        $fieldset2->addField('active', "select", array(
            "label" => $this->__('Status'),
            "options" => array('1' => $this->__('Active'), '0' => $this->__('Inactive')),
            "name" => 'active',
        ));

        $fieldset2->addField("sender_id", "select", array(
            "label" => $this->__("Sender"),
            "class" => "required-entry",
            "required" => true,
            "values" => Mage::getModel('nuntius/senders')->getSenders('email'),
            "name" => "sender_id",
        ));

        $fieldset2->addField('subject', 'text', array(
            'name' => 'subject',
            'label' => $this->__('Subject'),
            'title' => $this->__('Subject'),
            "required" => true,
        ));

        $fieldset2->addField('list_id', 'select', array(
            'name' => 'list_id',
            'label' => $this->__('List'),
            'title' => $this->__('List'),
            'required' => true,
            'values' => Mage::getSingleton('nuntius/lists')->getAllOptions(),
        ));

        if (Mage::helper('nuntius')->magna()) {
            $fieldset2->addField('segments_ids', 'multiselect', array(
                'name' => 'segments_ids[]',
                'label' => $this->__('Subscribers Segment'),
                'title' => $this->__('Subscribers Segment'),
                'required' => true,
                'values' => Mage::getSingleton('magna/segments')->getOptionArray(),
            ));
        }

        $outputFormatDate = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset2->addField('from_date', 'date', array(
            'name' => 'from_date',
            'format' => $outputFormatDate,
            'label' => $this->__('Active From Date'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
        ));
        $fieldset2->addField('to_date', 'date', array(
            'name' => 'to_date',
            'format' => $outputFormatDate,
            'label' => $this->__('Active To Date'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
        ));


        $this->setForm($form);

        if ($current) {
            $form->addValues($current->getData());
        }

        return parent::_prepareForm();
    }

}
