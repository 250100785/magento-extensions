<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Autoresponders_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry('current_autoresponder');

        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('page_');

        $event = $this->getRequest()->getparam('event');
        $campaignId = $this->getRequest()->getparam('campaign_id');
        $sendMoment = $this->getRequest()->getparam('send_moment');
        $productId = $this->getRequest()->getparam('product');
        $type = $this->getRequest()->getParam('type');
        $linkId = $this->getRequest()->getParam('link_id');

        $productName = '';
        if ($current->getId()) {
            $event = $current->getEvent();
            $campaignId = $current->getCampaignId();

            if ($sendMoment) {
                $current->setData('send_moment', $sendMoment);
            }

            $sendMoment = $current->getSendMoment();

            if ($current->getProduct()) {
                $productName = Mage::getModel('catalog/product')->load($current->getProduct())->getName();
            }
        } else {
            $current->setData('event', $event);
            $current->setData('campaign_id', $campaignId);
            $current->setData('send_moment', $sendMoment);
            $current->setData('product', $productId);
            $current->setData('link_id', $linkId);
        }

        $fieldset = $form->addFieldset('params_fieldset', array('legend' => $this->__('Settings')));

        $location = $this->getUrl('*/*/*', array('type' => $type, 'id' => $this->getRequest()->getParam('id')));

        $options = Mage::getModel('nuntius/autoresponders')->toOptionArray();

        if (!$event) {
            array_unshift($options, $this->__('Please Select'));
        }

        $script = "<script>function goUrl(url){

        var els=new Array('event','campaign_id','link_id','product','send_moment');

        var temp = '';
        Form.getElements( $('edit_form') ).each(function(item){

            if(els.indexOf(item.name)==-1)
            return;

            if(item.value.length==0)
            return;

            if(item.name =='form_key')
            return;

           temp += item.name+'/'+item.value+'/';

           });

             window.location=url+temp

         }</script>";

        $fieldset->addField('event', 'select', array(
            'name' => 'event',
            'label' => $this->__('Event Trigger'),
            'title' => $this->__('Event Trigger'),
            'options' => $options,
            'disabled' => $current->getId() ? true : false,
            "required" => true,
            "onchange" => "goUrl('$location')",
        ))->setAfterElementHtml($script);

        if ($event == 'campaign_open' || $event == 'campaign_link' || $event == 'campaign_click') {

            $options = Mage::getModel('nuntius/campaigns')->toFormValuesNonAuto();
            if ($event == 'campaign_link') {
                $location = "goUrl('$location')";

                if (!$campaignId) {
                    $options[''] = $this->__('Please Select');
                }
            } else {
                $location = '';
            }

            $fieldset->addField('campaign_id', "select", array(
                "label" => $this->__('Campaign'),
                "options" => $options,
                "name" => 'campaign_id',
                'value' => '',
                "onchange" => $location,
            ));
        }

        if ($event == 'campaign_link' && $campaignId) {

            $links = Mage::getModel('nuntius/links')->getHashForCampaign($campaignId);

            if (count($links) == 0) {
                $links = array('' => $this->__('No links detected in selected campaign'));
            }

            $fieldset->addField('link_id', "select", array(
                "label" => $this->__('Link'),
                "options" => $links,
                "required" => true,
                "name" => 'link_id',
            ));
        }

        if ($event == 'order_product') {

            $fieldset->addField('product', 'text', array(
                'name' => 'product',
                'label' => $this->__('Product ID'),
                'title' => $this->__('Product ID'),
                "note" => $productName . ' <a target="_blank" href="' . $this->getUrl('*/catalog_product') . '">' . $this->__('Go to Product Listing') . '</a>',
                "required" => true,
            ));
        }

        if ($event) {

            $options = array();

            if (!$current->getId() && !$sendMoment) {
                $options[''] = $this->__('Please Select');
            }
            $options['occurs'] = $this->__('When triggered');
            $options['after'] = $this->__('After...');

            $fieldset->addField('send_moment', "select", array(
                "label" => $this->__('Send Moment'),
                "options" => $options,
                "name" => 'send_moment',
                "required" => true,
                "onchange" => "goUrl('$location')",
            ));
        }

        if ($sendMoment == 'after') {
            $fieldset->addField('after_hours', "select", array(
                "label" => $this->__('After Hours'),
                "options" => array_combine(range(0, 23), range(0, 23)),
                "name" => 'after_hours',
            ));


            $fieldset->addField('after_days', "select", array(
                "label" => $this->__('After Days...'),
                "options" => array_combine(range(0, 30), range(0, 30)),
                "name" => 'after_days',
            ));
        }

        if ($event && $event != 'new_account' && $event != 'order_status' && $sendMoment) {
            $fieldset->addField('send_once', "select", array(
                "label" => $this->__('Send Only Once?'),
                "options" => array('1' => $this->__('Yes'), '0' => $this->__('Every Time Occurs')),
                "name" => 'send_once',
                "value" => '1',
                "note" => $this->__('To the same subscriber'),
            ));
        }

        if (($event == 'new_account' || $event == 'order_status' ) && $sendMoment) {
            $fieldset->addField('send_once', "hidden", array(
                "value" => 1,
                "name" => 'send_once',
            ));
        }

        if ($event == 'order_status' && $sendMoment) {
            $fieldset->addField('order_status', "select", array(
                "label" => $this->__('New Status'),
                "options" => Mage::getSingleton('sales/order_config')->getStatuses(),
                "name" => 'order_status',
            ));
        }

        if ($event == 'new_search' && $sendMoment) {
            $fieldset->addField('search', 'text', array(
                'name' => 'search',
                'label' => $this->__('Search Value'),
                'title' => $this->__('Search Value'),
                'required' => true,
                "note" => $this->__('Separate multiple values by comma ,'),
            ));

            $fieldset->addField('search_option', "select", array(
                "label" => $this->__('Query String Match'),
                "options" => array('eq' => $this->__('Equal'), 'like' => $this->__('Contains')),
                "name" => 'search_option',
            ));
        }

        if ($sendMoment) {
            $fieldset->addField('cancel_if_order', "select", array(
                "label" => $this->__('Cancel if made order'),
                "note" => $this->__('Do not send this responder if the customer made a purchase after this event and before the responder end date'),
                "options" => array('0' => $this->__('No'), '1' => $this->__('Yes')),
                "name" => 'cancel_if_order',
                "value" => '1',
            ));
        }

        $this->setForm($form);

        if ($current) {
            $form->addValues($current->getData());
        }
        return parent::_prepareForm();
    }

}
