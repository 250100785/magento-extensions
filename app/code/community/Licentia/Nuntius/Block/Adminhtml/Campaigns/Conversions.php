<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *


 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Campaigns_Conversions extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_campaigns_conversions';
        $this->_blockGroup = 'nuntius';
        $this->_headerText = $this->__('Campaign Conversions');
        if ($campaign = Mage::registry('current_campaign')) {

            $this->_headerText = $this->__('Campaign Conversions') . ' / ' . $campaign->getInternalName();

            $cancelUrl = $this->getUrl('*/nuntius_campaigns');

            $this->addButton('cancel_campaign', array('label' => $this->__('Back'),
                'onclick' => "window.location='$cancelUrl';", 'class' => 'back'));
        }
        parent::__construct();

        $this->_removeButton('add');
    }

}
