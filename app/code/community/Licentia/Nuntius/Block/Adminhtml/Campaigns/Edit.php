<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Campaigns_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        $this->_objectId = 'id';
        $this->_blockGroup = "nuntius";
        $this->_controller = 'adminhtml_campaigns';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Save'));
        $this->_updateButton('delete', 'label', $this->__('Delete'));

        $campaign = Mage::registry('current_campaign');

        if ($campaign->getId()) {

            if ($campaign->getRecurring() == '0') {
                $emailUrl = $this->getUrl('*/nuntius_followup/new', array('cid' => $campaign->getId()));
                $this->addButton('followup_email', array('label' => $this->__('Follow Up'),
                    'class' => "add",
                    'title' => "Follow Up",
                    'onclick' => "window.location='$emailUrl';"));
            }

            if ($campaign->getRecurring() != '0' && $campaign->getStatus() != 'finished') {
                $cancelUrl = $this->getUrl('*/*/cancel', array('id' => $campaign->getId()));
                $text = $this->__('Cancel this campaign? This action can not be undone');

                $this->addButton('cancel_campaign', array('label' => $this->__('Cancel Campaign'),
                    'onclick' => "if(!confirm('$text')){return false;}; window.location='$cancelUrl';"));
            }


            $previewUrl = $this->getUrl('*/*/preview', array('id' => $campaign->getId()));

            $this->_addButton('preview', array(
                'label' => $this->__('Preview'),
                'onclick' => "window.open('$previewUrl'); return false;",
                    )
            );
            $text = $this->__('Start the sending process now?');

            $this->addButton('send', array('label' => $this->__('Save & Send'),
                'class' => 'save saveandsendbutton ',
                'onclick' => "if(!confirm('$text')){return false;}; saveAndSend()"));
        }

        if ($campaign->getStatus() == 'finished') {
            $this->_removeButton('save');
            $this->_removeButton('send');

            $this->addButton('duplicate', array('label' => $this->__('Duplicate & Save'),
                'class' => 'save',
                'onclick' => "editForm.submit($('edit_form').action + 'op/duplicate/')"));
        } else {
            $this->_addButton("saveandcontinue", array(
                "label" => $this->__("Save and Continue Edit"),
                "onclick" => "saveAndContinueEdit()",
                "class" => "save",
                    ), -100);

            $this->_formScripts[] = " function saveAndContinueEdit(){
                    tabID = '';
                    $$('ul.tabs li a.active').each(function(item){
                         tabID = item.readAttribute('name');
                    })
                editForm.submit($('edit_form').action+'back/edit/tab_id/'+tabID);
            }";
        }

        $this->_formScripts[] = "

            function saveAndSend(){ editForm.submit($('edit_form').action + 'op/send/') }

            function toggleEditor() {
                if (tinyMCE.getInstanceById('block_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'block_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'block_content');
                }
            }";
    }

    public function getHeaderText() {

        $campaign = Mage::registry('current_campaign');

        if ($campaign->getId()) {
            return $this->__($this->htmlEscape($campaign->getInternalName()));
        } else {
            return $this->__('New Campaign');
        }
    }

    protected function _prepareLayout() {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

}
