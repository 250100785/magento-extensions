<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Campaigns_Edit_Tab_Email extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {


        $current = Mage::registry("current_campaign");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("nuntius_form", array("legend" => $this->__("Campaign Information")));
        $fieldset->addField("internal_name", "text", array(
            "label" => $this->__("Internal Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "internal_name",
        ));

        $fieldset->addField("sender_id", "select", array(
            "label" => $this->__("Sender"),
            "class" => "required-entry",
            "required" => true,
            "values" => Mage::getModel('nuntius/senders')->getSenders(),
            "name" => "sender_id",
        ));

        $fieldset->addField("subject", "text", array(
            "label" => $this->__("Subject"),
            "class" => "required-entry",
            "required" => true,
            "name" => "subject",
        ));

        $fieldset->addField('list_id', 'select', array(
            'name' => 'list_id',
            'label' => $this->__('List'),
            'title' => $this->__('List'),
            'required' => true,
            'values' => Mage::getSingleton('nuntius/lists')->getAllOptions(),
        ));

        if (Mage::helper('nuntius')->magna()) {
            $fieldset->addField('segments_ids', 'multiselect', array(
                'name' => 'segments_ids[]',
                'label' => $this->__('Subscribers Segment'),
                'title' => $this->__('Subscribers Segment'),
                'required' => true,
                'values' => Mage::getSingleton('magna/segments')->getOptionArray(),
            ));
        }

        if ($current->getData()) {
            $form->setValues($current->getData());
        }

        return parent::_prepareForm();
    }

}
