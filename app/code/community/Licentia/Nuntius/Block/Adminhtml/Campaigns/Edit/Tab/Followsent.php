<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *  


 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International 
 */
class Licentia_Nuntius_Block_Adminhtml_Campaigns_Edit_Tab_Followsent extends Licentia_Nuntius_Block_Adminhtml_Campaigns_Children_Grid {

    protected function _prepareCollection() {

        $followup = Mage::registry('current_followup');

        $collection = Mage::getModel('nuntius/campaigns')
                ->getResourceCollection()
                ->addFieldToFilter('followup_id', array('in' => $followup->getAllIds()));

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

}
