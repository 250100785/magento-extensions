<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Campaigns_Edit_Tab_Recurring extends Mage_Adminhtml_Block_Widget_Form {

    public function __construct() {
        parent::__construct();
        $this->setTemplate('nuntius/campaigns/recurring.phtml');
    }

    protected function _prepareForm() {

        $current = Mage::registry("current_campaign");

        if ($current->getId()) {
            $extraRun = $this->__('This campaign has already runned for %d time(s)', $current->getRunTimes());
        } else {
            $extraRun = '';
        }

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('content_fieldset', array('legend' => $this->__('Recurring Profile')));

        $outputFormat = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $outputFormatDate = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('subscriber_time', 'select', array(
            'name' => 'subscriber_time',
            'type' => 'options',
            'options' => array(0 => $this->__('No'), 1 => $this->__('Yes')),
            'label' => $this->__('Dynamic Deployment?'),
            'note' => $this->__('Use this option if you want to maximize conversions rates. This will send emails at hours that users are most likely to open them. There is a span of up to 24 hours with this option'),
        ));

        $fieldset->addField("recurring", "select", array(
            "label" => $this->__("Recurring Campaign?"),
            "class" => "required-entry",
            "onchange" => "handlecron()",
            "required" => true,
            "values" => Licentia_Nuntius_Model_Campaigns::getCronList(),
            "name" => "recurring",
        ));

        $fieldset->addField('deploy_at', 'date', array(
            'name' => 'deploy_at',
            'time' => true,
            'format' => $outputFormat,
            'label' => $this->__('Deploy Date'),
            'image' => $this->getSkinUrl('images/grid-cal.gif')
        ));

        $fieldset->addField("recurring_daily", "multiselect", array(
            "label" => $this->__("In Which days?"),
            "values" => Licentia_Nuntius_Model_Campaigns::getDaysList(),
            "name" => "recurring_daily",
        ));

        $fieldset->addField("recurring_unique", "select", array(
            "label" => $this->__("Unique Recipient?"),
            "note" => $this->__("Use this option if you do not want this campaign to be sent to the same recipient more than once."),
            "values" => array('0' => $this->__('No'), '1' => $this->__('Yes')),
            "name" => "recurring_unique",
        ));

        $fieldset->addField("recurring_day", "select", array(
            "label" => $this->__("In Which day?"),
            "values" => Licentia_Nuntius_Model_Campaigns::getDaysList(),
            "name" => "recurring_day",
        ));

        $fieldset->addField("recurring_monthly", "select", array(
            "label" => $this->__("In which day?"),
            "values" => Licentia_Nuntius_Model_Campaigns::getDaysMonthsList(),
            "name" => "recurring_monthly",
        ));

        $fieldset->addField("recurring_month", "select", array(
            "label" => $this->__("In which month?"),
            "values" => Licentia_Nuntius_Model_Campaigns::getMonthsList(),
            "name" => "recurring_month",
        ));

        $fieldset->addField("recurring_time", "select", array(
            "label" => $this->__("Run around"),
            "class" => "required-entry",
            "values" => Licentia_Nuntius_Model_Campaigns::getRunAroundList(),
            "name" => "recurring_time",
            "note" => $this->__("Please choose the start hour for this profile"),
        ));

        $fieldset->addField('recurring_first_run', 'date', array(
            'name' => 'recurring_first_run',
            'format' => $outputFormatDate,
            'required' => true,
            'label' => $this->__('First Run'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            "note" => $this->__("First time this campaign should run"),
        ));

        $fieldset->addField('run_until', 'date', array(
            'name' => 'run_until',
            'format' => $outputFormatDate,
            'label' => $this->__('End Date'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            "note" => $this->__("How long should this campaing run."),
        ));


        $fieldset->addField("run_times", "text", array(
            "label" => $this->__("Running times"),
            "name" => "run_times",
            "note" => $this->__("How many times should this campaing run. " . $extraRun),
        ));

        $fieldset->addField("end_method", "select", array(
            "label" => $this->__("Stop sending when"),
            "name" => "end_method",
            'values' => array(
                'both' => $this->__('We achieve the "End Date" AND the "Running Times" number'),
                'run_until' => $this->__('We achieve the "End Date" value'),
                'number' => $this->__('We achieve the "Running Times" number'),
                'any' => $this->__('We achieve the "End Date" OR the "Running Times" number')
            ),
        ));

        $this->setForm($form);

        if ($current->getData()) {
            $form->setValues($current->getData());
        } else {
            $date = new Zend_Date(null, $outputFormat);
            $date->addDay(7);
            $form->setValues(array('deploy_at' => $date));
        }

        return parent::_prepareForm();
    }

    public function getTemplateOptions() {

        return Mage::getModel('nuntius/templates')->getOptionArray();
    }

}
