<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Campaigns_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId('campaign_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Campaigns'));
    }

    protected function _beforeToHtml() {

        $current = Mage::registry('current_campaign');
        $followup = Mage::registry('current_followup');

        $this->addTab("form_section", array(
            "label" => $this->__("General"),
            "title" => $this->__("General"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_edit_tab_email")->toHtml(),
        ));

        $this->addTab("recurring_section", array(
            "label" => $this->__("Sending Options"),
            "title" => $this->__("Sending Options"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_edit_tab_recurring")->toHtml(),
        ));


        $this->addTab("content_section", array(
            "label" => $this->__("Content"),
            "title" => $this->__("Content"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_edit_tab_content")->toHtml(),
        ));

        if ($current->getId() && $current->getRecurring() != '0') {

            $this->addTab('children', array(
                'label' => $this->__('Children Campaigns'),
                "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_children_grid")->toHtml(),
            ));
        }

        if ($followup->count() > 0) {
            $this->addTab('followup', array(
                'label' => $this->__('Follow Up Queue'),
                "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_edit_tab_followup")->toHtml(),
            ));
            $this->addTab('followup_sent', array(
                'label' => $this->__('Follow Up Sents'),
                "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_edit_tab_followsent")->toHtml(),
            ));
        }

        if ($current->getId()) {

            $this->addTab('conversions', array(
                'label' => $this->__('Conversions'),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/gridconv', array('_current' => true)),
            ));

            $this->addTab("links_section", array(
                "label" => $this->__("Links"),
                "title" => $this->__("Links"),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/linksgrid', array('_current' => true)),
            ));

            $this->addTab("archive_section", array(
                "label" => $this->__("Messages Archive"),
                "title" => $this->__("Messages Archive"),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/archivegrid', array('_current' => true)),
            ));

            $this->addTab("queue_section", array(
                "label" => $this->__("Messages Queue"),
                "title" => $this->__("Messages Queue"),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/queuegrid', array('_current' => true)),
            ));

            $errors = $this->getLayout()->createBlock("nuntius/adminhtml_errors_grid");
            $resultErrors = $errors->toHtml();
            $totalErrors = $errors->getCollection()->getSize();
            if ($totalErrors > 0) {
                $extraErrors = "<strong class='error'>($totalErrors)</strong>";
            } else {
                $extraErrors = '(0)';
            }
            $this->addTab("error_section", array(
                "label" => $this->__("Messages Errors " . $extraErrors),
                "title" => $this->__("Messages Errors " . $extraErrors),
                "content" => $resultErrors,
            ));
        }

        if ($this->getRequest()->getParam('tab_id')) {
            $this->setActiveTab($this->getRequest()->getParam('tab_id'));
        }

        return parent::_beforeToHtml();
    }

}
