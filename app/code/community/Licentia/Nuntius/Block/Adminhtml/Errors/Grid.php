<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Errors_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('coupons_grid');
        $this->setDefaultSort('error_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('nuntius/errors')
                ->getResourceCollection();

        if ($id = $this->getRequest()->getParam('id')) {
            $collection->addFieldToFilter('campaign_id', $id);
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('error_id', array(
            'header' => $this->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'error_id',
        ));
        if (!($id = $this->getRequest()->getParam('id'))) {
            $this->addColumn('campaign_id', array(
                'header' => $this->__('Campaign'),
                'index' => 'campaign_id',
                'type' => 'options',
                'options' => Mage::getModel('nuntius/campaigns')->toFormValues(),
            ));
        }

        $this->addColumn('email', array(
            'header' => $this->__('Subscriber Email'),
            'index' => 'email',
        ));

        $this->addColumn('error_code', array(
            'header' => $this->__('Error Code'),
            'index' => 'error_code',
        ));

        $this->addColumn('error_message', array(
            'header' => $this->__('Error Message'),
            'index' => 'error_message',
        ));

        $this->addColumn('created_at', array(
            'header' => $this->__('Created at'),
            'align' => 'left',
            'type' => 'datetime',
            'index' => 'created_at',
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportXml', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('error_id');
        $this->getMassactionBlock()->setFormFieldName('errors');

        $this->getMassactionBlock()->addItem('tryagain', array(
            'label' => $this->__('Try Again'),
            'url' => $this->getUrl('*/nuntius_errors/massSend'),
            'confirm' => $this->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/nuntius_errors/massDelete'),
            'confirm' => $this->__('Are you sure?')
        ));

        return $this;
    }

    public function getGridUrl() {
        return $this->getUrl('*/nuntius_errors/grid', array('_current' => true));
    }

}
