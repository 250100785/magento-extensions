<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Followup_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry("current_followup");

        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('content_fieldset', array('legend' => $this->__('Follow Ups')));

        $fieldset->addField('name', "text", array(
            "label" => $this->__("Internal Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        $fieldset->addField("active", "select", array(
            "label" => $this->__("Active"),
            "class" => "required-entry",
            "required" => true,
            "values" => array('0' => $this->__('No'), '1' => $this->__('Yes')),
            "name" => "active",
        ));

        $fieldset->addField('subject', "text", array(
            "label" => $this->__("Subject"),
            "class" => "required-entry",
            "required" => true,
            "note" => $this->__('the {{subject}} tag will be replaced by the original campaign subject'),
            "name" => "subject",
        ));

        $fieldset->addField("recipients_options", "multiselect", array(
            "label" => $this->__("Send to subscribers that..."),
            "class" => "required-entry",
            "values" => Mage::getModel('nuntius/followup')->getOptionValues(),
            "name" => "recipients_options[]",
        ));

        $fieldset->addField('days', 'select', array(
            'name' => 'days',
            'label' => $this->__('Send after X days'),
            'title' => $this->__('Send after X days'),
            'required' => true,
            'options' => array_combine(range(1, 10), range(1, 10)),
        ));
        
        if (Mage::helper('nuntius')->magna()) {
            $fieldset->addField('segments_ids', 'multiselect', array(
                'name' => 'segments_ids[]',
                'label' => $this->__('Subscribers Segment'),
                'title' => $this->__('Subscribers Segment'),
                'required' => true,
                'values' => Mage::getSingleton('magna/segments')->getOptionArray(),
            ));
        }

        if ($current->getData()) {
            $form->setValues($current->getData());
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

}
