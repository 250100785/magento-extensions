<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Goals_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $location = $this->getUrl('*/*/*', array('_current' => true, 'goal_type' => false)) . 'goal_type/';

        $current = Mage::registry("current_goal");

        $goal = $this->getRequest()->getParam('goal_type');

        if ($current->getGoalType()) {
            $goal = $current->getGoalType();
        } else {
            $current->setData('goal_type', $goal);
            $current->setStatus(3);
        }

        $types = Mage::getModel('nuntius/goals')->getGoalTypes();

        if (!$current->getData('goal_type')) {
            array_unshift($types, $this->__('Please Select'));
        }

        $show = false;
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("nuntius_form", array("legend" => $this->__("Goal Information")));

        $fieldset->addField("goal_type", "select", array(
            "label" => $this->__("Goal Type"),
            "class" => "required-entry",
            "required" => true,
            "options" => $types,
            "name" => "goal_type",
            "disabled" => $current->getId() ? true : false,
            "onchange" => "window.location='$location'+this.value",
        ));

        if (Mage::helper('nuntius')->magna()) {
            if (stripos($goal, 'segment_') !== false) {
                $show = 1;

                $fieldset->addField("goal_type_option_id", "select", array(
                    "label" => $this->__("Segment"),
                    "class" => "required-entry",
                    "required" => true,
                    "options" => Mage::getModel('magna/segments')->toFormValues(),
                    "name" => "goal_type_option_id",
                    "disabled" => ($current->getStatus() <= 2) ? true : false,
                ));
            }
        }
        if (stripos($goal, 'global') !== false) {
            $show = 2;
        }

        if (stripos($goal, 'list_') !== false) {
            $show = 1;

            $fieldset->addField("goal_type_option_id", "select", array(
                "label" => $this->__("List"),
                "class" => "required-entry",
                "required" => true,
                "options" => Mage::getModel('nuntius/lists')->getOptionArray(),
                "name" => "goal_type_option_id",
            ));
        }

        if (stripos($goal, 'campaign_') !== false) {
            $show = 1;

            $fieldset->addField("goal_type_option_id", "select", array(
                "label" => $this->__("List"),
                "class" => "required-entry",
                "required" => true,
                "options" => Mage::getModel('nuntius/campaigns')->toFormValues(),
                "name" => "goal_type_option_id",
            ));
        }

        if ($show) {
            $fieldset->addField("variation", "text", array(
                "label" => $this->__("Variation"),
                "class" => "required-entry",
                "required" => true,
                "name" => "variation",
                "note" => "Valid values:"
                . "<br>19 - Expects final number 19"
                . "<br>+19 - Expects Final Number to be increased by 19"
                . "<br>+20% - Expects Final Number to be increased by 20%"
                . "<br>-9 - Expects Final Number to be decreased by 9"
                . "<br>-20% - Expects Final Number to be decreased by 20%",
            ));

            $fieldset->addField("name", "text", array(
                "label" => $this->__("Name"),
                "class" => "required-entry",
                "required" => true,
                "name" => "name",
            ));

            $fieldset->addField("description", "textarea", array(
                "label" => $this->__("Description"),
                "class" => "required-entry",
                "required" => true,
                "name" => "description",
            ));

            $outputFormatDate = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
            $fieldset->addField('start_date', 'date', array(
                'name' => 'start_date',
                "class" => "required-entry",
                "disabled" => ($current->getId() && $current->getStatus() <= 2) ? true : false,
                "required" => true,
                'format' => $outputFormatDate,
                'label' => $this->__('Start Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
            ));

            $fieldset->addField('end_date', 'date', array(
                'name' => 'end_date',
                "class" => "required-entry",
                "required" => true,
                'format' => $outputFormatDate,
                'label' => $this->__('End Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
            ));
        }

        if ($current) {
            $form->setValues($current->getData());
        }
        return parent::_prepareForm();
    }

}
