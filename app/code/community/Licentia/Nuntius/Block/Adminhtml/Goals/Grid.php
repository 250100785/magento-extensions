<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Goals_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('goal_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('nuntius/goals')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('goal_id', array(
            'header' => $this->__('ID'),
            'width' => '50px',
            'index' => 'goal_id',
        ));

        $this->addColumn('goal_type', array(
            'header' => $this->__('Type'),
            'align' => 'left',
            'type' => 'options',
            'options' => Mage::getModel('nuntius/goals')->getGoalTypes(),
            'index' => 'goal_type',
        ));

        $this->addColumn('goal_type_option_id', array(
            'header' => $this->__('Type Name'),
            'index' => 'goal_type_option_id',
            'filter' => false,
            'sortable' => false,
            'frame_callback' => array($this, 'optionResult'),
        ));

        $this->addColumn('name', array(
            'header' => $this->__('Goal Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('variation', array(
            'header' => $this->__('Goal'),
            'align' => 'left',
            'index' => 'variation',
        ));

        $this->addColumn('original_value', array(
            'header' => $this->__('Original Value'),
            'width' => '60px',
            'type' => 'number',
            'default' => 'N/A',
            'index' => 'original_value',
        ));
        $this->addColumn('current_value', array(
            'header' => $this->__('Current Value'),
            'type' => 'number',
            'index' => 'current_value',
            'frame_callback' => array($this, 'currentResult'),
        ));

        $this->addColumn('expected_value', array(
            'header' => $this->__('Expected Value'),
            'width' => '60px',
            'type' => 'number',
            'index' => 'expected_value',
        ));
        $this->addColumn('start_date', array(
            'header' => $this->__('Date Start'),
            'align' => 'left',
            'width' => '120px',
            'type' => 'date',
            'index' => 'start_date',
        ));

        $this->addColumn('end_date', array(
            'header' => $this->__('Date End'),
            'align' => 'left',
            'width' => '120px',
            'type' => 'date',
            'index' => 'end_date',
        ));

        $this->addColumn('result', array(
            'header' => $this->__('Result'),
            'align' => 'right',
            'index' => 'result',
            'type' => 'options',
            'options' => array(
                1 => $this->__('Acomplished'),
                0 => $this->__('Failed'),
                2 => $this->__('Running'),
                3 => $this->__('Stand By'),
            ),
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    public function optionResult($value, $row) {

        $goal = $row->getData('goal_type');

        if (Mage::helper('nuntius')->magna() && stripos($goal, 'segment_') !== false) {
            $type = 'segments';

            $model = Mage::getModel('magna/' . $type)->load($value);
        } else {

            if (stripos($goal, 'list_') !== false) {
                $type = 'lists';
            }
            if (stripos($goal, 'campaign_') !== false) {
                $type = 'campaigns';
            }

            if (stripos($goal, 'global') !== false) {
                return 'N/A';
            }

            $model = Mage::getModel('nuntius/' . $type)->load($value);
        }

        if ($model->getId()) {
            return $model->getName();
        }

        return 'N/A';
    }

    public function currentResult($value, $row) {

        $number = (int) $row->getData('expected_value') - (int) $row->getData('original_value');

        $valueColor = abs(round(($value - $row->getData('original_value')) * 100 / $number));

        $color = '#' . $this->percent2Color($valueColor);

        if ($row->getData('result') == 0) {
            $color = 'black; color: #FFF';
        }

        if ($row->getData('result') == 1) {
            $color = 'green; color: #FFF';
        }

        if ($row->getData('result') == 3) {
            $color = '';
        }

        return '<div style="text-align:center; background-color:' . $color . '; border-radius: 7px; font-weight: bold;">' . $value . ' (' . $valueColor . '%)</div>';
    }

    public function percent2Color($value, $brightness = 255, $max = 100, $thirdColorHex = '00') {

        if ($value >= $max) {
            return "008000; color: #FFF";
        }

        $first = (1 - ($value / $max)) * $brightness;
        $second = ($value / $max) * $brightness;

        // Find the influence of the middle color (yellow if 1st and 2nd are red and green)
        $diff = abs($first - $second);
        $influence = ($brightness - $diff) / 2;
        $first = intval($first + $influence);
        $second = intval($second + $influence);

        // Convert to HEX, format and return
        $firstHex = str_pad(dechex($first), 2, 0, STR_PAD_LEFT);
        $secondHex = str_pad(dechex($second), 2, 0, STR_PAD_LEFT);

        return $firstHex . $secondHex . $thirdColorHex;
    }

}
