<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *  


 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International 
 */
class Licentia_Nuntius_Block_Adminhtml_Links extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_links';
        $this->_blockGroup = 'nuntius';
        $this->_headerText = $this->__('Links Logs');

        parent::__construct();

        $this->_removeButton('add');
    }

}
