<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Lists_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $this->_objectId = "list_id";
        $this->_blockGroup = "nuntius";
        $this->_controller = "adminhtml_lists";

        #$this->_removeButton('delete');

        $this->_addButton("saveandcontinue", array(
            "label" => $this->__("Save and Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);

        $this->_formScripts[] = " function saveAndContinueEdit(){
                    tabID = '';
                    $$('ul.tabs li a.active').each(function(item){
                         tabID = item.readAttribute('name');
                    })
                editForm.submit($('edit_form').action+'back/edit/tab_id/'+tabID);
            }";
    }

    public function getHeaderText() {
        if (Mage::registry("current_list")->getId()) {
            return $this->__("Edit List '%s'", $this->htmlEscape(Mage::registry("current_list")->getName()));
        } else {
            return $this->__("Add List");
        }
    }

}
