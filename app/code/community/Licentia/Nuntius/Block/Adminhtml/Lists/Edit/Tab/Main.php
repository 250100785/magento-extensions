<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Lists_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry("current_list");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("nuntius_form", array("legend" => $this->__("List Information")));

        $fieldset->addField("name", "text", array(
            "label" => $this->__("List Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        $fieldset->addField("store_id", "select", array(
            "label" => $this->__("Store View"),
            "values" => Mage::getModel('adminhtml/system_store')->getStoreValuesForForm(),
            "required" => true,
            "note" => $this->__('Only applied to subscribers without Store View specified'),
            "name" => "store_id",
        ));

        $fieldset->addField("description", "textarea", array(
            "label" => $this->__("Description"),
            "name" => "description",
        ));

        $fieldset->addField("auto", "select", array(
            "label" => $this->__("Auto Add Customer"),
            "options" => array('0' => 'No', '1' => 'Yes'),
            "required" => true,
            "note" => $this->__('Select this option if you want every new customer added to this list automatically.'),
            "name" => "auto",
        ));

        if ($current) {
            $form->setValues($current->getData());
        }
        return parent::_prepareForm();
    }

}
