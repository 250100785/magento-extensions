<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Reports_Detail_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("nuntius_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle($this->__("Report Information"));
    }

    protected function _beforeToHtml() {

        $this->addTab("general", array(
            "label" => $this->__("General"),
            "title" => $this->__("General"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_reports_detail_general")->toHtml(),
        ));
        $this->addTab("location", array(
            "label" => $this->__("Location"),
            "title" => $this->__("Location"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_reports_detail_location")->toHtml(),
        ));
        $this->addTab("dates", array(
            "label" => $this->__("Dates"),
            "title" => $this->__("Dates"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_reports_detail_dates")->toHtml(),
        ));
        $this->addTab("applications", array(
            "label" => $this->__("Applications"),
            "title" => $this->__("Applications"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_reports_detail_applications")->toHtml(),
        ));
        $this->addTab("links", array(
            "label" => $this->__("Links"),
            "title" => $this->__("Links"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_reports_detail_links")->toHtml(),
        ));
        $this->addTab("conversions", array(
            "label" => $this->__("Conversions"),
            "title" => $this->__("Conversions"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_campaigns_edit_tab_conversions")->toHtml(),
        ));

        if ($this->getRequest()->getParam('tab_id')) {
            $this->setActiveTab($this->getRequest()->getParam('tab_id'));
        }

        return parent::_beforeToHtml();
    }

}
