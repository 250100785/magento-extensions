<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Reports_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('reports_grid');
        $this->setDefaultSort('report_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('nuntius/reports')
                ->getResourceCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('report_id', array(
            'header' => $this->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'report_id',
        ));

        $this->addColumn('list_id', array(
            'header' => $this->__('List'),
            'index' => 'list_id',
            'type' => 'options',
            'options' => Mage::getModel('nuntius/lists')->getOptionArray(),
        ));

        $this->addColumn('created_at', array(
            'header' => $this->__('Date'),
            'type' => 'date',
            'index' => 'created_at',
        ));

        $this->addColumn('subscribers', array(
            'header' => $this->__('Subscribers'),

            'index' => 'subscribers',
        ));
        $this->addColumn('subscribers_variation', array(
            'header' => $this->__('<-Var.'),
            'index' => 'subscribers_variation',
        ));

        $this->addColumn('campaigns', array(
            'header' => $this->__('Campaigns'),
            'index' => 'campaigns',
        ));

        $this->addColumn('campaigns_variation', array(
            'header' => $this->__('<-Var.'),
            'index' => 'campaigns_variation',
        ));

        $this->addColumn('clicks', array(
            'header' => $this->__('Clicks'),
            'index' => 'clicks',
        ));
        $this->addColumn('clicks_variation', array(
            'header' => $this->__('<-Var.'),
            'index' => 'clicks_variation',
        ));

        $this->addColumn('views', array(
            'header' => $this->__('Views'),
            'index' => 'views',
        ));
        $this->addColumn('views_variation', array(
            'header' => $this->__('<-Var.'),
            'index' => 'views_variation',
        ));

        $this->addColumn('conversions_number', array(
            'header' => $this->__('Conv.'),
            'index' => 'conversions_number',
        ));
        $this->addColumn('conversions_number_variation', array(
            'header' => $this->__('<-Var.'),
            'index' => 'conversions_number_variation',
        ));

        $this->addColumn('conversions_amount', array(
            'header' => $this->__('Amt.'),
            'index' => 'conversions_amount',
        ));
        $this->addColumn('conversions_amount_variation', array(
            'header' => $this->__('<-Var.'),
            'index' => 'conversions_amount_variation',
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportXml', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($item) {
        return false;
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}
