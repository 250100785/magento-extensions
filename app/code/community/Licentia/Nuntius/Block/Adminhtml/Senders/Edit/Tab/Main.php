<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Senders_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry("current_sender");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("nuntius_form", array("legend" => $this->__("Sender Information")));


        $fieldset->addField("name", "text", array(
            "label" => $this->__("Name"),
            "class" => "required-entry",
            "required" => true,
            "name" => "name",
        ));

        $fieldset->addField("email", "text", array(
            "label" => $this->__("Email"),
            "class" => "required-entry validate-email",
            "required" => true,
            "name" => "email",
            "note" => $this->__('Use a email domain name associated with your SMTP server, otherwise your email will most likely be considered SPAM'),
        ));


        if ($current) {
            $form->setValues($current->getData());
        }
        return parent::_prepareForm();
    }

}
