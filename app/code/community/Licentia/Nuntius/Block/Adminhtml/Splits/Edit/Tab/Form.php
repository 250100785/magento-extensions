<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Splits_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry('current_split');
        $option = $this->getRequest()->getParam('option');

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("nuntius_form", array("legend" => $this->__("Campaign Information")));

        if (!$option && !$current->getId()) {

            $options = Licentia_Nuntius_Model_Splits::getTestingOptions();
            $options[''] = $this->__('Please Select');

            $location = $this->getUrl('*/*/*', array('_current' => true)) . 'option/';

            $fieldset->addField('testing', "select", array(
                "label" => $this->__("Test..."),
                "class" => "required-entry",
                "required" => true,
                "default" => '',
                'onchange' => "window.location='$location'+this.value",
                "options" => $options,
                "name" => "testing",
            ));
        } else {

            $fieldset->addField('testing', 'hidden', array('name' => 'testing', 'value' => $option));

            $fieldset->addField('name', "text", array(
                "label" => $this->__("Internal Name"),
                "class" => "required-entry",
                "required" => true,
                "name" => "name",
            ));

            $fieldset->addField("active", "select", array(
                "label" => $this->__("Active"),
                "class" => "required-entry",
                "required" => true,
                "values" => array('0' => $this->__('No'), '1' => $this->__('Yes')),
                "name" => "active",
            ));

            $fieldset->addField('list_id', 'select', array(
                'name' => 'list_id',
                'label' => $this->__('List'),
                'title' => $this->__('List'),
                'required' => true,
                'values' => Mage::getSingleton('nuntius/lists')->getAllOptions(),
            ));

            $outputFormat = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
            $fieldset->addField('deploy_at', 'date', array(
                'name' => 'deploy_at',
                'time' => true,
                'required' => true,
                'format' => $outputFormat,
                'label' => $this->__('Test Deploy Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif')
            ));


            $fieldset->addField('days', 'select', array(
                'name' => 'days',
                'label' => $this->__('Send General Campaign after X days'),
                'title' => $this->__('Send General Campaign after X days'),
                'required' => true,
                'options' => array_combine(range(1, 10), range(1, 10)),
            ));

            $fieldset->addField('percentage', 'select', array(
                'name' => 'percentage',
                'label' => $this->__('Percentage Emails Send Test'),
                'title' => $this->__('Percentage Emails Send Test'),
                'required' => true,
                'options' => array_combine(range(5, 40, 5), range(5, 40, 5)),
            ));


            $fieldset->addField('winner', 'select', array(
                'name' => 'winner',
                'label' => $this->__('How to determine winner'),
                'title' => $this->__('How to determine winner'),
                'required' => true,
                'options' => Mage::getModel('nuntius/splits')->getWinnerOptions(),
            ));

            if (Mage::helper('nuntius')->magna()) {
                $fieldset->addField('segments_ids', 'multiselect', array(
                    'name' => 'segments_ids[]',
                    'label' => $this->__('Subscribers Segment'),
                    'title' => $this->__('Subscribers Segment'),
                    'required' => true,
                    'values' => Mage::getSingleton('magna/segments')->getOptionArray(),
                ));
            }
        }

        if ($current->getData()) {
            $form->setValues($current->getData());
        }

        return parent::_prepareForm();
    }

}
