<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Subscribers extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        parent::__construct();
        $this->_controller = 'adminhtml_subscribers';
        $this->_blockGroup = 'nuntius';
        $this->_headerText = $this->__('Subscribers');
        $this->_addButtonLabel = $this->__('Add Subscriber');

        $location = $this->getUrl('*/*/import');
        $this->_addButton('import', array(
            "label" => $this->__("Import Subscribers"),
            "onclick" => "window.location='$location'",
                ), -100);
    }

}
