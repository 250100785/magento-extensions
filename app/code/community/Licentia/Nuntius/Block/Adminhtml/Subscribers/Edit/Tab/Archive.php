<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Subscribers_Edit_Tab_Archive extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('archive_grid');
        $this->setDefaultSort('archive_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        $current = Mage::registry('current_subscriber');

        $collection = Mage::getModel('nuntius/archive')
                ->getResourceCollection()
                ->addFieldToFilter('subscriber_id', $current->getId());

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('archive_id', array(
            'header' => $this->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'archive_id',
        ));

        $this->addColumn('campaign_id', array(
            'header' => $this->__('Campaign Name'),
            'align' => 'left',
            'index' => 'campaign_id',
            'type' => 'options',
            'options' => Mage::getModel('nuntius/campaigns')->toFormValues(),
        ));

        $this->addColumn('subject', array(
            'header' => $this->__('Subject'),
            'align' => 'left',
            'index' => 'subject',
        ));

        $this->addColumn('sender_name', array(
            'header' => $this->__('Sender name'),
            'align' => 'left',
            'index' => 'sender_name',
        ));

        $this->addColumn('sender_email', array(
            'header' => $this->__('Sender Email'),
            'align' => 'left',
            'index' => 'sender_email',
        ));

        $this->addColumn('sent_date', array(
            'header' => $this->__('Date'),
            'align' => 'left',
            'index' => 'sent_date',
            'width' => '170px',
            'type' => 'datetime',
        ));


        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return false;
    }

    public function getGridUrl() {
        return $this->getUrl('*/nuntius_subscribers/archivegrid', array('_current' => true));
    }

}
