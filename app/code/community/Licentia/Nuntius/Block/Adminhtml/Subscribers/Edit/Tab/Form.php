<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Subscribers_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $current = Mage::registry('current_subscriber');

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("nuntius_form", array("legend" => $this->__("Subscriber information")));


        $storeS = Mage::getSingleton('nuntius/lists')->getOptionArray();

        $fieldset->addField('list_id', 'select', array(
            'name' => 'list_id',
            'label' => $this->__('List'),
            'title' => $this->__('List'),
            'options' => $storeS,
            'required' => true,
        ));

        $fieldset->addField("store_id", "select", array(
            "label" => $this->__("Store View"),
            "values" => Mage::getModel('adminhtml/system_store')->getStoreValuesForForm(),
            "required" => true,
            "name" => "store_id",
        ));

        $fieldset->addField('email', "text", array(
            "label" => $this->__('Email'),
            "class" => "required-entry validate-email",
            "required" => true,
            "name" => 'email',
        ));

        $fieldset->addField('firstname', "text", array(
            "label" => $this->__('First Name'),
            "name" => 'firstname',
        ));

        $fieldset->addField('lastname', "text", array(
            "label" => $this->__('Last Name'),
            "name" => 'lastname',
        ));

        $fieldset->addField('status', "select", array(
            "label" => $this->__('Status'),
            "name" => 'status',
            "value" => '1',
            "options" => array('0' => $this->__('Unsubscribed'), 1 => $this->__('Active')),
        ));

        $form->addValues(array('list_id' => $this->getRequest()->getParam('list_id')));

        if ($current->getId()) {
            $form->setValues($current->getData());
        }

        return parent::_prepareForm();
    }

}
