<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Subscribers_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("nuntius_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle($this->__("Subscriber Information"));
    }

    protected function _beforeToHtml() {

        $subscriber = Mage::registry('current_subscriber');

        $this->addTab("form_section", array(
            "label" => $this->__("General"),
            "title" => $this->__("General"),
            "content" => $this->getLayout()->createBlock("nuntius/adminhtml_subscribers_edit_tab_form")->toHtml(),
        ));

        if ($subscriber->getId()) {
            $this->addTab("conversions_section", array(
                "label" => $this->__("Conversions"),
                "title" => $this->__("Conversions"),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/gridconv', array('_current' => true)),
            ));

            $this->addTab("archive_section", array(
                "label" => $this->__("Emails Sent"),
                "title" => $this->__("Emails Sent"),
                'class' => 'ajax',
                'url' => $this->getUrl('*/*/archivegrid', array('_current' => true)),
            ));
        }

        if ($this->getRequest()->getParam('tab_id')) {
            $this->setActiveTab($this->getRequest()->getParam('tab_id'));
        }

        return parent::_beforeToHtml();
    }

}
