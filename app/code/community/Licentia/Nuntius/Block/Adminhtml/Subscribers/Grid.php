<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Subscribers_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('campaign_grid');
        $this->setDefaultSort('subscriber_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('nuntius/subscribers')
                ->getResourceCollection();

        $list = Mage::registry('current_list');
        if ($list->getId() > 0) {
            $collection->addFieldToFilter('list_id', $list->getListId());
        }

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('subscriber_id', array(
            'header' => $this->__('ID'),
            'align' => 'right',
            'width' => '50px',
            'index' => 'subscriber_id',
        ));
        $this->addColumn('customer_id', array(
            'header' => $this->__('Customer'),
            'align' => 'center',
            'width' => '50px',
            'index' => 'customer_id',
            'frame_callback' => array($this, 'customerResult'),
            'is_system' => true,
        ));

        $list = Mage::registry('current_list');
        if (!$list->getId()) {
            $this->addColumn('list_id', array(
                'header' => $this->__('List Name'),
                'type' => 'options',
                'width' => '150px',
                'align' => 'left',
                'options' => Mage::getModel('nuntius/lists')->getOptionArray(),
                'index' => 'list_id',
            ));
        }

        $this->addColumn('subscriber_firstname', array(
            'header' => $this->__('Name'),
            'index' => array('firstname', 'lastname'),
            'type' => 'text',
            'renderer' => 'Licentia_Nuntius_Block_Adminhtml_Widget_Grid_Column_Renderer_Concat',
            'separator' => ' ',
            'filter_index' => "CONCAT( firstname, ' ',lastname)",
        ));

        $this->addColumn('email', array(
            'header' => $this->__('Email'),
            'align' => 'left',
            'index' => 'email',
        ));

        $this->addColumn('status', array(
            'header' => $this->__('Status'),
            'type' => 'options',
            'align' => 'left',
            'options' => array(0 => $this->__('Unsubscribed'), 1 => $this->__('Active')),
            'index' => 'status',
        ));

        $this->addColumn('sent', array(
            'header' => $this->__('Emails Sent'),
            'align' => 'left',
            'index' => 'sent',
            'type' => 'number',
            'width' => '40px',
        ));

        $this->addColumn('views', array(
            'header' => $this->__('Email Views'),
            'align' => 'left',
            'index' => 'views',
            'type' => 'number',
            'width' => '50px',
        ));

        $this->addColumn('bounces', array(
            'header' => $this->__('Bounces'),
            'align' => 'left',
            'index' => 'bounces',
            'type' => 'number',
            'width' => '50px',
        ));

        $this->addColumn('conversions_number', array(
            'header' => $this->__('Conversions'),
            'align' => 'left',
            'width' => '80px',
            'type' => 'number',
            'index' => 'conversions_number',
        ));

        $this->addColumn('conversions_amount', array(
            'header' => $this->__('Conv. Amount'),
            'align' => 'left',
            'width' => '80px',
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'conversions_amount',
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportXml', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('subscriber_id');
        $this->getMassactionBlock()->setFormFieldName('subscribers');

        $this->getMassactionBlock()->addItem('unsubscribe', array(
            'label' => $this->__('Unsubscribe'),
            'url' => $this->getUrl('*/*/massUnsubscribe'),
            'confirm' => $this->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('subscribe', array(
            'label' => $this->__('Subscribe'),
            'url' => $this->getUrl('*/*/massSubscribe'),
            'confirm' => $this->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
            'confirm' => $this->__('Are you sure?')
        ));

        return $this;
    }

    public function customerResult($value) {

        if ((int) $value > 0) {
            $url = $this->getUrl('/customer/edit', array('id' => $value));
            return'<a href="' . $url . '">' . $this->__('Yes') . '</a>';
        }

        return $this->__('No');
    }

    public function getGridUrl() {
        $list = Mage::registry('current_list');
        return $this->getUrl('*/*/grid', array('_current' => true, 'list_id' => $list->getId()));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
