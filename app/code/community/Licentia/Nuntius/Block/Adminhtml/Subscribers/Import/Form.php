<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Block_Adminhtml_Subscribers_Import_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $this->__('Import Subscribers')));


        $fieldset->addField('list_id', 'select', array(
            'name' => 'list_id',
            'label' => $this->__('List'),
            'title' => $this->__('List'),
            'required' => true,
            'values' => Mage::getSingleton('nuntius/lists')->getAllOptions(),
        ));


        $fieldset->addField('separator', 'text', array(
            'name' => 'separator',
            'value' => ';',
            'note' => 'Use \t for [TAB]',
            'label' => $this->__('Field Separator'),
            'title' => $this->__('Field Separator'),
            'required' => true,
                )
        );

        $fieldset->addField('filename', "file", array(
            "label" => $this->__('File to Import'),
            'required' => true,
            "name" => 'filename',
        ));

        $form->setAction($this->getUrl('*/*/importSave'));
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setEnctype('multipart/form-data');
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }

}
