<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *  


 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International 
 */
class Licentia_Nuntius_Block_Coupons extends Mage_Catalog_Block_Product_Abstract implements Mage_Widget_Block_Interface {

    protected function _toHtml() {

        $params = $this->getData();

        $coupon = Mage::getModel('nuntius/coupons')->getCoupon($params);

        $this->setData('coupon', $coupon);

        return $coupon;
    }

}
