<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Helper_Data extends Mage_Core_Helper_Abstract {

    public function magna() {
        return (bool) Mage::getConfig()->getModuleConfig('Licentia_Magna');
    }

    public function getContentFromUrl($campaign, $subscriber) {
        if ($campaign->getUrl()) {
            if (stripos($campaign->getUrl(), '{') !== false) {
                $url = str_replace(array('{campaignId}', '{subscriberId}'), array($campaign->getId(), $subscriber->getId()), $campaign->getUrl());
                $message = file_get_contents($url);
                if ($message === false) {
                    throw new Exception('Can not fetch URL');
                }
            } else {
                if (Mage::registry('nuntius_campaign_url_' . $campaign->getId())) {
                    $message = Mage::registry('nuntius_campaign_url_' . $campaign->getId());
                } else {
                    $message = file_get_contents($campaign->getUrl());
                    Mage::register('nuntius_campaign_url_' . $campaign->getId(), $message);
                    if ($message === false) {
                        throw new Exception('Can not fetch URL');
                    }
                }
            }
        } else {
            $message = false;
        }

        return $message;
    }

    public function getCustomerId() {
        $customerId = Mage::getSingleton('customer/session')->getId();

        if ($customerId) {
            return $customerId;
        }

        $sessionModel = Mage::helper('persistent/session')->getSession();
        if ($sessionModel->getCustomerId()) {
            return $sessionModel->getCustomerId();
        }

        if (Mage::registry('current_customer') && Mage::registry('current_customer')->getId()) {
            return Mage::registry('current_customer')->getId();
        }

        return false;
    }

    public function getCustomer() {
        $return = Mage::getModel('customer/customer')->load($this->getCustomerId());

        if ($return->getId()) {
            return $return;
        }

        return false;
    }

    public function getSubscribersForSegments($dataObject, $listId = false) {

        if (!$this->magna()) {
            return array();
        }

        if ($listId === false) {
            $listId = $dataObject->getListId();
        }

        $segments = explode(',', $dataObject->getSegmentsIds());
        $customers = array();
        foreach ($segments as $segment) {
            $load = Mage::getModel('magna/segments')->load($segment);
            if ($load->getId()) {
                $customers[] = $load->getMatchingCustomersIds();
            }
        }
        $finalCustomersArray = array();
        foreach ($customers as $sendC) {
            $finalCustomersArray = array_merge($finalCustomersArray, $sendC);
        }

        $finalCustomers = Mage::getModel('nuntius/subscribers')
                ->getSubscribersInfo('subscriber_id', array_unique($finalCustomersArray), $listId);

        return $finalCustomers;
    }

    public function getSmtpTransport() {

        $smtp = Mage::getStoreConfig('nuntius/config');
        $config = array('auth' => $smtp['auth'], 'port' => $smtp['port']);

        if ($smtp['ssl'] != 'none') {
            $config['ssl'] = $smtp['ssl'];
        }

        if (strlen(trim($smtp['server'])) == 0) {
            return false;
        }

        if ($smtp['auth'] != 'none') {
            $config['username'] = $smtp['username'];
            $config['password'] = $smtp['password'];
        }

        return new Zend_Mail_Transport_Smtp($smtp['server'], $config);
    }

    public function isEnabled() {
        return (bool) Mage::getStoreConfig('nuntius/transactional/enable');
    }

    public function transactionalCampaign() {
        return (bool) Mage::getStoreConfig('nuntius/transactional/campaign');
    }

}
