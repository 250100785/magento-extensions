<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Helper_Newsletter extends Mage_Newsletter_Helper_Data {

    /**
     * Retrieve unsubsription url
     *
     * @param licentia_Nuntius_model_subscriber $subscriber
     * @return string
     */
    public function getUnsubscribeUrl($subscriber) {

        $params = array(
            'id' => $subscriber->getId(),
            'code' => $subscriber->getCode(),
            '_nosid' => true
        );

        $campaign = Mage::registry('current_campaign');
        if ($campaign) {
            $params['c'] = $campaign->getId();
        }

        $url = Mage::getModel('core/url')
                ->setStore($subscriber->getStoreId())
                ->getUrl('nuntius/subscriber/unsubscribe', $params);

        return $url;
    }

}
