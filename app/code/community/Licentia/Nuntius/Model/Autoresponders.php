<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Autoresponders extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/autoresponders');
    }

    public function toOptionArray() {

        return array(
            'new_account' => Mage::helper('nuntius')->__('Account - New'),
            'campaign_open' => Mage::helper('nuntius')->__('Campaign - Open'),
            'campaign_click' => Mage::helper('nuntius')->__('Campaign - Clicked Any Campaign Link'),
            'campaign_link' => Mage::helper('nuntius')->__('Campaign - Clicked Specific Campaign Link'),
            'new_search' => Mage::helper('nuntius')->__('Search - New'),
            'order_new' => Mage::helper('nuntius')->__('Order - New Order'),
            'order_product' => Mage::helper('nuntius')->__('Order - Bought Specific Product'),
            'order_status' => Mage::helper('nuntius')->__('Order - Order Status Changes'),
            'new_tag' => Mage::helper('nuntius')->__('Product - New Tag'),
            'new_review' => Mage::helper('nuntius')->__('Product - New Review'),
        );
    }

    public function changeStatus($event) {

        $order = $event->getEvent()->getOrder();
        $newStatus = $order->getData('status');
        $olderStatus = $order->getOrigData('status');

        if ($newStatus == $olderStatus) {
            return;
        }

        $email = $order->getCustomerEmail();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'order_status')
                ->addFieldToFilter('order_status', $newStatus);

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            $subscriber = $this->loadSubscriber($autoresponder, $email);

            if (!$subscriber) {
                break;
            }

            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function newSearch($event) {

        $query = Mage::helper('catalogsearch')->getQueryText();

        $customer = Mage::helper('nuntius')->getCustomer();
        if (!$customer) {
            return;
        }

        $session = Mage::getSingleton('customer/session');
        if ($session->getNuntiusSearch() && $session->getNuntiusSearch() == $query) {
            return;
        } else {
            $session->setNuntiusSearch($query);
        }

        $email = $customer->getEmail();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_search');

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            $subscriber = $this->loadSubscriber($autoresponder, $email);

            if (!$subscriber) {
                break;
            }

            $search = explode(',', $autoresponder->getSearch());

            foreach ($search as $string) {

                if ($autoresponder->getSearchOption() == 'eq' && strtolower($query) == strtolower($string)) {
                    $this->_insertData($autoresponder, $subscriber);
                }

                if ($autoresponder->getSearchOption() == 'like' && stripos($query, $string) !== false) {
                    $this->_insertData($autoresponder, $subscriber);
                }
            }
        }
    }

    public function newCustomer($event) {

        $customer = $event->getEvent()->getCustomer();
        $email = $customer->getEmail();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_account');

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            $subscriber = $this->loadSubscriber($autoresponder, $email);

            if (!$subscriber) {
                break;
            }
            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function newTag($event) {

        $customer = Mage::helper('nuntius')->getCustomer();
        if (!$customer) {
            return;
        }

        $tag = $event->getObject();
        $email = $customer->getEmail();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_tag');

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            $subscriber = $this->loadSubscriber($autoresponder, $email);

            if (!$subscriber) {
                break;
            }

            $autoresponder->setDataObjectId($tag->getId());
            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function newReview($event) {

        $customer = Mage::helper('nuntius')->getCustomer();
        if (!$customer) {
            return;
        }

        $review = $event->getObject();

        $email = $customer->getEmail();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'new_review');

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            $subscriber = $this->loadSubscriber($autoresponder, $email);

            if (!$subscriber) {
                break;
            }

            $autoresponder->setDataObjectId($review->getId());
            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function newOrder($event) {

        $order = $event->getEvent()->getOrder();
        $email = $order->getCustomerEmail();

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', array('in' => array('order_product', 'order_new')));

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            if ($autoresponder->getEvent() == 'order_product') {
                $items = $order->getAllItems();
                $ok = false;
                foreach ($items as $item) {
                    if ($item->getProductId() == $autoresponder->getProduct()) {
                        $ok = true;
                        break;
                    }
                }
                if ($ok === false) {
                    break;
                }
            }

            $subscriber = $this->loadSubscriber($autoresponder, $email);

            if (!$subscriber) {
                break;
            }

            $autoresponder->setDataObjectId($order->getId());
            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function newView($subscriber, $campaign) {

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', 'campaign_open')
                ->addFieldToFilter('campaign_id', $campaign->getId());

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {
            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function newClick($subscriber, $campaign) {

        $autoresponders = $this->_getCollection()
                ->addFieldToFilter('event', array('in' => array('campaign_link', 'campaign_click')))
                ->addFieldToFilter('campaign_id', $campaign->getId());

        if ($autoresponders->count() == 0)
            return;

        foreach ($autoresponders as $autoresponder) {

            if ($autoresponder->getEvent() == 'campaign_link') {

                $linkOpen = Mage::registry('nuntius_open_url');

                $links = Mage::getModel('nuntius/links')
                        ->getCollection()
                        ->addFieldToFilter('link_id', $autoresponder->getLinkId());

                if ($links->count() != 1) {
                    break;
                }

                $link = $links->getFirstItem()->getLink();

                if (stripos($linkOpen, $link) === false) {
                    break;
                }
            }

            $this->_insertData($autoresponder, $subscriber);
        }
    }

    public function loadSubscriber($autoresponder, $email) {

        $subscribers = Mage::getModel('nuntius/subscribers')->getCollection()
                ->addFieldToFilter('list_id', $autoresponder->getListId())
                ->addFieldToFilter('email', $email);

        if ($subscribers->count() == 0) {
            $list = Mage::getModel('nuntius/lists')->getListForStore(Mage::app()->getStore()->getId());

            if (!$list->getId()) {
                return;
            }

            $data['list_id'] = $list->getListId();
            $data['email'] = $email;
            $data['active'] = 1;
            try {
                return Mage::getModel('nuntius/subscribers')->setData($data)->save();
            } catch (Exception $e) {
                Mage::logException($e);
                return false;
            }
        }

        $subscriber = false;
        if ($subscribers->count() == 1) {

            $subscriber = $subscribers->getFirstItem();

            if (Mage::helper('nuntius')->magna()) {
                $segmentsIds = explode(',', $autoresponder->getData('segments_ids'));

                if (count($segmentsIds) > 0) {

                    if ((int) $subscriber->getCustomerId() == 0) {
                        return false;
                    }

                    $exists = false;
                    foreach ($segmentsIds as $segment) {
                        $ok = Mage::getModel('magna/segments')
                                ->load($segment)
                                ->getMatchingCustomersIds($subscriber->getCustomerId());

                        if (count($ok) == 1) {
                            $exists = true;
                            break;
                        }
                    }

                    if ($exists == false) {
                        return false;
                    }
                }
            }
        }

        return $subscriber;
    }

    public function calculateSendDate($autoresponder) {
        if ($autoresponder->getSendMoment() == 'occurs') {
            $date = Mage::app()->getLocale()->date()
                    ->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);
        }

        if ($autoresponder->getSendMoment() == 'after') {
            $date = Mage::app()->getLocale()->date();

            if ($autoresponder->getAfterHours() > 0) {
                $date->addHour($autoresponder->getAfterHours());
            }
            if ($autoresponder->getAfterDays() > 0) {
                $date->addDay($autoresponder->getAfterDays());
            }
            $date->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);
        }

        return $date;
    }

    public function send() {
        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

        $emails = Mage::getModel('nuntius/events')->getCollection()
                ->addFieldToFilter('sent', 0)
                ->addFieldToFilter('send_at', array('lteq' => $date));

        foreach ($emails as $cron) {

            $autoresponder = Mage::getModel('nuntius/autoresponders')->load($cron->getAutoresponderId());
            $subscriber = Mage::getModel('nuntius/subscribers')->load($cron->getSubscriberId());

            if (!$autoresponder->getId() || !$subscriber->getId()) {
                $cron->setSent(1)->save();
                continue;
            }

            if ($subscriber->getCustomerId() && $autoresponder->getCancelIfOrder()) {
                $order = Mage::getModel('sales/order')->getCollection()
                        ->addAttributeToSelect('customer_id')
                        ->addAttributeToFilter('customer_id', $subscriber->getCustomerId())
                        ->addAttributeToFilter('created_at', array('from' => $cron->getCreatedAt(), 'to' => $date));

                $total = 0;
                if (in_array($autoresponder->getEvent(), array('order_product', 'order_new'))) {
                    $total = 1;
                }

                if ($order->count() > $total) {
                    $cron->delete();
                    continue;
                }
            }

            $data = array();
            $data['list_id'] = $subscriber->getListId();
            $data['subject'] = $autoresponder->getSubject();
            $data['internal_name'] = $autoresponder->getName();
            $data['deploy_at'] = $cron->getSendAt();
            $data['message'] = $autoresponder->getMessage();
            $data['sender_id'] = $autoresponder->getSenderId();
            $data['recurring'] = '0';
            $data['auto'] = '1';
            $data['autoresponder'] = $autoresponder->getId();
            $data['autoresponder_recipient'] = $subscriber->getEmail();
            $data['autoresponder_event'] = $autoresponder->getEvent();
            $data['autoresponder_event_id'] = $cron->getId();
            $data['sent'] = 1;

            $service = Mage::getModel('nuntius/service')->getService();
            $campaign = Mage::getModel('nuntius/campaigns')->setData($data)->save();

            $data = array();
            $data['campaign'] = $campaign;
            $data['subscribers'] = $subscriber->getId();

            $service->setData($data)->buildQueue();

            $cron->setSent(1)->setSentAt($date)->save();
        }
    }

    protected function _insertData($autoresponder, $subscriber) {

        if ($autoresponder->getSendOnce() == 1) {
            $exists = Mage::getModel('nuntius/events')->getCollection()
                    ->addFieldToFilter('autoresponder_id', $autoresponder->getId())
                    ->addFieldToFilter('subscriber_id', $subscriber->getId());

            if ($exists->count() != 0) {
                return;
            }
        }

        $data = array();
        $data['send_at'] = $this->calculateSendDate($autoresponder);
        $data['autoresponder_id'] = $autoresponder->getId();
        $data['customer_id'] = $subscriber->getCustomerId();
        $data['subscriber_id'] = $subscriber->getId();
        $data['subscriber_firstname'] = $subscriber->getFirstname();
        $data['subscriber_lastname'] = $subscriber->getLastname();
        $data['subscriber_email'] = $subscriber->getEmail();
        $data['event'] = $autoresponder->getEvent();
        $data['created_at'] = new Zend_Db_Expr('NOW()');
        $data['sent'] = 0;
        $data['data_object_id'] = $autoresponder->getDataObjectId();

        Mage::getModel('nuntius/events')->setData($data)->save();
        $autoresponder->setData('number_subscribers', $autoresponder->getData('number_subscribers') + 1)->save();
    }

    public function toFormValues() {
        $return = array();
        $collection = $this->getCollection()
                ->addFieldToSelect('name')
                ->addFieldToSelect('autoresponder_id')
                ->setOrder('name', 'ASC');
        foreach ($collection as $autoresponder) {
            $return[$autoresponder->getId()] = $autoresponder->getName() . ' (ID:' . $autoresponder->getId() . ')';
        }

        return $return;
    }

    protected function _getCollection() {

        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATE);
        //Version Compatability
        $return = $this->getCollection()->addFieldToFilter('active', 1);
        $return->getSelect()
                ->where(" from_date <=? or from_date IS NULL ", $date)
                ->where(" to_date >=? or to_date IS NULL ", $date);

        return $return;
    }

}
