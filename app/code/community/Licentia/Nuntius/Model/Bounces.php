<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Bounces extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/bounces');
    }

    public function processBounces() {

        $smtp = Mage::getStoreConfig('nuntius/bounces');

        $config = array('ssl' => $smtp['ssl']);
        $config['password'] = $smtp['password'];
        $config['host'] = $smtp['server'];
        $config['user'] = $smtp['email'];

        if (!filter_var($smtp['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }

        try {
            $mail = new Zend_Mail_Storage_Imap($config);
        } catch (Exception $e) {
            Mage::logException($e);
            return;
        }

        $i = 0;
        foreach ($mail as $idx => $message) {
            $i++;
            #$messageId = $mail->getUniqueId($idx);
            $content = $message->getContent();

            $s = null;
            $c = null;
            $code = null;

            preg_match('/Xnuntius-sid:\s?(\d+)/', $content, $s);
            preg_match('/Xnuntius-cid:\s?(\d+)/', $content, $c);

            preg_match('/Diagnostic-Code:( smtp; )?(.*)"/', $content, $code);

            $subscriberId = (int) end($s);
            $campaignId = (int) end($c);
            $reason = end($code);

            $subscriber = Mage::getModel('nuntius/subscribers')->load($subscriberId);
            $campaign = Mage::getModel('nuntius/campaigns')->load($campaignId);

            if (!$subscriber->getId() || !$campaign->getId()) {
                return;
            }

            $subscriber->setData('bounces', $subscriber->getData('bounces') + 1)->save();
            $campaign->setData('bounces', $subscriber->getData('bounces') + 1)->save();

            $data = array();
            $data['campaign_id'] = $campaignId;
            $data['subscriber_id'] = $subscriberId;
            $data['code'] = $reason;
            $data['content'] = $message->getContent();
            $data['created_at'] = now();

            Mage::getModel('nuntius/bounces')->setData($data)->save();

            $mail->removeMessage($i);
        }
    }

}
