<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Campaigns extends Mage_Core_Model_Abstract {

    const MYSQL_DATE = 'yyyy-MM-dd';
    const MYSQL_DATETIME = 'yyyy-MM-dd HH:mm:ss';

    /**
     * Init resource model and id field
     */
    protected function _construct() {
        parent::_construct();
        $this->_init('nuntius/campaigns', 'campaign_id');
    }

    /**
     * Returns a list of available cron options
     *
     * @return type
     */
    public static function getCronList() {

        $list = array(
            '0' => Mage::helper('nuntius')->__('No'),
            'd' => Mage::helper('nuntius')->__('Daily'),
            'w' => Mage::helper('nuntius')->__('Weekly'),
            'm' => Mage::helper('nuntius')->__('Monthly'),
            'y' => Mage::helper('nuntius')->__('Yearly'));

        return $list;
    }

    /**
     * Returns a list o list days
     * @return array
     */
    public static function getDaysList() {

        $lista = array(
            '0' => Mage::helper('nuntius')->__('Sunday'),
            '1' => Mage::helper('nuntius')->__('Monday'),
            '2' => Mage::helper('nuntius')->__('Tuesday'),
            '3' => Mage::helper('nuntius')->__('Wednesday'),
            '4' => Mage::helper('nuntius')->__('Thursday'),
            '5' => Mage::helper('nuntius')->__('Friday'),
            '6' => Mage::helper('nuntius')->__('Saturday'));

        $list = array();
        foreach ($lista as $key => $value) {
            $list[] = array('value' => $key, 'label' => $value);
        }


        return $list;
    }

    /**
     * returns month list
     * @return array
     */
    public static function getMonthsList() {

        $list = array(
            '1' => Mage::helper('nuntius')->__('January'),
            '2' => Mage::helper('nuntius')->__('February'),
            '3' => Mage::helper('nuntius')->__('March'),
            '4' => Mage::helper('nuntius')->__('April'),
            '5' => Mage::helper('nuntius')->__('May'),
            '6' => Mage::helper('nuntius')->__('June'),
            '7' => Mage::helper('nuntius')->__('July'),
            '8' => Mage::helper('nuntius')->__('August'),
            '9' => Mage::helper('nuntius')->__('September'),
            '10' => Mage::helper('nuntius')->__('October'),
            '11' => Mage::helper('nuntius')->__('November'),
            '12' => Mage::helper('nuntius')->__('December'));

        return $list;
    }

    /**
     * Returns a list of hours
     * @return array
     */
    public static function getRunAroundList() {

        $return = array();

        for ($i = 0; $i <= 23; $i++) {
            $return[] = array('value' => $i, 'label' => str_pad($i, 2, '0', STR_PAD_LEFT) . ':00');
        }

        return $return;
    }

    /**
     * Retuns a list of possible days and expressions available to send a campaign.
     * Such expressions include Last day of the month, first Mondat, etcet
     * @return array
     */
    public static function getDaysMonthsList() {

        $list = array();

        for ($i = 1; $i <= 31; $i++) {
            $list[] = array('label' => $i, 'value' => $i);
        }


        $final = array();
        $final[] = array('label' => Mage::helper('nuntius')->__('Specific Day'), 'value' => $list);

        $days = self::getDaysList();

        $din = array();
        for ($i = 1; $i <= 4; $i++) {
            foreach ($days as $day) {
                $din[] = array('value' => $i . '-' . $day['value'], 'label' => Mage::helper('nuntius')->__('On the %s %s of the month', $i, $day['label']));
            }
        }

        foreach ($days as $day) {
            $din[] = array('value' => '|' . $day['value'], 'label' => Mage::helper('nuntius')->__('On the last %s of the month', $day['label']));
        }

        $din[] = array('value' => 'u-u', 'label' => Mage::helper('nuntius')->__('Last Day of the Month '));

        $final[] = array('label' => Mage::helper('nuntius')->__('Dynamic Day'), 'value' => $din);

        return $final;
    }

    /**
     * Send email campaign
     * @param Licentia_Nuntius_Model_Campaigns $campaign
     * @return boolean
     */
    protected function _queueEmail($campaign) {

        $segments = explode(',', $campaign->getSegmentsIds());

        $list = Mage::getModel('nuntius/lists')->load($campaign->getListId());
        Mage::unregister('current_list');
        Mage::register('current_list', $list);

        $service = Mage::getModel('nuntius/service')->getService();

        if ($segments[0] != '0') {

            $finalCustomers = Mage::helper('nuntius')->getSubscribersForSegments($campaign);

            if ($campaign->getRecurringUnique() == 1) {
                foreach ($finalCustomers as $key => $subscriberId) {
                    $unique = Mage::getModel('nuntius/history')->getCollection()
                            ->addFieldToSelect('campaign_id')
                            ->addFieldToFilter('campaign_id', $campaign->getParentId())
                            ->addFieldToFilter('subscriber_id', $subscriberId);
                    if ($unique->count() != 0) {
                        unset($finalCustomers[$key]);
                    }
                }
            }

            $data = array();
            $data['campaign'] = $campaign;
            $data['subscribers'] = $finalCustomers;

            try {
                Mage::dispatchEvent('nuntius_campaign_send_before', array('campaign' => $campaign, 'server_data' => $data));
                $result = $service->setData($data)->buildQueue();
                if ($result->getData('id')) {
                    Mage::dispatchEvent('nuntius_campaign_send_after', array('campaign' => $campaign, 'server_data' => $data));
                }
            } catch (Exception $e) {
                Mage::logException($e);
            }
        } else {
            $data = array();
            $data['campaign'] = $campaign;
            try {
                Mage::dispatchEvent('nuntius_campaign_send_before', array('campaign' => $campaign, 'server_data' => $data));

                $result = $service->setData($data)->buildQueue();
                if ($result->getData('id')) {
                    Mage::dispatchEvent('nuntius_campaign_send_after', array('campaign' => $campaign, 'server_data' => $data));
                }
            } catch (Exception $e) {
                Mage::logException($e);
                $result = array();
            }
        }

        return $result;
    }

    /**
     * Send campaign from CRON
     */
    public function queueCampaigns() {
        $date = Mage::app()->getLocale()->date();
        $now = $date->get(self::MYSQL_DATETIME);

        //Non-Recurring Campaigns
        $collection = Mage::getModel('nuntius/campaigns')
                ->getCollection()
                ->addFieldToFilter('status', 'standby')
                ->addFieldToFilter('deploy_at', array('lteq' => $now))
                ->addFieldToFilter('recurring', '0');

        foreach ($collection as $campaign) {
            Mage::unregister('current_campaign');
            Mage::register('current_campaign', $campaign);
            $this->_queueEmail($campaign);
        }

        //Recurring Campaigns
        $collectionRecurring = Mage::getModel('nuntius/campaigns')
                ->getCollection()
                ->addFieldToFilter('status', 'standby')
                ->addFieldToFilter('recurring_next_run', array('lteq' => $now))
                ->addFieldToFilter('recurring', array('neq' => '0'));

        foreach ($collectionRecurring as $campaign) {
            Mage::unregister('current_campaign');
            Mage::register('current_campaign', $campaign);

            $newCampaignData = $campaign->getData();
            unset($newCampaignData['campaign_id']);
            $newCampaignData['nex_run'] = new Zend_Db_Expr('NULL');
            $newCampaignData['recurring'] = 0;
            $newCampaignData['parent_id'] = $campaign->getId();
            $newCampaignData['auto'] = '1';
            $newCampaignData['internal_name'] = $newCampaignData['internal_name'] . ' [AUTO]';

            $newCampaignData['clicks'] = 0;
            $newCampaignData['unique_clicks'] = 0;
            $newCampaignData['views'] = 0;
            $newCampaignData['unique_views'] = 0;
            $newCampaignData['unsent'] = 0;
            $newCampaignData['sent'] = 0;
            $newCampaignData['bounces'] = 0;
            $newCampaignData['unsubscribes'] = 0;

            $newCampaign = Mage::getModel('nuntius/campaigns')->setData($newCampaignData)->save();
            $this->_queueEmail($newCampaign);

            $campaignFinal = Mage::getModel('nuntius/campaigns')->load($campaign->getId());
            $campaignFinal->updateCampaignAfterSend($campaignFinal);
        }
    }

    /**
     * Builds next send date for recurring campaigns
     * @param type $campaignData
     * @return type
     */
    public function getNextRecurringDate($campaignData) {


        if (isset($campaignData['recurring_last_run'])) {
            $now = Mage::app()
                    ->getLocale()
                    ->date()
                    ->setDate($campaignData['recurring_last_run'], self::MYSQL_DATE)
                    ->setTime($campaignData['recurring_last_run'], self::MYSQL_DATETIME);
        } else {
            $now = Mage::app()->getLocale()->date();
        }


        if ($campaignData['recurring'] == '0')
            return $campaignData['deploy_at'];

        $campaignData['recurring_time'] = str_pad($campaignData['recurring_time'], 2, '0', STR_PAD_LEFT);


        if (!isset($campaignData['recurring_first_run']) || strlen($campaignData['recurring_first_run']) == 0) {
            $campaignData['recurring_first_run'] = $now->get(self::MYSQL_DATE);
        }

        if (isset($campaignData['run_until'])) {
            $dateStart = Mage::app()->getLocale()->date()->setDate($campaignData['recurring_first_run'], self::MYSQL_DATE)->setHour(0)->setMinute(0)->setSecond(0);
        } else {
            $dateStart = $now;
        }

        $today = $dateStart->get(Zend_Date::WEEKDAY_DIGIT);

        switch ($campaignData['recurring']) {
            case 'd':
            case 'w':
                $oldDay = null;
                $days = explode(',', $campaignData['recurring_daily']);

                if ($campaignData['recurring'] == 'w') {
                    $days = explode(',', $campaignData['recurring_day']);
                }


                if (count($days) > 1) {
                    $index = array_search($today, $days);

                    if ($index === false) {

                        foreach ($days as $key => $day) {
                            if (!isset($oldDay)) {
                                $oldDay = $key;
                            }
                            if ($day > $today) {
                                $index = $oldDay;
                                break;
                            }

                            $oldDay = $key;
                        }

                        if ($index === false)
                            $index = $days[0];
                    }

                    if (isset($days[$index])) {
                        $nextDay = $days[$index];
                    } else {
                        $nextDay = reset($days);
                    }

                    $nextDay = $nextDay - $today;
                } else {

                    if ($today == 0) {
                        $nextDay = $days[0];
                    } else {
                        $nextDay = abs(7 - $today + $days[0]);
                    }

                    if ($nextDay == 7)
                        $nextDay = 0;
                }

                if ($nextDay < 0) {
                    $nextDay = $nextDay + 6;
                }

                $run = $dateStart->setMinute(0)
                        ->setSecond(0)
                        ->setHour($campaignData['recurring_time'])
                        ->addDay($nextDay);


                if ($now->get(self::MYSQL_DATETIME) >= $run->get(self::MYSQL_DATETIME)) {

                    if ($campaignData['recurring'] == 'w') {
                        $run = $now->setMinute(0)
                                ->setSecond(0)
                                ->setHour($campaignData['recurring_time'])
                                ->addWeek(1);
                    } else {
                        $run = $now->setMinute(0)
                                ->setSecond(0)
                                ->setHour($campaignData['recurring_time'])
                                ->addDay(1);
                    }
                }


                $nextDate = $run->get(self::MYSQL_DATETIME);

                break;

            case 'm':

                $nextDateTemp = $this->calculateRecurringMonth($dateStart, $campaignData);
                $nextDate = $nextDateTemp->get(self::MYSQL_DATETIME);

                break;
            case 'y':
                $dateStart->setMinute(0)
                        ->setSecond(0)
                        ->setHour($campaignData['recurring_time'])
                        ->setMonth($campaignData['recurring_month']);
                $day = $this->calculateRecurringMonth($dateStart, $campaignData)->get(Zend_Date::DAY);
                $run = $dateStart->setDay($day);

                if ($now->get(self::MYSQL_DATETIME) >= $run->get(self::MYSQL_DATETIME)) {

                    $run->setMinute(0)
                            ->setSecond(0)
                            ->setHour($campaignData['recurring_time'])
                            ->setMonth($campaignData['recurring_month']);
                    $run->addYear(1);

                    $day = $this->calculateRecurringMonth($run, $campaignData)->get(Zend_Date::DAY);
                    $run = $run->setDay($day);
                }

                $nextDate = $run->get(self::MYSQL_DATETIME);

                break;
        }


        return $nextDate;
    }

    /**
     * Builds next month to send recurring campaign
     * @param type $dateStart
     * @param type $campaignData
     * @param type $monthsToAdd
     * @return type
     */
    public function calculateRecurringMonth($dateStart, $campaignData, $monthsToAdd = null) {

        $now = Mage::app()->getLocale()->date();

        if (strpos($campaignData['recurring_monthly'], '|') !== false) {

            $tDate = $dateStart->setMinute(0)
                    ->setSecond(0)
                    ->setHour($campaignData['recurring_time']);

            $lastDay = cal_days_in_month(CAL_GREGORIAN, $tDate->get('MM'), $tDate->get('yyyy'));

            $calcDay = trim($campaignData['recurring_monthly'], '|');

            if ($monthsToAdd) {
                $tDate->addMonth($monthsToAdd);
            }

            $testDate = clone $dateStart;
            $testDate->setDay($lastDay);

            for ($i = $lastDay; $i >= $lastDay - 7; $i--) {
                $dayN = $testDate->get(Zend_Date::WEEKDAY_DIGIT);

                if ($dayN == $calcDay) {
                    $finalDay = $testDate->get(Zend_Date::DAY);
                    break;
                }
                $testDate->subDay(1);
            }

            $run = $dateStart->setMinute(0)
                    ->setSecond(0)
                    ->setHour($campaignData['recurring_time'])
                    ->setDay($finalDay);

            if ($now->get(self::MYSQL_DATETIME) > $run->get(self::MYSQL_DATETIME)) {
                $run = $this->calculateRecurringMonth($dateStart, $campaignData, 1);
            }
        } elseif ($campaignData['recurring_monthly'] == 'u-u') {

            $tDate = $dateStart->setMinute(0)
                    ->setSecond(0)
                    ->setHour($campaignData['recurring_time']);
            $lastDay = cal_days_in_month(CAL_GREGORIAN, $tDate->get('MM'), $tDate->get('yyyy'));
            $run = $dateStart->setDay($lastDay);
        } elseif (strpos($campaignData['recurring_monthly'], '-') !== false) {

            $calcDay = explode('-', $campaignData['recurring_monthly']);

            $tDate = $dateStart->setMinute(0)
                    ->setSecond(0)
                    ->setHour($campaignData['recurring_time']);

            if ($monthsToAdd) {
                $tDate->addMonth($monthsToAdd);
            }

            $testDate = clone $dateStart;
            $testDate->setDay(1);

            for ($i = 0; $i <= 6; $i++) {
                $dayN = $testDate->get(Zend_Date::WEEKDAY_DIGIT);

                if ($dayN == $calcDay[1]) {
                    $day = $testDate->get(Zend_Date::DAY);
                    break;
                }

                $testDate->addDay(1);
            }

            if ($calcDay[0] > 1) {
                $finalDay = $day + (($calcDay[0] - 1) * 7);
            } else {
                $finalDay = $day;
            }

            $run = $dateStart->setMinute(0)
                    ->setSecond(0)
                    ->setHour($campaignData['recurring_time'])
                    ->setDay($finalDay);

            if ($now->get(self::MYSQL_DATETIME) > $run->get(self::MYSQL_DATETIME)) {
                $run = $this->calculateRecurringMonth($dateStart, $campaignData, 1);
            }
        } else {
            $run = $dateStart->setMinute(0)
                    ->setSecond(0)
                    ->setHour($campaignData['recurring_time'])
                    ->setDay($campaignData['recurring_monthly']);

            if ($now->get(self::MYSQL_DATETIME) > $run->get(self::MYSQL_DATETIME)) {
                $dateStart->addMonth(1)
                        ->setMinute(0)
                        ->setSecond(0)
                        ->setHour($campaignData['recurring_time'])
                        ->setDay($campaignData['recurring_monthly']);
            }
        }

        return $run;
    }

    /**
     * Saves campaign
     * @return $this
     */
    public function save() {

        $this->setData('recurring_next_run', $this->getNextRecurringDate($this->getData()));

        $this->findLinksForCampaign($this);

        $this->setData('run_times_left', $this->getData('run_times') - $this->getData('run_times_left'));

        if ($this->getId()) {
            Mage::getModel('nuntius/followup')->updateSendDate($this);
        }

        return parent::save();
    }

    /**
     * Processes somo "meta data" for campaigns
     * @param Licentia_Nuntius_Model_Campaigns $campaign
     */
    public function updateCampaignAfterSend($campaign) {

        if ($campaign->getData('status') == 'running') {
            return;
        }

        $data = $campaign->getData();

        $now = Mage::app()->getLocale()->date();

        $data['recurring_last_run'] = $now->get(self::MYSQL_DATETIME);

        if ($data['recurring'] != '0') {

            $data['status'] = 'standby';

            $finishRun = false;
            if ($data['run_times_left'] <= 1 && $data['end_method'] != 'run_until') {
                $finishRun = true;
            }

            $data['run_times_left'] = new Zend_Db_Expr('run_times_left - 1');

            $finishDate = false;
            if ($now->get(self::MYSQL_DATE) > $data['run_until'] || $data['recurring_next_run'] > $data['run_until']) {
                $finishDate = true;
            }

            if (($data['end_method'] == 'run_until' && $finishDate) ||
                    ($data['end_method'] == 'any' && ($finishDate || $finishRun)) ||
                    ($data['end_method'] == 'number' && $finishRun) ||
                    ($data['end_method'] == 'both' && $finishDate && $finishRun)) {

                $data['status'] = 'finished';
            }
        } else {
            $data['status'] = 'finished';
        }

        $campaign->setData($data)->save();
    }

    /**
     * Returns a list of campaigns IDS and internal name
     * @return type
     */
    public function toFormValues() {
        $return = array();
        $collection = $this->getCollection()
                ->addFieldToSelect('internal_name')
                ->addFieldToSelect('campaign_id')
                ->addFieldToFilter('auto', 0)
                ->setOrder('internal_name', 'ASC');
        foreach ($collection as $campaign) {
            $return[$campaign->getId()] = $campaign->getInternalName() . ' (ID:' . $campaign->getId() . ')';
        }

        return $return;
    }

    /**
     * Returns a list of campaigns IDS and internal name
     * @return type
     */
    public function toFormValuesNonAuto() {
        $return = array();
        $collection = $this->getCollection()
                ->addFieldToFilter('auto', 0)
                ->addFieldToSelect('internal_name')
                ->addFieldToSelect('campaign_id')
                ->setOrder('internal_name', 'ASC');
        foreach ($collection as $campaign) {
            $return[$campaign->getId()] = $campaign->getInternalName() . ' (ID:' . $campaign->getId() . ')';
        }

        return $return;
    }

    /**
     *
     * @param Licentia_Nuntius_Model_Campaigns $campaign
     * @return Licentia_Nuntius_Model_Campaigns
     */
    public function findLinksForCampaign($campaign) {

        if (!$campaign->getId()) {
            return;
        }

        $message = Mage::helper('newsletter')->getTemplateProcessor()->filter($campaign->getMessage());

        $data = array();
        $data['campaign_id'] = $campaign->getId();

        $links = Mage::getModel('nuntius/links')
                ->getCollection()
                ->addFieldToFilter('campaign_id', $campaign->getId());

        $exists = array();
        $temp = array();
        foreach ($links as $link) {
            $exists[$link->getId()] = $link->getLink();
            #$link->delete();
        }

        $doc = new DOMDocument();
        $doc->loadHTML($message);
        foreach ($doc->getElementsByTagName('a') as $link) {

            $data['link'] = $link->getAttribute('href');
            $data['campaign_id'] = $campaign->getId();

            if (!filter_var($data['link'], FILTER_VALIDATE_URL)) {
                continue;
            }

            if (in_array($data['link'], $exists)) {
                $temp[] = $data['link'];
                continue;
            }

            $result = Mage::getModel('nuntius/links')->setData($data)->save();

            $temp[] = $result->getLink();
        }

        $links = Mage::getModel('nuntius/links')
                ->getCollection()
                ->addFieldToFilter('campaign_id', $campaign->getId());

        foreach ($links as $link) {

            if (!in_array($link->getLink(), $temp)) {
                $link->delete();
            }
        }

        return $this;
    }

    public function tryAgain($records) {

        $result = Mage::getModel('nuntius/service')->getService()->sendEmail(true, $records);
        foreach ($result->getData('messages') as $message) {
            $messages[] = $message;
        }

        return $messages;
    }

    public function sendEmails() {
        return Mage::getModel('nuntius/service')->getService()->sendEmail();
    }

}
