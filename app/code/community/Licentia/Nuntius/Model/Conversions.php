<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Conversions extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/conversions');
    }

    public function convertOrder($event) {

        $order = $event->getEvent()->getOrder();

        $orderId = $order->getId();

        $convTemp = Mage::getModel('nuntius/conversionstmp')
                ->getCollection()
                ->addFieldToFilter('order_id', $orderId);

        if ($convTemp->count() != 1) {
            return;
        }

        $conversion = $convTemp->getFirstItem();

        $campaign = Mage::getModel('nuntius/campaigns')->load($conversion->getCampaignId());
        $subscriber = Mage::getModel('nuntius/subscribers')->load($conversion->getSubscriberId());
        $link = Mage::getModel('nuntius/subscribers')->load($conversion->getLinkId());

        $conversion->delete();

        if (!$campaign->getId() || !$subscriber->getId()) {
            return false;
        }

        $data = array();
        $data['campaign_id'] = $campaign->getId();
        $data['campaign_name'] = $campaign->getInternalName();
        $data['list_id'] = $campaign->getListId();
        $data['subscriber_id'] = $subscriber->getId();
        $data['subscriber_email'] = $subscriber->getEmail();
        $data['subscriber_firstname'] = $subscriber->getFirstname();
        $data['subscriber_lastname'] = $subscriber->getLastname();
        $data['order_date'] = $order->getCreatedAt();
        $data['order_id'] = $order->getId();
        $data['order_amount'] = $order->getBaseGrandTotal();
        $data['customer_id'] = $order->getCustomerId();
        $data['link_id'] = $link->getId();
        $data['created_at'] = now();

        $this->setData($data)->save();

        $list = Mage::getModel('nuntius/lists')->load($campaign->getListId());

        if ($list->getId()) {
            $list->setData('conversions_number', $list->getData('conversions_number') + 1);
            $list->setData('conversions_amount', $list->getData('conversions_amount') + $data['order_amount']);
            $list->setData('conversions_average', round($list->getData('conversions_amount') / $list->getData('conversions_number'), 2));
            $list->save();
        }

        $campaign->setData('conversions_number', $campaign->getData('conversions_number') + 1);
        $campaign->setData('conversions_amount', $campaign->getData('conversions_amount') + $data['order_amount']);
        $campaign->setData('conversions_average', round($campaign->getData('conversions_amount') / $campaign->getData('conversions_number'), 2));
        $campaign->save();

        if ($campaign->getParentId()) {
            $parent = Mage::getModel('nuntius/campaigns')->load($campaign->getParentId());

            if ($parent->getId()) {
                $parent->setData('conversions_number', $parent->getData('conversions_number') + 1);
                $parent->setData('conversions_amount', $parent->getData('conversions_amount') + $data['order_amount']);
                $parent->setData('conversions_average', round($parent->getData('conversions_amount') / $parent->getData('conversions_number'), 2));
                $parent->save();
            }
        }

        if ($campaign->getSplitId()) {
            $split = Mage::getModel('nuntius/splits')->load($campaign->getSplitId());
            if ($split->getId()) {
                $split->setData('conversions_' . $campaign->getSplitVersion(), $split->getData('conversions_' . $campaign->getSplitVersion()) + 1);
                $split->save();
            }
        }

        $subscriber->setData('conversions_number', $subscriber->getData('conversions_number') + 1);
        $subscriber->setData('conversions_amount', $subscriber->getData('conversions_amount') + $data['order_amount']);
        $subscriber->setData('conversions_average', round($subscriber->getData('conversions_amount') / $subscriber->getData('conversions_number'), 2));
        $subscriber->save();

        if ($link->getId()) {
            $link->setData('conversions_number', $link->getData('conversions_number') + 1);
            $link->setData('conversions_amount', $link->getData('conversions_amount') + $data['order_amount']);
            $link->setData('conversions_average', round($link->getData('conversions_amount') / $link->getData('conversions_number'), 2));
            $link->save();
        }

        return true;
    }

}
