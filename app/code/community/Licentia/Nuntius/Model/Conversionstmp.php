<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Conversionstmp extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('nuntius/conversionstmp');
    }

    public function afterOrder($event) {
        $session = Mage::getSingleton('customer/session');

        if (!$session->getNuntiusConversion()) {
            return false;
        }

        $campaign = Mage::getModel('nuntius/campaigns')->load($session->getNuntiusConversionCampaign());
        $subscriber = Mage::getModel('nuntius/subscribers')->load($session->getNuntiusConversionSubscriber());

        if (!$campaign->getId() || !$subscriber->getId()) {
            return false;
        }

        $cLinks = Mage::getModel('nuntius/links')->getCollection()
                ->addFieldToFilter('campaign_id', $campaign->getId())
                ->addFieldToFilter('link', $session->getNuntiusConversionUrl());

        if ($cLinks->count() == 1) {
            $link = $cLinks->getFirstItem();
        } else {
            $link = new Varien_Object;
        }

        $order = $event->getEvent()->getOrder();

        $data = array();
        $data['campaign_id'] = $campaign->getId();
        $data['subscriber_id'] = $subscriber->getId();
        $data['order_id'] = $order->getId();
        $data['link_id'] = $link->getId();

        $this->setData($data)->save();

    }

}
