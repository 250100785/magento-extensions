<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Email extends Mage_Core_Model_Email {

    public function send() {

        if (!Mage::helper('nuntius')->isEnabled()) {
            return parent::send();
        }

        if (Mage::getStoreConfigFlag('system/smtp/disable')) {
            return $this;
        }

        $mail = new Zend_Mail();

        if (strtolower($this->getType()) == 'html') {
            $mail->setBodyHtml($this->getBody());
        } else {
            $mail->setBodyText($this->getBody());
        }

        $transport = Mage::helper('nuntius')->getSmtpTransport();

        $mail->setFrom($this->getFromEmail(), $this->getFromName())
                ->addTo($this->getToEmail(), $this->getToName())
                ->setSubject($this->getSubject())
                ->setReplyTo($this->getSenderEmail(), $this->getSenderName());

        $mail->send($transport);

        return $this;
    }

}
