<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Followup extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('nuntius/followup');
    }

    public function getOptionValues() {

        $options = array();
        $options[] = array('value' => 'no_open', 'label' => Mage::helper('nuntius')->__("Didn't Open the Message"));
        $options[] = array('value' => 'open', 'label' => Mage::helper('nuntius')->__("Open the Message"));
        $options[] = array('value' => 'no_click', 'label' => Mage::helper('nuntius')->__("Didn't click the Message"));
        $options[] = array('value' => 'click', 'label' => Mage::helper('nuntius')->__("Click the Message"));
        $options[] = array('value' => 'no_conversion', 'label' => Mage::helper('nuntius')->__("No Conversion"));
        $options[] = array('value' => 'conversion', 'label' => Mage::helper('nuntius')->__("Converted"));

        return $options;
    }

    public function cron() {

        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

        $followups = Mage::getModel('nuntius/followup')
                ->getCollection()
                ->addFieldToFilter('send_at', array('lteq' => $date))
                ->addFieldToFilter('sent', 0)
                ->addFieldToFilter('active', 1);

        foreach ($followups as $followup) {
            $campaign = Mage::getModel('nuntius/campaigns')->load($followup->getCampaignId());

            $recipients = $this->buildRecipients($followup, $campaign);

            $subscribers = Mage::getModel('nuntius/subscribers')
                    ->getCollection();

            if (count($recipients['include']) > 0) {
                $subscribers->addFieldToFilter('subscriber_id', array('in' => $recipients['include']));
            }
            if (count($recipients['exclude']) > 0) {
                $subscribers->addFieldToFilter('subscriber_id', array('nin' => $recipients['exclude']));
            }

            $subs = Mage::helper('nuntius')->getSubscribersForSegments($followup, $campaign->getListId());
            if ($subs) {
                $subscribers->addFieldToFilter('subscriber_id', array('in' => $subs));
            }

            $final = $subscribers->getAllIds();
            $followup->setRecipients(implode(',', $final))->save();

            $data = array();
            $data['list_id'] = $campaign->getListId();
            $data['subject'] = str_replace('{{subject}}', $campaign->getSubject(), $followup->getSubject());
            $data['internal_name'] = '[RM]' . $campaign->getInternalName();
            $data['deploy_at'] = $followup->getSendAt();
            $data['message'] = str_replace("{{message}}", $campaign->getMessage(), $followup->getMessage());
            $data['sender_id'] = $campaign->getSenderId();
            $data['recurring'] = '0';
            $data['auto'] = '1';
            $data['channel'] = 'email';
            $data['followup_id'] = $followup->getId();

            $service = Mage::getModel('nuntius/service')->getService();
            $newCampaign = Mage::getModel('nuntius/campaigns')->setData($data)->save();

            $data = array();
            $data['campaign'] = $newCampaign;
            $data['subscribers'] = $followup->getRecipients();

            $service->setData($data)->buildQueue();

            $followup->setSent(1)->save();
        }
    }

    public function buildRecipients($followup, $campaign) {

        $include = array();
        $exclude = array();

        $recipients = explode(',', $followup->getRecipientsOptions());

        if (
                (in_array('no_open', $recipients) && in_array('open', $recipients)) ||
                (in_array('no_click', $recipients) && in_array('click', $recipients)) ||
                (in_array('conversion', $recipients) && in_array('no_conversion', $recipients))
        ) {
            $include = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToFilter('list_id', $campaign->getListId())
                    ->getAllIds();
            return array('include' => $include, 'exclude' => $exclude);
        }

        if (in_array('open', $recipients)) {
            $tmp = Mage::getModel('nuntius/stats')
                    ->getCollection()
                    ->addFieldToFilter('type', 'views')
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->getAllIds('subscriber_id');

            $include = array_merge($include, $tmp);
        }

        if (in_array('no_open', $recipients)) {
            $tmp = Mage::getModel('nuntius/stats')
                    ->getCollection()
                    ->addFieldToFilter('type', 'views')
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->getAllIds('subscriber_id');

            $exclude = array_merge($exclude, $tmp);
        }

        if (in_array('click', $recipients)) {
            $tmp = Mage::getModel('nuntius/stats')
                    ->getCollection()
                    ->addFieldToFilter('type', 'clicks')
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->getAllIds('subscriber_id');

            $include = array_merge($include, $tmp);
        }

        if (in_array('click', $recipients)) {
            $tmp = Mage::getModel('nuntius/stats')
                    ->getCollection()
                    ->addFieldToFilter('type', 'clicks')
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->getAllIds('subscriber_id');
            $exclude = array_merge($exclude, $tmp);
        }

        if (in_array('conversion', $recipients)) {
            $tmp = Mage::getModel('nuntius/conversions')
                    ->getCollection()
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->getAllIds('subscriber_id');

            $include = array_merge($include, $tmp);
        }

        if (in_array('click', $recipients)) {
            $tmp = Mage::getModel('nuntius/conversions')
                    ->getCollection()
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->getAllIds('subscriber_id');
            $exclude = array_merge($exclude, $tmp);
        }

        return array('include' => $include, 'exclude' => $exclude);
    }

    public function updateSendDate($campaign) {

        $collection = $this->getCollection()
                ->addFieldToFilter('campaign_id', $campaign->getId());

        foreach ($collection as $item) {

            $date = Mage::app()
                    ->getLocale()
                    ->date()
                    ->setTime($campaign->getData('deploy_at'), Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME)
                    ->setDate($campaign->getData('deploy_at'), Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME)
                    ->addDay($item->getData('days'));

            $item->setData('send_at', $date->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME))
                    ->save();
        }
    }

}
