<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Goals extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('nuntius/goals');
    }

    public function getGoalTypes() {
        $info = array(
            'global_conversions' => Mage::helper('nuntius')->__('Conversions - Global'),
            'campaign_conversions' => Mage::helper('nuntius')->__('Conversions - Campaign'),
            'list_conversions' => Mage::helper('nuntius')->__('Conversions - List'),
            'global_views' => Mage::helper('nuntius')->__('Views - Global'),
            'campaign_views' => Mage::helper('nuntius')->__('Views - Campaign'),
            'list_views' => Mage::helper('nuntius')->__('Views - List'),
            'global_clicks' => Mage::helper('nuntius')->__('Clicks - Global'),
            'campaign_clicks' => Mage::helper('nuntius')->__('Clicks - Campaign'),
            'list_clicks' => Mage::helper('nuntius')->__('Clicks - List'),
            'global_subscribers' => Mage::helper('nuntius')->__('Subscribers - Global'),
            'segment_subscribers' => Mage::helper('nuntius')->__('Subscribers - Segment'),
            'list_subscribers' => Mage::helper('nuntius')->__('Subscribers - List'),
        );

        if (!Mage::helper('nuntius')->magna()) {
            foreach ($info as $key => $value) {
                if (stripos($value, 'segment_') !== false) {
                    unset($info[$key]);
                }
            }
        }

        return $info;
    }

    public function updateGoalCurrentValue(Licentia_Nuntius_Model_Goals $goal) {

        if ($goal->getResult() != 2) {
            return;
        }

        $goal->setCurrentValue($goal->getGoalCurrentValue($goal))->save();
    }

    public function updateGoalsCurrentValue() {
        foreach ($this->getCollection() as $goal) {
            $goal->updateGoalCurrentValue($goal);
        }
    }

    public function getGoalCurrentValue(Licentia_Nuntius_Model_Goals $goal) {

        $goalType = $goal->getData('goal_type');

        if (stripos($goalType, 'global') === false) {

            if (stripos($goalType, 'segment_') !== false) {
                $type = 'segments';
                $model = Mage::getModel('magna/' . $type)->load($this->getData('goal_type_option_id'));
            } else {
                if (stripos($goalType, 'list_') !== false) {
                    $type = 'lists';
                }
                if (stripos($goalType, 'campaign_') !== false) {
                    $type = 'campaigns';
                }

                $model = Mage::getModel('nuntius/' . $type)->load($this->getData('goal_type_option_id'));
            }

            if (stripos($goalType, '_conversions') !== false) {
                $value = $model->getData('conversions_amount');
            }
            if (stripos($goalType, '_views') !== false) {
                $value = $model->getData('views');
            }
            if (stripos($goalType, '_clicks') !== false) {
                $value = $model->getData('clicks');
            }
            if (stripos($goalType, '_subscribers') !== false) {

                if ($type == 'segments') {
                    $value = $model->getData('records');
                }

                if ($type == 'lists') {
                    $value = $model->getData('susbcribers');
                }
            }
        } else {

            if ($goalType == 'global_conversions') {
                $collection = Mage::getModel('nuntius/lists')->getCollection();
                $collection->getSelect()->reset('columns')
                        ->columns(array('total' => new Zend_Db_Expr('SUM(conversions_amount)')));
            }
            if ($goalType == 'global_views') {
                $collection = Mage::getModel('nuntius/lists')->getCollection();
                $collection->getSelect()->reset('columns')
                        ->columns(array('total' => new Zend_Db_Expr('SUM(views)')));
            }
            if ($goalType == 'global_clicks') {
                $collection = Mage::getModel('nuntius/lists')->getCollection();
                $collection->getSelect()->reset('columns')
                        ->columns(array('total' => new Zend_Db_Expr('SUM(clicks)')));
            }
            if ($goalType == 'global_subscribers') {
                $collection = Mage::getModel('nuntius/lists')->getCollection();
                $collection->getSelect()->reset('columns')
                        ->columns(array('total' => new Zend_Db_Expr('SUM(subscribers)')));
            }

            $value = $collection->getFirstItem()->getData('total');
        }

        return $value;
    }

    public function cron() {

        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATE);

        $collection = $this->getCollection()
                ->addFieldToFilter('result', array('nin' => array(0, 1)));

        foreach ($collection as $goal) {

            if (Mage::helper('nuntius')->magna()) {
                if (stripos($goal->getGoalType(), 'segment_') !== false) {
                    $segmentId = $goal->getGoalTypeOptionId();
                    Mage::getModel('magna/segments_list')->load($segmentId);
                }
            }
            if ($date == $goal->getStartDate()) {
                $goal->setOriginalValue($goal->getGoalCurrentValue($this));
            }

            $goal->updateGoalCurrentValue($goal);

            if ($goal->getData('start_date') <= $date && $goal->getData('end_date') >= $date) {
                $goal->setData('result', 2);
            }

            if ($goal->getData('end_date') < $date) {

                if ($goal->getData('expected_value') < $goal->getData('current_value')) {
                    $goal->setData('result', 0);
                } else {
                    $goal->setData('result', 1);
                }
            }

            $goal->save();
        }
    }

    public function save() {

        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATE);

        if ($this->getData('start_date') <= $date && $this->getData('end_date') >= $date) {
            $this->setData('result', 2);
        }

        if (!$this->getId()) {
            $this->setData('original_value', $this->getGoalCurrentValue($this));
            $this->setData('current_value', $this->getGoalCurrentValue($this));
        }

        if ($this->getId()) {
            $old = Mage::getModel('nuntius/goals')->load($this->getId());
            $this->setData('original_value', $old->getData('original_value'));

            if (in_array($this->getData('result'), array(0, 1, 2))) {
                $this->unsetData('start_date');
            }
        }

        $variation = $this->getData('variation');

        $current = $this->getData('original_value');

        if (stripos($variation, '-') === false && stripos($variation, '+') === false && stripos($variation, '%') === false) {
            $expectedValue = $variation;
        }

        if (stripos($variation, '-') !== false && stripos($variation, '%') === false) {
            $expectedValue = $current - abs($variation);
        }

        if (stripos($variation, '+') !== false && stripos($variation, '%') === false) {
            $expectedValue = $current + $variation;
        }

        if (stripos($variation, '%') !== false) {

            $value = str_replace(array('+', '%', '-'), "", $variation);

            if (stripos($variation, '+') !== false || stripos($variation, '-') === false) {

                $value = $current + $current * $value / 100;
            }

            if (stripos($variation, '-') !== false) {
                $value = $current - $current * $value / 100;
            }
            $expectedValue = $value;
        }

        $this->setData('expected_value', $expectedValue);

        return parent::save();
    }

}
