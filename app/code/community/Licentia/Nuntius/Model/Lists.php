<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Lists extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('nuntius/lists');
    }

    public function getOptionArray($storeId = null) {

        $lists = Mage::getModel('nuntius/lists')
                ->getCollection();

        if (!is_null($storeId))
            $lists->addFieldToFilter('store_id', $storeId);


        $lists->getData();

        $return = array();
        foreach ($lists as $list) {
            $return[$list['list_id']] = $list['name'];
        }
        return $return;
    }

    public function getAllOptions($storeId = null) {
        $res = array();
        foreach (self::getOptionArray($storeId) as $index => $value) {
            $res[] = array(
                'value' => $index,
                'label' => $value
            );
        }
        return $res;
    }

    public function getDefaultList() {
        $collection = $this->getCollection()->addFieldToFilter('default', 1);

        if ($collection->count() != 1) {
            return false;
        }

        return $collection->getFirstItem();
    }

    public function getListForStore($storeId) {

        $listId = Mage::getStoreConfig('nuntius/lists/list_id', $storeId);

        return Mage::getModel('nuntius/lists')->load($listId);
    }

}
