<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *  


 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International 
 */
class Licentia_Nuntius_Model_Mysql4_Links extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('nuntius/links', 'link_id');
    }

}
