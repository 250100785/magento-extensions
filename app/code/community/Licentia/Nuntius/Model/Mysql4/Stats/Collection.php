<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Mysql4_Stats_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        parent::_construct();
        $this->_init('nuntius/stats');
    }

    public function getAllIds($field = false) {
        if (!$field) {
            return parent::getAllIds();
        }

        $idsSelect = clone $this->getSelect();
        $idsSelect->reset(Zend_Db_Select::ORDER);
        $idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $idsSelect->reset(Zend_Db_Select::COLUMNS);
        $idsSelect->columns($field, 'main_table');
        return $this->getConnection()->fetchCol($idsSelect);
    }

    public function getStats($campaignId, $field) {


        $report = new $this;
        $report->addFieldToFilter('campaign_id', $campaignId);
        $report->addFieldToFilter('type', 'views');
        $report->getSelect()->reset('columns')
                ->columns(array('total' => new Zend_Db_Expr('COUNT(*)'), $field))
                ->group($field)->order('total DESC');

        $countries = array();
        $countries[] = array('Browser', 'views', 'Clicks');

        foreach ($report as $item) {
            $countries[] = array($item->getData($field), (int) $item->getData('total'));
        }

        $report = new $this;
        $report->addFieldToFilter('campaign_id', $campaignId);
        $report->addFieldToFilter('type', 'clicks');
        $report->getSelect()->reset('columns')
                ->columns(array('total' => new Zend_Db_Expr('COUNT(*)'), $field))
                ->group($field)->order('total DESC');

        foreach ($report as $item) {

            if (count($countries) > 1) {
                foreach ($countries as $key => $country) {
                    if ($country[0] == $item->getData($field)) {
                        $countries[$key][2] = (int) $item->getData('total');
                        break;
                    }
                    if (!isset($countries[$key][2])) {
                        $countries[$key][2] = 0;
                    }
                }
            } else {
                $countries[] = array($item->getData($field), 0, (int) $item->getData('total'));
            }
        }

        return $countries;
    }

    public function getGeneral($campaignId, $option, $unique = false) {
        $report = new $this;
        $report->addFieldToFilter('campaign_id', $campaignId);
        $report->addFieldToFilter('type', $option);
        $report->getSelect()->reset('columns')
                ->columns(array('total' => new Zend_Db_Expr('COUNT(*)')));

        if ($unique) {
            $report->getSelect()
                    ->group(array('subscriber_id'));
        }

        return $report->getData();
    }

    public function getCities($campaignId = false) {

        $report = new $this;
        $report->addFieldToFilter('campaign_id', $campaignId);
        $report->addFieldToFilter('type', 'views');
        $report->addFieldToFIlter('city', array('notnull' => true));
        $report->addFieldToFIlter('country', array('notnull' => true));
        $report->getSelect()->reset('columns')
                ->columns(array('total' => new Zend_Db_Expr('COUNT(*)'), 'city', 'country'))
                ->group(array('city', 'country'))->order('total DESC');

        $countries = array();
        $countries[] = array('Country', 'City', 'views', 'Clicks');

        foreach ($report as $item) {
            $countries[] = array($item->getData('country'), $item->getData('city'), (int) $item->getData('total'));
        }

        $report = new $this;
        $report->addFieldToFilter('campaign_id', $campaignId);
        $report->addFieldToFilter('type', 'clicks');
        $report->addFieldToFIlter('city', array('notnull' => true));
        $report->addFieldToFIlter('country', array('notnull' => true));
        $report->getSelect()->reset('columns')
                ->columns(array('total' => new Zend_Db_Expr('COUNT(*)'), 'city', 'country'))
                ->group(array('city', 'country'))->order('total DESC');

        foreach ($report as $item) {
            foreach ($countries as $key => $country) {
                if ($country[1] == $item->getData('city')) {
                    $countries[$key][3] = (int) $item->getData('total');
                    break;
                }
                if (!isset($countries[$key][2])) {
                    $countries[$key][3] = 0;
                }
            }
        }

        return $countries;
    }

    public function getLinks($campaignId) {

        $links = Mage::getModel('nuntius/links')->getCollection();
        $links->addFieldToFilter('campaign_id', $campaignId);

        $countries = array();
        $countries[] = array('Url', 'Opens', 'Conversions');

        foreach ($links as $item) {
            $countries[] = array($item->getData('link'), (int) $item->getData('clicks'), (int) $item->getData('conversions_number'));
        }
        return $countries;
    }

    public function addTimeToSelect($field = 'event_at') {

        $this->getSelect()->columns(array('count_' . $field => new Zend_Db_Expr('COUNT(*)'), $field => new Zend_Db_Expr("DATE_FORMAT($field,'%H')")))
                ->group(new Zend_Db_Expr("DATE_FORMAT($field,'%H')"));
        return $this;
    }

}
