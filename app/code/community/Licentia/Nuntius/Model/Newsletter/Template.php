<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Newsletter_Template extends Mage_Newsletter_Model_Template {

    public function send($subscriber, array $variables = array(), $name = null, Mage_Newsletter_Model_Queue $queue = null) {

        if (!$this->isValidForSend()) {
            return false;
        }

        if (!Mage::helper('nuntius')->isEnabled()) {
            return parent::send($subscriber, $variables, $name, $queue);
        }

        $email = '';
        if ($subscriber instanceof Mage_Newsletter_Model_Subscriber) {
            $email = $subscriber->getSubscriberEmail();
            if (is_null($name) && ($subscriber->hasCustomerFirstname() || $subscriber->hasCustomerLastname())) {
                $name = $subscriber->getCustomerFirstname() . ' ' . $subscriber->getCustomerLastname();
            }
        } else {
            $email = (string) $subscriber;
        }

        $mail = $this->getMail();
        $mail->setReturnPath($this->getTemplateSenderEmail());

        $transport = Mage::helper('nuntius')->getSmtpTransport();

        $mail->addTo($email, $name);
        $text = $this->getProcessedTemplate($variables, true);

        if ($this->isPlain()) {
            $mail->setBodyText($text);
        } else {
            $mail->setBodyHTML($text);
        }

        $mail->setSubject($this->getProcessedTemplateSubject($variables));
        $mail->setFrom($this->getTemplateSenderEmail(), $this->getTemplateSenderName());

        $mail->setReplyTo($this->getTemplateSenderEmail(), $this->getTemplateSenderName());

        try {
            $mail->send($transport);

            $this->_mail = null;
            if (!is_null($queue)) {
                $subscriber->received($queue);
            }
        } catch (Exception $e) {
            if ($subscriber instanceof Mage_Newsletter_Model_Subscriber) {
                $problem = Mage::getModel('newsletter/problem');
                $problem->addSubscriberData($subscriber);
                if (!is_null($queue)) {
                    $problem->addQueueData($queue);
                }
                $problem->addErrorData($e);
                $problem->save();

                if (!is_null($queue)) {
                    $subscriber->received($queue);
                }
            } else {
                throw $e;
            }
            return false;
        }

        return true;
    }

}
