<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Observer {

    public function notifyBuild() {
        $admin = Mage::getSingleton('admin/session')->getUser();
        if (!$admin)
            return;

        if (version_compare(Mage::getVersion(), '1.7') == -1) {
            $parent = Mage::getSingleton('admin/config')->getAdminhtmlConfig()->getNode('menu');
            list($element) = $parent->xpath('nuntius/children/report/children/coupons');
            unset($element->{0});
        }
    }

    public function loadVariables($event) {

        $referer = Mage::app()->getRequest()->getServer('HTTP_REFERER');

        if (stripos($referer, 'nuntius_') === false)
            return;

        $customVariables = Mage::getModel('core/variable')->getVariablesOptionArray(true);
        $storeContactVariabls = Mage::getModel('core/source_email_variables')->toOptionArray(true);

        $nuntius = array('label' => Mage::helper('nuntius')->__('Subscriber Variables'),
            'value' => array(
                array('label' => Mage::helper('nuntius')->__('First Name'), 'value' => '{{var subscriber.getFirstname()}}'),
                array('label' => Mage::helper('nuntius')->__('Last Name'), 'value' => '{{var subscriber.getLastname()}}'),
                array('label' => Mage::helper('nuntius')->__('Full Name'), 'value' => '{{var subscriber.getName()}}'),
                array('label' => Mage::helper('nuntius')->__('Email'), 'value' => '{{var subscriber.getEmail()}}'),
                array('label' => Mage::helper('nuntius')->__('Date of Birth'), 'value' => '{{var subscriber.getDob()}}'),
                array('label' => Mage::helper('nuntius')->__('Unsubscription Link'), 'value' => '{{var subscriber.getUnsubscriptionLink()}}'),
            )
        );

        preg_match('/nuntius_autoresponders\/edit\/id\/(\d.*?)\//', $referer, $auto);
        preg_match('/nuntius_autoresponders(.*)\/event\/(\w.*?)\//', $referer, $auto1);

        $event = false;
        if (isset($auto1[2])) {
            $event = $auto1[2];
        }
        if (isset($auto[1])) {
            $autoresponder = Mage::getModel('nuntius/autoresponders')->load($auto[1]);
            if ($autoresponder->getId()) {
                $event = $autoresponder->getEvent();
            }
        }

        $autoresponderVariables = array();

        if ($event == 'new_tag') {
            $autoresponderVariables = array('label' => Mage::helper('nuntius')->__('Autorresponder Variables'),
                'value' => array(
                    array('label' => Mage::helper('nuntius')->__('Tag Name'), 'value' => '{{var tag.getName()}}'),
                )
            );
        }
        if ($event == 'new_review') {
            $autoresponderVariables = array('label' => Mage::helper('nuntius')->__('Autoresponder Variables'),
                'value' => array(
                    array('label' => Mage::helper('nuntius')->__('Review Title'), 'value' => '{{var review.getTitle()}}'),
                    array('label' => Mage::helper('nuntius')->__('Review Detail'), 'value' => '{{var review.getDetail()}}'),
                )
            );
        }
        if (in_array($event, array('order_new', 'order_status', 'order_product'))) {
            $autoresponderVariables = array('label' => Mage::helper('nuntius')->__('Autoresponder Variables'),
                'value' => array(
                    array('label' => Mage::helper('nuntius')->__('Customer Name'), 'value' => '{{htmlescape var=$order.getCustomerName()}}'),
                    array('label' => Mage::helper('nuntius')->__('Order Id'), 'value' => '{{var order.increment_id}}'),
                    array('label' => Mage::helper('nuntius')->__('Order Created At (datetime)'), 'value' => "{{var order.getCreatedAtFormated('long')}}"),
                    array('label' => Mage::helper('nuntius')->__('Billing Address'), 'value' => "{{var order.getBillingAddress().format('html')}}"),
                    array('label' => Mage::helper('nuntius')->__('Payment Details'), 'value' => '{{var payment_html}}'),
                    array('label' => Mage::helper('nuntius')->__('Shipping Address'), 'value' => "{{var order.getShippingAddress().format('html')}}"),
                    array('label' => Mage::helper('nuntius')->__('Shipping Description'), 'value' => '{{var order.getShippingDescription()}}'),
                    array('label' => Mage::helper('nuntius')->__('Order Items Grid'), 'value' => '{{layout handle="sales_email_order_items" order="$order"}}'),
                    array('label' => Mage::helper('nuntius')->__('Email Order Note'), 'value' => '{{var order.getEmailCustomerNote()}}'),
                )
            );
        }

        $variables = array($storeContactVariabls, $customVariables, $nuntius);

        if (count($autoresponderVariables) > 0) {
            array_unshift($variables, $autoresponderVariables);
        }
        echo Zend_Json::encode($variables);
        die();
    }

    public function validateCoupon($event) {
        $request = $event->getControllerAction()->getRequest();

        $coupon = $request->getParam('coupon_code');

        $model = Mage::getModel('nuntius/coupons');

        if (!$model->validateCoupon($coupon))
            $request->setparam('coupon_code', 'INVALID_COUPON_' . time());
    }

    public function newCustomer($event) {

        $customer = $event->getEvent()->getCustomer();
        $storeId = $customer->getStoreId();

        $list = Mage::getModel('nuntius/lists')->getListForStore($storeId);

        if (!$list->getAuto()) {
            return false;
        }

        try {
            Mage::getModel('nuntius/subscribers')
                    ->addCustomerToList($customer->getId(), $list->getListId());
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

    public function addToAutoList($event) {

        $order = $event->getEvent()->getOrder();

        $customer = Mage::getModel('nuntius/subscribers')
                ->findCustomer($order->getCustomerEmail(), 'email');

        if (!$customer) {
            return false;
        }

        $storeId = $customer->getStoreId();

        $list = Mage::getModel('nuntius/lists')->getListForStore($storeId);

        if (!$list->getAuto()) {
            return false;
        }

        try {
            Mage::getModel('nuntius/subscribers')
                    ->addCustomerToList($customer->getId(), $list->getListId());
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }

}
