<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Products extends Mage_Core_Model_Abstract {

    public function toOptionArray() {
        return array(
            array('value' => 'attributes', 'label' => Mage::helper('nuntius')->__('Products Attributes')),
            array('value' => 'related_order', 'label' => Mage::helper('nuntius')->__('Related Products From Last Completed Order')),
            array('value' => 'related', 'label' => Mage::helper('nuntius')->__('Related Products From Previous Completed Orders')),
            array('value' => 'abandoned', 'label' => Mage::helper('nuntius')->__('Products In Abandoned Cart')),
            array('value' => 'categories', 'label' => Mage::helper('nuntius')->__('Categories Views')),
            array('value' => 'wishlist', 'label' => Mage::helper('nuntius')->__('Wishlist Items')),
            array('value' => 'views', 'label' => Mage::helper('nuntius')->__('Product Views')),
            array('value' => 'recent', 'label' => Mage::helper('nuntius')->__('Recent Added')),
        );
    }

}
