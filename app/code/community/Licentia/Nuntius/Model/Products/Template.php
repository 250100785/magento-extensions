<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Products_Template extends Mage_Core_Model_Abstract {

    public function toOptionArray() {

        $design = Mage::getBaseDir();
        $files = glob($design . '/app/design/frontend/base/default/template/nuntius/widgets/*');

        $template = array();
        foreach ($files as $file) {
            $filename = basename($file);
            $template['nuntius/widgets/' . $filename] = $filename;
        }

        return $template;
    }

}
