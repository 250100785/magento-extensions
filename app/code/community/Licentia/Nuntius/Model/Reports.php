<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Reports extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('nuntius/reports');
    }

    public function cron() {

        $date = Mage::app()->getLocale()->date()->subDay(1)->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATE);
        $lists = Mage::getModel('nuntius/lists')->getCollection();

        foreach ($lists as $list) {
            $prev = Mage::getModel('nuntius/reports')->getCollection()
                    ->addFieldToFilter('created_at', array('lt' => $date))
                    ->addFieldToFilter('list_id', $list->getId())
                    ->setOrder('created_at', 'DESC')
                    ->setPageSize(1);

            if ($prev->count() == 1) {
                $previous = $prev->getFirstItem();
            } else {
                $previous = new Varien_Object;
            }

            $campaigns = Mage::getModel('nuntius/campaigns')
                    ->getCollection()
                    ->addFieldToSelect('campaign_id')
                    ->addFieldToFilter('list_id', $list->getId())
                    ->count();

            $data = array();
            $data['list_id'] = $list->getId();
            $data['created_at'] = $date;
            $data['subscribers'] = (int) $list->getData('subscribers');
            $data['subscribers_variation'] = (int) $list->getData('subscribers') - $previous->getData('subscribers');
            $data['clicks'] = (int) $list->getData('clicks');
            $data['clicks_variation'] = (int) $list->getData('clicks') - $previous->getData('clicks');
            $data['views'] = (int) $list->getData('views');
            $data['views_variation'] = (int) $list->getData('views') - $previous->getData('views');
            $data['conversions_number'] = (int) $list->getData('conversions_number');
            $data['conversions_number_variation'] = (int) $list->getData('conversions_number') - $previous->getData('conversions_number');
            $data['conversions_amount'] = (int) $list->getData('conversions_amount');
            $data['conversions_amount_variation'] = (int) $list->getData('conversions_amount') - $previous->getData('conversions_amount');
            $data['campaigns'] = (int) $campaigns;
            $data['campaigns_variation'] = (int) $campaigns - $previous->getData('campaigns');
            try {
                $this->setData($data)->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        return;
    }

}
