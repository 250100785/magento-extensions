<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Service_ServiceAbstract extends Varien_Object {

    public function buildQueue() {

        $campaign = $this->getData('campaign');
        $subscribersIds = $this->getData('subscribers');

        $subscribers = Mage::getModel('nuntius/subscribers')
                ->getSubscribersForList($campaign->getData('list_id'));
        if ($subscribersIds) {
            if (!is_array($subscribersIds)) {
                $subscribersIds = explode(',', $subscribersIds);
            }
            $subscribers->addfieldToFilter('subscriber_id', array('in' => $subscribersIds));
        }

        $message = $campaign->getMessage();
        $buildFrom = $campaign->getSenderId();
        $sender = Mage::getModel('nuntius/senders')->load($buildFrom);

        $queue = Mage::getModel('nuntius/queue');
        $history = Mage::getModel('nuntius/history');
        $i = 0;
        $errors = false;
        foreach ($subscribers as $subscriber) {

            $count = $history->getCollection()
                    ->addFieldToFilter('campaign_id', $campaign->getId())
                    ->addFieldToFilter('subscriber_id', $subscriber->getId());

            if ($count->count() > 0) {
                continue;
            }

            $message = $this->parseDynamicMessageContent($campaign, $subscriber);

            $data = array();
            $data['campaign_id'] = $campaign->getId();
            $data['sender_name'] = $sender->getName();
            $data['sender_email'] = $sender->getEmail();
            $data['subscriber_id'] = $subscriber->getId();
            $data['name'] = $subscriber->getName();
            $data['email'] = $subscriber->getEmail();
            $data['subject'] = $campaign->getSubject();
            $data['message'] = $message;
            $data['headers'] = $this->_buildHeaders($campaign, $sender, $subscriber);
            $data['headers'] = serialize($data['headers']);
            if ($campaign->getData('subscriber_time') == 1) {
                $data['send_date'] = $this->getSendTime($campaign, $subscriber);
            }

            try {
                $queue->setData($data)->save();
                $history->setData($data)->save();
                $i++;
            } catch (Exception $e) {
                Mage::logException($e);
                $errors = true;
            }
        }

        $status = ($i == 0) ? 'finished' : 'running';

        $campaign->setData('unsent', $i)->setData('status', $status)->save();

        $this->setData('totalEmails', $i);
        $this->setData('errors', $errors);
        $this->setData('id', true);
        $this->setData('campaign', $campaign);

        if ($campaign->getParentId()) {
            $parent = Mage::getModel('nuntius/campaigns')->load($campaign->getParentId());
            $parent->setData('unsent', $parent->getData('unsent') + $i);
            $parent->save();
        }

        return $this;
    }

    public function getDefaultStoreId() {
        if (is_null($this->_defaultStoreId)) {

            $campaign = Mage::registry('current_campaign');

            if (!$campaign->getId()) {
                $this->_defaultStoreId = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
            }

            $list = Mage::getModel('nuntius/lists')->load($campaign->getListId());

            if ($list->getId() || (int) $list->getStore() == 0) {
                $this->_defaultStoreId = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
            }

            $this->_defaultStoreId = $list->getStoreId();
        }

        return $this->_defaultStoreId;
    }

    /**
     *
     * @param Licentia_Nuntius_Model_Campaigns $campaign
     * @param Licentia_Nuntius_Model_Subscribers $subscriber
     * @return string
     */
    public function parseDynamicMessageContent(Licentia_Nuntius_Model_Campaigns $campaign, Licentia_Nuntius_Model_Subscribers $subscriber) {

        $subscriberId = $subscriber->getSubscriberId();

        Mage::unregister('current_subscriber');
        Mage::register('current_subscriber', $subscriber);

        $customer = $subscriber->getCustomerId();

        $storeId = $subscriber->getStoreId() ? $subscriber->getStoreId() : $this->getDefaultStoreId();
        $currentStoreId = Mage::app()->getStore()->getId();

        Mage::dispatchEvent('nuntius_campaign_view', array('campaign' => $campaign, 'customer_id' => $customer, 'subscriber' => $subscriber));

        $variables = array();
        $variables['subscriber'] = $subscriber;
        $variables['campaign'] = $campaign;
        if ($campaign->getUrl()) {
            $message = Mage::helper('nuntius')->getContentFromUrl($campaign, $subscriber);
        } else {

            Mage::app()->getStore()->setId($storeId);

            if ($campaign->getAutoresponderEventId() > 0) {
                $event = Mage::getModel('nuntius/events')->load($campaign->getAutoresponderEventId());

                if ($event->getId()) {
                    if ($event->getEvent() == 'new_tag') {
                        $variables['tag'] = Mage::getModel('tag/tag')->load($event->getDataObjectId());
                    }
                    if ($event->getEvent() == 'new_review') {
                        $variables['review'] = Mage::getModel('review/review')->load($event->getDataObjectId());
                    }
                    if (in_array($event->getEvent(), array('order_new', 'order_status', 'order_product'))) {
                        $variables['order'] = Mage::getModel('sales/order')->load($event->getDataObjectId());
                        $paymentBlock = Mage::helper('payment')->getInfoBlock($variables['order']->getPayment())
                                ->setIsSecureMode(true);
                        $paymentBlock->getMethod()->setStore($storeId);
                        $paymentBlockHtml = $paymentBlock->toHtml();

                        $variables['payment_html'] = $paymentBlockHtml;
                    }
                }
            }

            $imgUrl = Mage::getModel('core/url')
                    ->setStore($storeId)
                    ->getUrl('nuntius/campaign/stat', array('c' => $campaign->getCampaignId(), 'u' => $subscriberId, '_nosid' => true));

            $message = $campaign->getMessage();

            if (stripos($message, '{{') !== false) {
                $message = Mage::helper('newsletter')
                        ->getTemplateProcessor()
                        ->setVariables($variables)
                        ->filter($message);
            }
        }

        $message .= ' <img width="1" height="1" src="' . $imgUrl . '" border="0"> ';

        Mage::app()->getStore()->setId($currentStoreId);

        $doc = new DOMDocument();
        $doc->loadHTML('<?xml encoding="UTF-8">' . $message);
        foreach ($doc->getElementsByTagName('a') as $link) {

            $urlParams = array('u' => $subscriber->getSubscriberId(), 'c' => $campaign->getCampaignId(), 'url' => base64_encode($link->getAttribute('href')));

            $link->setAttribute('href', Mage::getModel('core/url')
                            ->setStore($storeId)
                            ->getUrl('nuntius/campaign/go/', $urlParams));
        }

        foreach ($doc->childNodes as $item) {
            if ($item->nodeType == XML_PI_NODE) {
                $doc->removeChild($item);
                $doc->encoding = 'UTF-8';
            }
        }
        return $doc->saveHTML();
    }

    public function isDefaultListDefined() {
        $id = Mage::getConfig()->getNode('default/nuntius/lists/list_id');

        if (!$id) {
            return false;
        }

        $list = Mage::getModel('nuntius/lists')->load($id);

        if (!$list->getId()) {
            return false;
        }

        return true;
    }

    public function validateSenders() {

        $senders = Mage::getModel('nuntius/senders')
                ->getCollection();

        if ($senders->count() == 0) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param Licentia_Nuntius_Model_Campaigns $campaign
     * @param Licentia_Nuntius_Model_Subscribers $subscriber
     * @return end_Db_Expr|string
     */
    public function getSendTime(Licentia_Nuntius_Model_Campaigns $campaign, Licentia_Nuntius_Model_Subscribers $subscriber) {

        $shour = $subscriber->getSendTime();
        if ($shour == -1) {
            return new Zend_Db_Expr('NULL');
        }

        $hour = date('H', strtotime($campaign->getDeployAt()));

        if ($hour == $shour) {
            return new Zend_Db_Expr('NULL');
        }

        if ($hour < $shour) {
            $nHour = $shour - $hour;
            return date('Y-m-d H', strtotime('now +' . $nHour . 'hours')) . ':00:00';
        }

        if ($hour > $shour) {
            $nHour = 24 - $hour + $shour;
            return date('Y-m-d H', strtotime('now +' . $nHour . 'hours')) . ':00:00';
        }
    }

    protected function _buildHeaders($campaign, $sender, $subscriber) {

        $smtp = Mage::getStoreConfig('nuntius');

        $headersNoNo = array('cc', 'bcc', 'return-path');

        $returnHeaders = array();
        $headersSmtp = $smtp['config']['headers'];

        $find = array('{campaignId}', '{fromEmail}', '{fromName}', '{toEmail}', '{toName}', '{campaignName}', '{subject}');
        $replace = array($campaign->getId(), $sender->getEmail(), $sender->getName(), $subscriber->getEmail(), $subscriber->getName(), $campaign->getInternalName(), $campaign->getSubject());
        $headers = explode("\n", str_replace($find, $replace, $headersSmtp));

        foreach ($headers as $header) {
            $parts = explode('|', $header);
            if (count($parts) != 2) {
                continue;
            }
            $name = trim($parts[0]);
            $value = trim($parts[1]);

            if (in_array($name, $headersNoNo)) {
                continue;
            }
            $returnHeaders[$name] = $value;
        }

        if ($smtp['bounces']['email']) {
            $returnHeaders['return-path'] = $smtp['bounces']['email'];
        }
        $returnHeaders['Xnuntius-cid'] = $campaign->getId();
        $returnHeaders['Xnuntius-sid'] = $subscriber->getId();

        return $returnHeaders;
    }
}
