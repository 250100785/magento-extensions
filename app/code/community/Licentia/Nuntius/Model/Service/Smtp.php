<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Service_Smtp extends Licentia_Nuntius_Model_Service_ServiceAbstract {

    protected $_campaigns = array();

    public function sendEmail($tryAgain = false, $errorIds = array()) {

        if (Mage::getStoreConfigFlag('system/smtp/disable')) {
            return $this;
        }

        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

        $count = (int) Mage::getStoreConfig('nuntius/config/count');
        if ($count == 0) {
            $count = 100;
        }

        $headersNoNo = array('to', 'cc', 'bcc', 'from', 'subject',
            'reply-to', 'return-path', 'date', 'message-id',
        );

        $transport = Mage::helper('nuntius')->getSmtpTransport();

        if (!$transport) {
            return false;
        }

        if ($tryAgain) {
            $queue = Mage::getModel('nuntius/errors')
                    ->getCollection()
                    ->addFieldToFilter('error_id', array('in' => $errorIds));
        } else {
            $queue = Mage::getModel('nuntius/queue')
                    ->getCollection()
                    ->setPageSize($count)
                    ->addFieldToFilter(
                            array('send_date', 'send_date'), array(
                        array('lteq' => $date),
                        array('null' => true)
                    ))
                    ->setCurPage(1);
        }

        $errors = Mage::getModel('nuntius/errors');
        $archive = Mage::getModel('nuntius/archive');

        $campaignData = array();
        $i = 0;
        $a = 0;
        foreach ($queue as $message) {

            $subscriber = Mage::getModel('nuntius/subscribers')->load($message->getSubscriberId());

            if (!isset($this->_campaigns[$message->getCampaignId()])) {
                $campaign = Mage::getModel('nuntius/campaigns')->load($message->getCampaignId());
                $this->_campaigns[$message->getCampaignId()] = $campaign;
            } else {
                $campaign = $this->_campaigns[$message->getCampaignId()];
            }

            $mail = new Zend_Mail('UTF-8');
            $mail->addTo($message->getEmail(), $message->getName());
            $mail->setSubject($message->getSubject());
            $mail->setBodyHtml($message->getMessage());
            $mail->setFrom($message->getSenderEmail(), $message->getSenderName());

            $headers = unserialize($message->getHeaders());

            foreach ($headers as $name => $value) {

                if (in_array($name, $headersNoNo)) {
                    if ($name == 'bcc') {
                        $mail->addBcc($value);
                    }
                    if ($name == 'cc') {
                        $mail->addCc($value);
                    }
                    if ($name == 'reply-to') {
                        $mail->setReplyTo($value);
                    }
                    if ($name == 'return-path') {
                        $mail->setReturnPath($value);
                    }
                } else {
                    $mail->addHeader($name, $value);
                }
            }

            if (!isset($campaignData[$campaign->getId()])) {
                $campaignData[$campaign->getId()]['sent'] = 0;
            }

            try {
                $mail->send($transport);

                $campaignData[$campaign->getId()]['sent'] = $campaignData[$campaign->getId()]['sent'] + 1;

                $subscriber->setData('sent', $subscriber->getData('sent') + 1)->save();

                $data = $message->getData();
                $data['sent_date'] = now();
                $archive->setData($data)->save();

                $message->delete();
            } catch (Exception $e) {
                Mage::logException($e);
                if (!$tryAgain) {

                    if ($message->getAttempts() > 2) {
                        $data = array();
                        $data['campaign_id'] = $message->getCampaignId();
                        $data['subject'] = $message->getSubject();
                        $data['subscriber_id'] = $message->getSubscriberId();
                        $data['email'] = $message->getEmail();
                        $data['error_code'] = $e->getCode();
                        $data['error_message'] = $e->getMessage();
                        $data['created_at'] = now();

                        $dataInsert = array_merge($data, $message->getData());
                        $errors->setData($dataInsert)->save();

                        $message->delete();
                        $a++;
                    } else {
                        $message->setData('attempts', $message->getData('attempts') + 1)->save();
                    }
                }
            }
        }

        $messages = array();
        foreach ($this->_campaigns as $campaign) {
            $pending = Mage::getModel('nuntius/queue')
                    ->getCollection()
                    ->addFieldToSelect('campaign_id')
                    ->addFieldToFilter('campaign_id', $campaign->getId());

            if ($pending->count() == 0) {
                $campaign->setData('status', 'finished');
            } else {
                $campaign->setData('status', 'running');
            }

            $campaign->setData('sent', $campaign->getData('sent') + $campaignData[$campaign->getId()]['sent']);
            $campaign->save();

            if ($pending->count() == 0) {
                $campaign->updateCampaignAfterSend($campaign);
            }

            if ($campaign->getParentId()) {
                $parent = Mage::getModel('nuntius/campaigns')->load($campaign->getParentId());
                $parent->setData('sent', $parent->getData('sent') + $campaignData[$campaign->getId()]['sent']);
                $parent->save();
            }

            if ($tryAgain) {
                $messages[] = Mage::helper('nuntius')->__('Campaign %s: %d emails sent, %d not sent', $campaign->getInternalName(), $campaignData[$campaign->getId()]['sent'], $campaignData[$campaign->getId()]['unsent']);
            }
        }

        $this->setData('sent', $i);

        $this->setData('messages', $messages);

        return $this;
    }

    public function validateEnvironmentQuick() {

        $auth = Mage::getSingleton('admin/session');

        if (!$auth) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('nuntius')->__('Please configure your service bellow'));
            return false;
        }

        if ($auth->getUser()->getData('nuntiusAuth') === true) {
            return true;
        }

        if (!$this->isDefaultListDefined()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('nuntius')->__('Please define your List Mapping.'));
            return false;
        }

        Mage::getSingleton('admin/session')->getUser()->setData('nuntiusAuth', true);

        return true;
    }

    public function validateEnvironment($email = false) {

        $smtp = Mage::getStoreConfig('nuntius/config');

        $config = array('auth' => $smtp['auth'], 'port' => $smtp['port']);

        if ($smtp['ssl'] != 'none') {
            $config['ssl'] = $smtp['ssl'];
        }

        if ($smtp['auth'] != 'none') {
            $config['username'] = $smtp['username'];
            $config['password'] = $smtp['password'];
        }

        $message = "Hi there,\n\nThis is a test message to check if your settings are defined correctly.\n\nIf you received this email, everything seams to be working fine\n\nBe happy... :)";
        $transport = new Zend_Mail_Transport_Smtp($smtp['server'], $config);
        $mail = new Zend_Mail();
        $mail->setBodyText($message);
        $mail->setFrom($email);
        $mail->addTo($email);
        $mail->setSubject('Test Message - Magento|Nuntius');

        try {
            $mail->send($transport);
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('nuntius')->__('Everything Seams To Be OK with your SMTP Configuration!!!'));
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('nuntius')->__('Error SMTP Configuration: ') . $e->getMessage());
        }

        $smtp = Mage::getStoreConfig('nuntius/bounces');

        $config = array('ssl' => $smtp['ssl']);
        $config['password'] = $smtp['password'];
        $config['host'] = $smtp['server'];
        $config['user'] = $smtp['email'];

        if (strlen($smtp['email']) > 0) {
            try {
                new Zend_Mail_Storage_Imap($config);
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('nuntius')->__('Everything Seams To Be OK with your Bounces Configuration!!!'));
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('nuntius')->__('Error Bounces Configuration: ') . $e->getMessage());
            }
        }
    }

}
