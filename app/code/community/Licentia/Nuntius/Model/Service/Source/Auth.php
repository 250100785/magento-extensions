<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Service_Source_Auth {

    public function toOptionArray() {
        return array(
            "none" => Mage::helper('nuntius')->__('None (ignore username/password)'),
            "login" => Mage::helper('nuntius')->__('Login'),
            "plain" => Mage::helper('nuntius')->__('Plain'),
            "crammd5" => Mage::helper('nuntius')->__('CRAM-MD5')
        );
    }

}
