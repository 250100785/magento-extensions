<?php

/**
 * Licentia Nuntius - Advanced Email and SMS Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Service_Source_Lists {

    public function toOptionArray() {

        $lists = Mage::getModel('nuntius/lists')->getCollection();

        if ($lists->count() == 0) {
            return array(
                "0" => Mage::helper('nuntius')->__('None. Please add one'),
            );
        } else {

            $return = array();
            foreach ($lists as $list) {
                $return[] = array('value' => $list->getId(), 'label' => $list->getName());
            }

            return $return;
        }
    }

}
