<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Service_Source_Services  {

    public function toOptionArray() {
        return array(
            array('label' => 'Disabled', 'value' => 'none'),
            array('label' => 'E-Goi', 'value' => 'egoi'),
            array('label' => 'MailGun', 'value' => 'mailgun'),
            array('label' => 'MailJet', 'value' => 'mailjet'),
            array('label' => 'Custom SMTP Server', 'value' => 'smtp'),
        );
    }
}