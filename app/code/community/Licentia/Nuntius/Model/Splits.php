<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Splits extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/splits');
    }

    public static function getTestingOptions() {
        return array(
            'sender' => Mage::helper('nuntius')->__('Sender'),
            'subject' => Mage::helper('nuntius')->__('Subject'),
            'message' => Mage::helper('nuntius')->__('Message'),
            'sender_subject_message' => Mage::helper('nuntius')->__('Sender + Subject + Message'),
            'sender_subject' => Mage::helper('nuntius')->__('Sender + Subject'),
            'sender_message' => Mage::helper('nuntius')->__('Sender + Message'),
            'subject_message' => Mage::helper('nuntius')->__('Subject + Message'),
        );
    }

    public function getSubscribersForTest($split) {

        $collectionTemp = Mage::getModel('nuntius/subscribers')
                ->getCollection()
                ->addFieldToSelect('subscriber_id')
                ->addFieldToFilter('list_id', $split->getListId());

        $total = $collectionTemp->count();
        $limit = round($total * $split->getPercentage() / 100);

        unset($collectionTemp);

        $collection = Mage::getModel('nuntius/subscribers')
                ->getCollection()
                ->addFieldToSelect('subscriber_id')
                ->addFieldToFilter('list_id', $split->getListId());

        $subs = Mage::helper('nuntius')->getSubscribersForSegments($split);
        if ($subs) {
            $collection->addFieldToFilter('subscriber_id', array('in' => $subs));
        }

        $collection->setPageSize($limit)
                ->setOrder('subscriber_id', 'ASC');

        $final = array();

        $i = 1;
        foreach ($collection as $subscriber) {
            $a = $i > round($limit / 2) ? 0 : 1;
            $final[$a][] = $subscriber->getId();
            $i++;
        }

        $split->setData('last_subscriber_id', end($final[1]))->save();

        return $final;
    }

    public function getSubscribersForGeneral($split) {

        $collection = Mage::getModel('nuntius/subscribers')->getCollection()
                ->addFieldToFilter('list_id', $split->getListId())
                ->addFieldToFilter('subscriber_id', array('gt' => $split->getLastSubscriberId()));

        $subs = Mage::helper('nuntius')->getSubscribersForSegments($split);
        if ($subs) {
            $collection->addFieldToFilter('subscriber_id', array('in' => $subs));
        }

        return $collection->getAllIds('subscriber_id');
    }

    public function cron() {
        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

        $collectionPercentage = Mage::getModel('nuntius/splits')
                ->getCollection()
                ->addFieldToFilter('sent', 0)
                ->addFieldToFilter('active', 1)
                ->addFieldToFilter('deploy_at', array('lteq' => $date));


        foreach ($collectionPercentage as $split) {
            $recipients = array();
            $subscribers = $this->getSubscribersForTest($split);

            $recipients['a'] = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('subscriber_id')
                    ->addFieldToFilter('subscriber_id', array('in' => $subscribers[0]))
                    ->getAllIds();

            $recipients['b'] = Mage::getModel('nuntius/subscribers')
                    ->getCollection()
                    ->addFieldToSelect('subscriber_id')
                    ->addFieldToFilter('subscriber_id', array('in' => $subscribers[1]))
                    ->getAllIds();

            foreach (array('a', 'b') as $version) {
                $this->_sendCampaignData($split, $version, $recipients[$version]);
            }

            $split->setData('sent', 1)
                    ->setData('recipients_a', implode(',', $recipients['a']))
                    ->setData('recipients_b', implode(',', $recipients['b']))
                    ->save();
        }

        $collectionGeneral = Mage::getModel('nuntius/splits')
                ->getCollection()
                ->addFieldToFilter('sent', 1)
                ->addFieldToFilter('closed', 0)
                ->addFieldToFilter('active', 1)
                ->addFieldToFilter('winner', array('neq' => 'manually'))
                ->addFieldToFilter('send_at', array('lteq' => $date));


        foreach ($collectionGeneral as $split) {
            $subscribers = $this->getSubscribersForGeneral($split);

            $winner = $split->getData('winner');

            if (
                    ($winner == 'views' && $split->getData('views_a') >= $split->getData('views_b')) ||
                    ($winner == 'clicks' && $split->getData('clicks_a') >= $split->getData('clicks_b')) ||
                    ($winner == 'conversions' && $split->getData('conversions_a') >= $split->getData('conversions_b'))
            ) {
                $version = 'a';
            } else {
                $version = 'b';
            }

            $this->_sendCampaignData($split, $version, $subscribers, true);

            $split->setData('closed', 1)->setData('active', 0)->setData('recipients', implode(',', $subscribers))->save();
        }

        return true;
    }

    public function sendManually($split, $version) {
        $subscribers = $this->getSubscribersForGeneral($split);
        $this->_sendCampaignData($split, $version, $subscribers, true);
        $split->setData('closed', 1)->setData('recipients', implode(',', $subscribers))->save();
    }

    protected function _sendCampaignData($split, $version, $recipients, $final = false) {

        $options = explode('_', $split->getData('testing'));
        $message = (in_array('message', $options)) ? $version : 'a';
        $sender = (in_array('sender', $options)) ? $version : 'a';
        $subject = (in_array('subject', $options)) ? $version : 'a';

        $data = array();
        $data['list_id'] = $split->getListId();
        $data['subject'] = $split->getData('subject_' . $subject);
        $data['internal_name'] = '[A/B] ' . $split->getName();
        $data['deploy_at'] = $split->getDeployAt();
        $data['message'] = $split->getData('message_' . $message);
        $data['sender_id'] = $split->getData('sender_id_' . $sender);
        $data['recurring'] = '0';
        $data['auto'] = ($final) ? 0 : 1;
        $data['split_id'] = $split->getId();
        $data['split_version'] = $version;
        $data['split_final'] = ($final) ? 1 : 0;

        $service = Mage::getModel('nuntius/service')->getService();
        $newCampaign = Mage::getModel('nuntius/campaigns')->setData($data)->save();

        $dataSend = array();
        $dataSend['campaign'] = $newCampaign;
        $dataSend['subscribers'] = $recipients;

        $service->setData($dataSend)->buildQueue();

        if ($final == 1) {
            $this->updateStatsForMainSplit($split, $newCampaign);
        }

        return $newCampaign;
    }

    public function updateStatsForMainSplit($split, $campaign) {
        $write = Mage::getSingleton('core/resource')->getConnection('core_write');

        $tables = array('conversions', 'stats', 'coupons');

        $campaigns = Mage::getModel('nuntius/campaigns')
                ->getCollection()
                ->addFieldToFilter('split_id', $split->getId())
                ->addFieldToFilter('split_final', 0);

        foreach ($campaigns as $record) {
            foreach ($tables as $table) {
                $tablename = Mage::getSingleton('core/resource')->getTableName('nuntius/' . $table);
                $write->update($tablename, array('campaign_id' => $campaign->getId()), array('campaign_id = ?' => $record->getId()));
            }

            $campaign->setData('clicks', $record->getData('clicks') + $campaign->getData('clicks'));
            $campaign->setData('unique_clicks', $record->getData('unique_clicks') + $campaign->getData('unique_clicks'));
            $campaign->setData('views', $record->getData('views') + $campaign->getData('views'));
            $campaign->setData('unique_views', $record->getData('unique_views') + $campaign->getData('unique_views'));

            $campaign->save();
        }
    }

    public function getFinalCampaign($split, $field = false) {

        if ($split->getClosed() == 0)
            return false;

        $campaigns = Mage::getModel('nuntius/campaigns')
                ->getCollection()
                ->addFieldToFilter('split_id', $split->getId())
                ->addFieldToFilter('split_final', 1);

        if ($field) {
            $campaigns->addFieldToSelect($field);
        }

        if ($campaigns->count() == 1) {
            return $campaigns->getFirstItem();
        }
        return new Varien_Object;
    }

    public function getWinnerOptions() {
        return array(
            'views' => Mage::helper('nuntius')->__('Views'),
            'clicks' => Mage::helper('nuntius')->__('Clicks'),
            'conversions' => Mage::helper('nuntius')->__('Conversions'),
            'manually' => Mage::helper('nuntius')->__('Manually')
        );
    }

}
