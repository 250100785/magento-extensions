<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
require 'Browscap.php';

use phpbrowscap\Browscap;

class Licentia_Nuntius_Model_Stats extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/stats');
    }

    public function logViews($campaign, $subscriber) {

        if (!$campaign->getId() || !$subscriber->getId())
            return;

        Mage::getModel('nuntius/autoresponders')->newView($subscriber, $campaign);

        return $this->_logData('views', $campaign, $subscriber);
    }

    public function logClicks($campaign, $subscriber) {

        if (!$campaign->getId() || !$subscriber->getId())
            return;

        $session = Mage::getSingleton('customer/session');

        if ($session->getData('nuntius_' . $campaign->getId() . '_click') == true)
            return;

        Mage::getModel('nuntius/autoresponders')->newClick($subscriber, $campaign);

        return $this->_logData('clicks', $campaign, $subscriber);
    }

    protected function _logData($type, $campaign, $subscriber) {

        $list = Mage::getModel('nuntius/lists')->load($campaign->getListId());

        if ($list->getI()) {
            $list->setData($type, $list->getData($type) + 1)->save();
        }

        $isUniqueCollection = Mage::getModel('nuntius/stats')->getCollection()
                ->addFieldToSelect('subscriber_id')
                ->addFieldToFilter('subscriber_id', $subscriber->getId())
                ->addFieldToFilter('type', $type)
                ->setPageSize(1);

        if ($isUniqueCollection->count() != 0) {
            $isUnique = false;
        } else {
            $isUnique = true;
        }

        $bc = new Browscap('./var/tmp');
        try {
            $bResult = $bc->getBrowser();
            if ($bResult->Browser == 'Default Browser') {
                $bResult = false;
            }
        } catch (Exception $e) {
            $bResult = false;
        }

        $ipResult = false;
        if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)) {
            $ip = $_SERVER['REMOTE_ADDR'];
            $json = file_get_contents("http://api.hostip.info/get_json.php?ip=$ip");
            try {
                $ipResult = Zend_Json::decode($json);
            } catch (Exception $e) {
                $ipResult = false;
                Mage::logException($e);
            }
        }
        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

        $data = array();
        $data['campaign_id'] = $campaign->getId();
        $data['subscriber_id'] = $subscriber->getId();
        $data['customer_id'] = $subscriber->getCustomerId();
        $data['event_at'] = $date;
        $data['type'] = $type;
        if ($ipResult) {
            $data['ip'] = $ip;
            $data['country'] = $ipResult['country_name'];
            $data['country_code'] = $ipResult['country_code'];
            $data['city'] = $ipResult['city'];
        }
        if ($bResult) {
            $data['platform'] = $bResult->Platform;
            $data['browser'] = $bResult->Browser;
            $data['version'] = $bResult->Version;
        }

        Mage::getModel('nuntius/stats')->setData($data)->save();

        $campaign->setData($type, $campaign->getData($type) + 1);
        if ($isUnique) {
            $campaign->setData('unique_' . $type, $campaign->getData('unique_' . $type) + 1);
        }
        $campaign->save();


        if ($campaign->getParentId()) {

            $parent = Mage::getModel('nuntius/campaigns')->load($campaign->getParentId());
            if ($parent->getId()) {
                $parent->setData($type, $parent->getData($type) + 1);
                if ($isUnique) {
                    $parent->setData('unique_' . $type, $parent->getData('unique_' . $type) + 1);
                }
                $parent->save();
            }
        }

        if ($campaign->getSplitId()) {
            $split = Mage::getModel('nuntius/splits')->load($campaign->getSplitId());
            if ($split->getId()) {
                $split->setData($type . '_' . $campaign->getSplitVersion(), $split->getData($type . '_' . $campaign->getSplitVersion()) + 1);
                $split->save();
            }
        }

        $subscriber->setData($type, $subscriber->getData($type) + 1)->save();
    }

    public function loadCampaign() {

        return $this->getCollection();
    }

    public function randomStats() {
        $campaigns = Mage::getModel('nuntius/campaigns')->getCollection()->getAllIds();
        $subscribers = Mage::getModel('nuntius/subscribers')->getCollection()->getAllIds();
        $locale = new Zend_Locale('en_US');
        $countries = ($locale->getTranslationList('Territory', 'en', 2));
        asort($countries, SORT_LOCALE_STRING);

        $types = array(0 => 'views', 1 => 'clicks');

        for ($a = 0; $a <= 10000; $a++) {
            $date = rand(2013, 2014) . '-' . str_pad(rand(1, 12), 2, '0', STR_PAD_LEFT) . '-' . str_pad(rand(1, 28), 2, '0', STR_PAD_LEFT) . ' ' . str_pad(rand(0, 23), 2, '0', STR_PAD_LEFT) . ':' . str_pad(rand(0, 59), 2, '0', STR_PAD_RIGHT) . ':' . str_pad(rand(0, 59), 2, '0', STR_PAD_RIGHT);

            $data = array();
            $data['campaign_id'] = $campaigns[array_rand($campaigns)];
            $data['subscriber_id'] = $subscribers[array_rand($subscribers)];
            $data['event_at'] = $date;
            $data['type'] = $types[array_rand($types)];
            $data['country'] = $countries[array_rand($countries)];
            #$data['city'] = $countries[array_rand($countries)];

            Mage::getModel('nuntius/stats')->setData($data)->save();
        }
    }

    public function clearOldStats() {

        $days = (int) Mage::getStoreConfig('nuntius/config/stats');
        if ($days == 0) {
            return;
        }

        $date = Mage::app()->getLocale()->date()->subDay($days)->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATE);

        $stats = Mage::getModel('nuntius/stats')->getCollection()
                ->addFieldToFilter('event_at', array('lt' => $date));

        foreach ($stats as $stat) {
            $stat->delete();
        }

        return;
    }

}
