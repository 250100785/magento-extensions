<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Subscribers extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/subscribers');
    }

    public function getUnsubscriptionLink() {
        return Mage::helper('nuntius/newsletter')->getUnsubscribeUrl($this);
    }

    public function getName() {

        $name = $this->getData('firstname') . ' ' . $this->getData('lastname');

        if (strlen($name) == 1) {
            $name = substr($this->getEmail(), 0, stripos($this->getEmail(), '@'));
        }

        return $name;
    }

    public function getFirstname() {
        return $this->getData('firstname');
    }

    public function getLastname() {
        return $this->getData('lastname');
    }

    public function getEmail() {
        return $this->getData('email');
    }

    public function getDob() {
        return $this->getData('birth_date');
    }

    public function findCustomer($value, $attribute = 'entity_id') {
        $customers = Mage::getModel('customer/customer')
                ->getCollection()
                ->addAttributeToSelect('firstname')
                ->addAttributeToSelect('lastname')
                ->addAttributeToSelect('store_id')
                ->addAttributeToSelect('dob')
                ->addAttributeToFilter($attribute, $value);

        if ($customers->count() == 1) {
            $customer = $customers->getFirstItem();

            return $customer;
        }

        return false;
    }

    public function importCoreNewsletterSubscribers() {

        $news = Mage::getModel('newsletter/subscriber')
                ->getCollection()
                ->addFieldToFilter('subscriber_status', 1);

        foreach ($news as $subscriber) {
            if (!$subscriber->getStoreId())
                continue;

            $data = array();
            $list = Mage::getModel('nuntius/lists')->getListForStore($subscriber->getStoreId());

            if (!$list->getId()) {
                continue;
            }

            if ($this->subscriberExists('email', $subscriber->getSubscriberEmail(), $list->getId())) {
                continue;
            }

            $customer = $this->findCustomer($subscriber->getCustomerId());

            $data['email'] = $subscriber->getData('subscriber_email');
            $data['status'] = 1;
            $data['store_id'] = $subscriber->getData('store_id');

            if ($subscriber->getCustomerId()) {
                $data['customer_id'] = $subscriber->getCustomerId();
            }

            if ($customer) {
                $data['email'] = $customer->getData('email');
                $data['firstname'] = $customer->getData('firstname');
                $data['lastname'] = $customer->getData('lastname');
            }

            $data['list_id'] = $list->getId();

            try {
                Mage::getModel('nuntius/subscribers')->setData($data)->save();
            } catch (Exception $e) {
                Mage::logException($e);
                #Mage::getSingleton('adminhtml/session')->addNotice($e->getMessage());
            }
        }
    }

    public function getCountryList() {

        $collection = Mage::getModel('nuntius/subscribers')->getCollection();
        $collection->getSelect()->reset('columns')->columns(array('distinct' => new Zend_Db_Expr('DISTINCT(country)')));


        $result = $collection->getData();
        $list = array();

        foreach ($result as $country) {
            if (strlen($country['distinct']) == 0)
                continue;

            $list[$country['distinct']] = $country['distinct'];
        }

        return $list;
    }

    public function subscriberExists($field, $value, $listId) {

        $model = Mage::getModel('nuntius/subscribers')
                ->getCollection()
                ->addFieldToFilter($field, $value)
                ->addFieldToFilter('list_id', $listId);

        if ($model->count() != 1) {
            return false;
        }

        return $model->getFirstItem();
    }

    public function save() {

        $data = $this->getData();

        $customer = $this->findCustomer($data['email'], 'email');

        if ($customer) {
            $data['customer_id'] = $customer->getId();
            $data['birth_date'] = $customer->getData('dob');
            $data['firstname'] = $customer->getData('firstname');
            $data['lastname'] = $customer->getData('lastname');

            if ($customer->getStoreId()) {
                $data['store_id'] = $customer->getStoreId();
            }
        }
        $this->addData($data);

        if (strlen($this->getCode()) == 0) {
            $this->setData('code', md5($this->getId() . $this->getEmail()));
        }

        if (!$this->getId()) {
            $data['add_date'] = now();

            $list = Mage::getModel('nuntius/lists')->load($this->getListId());
            if ($list->getId()) {
                if ($this->getStatus() == 1) {
                    $list->setData('subscribers', $list->getData('subscribers') + 1)->save();
                }
                if ($this->getStatus() == 0) {
                    $list->setData('subscribers', $list->getData('subscribers') - 1)->save();
                }
            }
        }


        $core = array();
        $core['subscriber_email'] = $this->getData('email');
        $core['subscriber_status'] = ($this->getStatus() == 1) ? 1 : 3;
        $core['subscriber_confirm_code'] = $this->getCode();
        $core['store_id'] = $this->getData('store_id');
        if ($data['customer_id']) {
            $core['customer_id'] = $data['customer_id'];
        }
        if (!Mage::registry('nuntius_core_first')) {
            Mage::unregister('nuntius_core_save');
            Mage::register('nuntius_core_save', true);

            Mage::getModel('newsletter/subscriber')
                    ->loadByEmail($this->getData('email'))
                    ->addData($core)
                    ->setImportMode(true)
                    ->save();
        }
        return parent::save();
    }

    public function getSubscribersInfo($field, $ids, $listId) {

        $model = $this->getCollection()
                ->addFieldToSelect($field)
                ->addFieldToFilter('list_id', $listId)
                ->addFieldToFilter('customer_id', array('in' => $ids));

        $result = array();

        foreach ($model as $subscriber) {
            if (strlen($subscriber->getData($field)) > 0)
                $result[] = $subscriber->getData($field);
        }

        return $result;
    }

    public function getSubscribersForList($listId) {
        return $this->getCollection()
                        ->addFieldToFilter('status', 1)
                        ->addFieldToFilter('list_id', $listId);
    }

    public function removeCustomerFromList($customerId, $listId) {

        $customers = Mage::getModel('nuntius/subscribers')
                ->getCollection()
                ->addFieldToSelect('subscriber_id')
                ->addFieldToFilter('list_id', $listId)
                ->addFieldToFilter('customer_id', $customerId);

        if ($customers->count() != 1)
            return false;

        $customer = $customers->getFirstItem()->getData();

        $data = array();
        $data['list_id'] = $listId;
        $data['subscriber_id'] = $customer['subscriber_id'];

        Mage::getModel('nuntius/subscribers')->load($customer['subscriber_id'])->addData($data)->delete();
    }

    public function addCustomerToList($customerId, $listId) {

        $data = array();

        $customer = $this->findCustomer($customerId);

        if ($customer) {
            $data['status'] = '1';
            $data['store_id'] = $customer->getStoreId();
            $data['customer_id'] = $customerId;
            $data['email'] = $customer->getData('email');
            $data['birth_date'] = $customer->getData('dob');
            $data['firstname'] = $customer->getData('firstname');
            $data['lastname'] = $customer->getData('lastname');

            if ($this->subscriberExists('email', $customer->getEmail(), $listId)) {
                return true;
            }
        } else {
            return false;
        }

        $data['list_id'] = $listId;
        return Mage::getModel('nuntius/subscribers')->setData($data)->save();
    }

    public function updateFromNewsletterCore($event) {

        if (!Mage::registry('nuntius_core_save')) {
            Mage::unregister('nuntius_core_first');
            Mage::register('nuntius_core_first', true);
        }

        if (Mage::registry('nuntius_core_save')) {
            return;
        }
        $subscriber = $event->getDataObject();
        $storeId = $subscriber->getStoreId();
        $email = $subscriber->getSubscriberEmail();
        $customerId = $subscriber->getCustomerId();

        if ($storeId == 0) {
            return false;
        }

        $list = Mage::getModel('nuntius/lists')->getListForStore($storeId);

        if (!$list->getId()) {
            return false;
        } else {
            $listId = $list->getListId();
        }

        $subscriber->setImportMode(true);

        if ($subscriber->getSubscriberStatus() == 1) {

            if ($customerId) {
                $this->addCustomerToList($customerId, $listId);
            } else {
                $data = array();
                $data['list_id'] = $listId;
                $data['store_id'] = $storeId;
                $data['email'] = $email;
                $this->setData($data);
                $this->save();
            }
        } elseif ($subscriber->getSubscriberStatus() == 3) {

            $sub = $this->getCollection()
                    ->addFieldToFilter('email', $email)
                    ->addFieldToFilter('list_id', $listId);

            if ($sub->count() == 1) {
                $sub->getFirstItem()->setData('status', 0)->save();
            }
        }
    }

    public function import($file, $form) {

        ini_set('auto_detect_line_endings', TRUE);

        $list = Mage::getModel('nuntius/lists')->load($form['list_id']);

        if (!$list->getId()) {
            throw new Model_Core_Exception('List not found');
        }
        $storeId = $list->getStoreId();

        $row = 1;
        $new = 0;
        $updated = 0;
        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $form['separator'])) !== FALSE) {

                if ($row == 1) {

                    foreach ($data as $key => $tData) {
                        $data[$key] = preg_replace('/\W/si', "", $tData);
                    }

                    $data = array_map('strtolower', $data);
                    $map = $data;
                    $row++;
                    continue;
                }

                $data = array_combine($map, array_map('trim', $data));

                if (count($data) < 2) {
                    throw new Exception('Invalid column count');
                }

                if (!isset($data['email'])) {
                    throw new Exception('Email column not found');
                }

                $subscriber = Mage::getModel('nuntius/subscribers')
                        ->getCollection()
                        ->addFieldToFilter('email', $data['email'])
                        ->addFieldToFilter('list_id', $form['list_id']);

                if ($subscriber->count() == 0) {
                    $subscriber = Mage::getModel('nuntius/subscribers');
                    $new++;
                } else {
                    $updated++;
                    $subscriber = $subscriber->getFirstItem();
                }

                $data['list_id'] = $form['list_id'];

                if (!isset($data['store_id'])) {
                    $data['store_id'] = $storeId;
                }

                $subscriber->addData($data)->save();

                $row++;
            }
            fclose($handle);
        }

        return array('added' => $new, 'updated' => $updated);
    }

    public function updateSendTime() {

        $subscribers = Mage::getModel('nuntius/subscribers')
                ->getCollection()
                ->addFieldToSelect('subscriber_id');

        foreach ($subscribers as $subscriber) {

            $conv = Mage::getModel('nuntius/conversions')
                    ->getCollection()
                    ->addFieldToSelect('created_at')
                    ->addTimeToSelect()
                    ->setOrder('count_created_at', 'DESC')
                    ->setPageSize(2)
                    ->addFieldToFilter('subscriber_id', $subscriber->getId());

            $hour = false;
            foreach ($conv as $c) {

                if ($c->getData('created_at') == 0) {
                    continue;
                }
                $hour = $c->getData('created_at');
                break;
            }

            if ($hour === false) {
                $stats = Mage::getModel('nuntius/stats')
                        ->getCollection()
                        ->addFieldToFilter('subscriber_id', $subscriber->getId())
                        ->addFieldToSelect('event_at')
                        ->addTimeToSelect()
                        ->setOrder('count_event_at', 'DESC')
                        ->setPageSize(2);

                $hour = false;
                foreach ($stats as $c) {

                    if ($c->getData('event_at') == 0) {
                        continue;
                    }
                    $hour = $c->getData('event_at');
                    break;
                }
            }

            if ($hour === false) {
                $hour = -1;
            }

            $subscriber->setData('send_time', $hour)->save();
        }
    }

    public function delete() {

        $email = $this->getEmail();

        if (!$this->getEmail()) {
            $this->load($this->getId());
            $email = $this->getEmail();
        }

        $core = Mage::getModel('newsletter/subscriber')->loadByEmail($email);

        if ($core->getId()) {
            $core->delete();
        }

        $list = Mage::getModel('nuntius/lists')->load($this->getListId());
        if ($list->getId()) {
            $list->setData('subscribers', $list->getData('subscribers') - 1)->save();
        }
        return parent::delete();
    }

}
