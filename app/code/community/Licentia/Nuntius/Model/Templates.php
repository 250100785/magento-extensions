<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *  


 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International 
 */
class Licentia_Nuntius_Model_Templates extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/templates');
    }

    public function getOptionArray() {

        $list = Mage::getModel('nuntius/templates')
                ->getCollection()
                ->addFieldToSelect('template_id')
                ->addFieldToSelect('name');

        $result = array();

        foreach ($list as $template) {
            $result[] = array('value' => $template->getId(), 'label' => $template->getName());
        }

        return $result;
    }

}
