<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Unsubscribes extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/unsubscribes');
    }

    /**
     * Logs unsubscribes
     * @param Licentia_Nuntius_Model_Campaigns $campaign
     * @param Licentia_Nuntius_Model_Subscribers $subscriber
     * @return Licentia_Nuntius_Model_Unsubscribes
     */

    public function unsubscribe(Licentia_Nuntius_Model_Campaigns $campaign,
            Licentia_Nuntius_Model_Subscribers $subscriber) {

        $unsubs = Mage::getModel('nuntius/unsubscribes')
                ->getCollection()
                ->addFieldToFilter('campaign_id', $campaign->getId())
                ->addFieldToFilter('email', $subscriber->getEmail());

        if ($unsubs->count() == 0) {

            $campaign->setUnsubscribes($campaign->getData('unsubscribes') + 1)->save();

            $data = array();
            $data['campaign_id'] = $campaign->getId();
            $data['email'] = $subscriber->getEmail();
            $data['unsubscribed_at'] = now();
            $this->setData($data)->save();
        }

        return $this;
    }

}
