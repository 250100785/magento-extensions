<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Model_Urls extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init('nuntius/urls');
    }

    public function logUrl($campaign, $subscriber, $url) {

        if (!$campaign->getId() || !$subscriber->getId()) {
            return;
        }

        $links = Mage::getModel('nuntius/links')->getCollection()
                ->addFieldToFilter('campaign_id', $campaign->getId())
                ->addFieldToFilter('link', $url);

        if ($links->count() > 0) {
            $link = $links->getFirstItem();
            $link->setData('clicks', $link->getData('clicks') + 1)->save();
        } else {
            $data = array();
            $data['link'] = $url;
            $data['campaign_id'] = $campaign->getId();
            $data['clicks'] = 1;
            $link = Mage::getModel('nuntius/links')->setData($data)->save();
        }

        $data = array();
        $data['campaign_id'] = $campaign->getId();
        $data['subscriber_id'] = $subscriber->getId();
        $data['subscriber_firstname'] = $subscriber->getFirstname();
        $data['subscriber_lastname'] = $subscriber->getLastname();
        $data['subscriber_email'] = $subscriber->getEmail();
        $data['customer_id'] = $subscriber->getCustomerId();
        $data['url'] = $url;
        $data['link_id'] = $link->getId();
        $data['visit_at'] = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);
        $this->setData($data)->save();

        return true;
    }

}
