<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_AutorespondersController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/autoresponders');

        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Autoresponders'));

        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_autoresponders'));
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Autoresponders'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/autoresponders')->load($id);

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('Autoresponder does not exist'));
                $this->_redirect('*/*/');
                return;
            }

            if (Mage::helper('nuntius')->magna()) {
                if (!$model->getData('segments_ids')) {
                    $model->setData('segments_ids', '0');
                }
                $model->setData("segments_ids", explode(',', $model->getData('segments_ids')));
            }
        }


        $data = $this->_getSession()->getFormData();

        if (!empty($data)) {
            $model->addData($data);
        }
        Mage::register('current_autoresponder', $model);

        $this->_title($model->getId() ? $model->getName() : $this->__('New'));

        $this->_initAction();

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_autoresponders_edit'))
                ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_autoresponders_edit_tabs'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function defaultTemplateAction() {
        $templateCode = $this->getRequest()->getParam('code');

        $template = Mage::getModel('nuntius/templates')->load($templateCode);

        if (!$template->getId())
            return;

        $template->setData(array('message' => $template->getMessage()));

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($template->getData()));
    }

    public function saveAction() {

        if ($this->getRequest()->getPost()) {

            $data = $this->getRequest()->getPost();
            $data = $this->_filterDates($data, array('from_date', 'to_date'));

            $id = $this->getRequest()->getParam('id');

            $model = Mage::getModel('nuntius/autoresponders');
            try {
                if ($id) {
                    $model->setId($id);
                }

                if (Mage::helper('nuntius')->magna()) {
                    if (array_search(0, $data['segments_ids']) !== false) {
                        unset($data['segments_ids'][array_search(0, $data['segments_ids'])]);
                    }
                    if (count($data['segments_ids']) > 0) {
                        $data['segments_ids'] = implode(',', $data['segments_ids']);
                    } else {
                        unset($data['segments_ids']);
                    }
                }

                $data['product'] = trim($data['product']);

                $model->addData($data);


                if ($model->getData('event') == 'order_product') {
                    $product = Mage::getModel('catalog/product')->load($model->getData('product'));

                    if (!$product->getId()) {
                        throw new Mage_Core_Exception('Product Not Found');
                    }
                }

                $model->save();

                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess($this->__('The Autoresponder has been saved.'));

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);

                if ($this->getRequest()->getParam('id')) {
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                } else {
                    $this->_redirect('*/*/new');
                }
                return;
            } catch (Exception $e) {
                if (get_class($e) != 'Exception') {
                    $this->_getSession()->addError($e->getMessage());
                } else {
                    $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                }
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/new', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {

        if ($id = $this->getRequest()->getParam('id')) {
            try {

                $model = Mage::getModel('nuntius/autoresponders');
                $model->load($id);
                $model->delete();

                $this->_getSession()->addSuccess($this->__('The Autoresponder has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                if (get_class($e) != 'Exception') {
                    $this->_getSession()->addError($e->getMessage());
                } else {
                    $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                }
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find a Autoresponder to delete.'));
        $this->_redirect('*/*/');
    }

}
