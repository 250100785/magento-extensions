<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_CampaignsController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/campaigns');

        $service = Mage::getModel('nuntius/service')->getService()->validateEnvironmentQuick();

        if (!$service) {
            $this->_redirect('*/system_config/edit', array('section' => 'nuntius'));
        }
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Campaigns'));
        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_campaigns'));

        $this->_addBreadcrumb($this->__('Campaigns'), $this->__('Campaigns'))
                ->renderLayout();
    }

    public function previewAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Campaigns'));
        $this->_initAction();

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/campaigns')->load($id);
        Mage::register('current_campaign', $model);

        $this->_addBreadcrumb($this->__('Campaigns'), $this->__('Campaigns'))
                ->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Campaigns'));
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/campaigns')->load($id);

        $followups = Mage::getModel('nuntius/followup')->getCollection()
                ->addFieldToFilter('campaign_id', $model->getId());

        Mage::register('current_followup', $followups);

        if ($model->getId() || $id == false) {

            $model->setData('specific_dates', explode(',', $model->getData('specific_dates')));

            if ($model->getStatus() == 'finished') {
                $this->_getSession()->addNotice($this->__("This campaign is now closed. You can't modify it. Click on 'Duplicate & Save' to duplicate it and edit."));
            }

            if (Mage::helper('nuntius')->magna()) {
                if (!$model->getData('segments_ids')) {
                    $model->setData('segments_ids', '0');
                }
                $model->setData("segments_ids", explode(',', $model->getData('segments_ids')));
            }
            $model->setData("recurring_daily", explode(',', $model->getData('recurring_daily')));

            $data = $this->_getSession()->getFormData();

            if (!empty($data)) {
                $model->addData($data);
            }
            Mage::register('current_campaign', $model);

            $this->_title($model->getId() ? $model->getInternalName() : $this->__('New'));

            $this->_initAction();

            $this->_addBreadcrumb($this->__('Campaigns'), $this->__('Campaigns'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_campaigns_edit'))
                    ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_campaigns_edit_tabs'));
            $this->renderLayout();
        } else {
            $this->_getSession()->addError($this->__('Campaign does not exist'));
            $this->_redirect('*/*/');
            return;
        }
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridconvAction() {
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('nuntius/campaigns')->load($id);

        if ($model->getId()) {
            Mage::register('current_campaign', $model);

            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function linksgridAction() {
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('nuntius/campaigns')->load($id);

        if ($model->getId()) {
            Mage::register('current_campaign', $model);
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function queueDeleteAction() {
        $queues = $this->getRequest()->getParam('ids');
        $changesNum = 0;

        if (!is_array($queues)) {
            $this->_getSession()->addError($this->__('Please select item(s).'));
        } else {
            try {
                foreach ($queues as $queue) {
                    Mage::getModel('nuntius/queue')->load($queue)->delete();
                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were deleted.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function queuegridAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/campaigns')->load($id);
        if ($model->getId()) {
            Mage::register('current_campaign', $model);
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function archivegridAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/campaigns')->load($id);
        if ($model->getId()) {
            Mage::register('current_campaign', $model);
            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function saveAction() {

        if ($this->getRequest()->getPost()) {

            $data = $this->getRequest()->getPost();

            $id = $this->getRequest()->getParam('id');
            $data = $this->_filterDateTime($data, array('deploy_at'));

            if ($this->getRequest()->getParam('op') == 'send') {
                $data['deploy_at'] = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);
            }

            $data = $this->_filterDates($data, array('recurring_first_run', 'run_until'));


            if (!isset($data['recurring_daily'])) {
                $data['recurring_daily'] = range(0, 6);
            }

            $data['list_id'] = $data['list_id'];
            $data['recurring_daily'] = implode(',', $data['recurring_daily']);

            if (Mage::helper('nuntius')->magna()) {
                if (array_search(0, $data['segments_ids']) !== false) {
                    unset($data['segments_ids'][array_search(0, $data['segments_ids'])]);
                }
                if (count($data['segments_ids']) > 0) {
                    $data['segments_ids'] = implode(',', $data['segments_ids']);
                } else {
                    unset($data['segments_ids']);
                }
            }

            $model = Mage::getModel('nuntius/campaigns');

            try {

                if (trim(strlen($data['url'])) > 0) {
                    if (!filter_var($data['url'], FILTER_VALIDATE_URL)) {
                        throw new Mage_Core_Exception($this->__('Invalid URL'));
                    }
                    $content = file_get_contents($data['url']);
                    if ($content === false) {
                        throw new Mage_Core_Exception($this->__('Can not fetch URL content'));
                    }
                    unset($content);
                }

                if (stripos($data['message'], '{{var subscriber.getUnsubscriptionLink()}}') === false) {
                    throw new Mage_Core_Exception($this->__('Unsubscription Link is mandatory'));
                }

                if ($id && $this->getRequest()->getParam('op') != 'duplicate') {
                    $model->setId($id);
                }
                if ($this->getRequest()->getParam('op') == 'duplicate') {
                    $data['deploy_at'] = Mage::app()->getLocale()->date()->addDay(6)->get('yyyy-MM-dd');
                    $data['status'] = 'standby';
                    $data['clicks'] = 0;
                    $data['unique_clicks'] = 0;
                    $data['views'] = 0;
                    $data['unique_views'] = 0;
                    $data['unsent'] = 0;
                    $data['sent'] = 0;
                    $data['bounces'] = 0;
                    $data['unsubscribes'] = 0;
                    unset($data['run_times_left']);
                    $data['conversions_number'] = 0;
                    $data['conversions_amount'] = 0;
                    $data['conversions_Average'] = 0;
                }

                if ($model->getId()) {
                    if ($data['recurring'] != '0') {
                        $followupData['active'] = '0';
                    }
                    $followup = Mage::getModel('nuntius/followup')->load($model->getId(), 'campaign_id');
                    if ($followup->getId()) {
                        $followup->setData($followupData)->save();
                    }
                }

                if (!isset($data['deploy_at']) || strlen($data['deploy_at']) == 0) {
                    $data['deploy_at'] = $data['recurring_first_run'] . ' ' . str_pad($data['recurring_time'], 2, 0, STR_PAD_LEFT) . '00:00';
                }

                $model->addData($data);
                $model->save();

                $this->_getSession()->setFormData(false);

                if ($this->getRequest()->getParam('op') == 'send') {
                    $this->_redirect('*/*/send', array('id' => $model->getId()));
                    return;
                }

                if ($this->getRequest()->getParam('op') == 'duplicate') {
                    $this->_getSession()->addSuccess($this->__('The campaign has been duplicated. You are now working on the duplicated one'));
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                } else {

                    $this->_getSession()->addSuccess($this->__('The campaign has been saved.'));
                }

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }

                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);

                if ($this->getRequest()->getParam('id')) {
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                } else {
                    $this->_redirect('*/*/new');
                }

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred while saving the campaign data. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/new', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {

        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('nuntius/campaigns');
                $model->load($id);
                $model->delete();

                $this->_getSession()->addSuccess($this->__('The campaign has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred while deleting the campaign. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find a campaign to delete.'));
        $this->_redirect('*/*/');
    }

    public function defaultTemplateAction() {
        $templateCode = $this->getRequest()->getParam('code');

        $template = Mage::getModel('nuntius/templates')->load($templateCode);

        if (!$template->getId())
            return;

        $template->setData(array('message' => $template->getMessage()));

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($template->getData()));
    }

    public function sendAction() {

        $id = $this->getRequest()->getParam('id');
        $campaign = Mage::getModel('nuntius/campaigns')->load($id);

        $date = Mage::app()->getLocale()->date()->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

        try {
            $campaign->setData('deploy_at', $date)->save();
            $this->_getSession()->addSuccess($this->__('Campaign deployment initiated...'));
            $this->_redirect('*/*/');
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::logException($e);
            $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_redirect('*/*/', array('id' => $this->getRequest()->getParam('id')));
        }
    }

    public function cancelAction() {

        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('nuntius/campaigns');
                $model->load($id)->setData('status', 'finished')->save();

                $this->_getSession()->addSuccess($this->__('The campaign has been canceled.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred while canceling the campaign. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find a campaign to cancel.'));
        $this->_redirect('*/*/');
    }

    public function conversionsAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Campaigns'))->_title($this->__('Conversions'));

        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('nuntius/campaigns')->load($id);

        if ($model->getId()) {
            Mage::register('current_campaign', $model);

            $this->_initAction();
            $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_campaigns_conversions'));
            $this->renderLayout();
        } else {
            $this->_getSession()->addError($this->__('Campaign does not exist'));
            $this->_redirect('*/*/');
        }
    }

}
