<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_FollowupController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/followup');
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Follow Up'));

        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_followup'));
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Follow Up'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/followup')->load($id);

        if (!$model->getId() && $cid = $this->getRequest()->getParam('cid')) {
            $campaign = Mage::getModel('nuntius/campaigns')->load($cid);

            if (!$campaign->getId()) {
                $this->_getSession()->addError($this->__('Campaign not found'));
                $this->_redirect('*/*/');
                return;
            }

            Mage::register('current_campaign', $campaign);
        }


        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('Follow Up does not exist'));
                $this->_redirect('*/*/');
                return;
            }
            if (Mage::helper('nuntius')->magna()) {
                if (!$model->getData('segments_ids')) {
                    $model->setData('segments_ids', '0');
                }
                $model->setData("segments_ids", explode(',', $model->getData('segments_ids')));
            }
        }

        $data = $this->_getSession()->getFormData();

        if (!empty($data)) {
            $model->addData($data);
        }
        Mage::register('current_followup', $model);

        if ($model->getId()) {
            $campaign = Mage::getModel('nuntius/campaigns')->load($model->getCampaignId());
            Mage::register('current_campaign', $campaign);
        } else {
            $data['campaign_id'] = $cid;
        }

        $model->setData('recipients_options', explode(',', $model->getData('recipients_options')));

        $this->_title($model->getId() ? $model->getName() : $this->__('New'));

        $this->_initAction();

        $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_followup_edit'))
                ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_followup_edit_tabs'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function defaultTemplateAction() {
        $templateCode = $this->getRequest()->getParam('code');

        $template = Mage::getModel('nuntius/templates')->load($templateCode);

        if (!$template->getId())
            return;

        $template->setData(array('message' => $template->getMessage()));

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($template->getData()));
    }

    public function saveAction() {

        if ($this->getRequest()->getPost()) {

            $data = $this->getRequest()->getPost();
            $id = $this->getRequest()->getParam('id');
            $cid = $this->getRequest()->getParam('cid');

            $model = Mage::getModel('nuntius/followup');

            try {
                if ($id) {
                    $model->setId($id);
                }


                if (!$model->getId()) {
                    $campaign = Mage::getModel('nuntius/campaigns')->load($cid);
                    $data['campaign_id'] = $cid;
                } else {
                    $tmpFollow = Mage::getModel('nuntius/followup')->load($model->getId());
                    $campaign = Mage::getModel('nuntius/campaigns')->load($tmpFollow->getCampaignId());
                }

                if (!$campaign->getId()) {
                    $this->_getSession()->addError($this->__('Campaign not found'));
                    $this->_redirectReferer();
                    return;
                }

                if ($campaign->getRecurring() != '0' && $data['active'] != '0') {
                    $data['active'] = '0';
                    $this->_getSession()->addNotice($this->__("You can't create Follow Ups for recurring campaigns. Follow Up is inactive."));
                }

                if (Mage::helper('nuntius')->magna()) {
                    if (array_search(0, $data['segments_ids']) !== false) {
                        unset($data['segments_ids'][array_search(0, $data['segments_ids'])]);
                    }
                    if (count($data['segments_ids']) > 0) {
                        $data['segments_ids'] = implode(',', $data['segments_ids']);
                    } else {
                        unset($data['segments_ids']);
                    }
                }


                $date = Mage::app()
                        ->getLocale()
                        ->date()
                        ->setTime($campaign->getData('deploy_at'), Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME)
                        ->setDate($campaign->getData('deploy_at'), Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME)
                        ->addDay($data['days']);

                $data['send_at'] = $date->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);
                $data['recipients_options'] = implode(',', $data['recipients_options']);

                $model->addData($data);
                $model->save();

                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess($this->__('The Follow Up has been saved.'));

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);

                if ($this->getRequest()->getParam('id')) {
                    $this->_redirect('*/*/edit', array('cid' => $this->getRequest()->getParam('cid'), 'id' => $this->getRequest()->getParam('id')));
                } else {
                    $this->_redirect('*/*/new');
                }
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/new', array('cid' => $this->getRequest()->getParam('cid'), 'id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {


        if ($id = $this->getRequest()->getParam('id')) {
            try {

                $model = Mage::getModel('nuntius/followup')->load($id);
                $model->delete();

                $this->_getSession()->addSuccess($this->__('The Follow Up has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find a Follow Up to delete.'));
        $this->_redirect('*/*/');
    }

}
