<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_ListsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/lists');

        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Lists'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_lists'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Lists'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/lists');
        Mage::register('current_list', $model);

        if ($id) {
            $model->load($id);
            if (!$model->getListId()) {
                $this->_getSession()->addError($this->__('This list no longer exists.'));
                $this->_redirect('*/*');
                return;
            }
        }

        $this->_title($model->getListId() ? $model->getTitle() : $this->__('New List'));

        // set entered data if was error when we do save
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_lists_edit'))
                ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_lists_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {

        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');

            $model = Mage::getModel('nuntius/lists');

            try {
                if ($id) {
                    $model->setId($id);
                }

                $model->addData($data)->save();

                $this->_getSession()->addSuccess($this->__('List was successfully saved'));
                $this->_getSession()->setFormData(false);

                $store = Mage::getModel('core/config');
                $store->saveConfig('nuntius/config/list_id', $model->getId(), 'stores', $data['store_id']);

                Mage::getConfig()->reinit();
                Mage::app()->reinitStores();


                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                if (get_class($e) != 'Exception') {
                    $this->_getSession()->addError($e->getMessage());
                } else {
                    $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                }
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find List to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {

        if ($id = $this->getRequest()->getParam('id')) {
            try {

                $model = Mage::getModel('nuntius/lists');
                $model->load($id);
                $model->delete();

                $this->_getSession()->addSuccess($this->__('The List has been deleted.'));
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                if (get_class($e) != 'Exception') {
                    $this->_getSession()->addError($e->getMessage());
                } else {
                    $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                }
                Mage::logException($e);
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        $this->_getSession()->addError($this->__('Unable to find List to delete.'));
        $this->_redirect('*/*/');
    }

    public function validateEnvironmentAction() {

        $params = $this->getRequest()->getParams();
        $email = $params['email'];

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->_getSession()->addError($this->__('Please insert a valid Email Address'));
            $this->_redirectReferer();
            return;
        }

        Mage::getModel('nuntius/service')->getService()->validateEnvironment($email);

        $this->_redirectReferer();
    }

}
