<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_ReportsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/reports');

        return $this;
    }

    public function indexAction() {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_reports'));
        $this->renderLayout();
    }

    public function detailAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Reports'))->_title($this->__('Detail'));
        $id = $this->getRequest()->getParam('id');

        $camp = Mage::getModel('nuntius/campaigns')->load($id);

        if (!$camp->getId()) {
            $this->_getSession()->addError($this->__('Campaign does not exist'));
            $this->_redirectReferer();
        }
        if ($camp->getSent() == 0) {
            $this->_getSession()->addError($this->__('No messages have been sent yet from this campaign.'));
            $this->_redirectReferer();
            return;
        }

        if ($camp->getRecurring() != '0') {
            $this->_redirect('*/nuntius_campaigns/edit', array('id' => $camp->getId(), 'tab_id' => 'reports_edit_tabs_children'));
            return;
        }

        $stats = Mage::getModel('nuntius/stats')->loadCampaign($camp);

        Mage::register('current_report_campaign', $camp);
        Mage::register('current_campaign', $camp);
        Mage::register('current_stats', $stats);

        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_reports_detail'))
                ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_reports_detail_tabs'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Export reports grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'reports.csv';
        $content = $this->getLayout()->createBlock('nuntius/adminhtml_reports_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export reports grid to XML format
     */
    public function exportXmlAction() {
        $fileName = 'reports.xml';
        $content = $this->getLayout()->createBlock('nuntius/adminhtml_reports_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

}
