<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_SendersController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/senders');

        return $this;
    }

    public function indexAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Senders'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_senders'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Senders'))->_title($this->__('Edit'));
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('nuntius/senders')->load($id);

        $this->_title($model->getId() ? $model->getName() : $this->__('New'));

        if ($model->getId() || $id == 0) {
            $data = $this->_getSession()->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }
            Mage::register('current_sender', $model);

            $this->_initAction();

            $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_senders_edit'))
                    ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_senders_edit_tabs'));

            $this->renderLayout();
        } else {
            $this->_getSession()->addError($this->__('Template does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();


        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('nuntius/senders');

            try {

                $model->setData($data);

                if ($id) {
                    $model->setId($id);
                }

                $model->save();
                $this->_getSession()->addSuccess($this->__('Sender was successfully saved'));
                $this->_getSession()->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }

                $this->_redirect('*/*/index/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit/id/' . $id);
                return;
            }
        }


        $this->_getSession()->addError($this->__('Unable to find Sender to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {

        if ($this->getRequest()->getParam('id')) {

            $id = $this->getRequest()->getParam('id');

            try {
                $model = Mage::getModel('nuntius/senders');
                $model->setId($id);

                $collection = Mage::getModel('nuntius/campaigns')->getCollection()
                        ->addFieldToFilter('status', array('nin' => array('finished', 'sent')))
                        ->addFieldToFilter('sender_id', $model->getId());

                if ($collection->count() != 0) {
                    throw new Mage_Core_Exception($this->__('You can not delete this sender because there are pending campaigns making use of it'));
                }

                $model->delete();

                $this->_getSession()->addSuccess($this->__('Sender was successfully deleted'));

                $this->_redirect('*/*/edit/id/' . $id);
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/index');
                return;
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

}
