<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_SplitsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/splits');

        return $this;
    }

    public function indexAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Splits A/B'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_splits'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Splits'));
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('nuntius/splits')->load($id);

        $this->_title($model->getId() ? $model->getName() : $this->__('New'));

        $option = $this->getRequest()->getParam('option');
        $options = Licentia_Nuntius_Model_Splits::getTestingOptions();

        if (!$model->getId() && $option && !in_array($option, array_keys($options))) {
            throw new Exception($this->__('Invalid Testing Option'));
        }


        if ($model->getId() || $id == 0) {
            $data = $this->_getSession()->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }
            Mage::register('current_split', $model);

            $this->_initAction();

            if (Mage::helper('nuntius')->magna()) {
                if (!$model->getData('segments_ids')) {
                    $model->setData('segments_ids', '0');
                }
                $model->setData("segments_ids", explode(',', $model->getData('segments_ids')));
            }
            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_splits_edit'))
                    ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_splits_edit_tabs'));

            $this->renderLayout();
        } else {
            $this->_getSession()->addError($this->__('Split Campaign does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        $data = $this->getRequest()->getPost();

        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('nuntius/splits');

            try {

                $data = $this->_filterDateTime($data, array('deploy_at'));

                if (Mage::helper('nuntius')->magna()) {
                    if (array_search(0, $data['segments_ids']) !== false) {
                        unset($data['segments_ids'][array_search(0, $data['segments_ids'])]);
                    }
                    if (count($data['segments_ids']) > 0) {
                        $data['segments_ids'] = implode(',', $data['segments_ids']);
                    } else {
                        unset($data['segments_ids']);
                    }
                }

                $date = Mage::app()->getLocale()->date();
                $date2 = Mage::app()->getLocale()->date();

                $dateDays = $date->setTime($data['deploy_at'], Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME)
                        ->setDate($data['deploy_at'], Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME)
                        ->addDay($data['days']);

                $data['send_at'] = $dateDays->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME);

                $model->setData($data);
                if ($id) {
                    $model->setId($id);
                }

                if ($date2->get(Licentia_Nuntius_Model_Campaigns::MYSQL_DATETIME) > $data['deploy_at'] &&
                        $model->getSend() == 0) {
                    throw new Mage_Core_Exception($this->__('Your deploy date cannot be earlier than now.'));
                }

                $model->save();
                $this->_getSession()->addSuccess($this->__('Split Campaign was successfully saved'));
                $this->_getSession()->setFormData(false);

                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }

                $this->_redirect('*/*/index/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('option' => $data['testing'], 'id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit/id/' . $id);
                return;
            }
        }


        $this->_getSession()->addError($this->__('Unable to find Split Campaign to save'));
        $this->_redirect('*/*/');
    }

    public function sendAction() {
        $id = $this->getRequest()->getParam('id');
        $version = $this->getRequest()->getVersion();

        if (!in_array($version, array('a', 'b'))) {
            $this->_getSession()->addError($this->__('Invalid Version'));
            $this->_redirectReferer();
            return;
        }

        $split = Mage::getModel('nuntius/splits')->load($id);

        if (!$split->getId()) {
            $this->_getSession()->addError($this->__('Unable to find Split Campaign'));
            $this->_redirectReferer();
            return;
        }
        if (
                $split->getClosed() == 1 ||
                $split->getActive() == 0 ||
                $split->getWinner() != 'manually' ||
                $split->getSent() == 0
        ) {

            $this->_getSession()->addError($this->__('Unable to perform action. Please verify all requisites'));
            $this->_redirectReferer();
            return;
        }

        try {
            $split->sendManually($split, $version, true);
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            $this->_redirectReferer();
            return;
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
            Mage::logException($e);
            $this->_redirect('*/*/index');
        }
    }

    public function deleteAction() {

        if ($this->getRequest()->getParam('id')) {

            $id = $this->getRequest()->getParam('id');

            try {
                $model = Mage::getModel('nuntius/splits');
                $model->setId($id)->delete();

                $this->_getSession()->addSuccess($this->__('Split Campaign was successfully deleted'));

                $this->_redirect('*/*/index');
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/index');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function defaultTemplateAction() {
        $templateCode = $this->getRequest()->getParam('code');

        $template = Mage::getModel('nuntius/templates')->load($templateCode);

        if (!$template->getId())
            return;

        $template->setData(array('message_a' => $template->getMessage()));

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($template->getData()));
    }

    public function defaultTemplatebAction() {
        $templateCode = $this->getRequest()->getParam('code');

        $template = Mage::getModel('nuntius/templates')->load($templateCode);

        if (!$template->getId())
            return;

        $template->setData(array('message_b' => $template->getMessage()));

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($template->getData()));
    }

}
