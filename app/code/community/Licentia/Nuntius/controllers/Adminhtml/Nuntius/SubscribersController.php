<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_Adminhtml_Nuntius_SubscribersController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('nuntius/subscribers');

        return $this;
    }

    public function indexAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Subscribers'));

        if ($id = $this->getRequest()->getParam('list_id')) {
            $list = Mage::getModel('nuntius/lists')->load($id);
        } else {
            $list = new Varien_Object;
        }
        Mage::register('current_list', $list);

        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_subscribers'));
        $this->renderLayout();
    }

    public function gridAction() {
        $list = Mage::getModel('nuntius/lists')->load($this->getRequest()->getParam('list_id', 0));
        Mage::register('current_list', $list);

        $this->loadLayout();
        $this->renderLayout();
    }

    public function archivegridAction() {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/subscribers')->load($id);
        Mage::register('current_subscriber', $model);

        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridconvAction() {

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('nuntius/subscribers')->load($id);

        if ($model->getId()) {
            Mage::register('current_subscriber', $model);

            $this->loadLayout();
            $this->renderLayout();
        }
    }

    public function listAction() {
        $this->_title($this->__('Nuntius'))->_title($this->__('Subscribers'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_subscribers'));
        $this->renderLayout();
    }

    public function editAction() {

        $this->_title($this->__('Nuntius'))->_title($this->__('Subscribers'))->_title($this->__('Edit'));
        $id = $this->getRequest()->getParam('id');

        $model = Mage::getModel('nuntius/subscribers')->load($id);

        $this->_title($model->getId() ? $model->getName() : $this->__('New'));

        if ($model->getId() || $id == 0) {
            $data = $this->_getSession()->getFormData(true);
            if (!empty($data)) {
                $model->addData($data);
            }

            Mage::register('current_subscriber', $model);

            $this->_initAction();

            $this->_addBreadcrumb($this->__('Subscriber Manager'), $this->__('Subscriber Manager'));
            $this->_addBreadcrumb($this->__('Subscriber'), $this->__('Subscriber'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_subscribers_edit'))
                    ->_addLeft($this->getLayout()->createBlock('nuntius/adminhtml_subscribers_edit_tabs'));

            $this->renderLayout();
        } else {
            $this->_getSession()->addError($this->__('Subscriber does not exist'));
            $this->_redirect('*/*/');
        }
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {

        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');

            $data['status'] = 1;

            $model = Mage::getModel('nuntius/subscribers');
            $model->addData($data);

            try {

                if ($id) {
                    $model->setId($id);
                }

                $model->save();

                $this->_getSession()->addSuccess($this->__('Subscriber was successfully saved'));
                $this->_getSession()->setFormData(false);


                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));
                    return;
                }
                $this->_redirect('*/*/index/');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit/id/' . $id);
                return;
            }
        }

        $this->_getSession()->addError($this->__('Unable to find subscriber to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {

        if ($this->getRequest()->getParam('id')) {

            try {

                $id = $this->getRequest()->getParam('id');
                $db = Mage::getModel('nuntius/subscribers')->load($id);
                $db->delete();

                $this->_getSession()->addSuccess($this->__('Subscriber was successfully deleted'));

                $this->_redirect('*/*/');
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function massUnsubscribeAction() {
        $subscribers = $this->getRequest()->getParam('subscribers');
        $changesNum = 0;

        if (!is_array($subscribers)) {
            $this->_getSession()->addError($this->__('Please select subscriber(s).'));
        } else {
            try {
                foreach ($subscribers as $subscriber) {
                    Mage::getModel('nuntius/subscribers')->load($subscriber)->setData('status', '0')->save();

                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were updated.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }
    public function massSubscribeAction() {
        $subscribers = $this->getRequest()->getParam('subscribers');
        $changesNum = 0;

        if (!is_array($subscribers)) {
            $this->_getSession()->addError($this->__('Please select subscriber(s).'));
        } else {
            try {
                foreach ($subscribers as $subscriber) {
                    Mage::getModel('nuntius/subscribers')->load($subscriber)->setData('status', '1')->save();

                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were updated.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function massDeleteAction() {
        $subscribers = $this->getRequest()->getParam('subscribers');
        $changesNum = 0;

        if (!is_array($subscribers)) {
            $this->_getSession()->addError($this->__('Please select subscriber(s).'));
        } else {
            try {
                foreach ($subscribers as $subscriber) {
                    Mage::getModel('nuntius/subscribers')->load($subscriber)->delete();
                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were deleted.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }

        $this->_redirectReferer();
    }

    public function importAction() {
        $this->_title($this->__('Subscribers'))->_title($this->__('Import'));

        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('nuntius/adminhtml_subscribers_import'));
        $this->renderLayout();
    }

    public function importSaveAction() {

        if (isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {

            try {
                $path = Mage::getBaseDir() . DS . 'var' . DS . 'tmp' . DS . 'nuntius' . DS;
                $uploader = new Varien_File_Uploader('filename');
                $uploader->setAllowedExtensions(array('csv'));
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $uploader->save($path, $_FILES['filename']['name']);

                $data = $this->getRequest()->getPost();
                $name = $path . $uploader->getUploadedFileName();

                $count = Mage::getModel('nuntius/subscribers')->import($name, $data);

                $this->_getSession()->addSuccess($this->__('A total of <strong>%d</strong> where added and <strong>%d</strong> where updated', $count['added'], $count['updated']));
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/import');
                return;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Export reports grid to CSV format
     */
    public function exportCsvAction() {

        if ($id = $this->getRequest()->getParam('list_id')) {
            $list = Mage::getModel('nuntius/lists')->load($id);
        } else {
            $list = new Varien_Object;
        }
        Mage::register('current_list', $list);
        $fileName = 'subscribers.csv';
        $content = $this->getLayout()->createBlock('nuntius/adminhtml_subscribers_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export reports grid to XML format
     */
    public function exportXmlAction() {

        if ($id = $this->getRequest()->getParam('list_id')) {
            $list = Mage::getModel('nuntius/lists')->load($id);
        } else {
            $list = new Varien_Object;
        }

        Mage::register('current_list', $list);
        $fileName = 'subscribers.xml';
        $content = $this->getLayout()->createBlock('nuntius/adminhtml_subscribers_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function importCoreAction() {
        try {
            Mage::getModel('nuntius/subscribers')->importCoreNewsletterSubscribers();
            $this->_getSession()->addSuccess($this->__('Subscribers Imported.'));
        } catch (Exception $ex) {
            $this->_getSession()->addError($ex->getMessage());
        }

        $this->_redirectReferer();
    }

}
