<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_CampaignController extends Mage_Core_Controller_Front_Action {

    public function userAction() {

        $campaignId = $this->getRequest()->getParam('c');
        $u = $this->getRequest()->getParam('u');
        $sid = $this->getRequest()->getParam('sid');

        $campaign = Mage::getModel('nuntius/campaigns')->load($campaignId);

        if (!$campaign->getId())
            return;

        $model = Mage::getModel('nuntius/subscribers');

        if ($sid) {
            if (filter_var($sid, FILTER_VALIDATE_EMAIL)) {
                $subscriber = $model->load($sid, 'email');
            } else {
                $subscriber = $model->load($sid);
            }
        } else if ($u) {
            $subscriber = $model->load($u);
        } else {
            $subscriber = new Varien_Object;
        }
        if ($campaign->getUrl()) {
            $text = Mage::helper('nuntius')->getContentFromUrl($campaign, $subscriber);
        } else {
            $variables = array();
            $variables['subscriber'] = $subscriber;
            $variables['campaign'] = $campaign;

            $text = Mage::helper('newsletter')
                    ->getTemplateProcessor()
                    ->setVariables($variables)
                    ->filter($campaign->getMessage());
        }
        echo $text;
        die();
    }

    public function goAction() {

        $request = $this->getRequest();
        $subscriberId = $request->getParam('u');
        $campaignId = $request->getParam('c');

        $url = base64_decode($request->getParam('url'));
        Mage::register('nuntius_open_url', $url);

        if (!$campaignId && !$subscriberId) {
            $campaignId('LOCATION:' . $url);
            return;
        }

        $session = Mage::getSingleton('customer/session');
        $session->setNuntiusConversion(true);
        $session->setNuntiusConversionCampaign($campaignId);
        $session->setNuntiusConversionSubscriber($subscriberId);
        $session->setNuntiusConversionUrl($url);

        $campaign = Mage::getModel('nuntius/campaigns')->load($campaignId);
        $subscriber = Mage::getModel('nuntius/subscribers')->load($subscriberId);
        Mage::getModel('nuntius/stats')->logClicks($campaign, $subscriber);
        Mage::getModel('nuntius/urls')->logUrl($campaign, $subscriber, $url);

        $request->setParam('u', null);
        $request->setParam('c', null);

        header('LOCATION: ' . $url);

        exit;
    }

    public function statAction() {
        $camp = $this->getRequest()->getParam('c');
        $u = $this->getRequest()->getParam('u');
        $campaign = Mage::getModel('nuntius/campaigns')->load($camp);

        $user = Mage::getModel('nuntius/subscribers')
                ->getCollection()
                ->addFieldToFilter('list_id', $campaign->getListId())
                ->addFieldToFilter('subscriber_id', $u);

        $subscriber = new Varien_Object();
        if ($user->count() == 1) {
            $subscriber = $user->getFirstItem();
        }

        Mage::getModel('nuntius/stats')->logViews($campaign, $subscriber);

        $im = imagecreatetruecolor(1, 1);
        imagefilledrectangle($im, 0, 0, 99, 99, 0xFFFFFF);
        header('Content-Type: image/gif');

        imagegif($im);
        imagedestroy($im);
        die();
    }

}
