<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Nuntius_SubscriberController extends Mage_Core_Controller_Front_Action {

    public function unsubscribeAction() {
        $camp = $this->getRequest()->getParam('c');
        $id = $this->getRequest()->getParam('id');
        $code = $this->getRequest()->getParam('code');

        $session = Mage::getSingleton('core/session');

        $subscriber = Mage::getModel('nuntius/subscribers')->load($id);

        if ($subscriber->getCode() != $code) {
            Mage::getSingleton('customer/session')->addErrors($this->__('There was a problem with the un-subscription.'));
            $this->_redirect('/');
            return;
        }

        $campaign = Mage::getModel('nuntius/campaigns')->load($camp);
        if ($campaign->getId()) {
            Mage::getModel('nuntius/unsubscribes')->unsubscribe($campaign, $subscriber);
        }

        $coreN = Mage::getModel('newsletter/subscriber')->getCollection()
                ->addFieldToFilter('subscriber_email', $subscriber->getEmail())
                ->addFieldToFilter('store_id', $subscriber->getStoreId());

        if ($coreN->count() == 1) {
            $coreS = $coreN->getFirstItem();

            try {
                $coreS->setCheckCode($coreS->getCode())
                        ->unsubscribe();
                $session->addSuccess($this->__('You have been unsubscribed.'));
            } catch (Mage_Core_Exception $e) {
                $session->addException($e, $e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the un-subscription.'));
            }
        } else {
            try {
                $subscriber->delete();
                $session->addSuccess($this->__('You have been unsubscribed.'));
            } catch (Mage_Core_Exception $e) {
                $session->addException($e, $e->getMessage());
            } catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the un-subscription.'));
            }
        }

        $this->_redirect('/');
    }

}
