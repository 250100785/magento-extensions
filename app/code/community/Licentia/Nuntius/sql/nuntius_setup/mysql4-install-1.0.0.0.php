<?php

/**
 * Licentia Nuntius - Advanced Email Marketing Automation for Magento
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Advanced Email Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
$installer = $this;
$installer->startSetup();

$installer->run("

-- ----------------------------
--  Table structure for `nuntius_autoresponders`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_autoresponders')}`;
CREATE TABLE `{$this->getTable('nuntius_autoresponders')}` (
  `autoresponder_id` int(11) NOT NULL AUTO_INCREMENT,
  `segments_ids` varchar(11) DEFAULT NULL,
  `event` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `send_moment` enum('occurs','after') NOT NULL DEFAULT 'occurs',
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `after_days` smallint(2) DEFAULT NULL,
  `after_hours` smallint(1) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  `product` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `list_id` varchar(255) DEFAULT NULL,
  `sender_id` int(11) NOT NULL,
  `number_subscribers` int(11) DEFAULT NULL,
  `send_once` enum('0','1') DEFAULT '1',
  `search` varchar(255) DEFAULT NULL,
  `search_option` enum('eq','like') DEFAULT 'eq',
  `order_status` varchar(255) DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `cancel_if_order` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`autoresponder_id`,`sender_id`),
  KEY `event` (`event`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_AUTR_CAMPAIGNS` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Autorepsonders lists';

-- ----------------------------
--  Table structure for `nuntius_autoresponders_events`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_autoresponders_events')}`;
CREATE TABLE `{$this->getTable('nuntius_autoresponders_events')}` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `event` varchar(255) DEFAULT NULL,
  `autoresponder_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `subscriber_firstname` varchar(255) DEFAULT NULL,
  `subscriber_lastname` varchar(255) DEFAULT NULL,
  `subscriber_email` varchar(255) DEFAULT NULL,
  `send_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `sent` enum('0','1') NOT NULL DEFAULT '0',
  `sent_at` datetime DEFAULT NULL,
  `data_object_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `autoresponder_id` (`autoresponder_id`),
  KEY `subscriber_id` (`subscriber_id`),
  CONSTRAINT `FK_NUNTIUS_EVENT_AUTR` FOREIGN KEY (`autoresponder_id`) REFERENCES `{$this->getTable('nuntius_autoresponders')}` (`autoresponder_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_EVENT_SUBSCRIBER` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Autoresponders Events';

-- ----------------------------
--  Table structure for `nuntius_bounces`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_bounces')}`;
CREATE TABLE `{$this->getTable('nuntius_bounces')}` (
  `bounce_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`bounce_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `subscriber_id` (`subscriber_id`),
  CONSTRAINT `FK_NUNTIUS_BOUN_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_BOUN_SUBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Bounces List';

-- ----------------------------
--  Table structure for `nuntius_campaigns`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_campaigns')}`;
CREATE TABLE `{$this->getTable('nuntius_campaigns')}` (
  `campaign_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `list_id` int(11) NOT NULL,
  `sender_id` int(255) NOT NULL,
  `segments_ids` varchar(255) DEFAULT NULL,
  `internal_name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text,
  `deploy_at` datetime NOT NULL,
  `recurring_month` varchar(255) DEFAULT NULL,
  `recurring_monthly` varchar(255) DEFAULT NULL,
  `recurring_day` varchar(255) DEFAULT NULL,
  `recurring_daily` varchar(255) DEFAULT NULL,
  `recurring_last_run` datetime DEFAULT NULL,
  `recurring_next_run` datetime DEFAULT NULL,
  `recurring_time` int(11) DEFAULT NULL,
  `recurring_first_run` date DEFAULT NULL,
  `run_until` date DEFAULT NULL,
  `run_times` int(11) DEFAULT NULL,
  `run_times_left` int(11) DEFAULT NULL,
  `end_method` enum('run_until','both','number','any') NOT NULL DEFAULT 'run_until',
  `recurring` varchar(2) DEFAULT '0',
  `auto` enum('0','1') NOT NULL DEFAULT '0',
  `status` enum('standby','finished','running') NOT NULL DEFAULT 'standby',
  `segments_options` enum('merge','intersect') NOT NULL DEFAULT 'merge',
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `conversions_number` int(11) NOT NULL DEFAULT '0',
  `conversions_amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `conversions_average` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `recurring_unique` enum('0','1') DEFAULT '0',
  `url` varchar(255) DEFAULT NULL,
  `clicks` int(11) NOT NULL DEFAULT '0',
  `unique_clicks` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `unique_views` int(11) NOT NULL DEFAULT '0',
  `unsent` int(11) NOT NULL,
  `sent` int(11) NOT NULL DEFAULT '0',
  `bounces` int(11) NOT NULL DEFAULT '0',
  `autoresponder` int(11) DEFAULT '0',
  `autoresponder_recipient` varchar(255) DEFAULT NULL,
  `autoresponder_event` varchar(255) DEFAULT NULL,
  `autoresponder_event_id` int(11) DEFAULT NULL,
  `followup_id` int(11) DEFAULT NULL,
  `split_id` int(11) DEFAULT NULL,
  `split_version` varchar(255) DEFAULT NULL,
  `split_final` tinyint(4) DEFAULT NULL,
  `unsubscribes` int(11) NOT NULL DEFAULT '0',
  `subscriber_time` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`campaign_id`),
  KEY `list_id` (`list_id`),
  KEY `parent_id` (`parent_id`),
  KEY `status` (`status`),
  KEY `recurring` (`recurring`),
  KEY `recurring_next_run` (`recurring_next_run`),
  CONSTRAINT `FK_NUNTIUS_CAMP_CID` FOREIGN KEY (`parent_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_CAMP_LIST` FOREIGN KEY (`list_id`) REFERENCES `{$this->getTable('nuntius_lists')}` (`list_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Campaigns';

-- ----------------------------
--  Table structure for `nuntius_campaigns_followup`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_campaigns_followup')}`;
CREATE TABLE `{$this->getTable('nuntius_campaigns_followup')}` (
  `followup_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `segments_ids` varchar(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `active` enum('0','1') DEFAULT '0',
  `recipients` text,
  `recipients_options` varchar(255) DEFAULT NULL,
  `send_at` datetime DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `sent` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`followup_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_FOLLOW_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Follow up';

-- ----------------------------
--  Table structure for `nuntius_campaigns_links`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_campaigns_links')}`;
CREATE TABLE `{$this->getTable('nuntius_campaigns_links')}` (
  `link_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `conversions_number` int(11) DEFAULT NULL,
  `conversions_amount` decimal(10,4) DEFAULT NULL,
  `conversions_average` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`link_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_LINKS_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Campaigns Links';

-- ----------------------------
--  Table structure for `nuntius_campaigns_splits`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_campaigns_splits')}`;
CREATE TABLE `{$this->getTable('nuntius_campaigns_splits')}` (
  `split_id` int(11) NOT NULL AUTO_INCREMENT,
  `segments_ids` varchar(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `subject_a` varchar(255) DEFAULT NULL,
  `subject_b` varchar(255) DEFAULT NULL,
  `sender_id_a` int(11) DEFAULT NULL,
  `sender_id_b` int(11) DEFAULT NULL,
  `message_a` text,
  `message_b` text,
  `views_a` int(11) NOT NULL DEFAULT '0',
  `views_b` int(11) NOT NULL DEFAULT '0',
  `clicks_a` int(11) NOT NULL DEFAULT '0',
  `clicks_b` int(11) NOT NULL DEFAULT '0',
  `conversions_a` int(11) NOT NULL DEFAULT '0',
  `conversions_b` int(11) NOT NULL DEFAULT '0',
  `days` smallint(3) DEFAULT NULL,
  `list_id` int(11) NOT NULL,
  `deploy_at` datetime DEFAULT NULL,
  `sent` enum('0','1') NOT NULL DEFAULT '0',
  `active` enum('0','1') NOT NULL DEFAULT '0',
  `send_at` datetime DEFAULT NULL,
  `winner` varchar(255) DEFAULT NULL,
  `percentage` varchar(255) DEFAULT NULL,
  `last_subscriber_id` int(11) DEFAULT NULL,
  `closed` enum('0','1') NOT NULL DEFAULT '0',
  `recipients_a` text,
  `recipients_b` text,
  `recipients` text,
  `testing` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`split_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - AB Testing';

-- ----------------------------
--  Table structure for `nuntius_conversions`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_conversions')}`;
CREATE TABLE `{$this->getTable('nuntius_conversions')}` (
  `conversion_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) NOT NULL,
  `subscriber_id` int(11) NOT NULL,
  `link_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `order_date` datetime DEFAULT NULL,
  `order_amount` decimal(8,4) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `subscriber_email` varchar(255) DEFAULT NULL,
  `subscriber_firstname` varchar(255) DEFAULT NULL,
  `subscriber_lastname` varchar(255) DEFAULT NULL,
  `campaign_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`conversion_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `subscriber_id` (`subscriber_id`),
  CONSTRAINT `FK_NUNTIUS_CONV_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_CONV_SUBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Conversions';

-- ----------------------------
--  Table structure for `nuntius_conversions_tmp`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_conversions_tmp')}`;
CREATE TABLE `{$this->getTable('nuntius_conversions_tmp')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `link_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Conversions before Invoice';

-- ----------------------------
--  Table structure for `nuntius_coupons`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_coupons')}`;
CREATE TABLE `{$this->getTable('nuntius_coupons')}` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_code` varchar(255) DEFAULT NULL,
  `subscriber_email` varchar(255) DEFAULT NULL,
  `times_used` int(11) DEFAULT NULL,
  `force` enum('0','1') DEFAULT '0',
  `rule_id` int(10) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `used_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `UNQ_COUPON` (`coupon_code`),
  KEY `rule_id` (`rule_id`),
  KEY `subscriber_email` (`subscriber_email`),
  KEY `coupon_code` (`coupon_code`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_CAMP_COUPONS` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Coupons Usage';

-- ----------------------------
--  Table structure for `nuntius_goals`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_goals')}`;
CREATE TABLE `{$this->getTable('nuntius_goals')}` (
  `goal_id` int(11) NOT NULL AUTO_INCREMENT,
  `goal_type` varchar(255) DEFAULT NULL,
  `goal_type_option_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `result` enum('0','1','2','3') DEFAULT '3',
  `variation` varchar(255) DEFAULT NULL,
  `expected_value` int(11) DEFAULT NULL,
  `current_value` int(11) DEFAULT NULL,
  `original_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`goal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Goals';

-- ----------------------------
--  Table structure for `nuntius_history`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_history')}`;
CREATE TABLE `{$this->getTable('nuntius_history')}` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`history_id`),
  UNIQUE KEY `uni_subs_camp` (`campaign_id`,`subscriber_id`),
  KEY `subscriber_id` (`subscriber_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_HIST_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_HIST_SUBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - History';

-- ----------------------------
--  Table structure for `nuntius_lists`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_lists')}`;
CREATE TABLE `{$this->getTable('nuntius_lists')}` (
  `list_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `auto` enum('0','1') NOT NULL DEFAULT '0',
  `subscribers` int(11) NOT NULL DEFAULT '0',
  `conversions_number` int(11) DEFAULT NULL,
  `conversions_amount` decimal(10,4) DEFAULT NULL,
  `conversions_average` decimal(10,4) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  PRIMARY KEY (`list_id`),
  KEY `store_id` (`store_id`),
  KEY `is_active` (`is_active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Lists';

-- ----------------------------
--  Table structure for `nuntius_messages_archive`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_messages_archive')}`;
CREATE TABLE `{$this->getTable('nuntius_messages_archive')}` (
  `archive_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `sent_date` datetime DEFAULT NULL,
  `headers` text,
  `attempts` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`archive_id`),
  UNIQUE KEY `uni_subs_camp` (`campaign_id`,`subscriber_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `subscriber_id` (`subscriber_id`),
  CONSTRAINT `FK_NUNTIUS_ARCH_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_ARCH_SUBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Messages Archives';

-- ----------------------------
--  Table structure for `nuntius_messages_error`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_messages_error')}`;
CREATE TABLE `{$this->getTable('nuntius_messages_error')}` (
  `error_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `html` text,
  `sent_date` datetime DEFAULT NULL,
  `sent` enum('0','1') NOT NULL DEFAULT '0',
  `headers` text,
  `attempts` smallint(4) DEFAULT NULL,
  `error_code` varchar(255) DEFAULT NULL,
  `error_message` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`error_id`),
  KEY `subscriber_id` (`subscriber_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_ERROR_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_ERROR_SUBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Messages Error';

-- ----------------------------
--  Table structure for `nuntius_messages_queue`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_messages_queue')}`;
CREATE TABLE `{$this->getTable('nuntius_messages_queue')}` (
  `queue_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `headers` text,
  `attempts` smallint(4) DEFAULT NULL,
  `send_date` datetime DEFAULT NULL,
  PRIMARY KEY (`queue_id`),
  UNIQUE KEY `uni_subs_camp` (`campaign_id`,`subscriber_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `subscriber_id` (`subscriber_id`),
  CONSTRAINT `FK_NUNTIUS_QUEUE_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_QUEUE_SUBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Messages Queue';

-- ----------------------------
--  Table structure for `nuntius_reports`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_reports')}`;
CREATE TABLE `{$this->getTable('nuntius_reports')}` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` date DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  `subscribers_variation` int(11) NOT NULL,
  `subscribers` int(11) NOT NULL,
  `conversions_number` int(11) NOT NULL,
  `conversions_number_variation` int(11) NOT NULL,
  `conversions_amount` decimal(10,4) NOT NULL,
  `conversions_amount_variation` decimal(10,4) NOT NULL,
  `views` int(11) NOT NULL,
  `views_variation` int(11) NOT NULL,
  `clicks` int(11) NOT NULL,
  `clicks_variation` int(11) NOT NULL,
  `campaigns` int(11) NOT NULL,
  `campaigns_variation` int(11) NOT NULL,
  PRIMARY KEY (`report_id`),
  UNIQUE KEY `uniq_date_listid` (`created_at`,`list_id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `FK_NUNTIUS_REP_LIST` FOREIGN KEY (`list_id`) REFERENCES `{$this->getTable('nuntius_lists')}` (`list_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Basic Report';

-- ----------------------------
--  Table structure for `nuntius_senders`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_senders')}`;
CREATE TABLE `{$this->getTable('nuntius_senders')}` (
  `sender_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Senders';

-- ----------------------------
--  Table structure for `nuntius_stats`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_stats')}`;
CREATE TABLE `{$this->getTable('nuntius_stats')}` (
  `stat_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `event_at` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `platform` varchar(255) DEFAULT NULL,
  `browser` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `type` enum('views','clicks') NOT NULL DEFAULT 'views',
  PRIMARY KEY (`stat_id`),
  KEY `campaign_id` (`campaign_id`),
  KEY `customer_id` (`customer_id`),
  KEY `susbcriber_id` (`subscriber_id`),
  CONSTRAINT `FK_NUNTIUS_STATS_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_STATS_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_STATS_SUSBS` FOREIGN KEY (`subscriber_id`) REFERENCES `{$this->getTable('nuntius_subscribers')}` (`subscriber_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Campaigns Stats';

-- ----------------------------
--  Table structure for `nuntius_subscribers`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_subscribers')}`;
CREATE TABLE `{$this->getTable('nuntius_subscribers')}` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `store_id` smallint(5) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `list_id` int(11) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cellphone` varchar(255) DEFAULT NULL,
  `add_date` date DEFAULT NULL,
  `language` varchar(255) DEFAULT NULL,
  `birth_date` varchar(255) DEFAULT NULL,
  `status` enum('0','1') DEFAULT '1',
  `bounces` varchar(255) DEFAULT NULL,
  `sent` varchar(255) DEFAULT NULL,
  `views` varchar(255) DEFAULT NULL,
  `clicks` int(11) DEFAULT NULL,
  `conversions_number` int(11) DEFAULT NULL,
  `conversions_amount` decimal(10,4) DEFAULT NULL,
  `conversions_average` decimal(10,4) DEFAULT NULL,
  `send_time` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `unq_list_email` (`list_id`,`email`),
  KEY `email` (`email`),
  KEY `customer_id` (`customer_id`),
  KEY `list_id` (`list_id`),
  CONSTRAINT `FK_NUNTIUS_SUBS_CUST` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `FK_NUNTIUS_SUBS_LIST` FOREIGN KEY (`list_id`) REFERENCES `{$this->getTable('nuntius_lists')}` (`list_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Subscribers List';

-- ----------------------------
--  Table structure for `nuntius_templates`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_templates')}`;
CREATE TABLE `{$this->getTable('nuntius_templates')}` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `message` text,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`template_id`),
  KEY `status_i` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Templates';

-- ----------------------------
--  Table structure for `nuntius_unsubscribes`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_unsubscribes')}`;
CREATE TABLE `{$this->getTable('nuntius_unsubscribes')}` (
  `unsubscribe_id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `unsubscribed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`unsubscribe_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `FK_NUNTIUS_UNS_CAMP` FOREIGN KEY (`campaign_id`) REFERENCES `{$this->getTable('nuntius_campaigns')}` (`campaign_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Unsubscribes';

-- ----------------------------
--  Table structure for `nuntius_urls`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_urls')}`;
CREATE TABLE `{$this->getTable('nuntius_urls')}` (
  `url_id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) DEFAULT NULL,
  `campaign_id` int(11) DEFAULT NULL,
  `subscriber_id` int(11) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `visit_at` datetime DEFAULT NULL,
  `subscriber_firstname` varchar(255) DEFAULT NULL,
  `subscriber_lastname` varchar(255) DEFAULT NULL,
  `subscriber_email` varchar(255) DEFAULT NULL,
  `subscriber_cellphone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`url_id`),
  KEY `link_id` (`link_id`),
  CONSTRAINT `FK_NUNTIUS_URL_LINK` FOREIGN KEY (`link_id`) REFERENCES `{$this->getTable('nuntius_campaigns_links')}` (`link_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - URLs';

-- ----------------------------
--  Table structure for `nuntius_widget_cache`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('nuntius_widget_cache')}`;
CREATE TABLE `{$this->getTable('nuntius_widget_cache')}` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `products_ids` text,
  `build_date` datetime DEFAULT NULL,
  `identifier` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `FK_NUNTIUS_WDGT_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuntius - Cache';

");

$installer->run("INSERT INTO `{$this->getTable('nuntius_lists')}` (list_id, name, description, is_active, auto) VALUES (1,'General','',1,1)");

$installer->endSetup();