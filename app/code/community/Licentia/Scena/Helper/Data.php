<?php

/**
 * Licentia Scena - Background Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Background Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Scena_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getBackgroundCss() {

        if (!Mage::getStoreConfig('scena/background/enable')) {
            return false;
        }

        $product = Mage::registry('current_product');

        if ($product && $product->getData('scena_background_image')) {
            $url = Mage::getBaseUrl('media') . 'catalog/product/' . $product->getData('scena_background_image');
        } else {

            $category = Mage::registry('current_category');

            if (!$category) {
                if (Mage::getStoreConfig('scena/background/root')) {
                    $category = Mage::getModel('catalog/category')->load(Mage::app()->getStore()->getRootCategoryId());
                } else {
                    return;
                }
            }

            if (!$category->getData('scena_background_image')) {
                $category = $this->getParentTopCategory($category);
            }

            if (!$category->getData('scena_background_image'))
                return;

            $url = Mage::getBaseUrl('media') . 'catalog/category/' . $category->getData('scena_background_image');
        }

        $styles = Mage::getStoreConfig('scena/background');

        $class = $styles['class'];
        unset($styles['enable']);
        unset($styles['root']);
        unset($styles['class']);

        $txt = '';
        foreach ($styles as $key => $style) {
            if (strlen($style) > 0) {
                $txt .= " background-" . $key . ": " . $style . " !important; ";
            }
        }


        $css = $class . '{background-image: url("' . $url . '") !important; ' . $txt . '}';

        return $css;
    }

    public function getParentTopCategory($category) {
        if ($category->getLevel() == 1 || $category->getData('scena_background_image')) {
            return $category;
        } else {
            $parentCategory = Mage::getModel('catalog/category')->load($category->getParentId());
            return $this->getParentTopCategory($parentCategory);
        }
    }

}
