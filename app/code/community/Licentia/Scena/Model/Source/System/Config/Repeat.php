<?php

/**
 * Licentia Scena - Background Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Background Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Scena_Model_Source_System_Config_Repeat {

    public function toOptionArray() {

        $return = array();
        $return[] = array('value' => '', 'label' => '--Ignore--');
        $return[] = array('value' => 'repeat', 'label' => 'Repeat');
        $return[] = array('value' => 'no-repeat', 'label' => 'No Repeat');
        $return[] = array('value' => 'repeat-y', 'label' => 'Repeat Y');
        $return[] = array('value' => 'repeat-x', 'label' => 'Repeat X');
        $return[] = array('value' => 'inherit', 'label' => 'Inherit');

        return $return;
    }

}
