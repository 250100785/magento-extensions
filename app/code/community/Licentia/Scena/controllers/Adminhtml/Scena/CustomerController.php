<?php

/**
 * Licentia Scena - Background Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Background Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */
class Licentia_Scena_Adminhtml_Scena_CustomerController extends Mage_Adminhtml_Controller_action {

    public function clearAction() {
        try {

            $col = Mage::getModel('eav/entity_attribute')
                    ->getCollection()
                    ->addFieldToFilter('attribute_code', 'scena_background_image');

            foreach ($col as $item) {
                $item->delete();
            }

            $this->_getSession()->addSuccess($this->__('Attributes Removed. You can now remove the extension'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirectReferer();
    }

}
