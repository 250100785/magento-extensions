<?php
/**
 * Licentia Scena - Background Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the European Union Public Licence
 * It is available through the world-wide-web at this URL:
 * http://joinup.ec.europa.eu/software/page/eupl/licence-eupl
 *
 * @title      Background Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    European Union Public Licence
 */

$installer = $this;
$installer->startSetup();

$this->addAttribute('catalog_product', 'scena_background_image', array(
    'group' => 'General',
    'type' => 'varchar',
    'backend' => 'scena/catalog_attribute_backend_image',
    'label' => 'Background Image',
    'input' => 'image',
    'class' => '',
    'source' => '',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => true,
    'required' => false,
    'user_defined' => false,
    'default' => '',
    'searchable' => false,
    'filterable' => false,
    'comparable' => false,
    'visible_on_front' => true,
    'unique' => false,
        )
);

$installer->addAttribute('catalog_category', 'scena_background_image', array(
    'group'         => 'General Information',
    'type'          => 'varchar',
    'backend'       => 'scena/catalog_attribute_backend_cimage',
    'label'         => 'Background Image',
    'input'         => 'image',
    'required'      => false,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'       => 1,
));


$installer->endSetup();
