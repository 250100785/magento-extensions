<?php

/**
 * Licentia Magna -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Sec_Model_Observer {

    public function beforeDelete($event) {
        $admin = Mage::getSingleton('admin/session')->getUser();

        if (!$admin)
            return;

        $userId = $admin->getId();


        $object = $event->getObject();

        $deny = array(
            'Licentia_Nuntius_Model_Senders',
            'Licentia_Nuntius_Model_Lists',
            'Licentia_Nuntius_Model_Conversions',
            'Licentia_Nuntius_Model_Campaigns',
            'Licentia_Nuntius_Model_Autoresponders',
            'Licentia_Nuntius_Model_Goals',
            'Licentia_Nuntius_Model_Splits',
            'Licentia_Nuntius_Model_Subscribers',
            'Licentia_Nuntius_Model_Templates',
            'Licentia_Magna_Model_Segments',
            'Mage_Catalog_Model_Product',
        );
        $id = $object->getId();
        $class = get_class($object);

        if ($userId > 1 && $id == 1 && in_array($class, $deny)) {
            throw new Mage_Core_Exception('Demo Message: You can not delete the record with ID 1');
        }
    }

    public function beforeUpdate($event) {
        $admin = Mage::getSingleton('admin/session')->getUser();

        if (!$admin)
            return;

        $userId = $admin->getId();


        $object = $event->getObject();

        $deny = array(
            'Licentia_Nuntius_Model_Senders',
            'Licentia_Nuntius_Model_Lists',
            'Licentia_Nuntius_Model_Conversions',
            'Licentia_Nuntius_Model_Campaigns',
            'Licentia_Nuntius_Model_Autoresponders',
            'Licentia_Nuntius_Model_Goals',
            'Licentia_Nuntius_Model_Splits',
            'Licentia_Nuntius_Model_Subscribers',
            'Licentia_Nuntius_Model_Templates',
            'Licentia_Magna_Model_Segments',
            'Mage_Catalog_Model_Product',
        );
        $id = $object->getId();
        $class = get_class($object);

        if ($userId > 1 && $id == 1 && in_array($class, $deny)) {
            throw new Mage_Core_Exception('Demo Message: You can not edit the record with ID 1');
        }
    }

}
