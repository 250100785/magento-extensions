<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Customer_Edit_Tab_View_Ltv extends Mage_Adminhtml_Block_Template {

    public function getCustomer() {
        return Mage::registry('current_customer');
    }

    public function getCustomerReports() {
        return Mage::getModel('stella/reports_customer')->getCustomerReports($this->getCustomer()->getId());
    }

    public function getCustomerInfo() {
        return Mage::getModel('stella/customer')->getCustomerInfo($this->getCustomer()->getId());
    }

    public function getEquations() {
        return Mage::getModel('stella/equations')->load(1);
    }

}
