<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Customer_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('stella/customer')->getCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareGrid() {
        parent::_prepareGrid();
        $columns = array(
            'number_orders' => 'SUM(number_orders)',
            'amount' => 'SUM(amount)',
            'profit' => 'SUM(profit)',
            'taxes' => 'SUM(taxes)',
            'order_average' => 'AVG(order_average)',
            'discount' => 'SUM(discount)',
            'cost_services' => 'SUM(cost_services)',
            'cost_goods' => 'SUM(cost_goods)'
        );
        Mage::helper('stella')->getColumnsForTotals($this, $columns);
    }

    protected function _prepareColumns() {

        $this->addColumn('item_id', array(
            'header' => $this->__('ID'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'item_id',
        ));
        $this->addColumn('customer_id', array(
            'header' => $this->__('Cust. ID'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'customer_id',
        ));

        $this->addColumn('customer_name', array(
            'header' => $this->__('Customer Name'),
            'index' => 'customer_name',
        ));

        $this->addColumn('customer_email', array(
            'header' => $this->__('Customer Email'),
            'index' => 'customer_email',
        ));

        $this->addColumn('number_orders', array(
            'header' => $this->__('Orders'),
            'type' => 'number',
            'index' => 'number_orders',
        ));

        $this->addColumn('amount', array(
            'header' => $this->__('Amount'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'amount',
        ));

        $this->addColumn('profit', array(
            'header' => $this->__('Profit'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'profit',
        ));

        $this->addColumn('taxes', array(
            'header' => $this->__('Taxes'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'taxes',
        ));

        $this->addColumn('order_average', array(
            'header' => $this->__('Order Average'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'order_average',
        ));

        $this->addColumn('discount', array(
            'header' => $this->__('Discount'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'discount',
        ));

        $this->addColumn('cost_services', array(
            'header' => $this->__('Cost Services'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'cost_services',
        ));

        $this->addColumn('cost_goods', array(
            'header' => $this->__('Cost Goods'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'cost_goods',
        ));
        $this->addColumn('last_order', array(
            'header' => $this->__('Last Order'),
            'type' => 'datetime',
            'index' => 'last_order',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return false;
    }

}
