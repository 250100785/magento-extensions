<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Equations_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $current = Mage::registry("current_equation");

        $form = new Varien_Data_Form();
        $this->setForm($form);

        for ($i = 1; $i <= 3; $i++) {

            $fieldset[$i] = $form->addFieldset("stella_form_" . $i, array("legend" => $this->__("Equation $i"), 'class' => 'fieldset-wide'));

            $fieldset[$i]->addField("name_" . $i, "text", array(
                "label" => $this->__("Equation Name " . $i),
                "name" => "name_" . $i,
            ));

            $fieldset[$i]->addField("equation_" . $i, "textarea", array(
                "label" => $this->__("Equation Code " . $i),
                "style" => "height:50px",
                "name" => "equation_" . $i,
                "note" => $this->__('Please always use operation signs:<br> 3(p*t) => KO<br> 3*(p*t) =>OK'),
            ));
        }
        if ($current) {
            $currentValues = $current->getData();
            $form->addValues($currentValues);
        }

        return parent::_prepareForm();
    }

}
