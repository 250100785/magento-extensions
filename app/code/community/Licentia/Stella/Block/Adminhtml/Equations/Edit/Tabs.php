<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Equations_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("stella_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle($this->__("Item Information"));
    }

    protected function _beforeToHtml() {
        $this->addTab("main_section", array(
            "label" => $this->__("General"),
            "title" => $this->__("General"),
            "content" => $this->getLayout()->createBlock("stella/adminhtml_equations_edit_tab_main")->toHtml(),
        ));
        $this->addTab("help_section", array(
            "label" => $this->__("HELP!!!!"),
            "title" => $this->__("HELP!!!!"),
            "content" => $this->getLayout()->createBlock("stella/adminhtml_equations_edit_tab_help")->toHtml(),
        ));

        if ($this->getRequest()->getParam('tab_id')) {
            $this->setActiveTab($this->getRequest()->getParam('tab_id'));
        }

        return parent::_beforeToHtml();
    }

}
