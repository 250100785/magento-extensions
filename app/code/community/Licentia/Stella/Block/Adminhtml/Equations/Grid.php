<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Equations_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('equation_id');
        $this->setDefaultDir('DESC');
        $this->setFilterVisibility(false);
        $this->setSortable(false);
        $this->setPagerVisibility(false);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('stella/equations')->getCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $info = Mage::getModel('stella/equations')->load(1);

        $this->addColumn('equation_1', array(
            'header' => $info->getData('name_1'),
            'align' => 'center',
            'width' => '33%',
            'index' => 'equation_1',
        ));

        $this->addColumn('equation_2', array(
            'header' => $info->getData('name_2'),
            'align' => 'center',
            'style' => 'font-size:20px',
            'width' => '33%',
            'index' => 'equation_2',
        ));

        $this->addColumn('equation_3', array(
            'header' => $info->getData('name_3'),
            'align' => 'center',
            'width' => '33%',
            'index' => 'equation_3',
        ));


        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
