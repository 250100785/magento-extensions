<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Items extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $type = Mage::registry('current_type');

        $this->_controller = 'adminhtml_items';
        $this->_blockGroup = 'stella';
        $this->_headerText = $this->__('Items') . ' / ' . $type->getName();
        $this->_addButtonLabel = $this->__('Add ' . $type->getName() . ' Item');

        parent::__construct();

        $url = $this->getUrl('*/*/new', array('tid' => $this->getRequest()->getParam('tid')));
        $this->_updateButton('add', 'onclick', "window.location='$url'");

        $urlTypes = $this->getUrl('*/stella_types/');
        $data = array('label' => 'Back to Types', 'onclick' => "window.location='$urlTypes'", 'class' => 'back');
        $this->_addButton('types', $data, 0, 1);
    }

}
