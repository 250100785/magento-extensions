<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Items_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {
        parent::__construct();
        $this->_objectId = "item_id";
        $this->_blockGroup = "stella";
        $this->_controller = "adminhtml_types";

        $current = Mage::registry("current_item");

        $tid = $this->getRequest()->getParam('tid');

        if ($current->getTypeId()) {
            $tid = $current->getTypeId();
        }

        $this->_addButton("saveandcontinue", array(
            "label" => $this->__("Save and Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);

        $this->_formScripts[] = " function saveAndContinueEdit() {
                    tabID = '';
                    $$('ul.tabs li a.active').each(function (item) {
                         tabID = item.readAttribute('name');
                    })
                editForm.submit($('edit_form').action+'back/edit/tab_id/'+tabID);
            }";

        $url = $this->getUrl('*/*/', array('tid' => $tid));
        $this->_updateButton('back', 'onclick', "window.location='$url'");

        if ($current->getIsSystem()) {
            $this->_removeButton('delete');
        }
    }

    public function getHeaderText() {
        if (Mage::registry("current_item")->getId()) {
            return $this->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("current_item")->getName()));
        } else {
            return $this->__("Add Item");
        }
    }

}
