<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Items_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $current = Mage::registry("current_item");
        $type = Mage::registry("current_type");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("stella_form", array("legend" => $this->__("Item Information")));

        if ($type->getTypeId() == 1) {
            $fieldset->addField("code", "select", array(
                "label" => $this->__("Payment Option"),
                "name" => "code",
                "options" => Mage::helper('stella')->getAllPaymentMethods(),
            ));
        } elseif ($type->getTypeId() == 2) {
            $fieldset->addField("code", "select", array(
                "label" => $this->__("Shipping Option"),
                "name" => "code",
                "options" => Mage::helper('stella')->getAllShippingMethods(),
            ));
        } else {
            $fieldset->addField("name", "text", array(
                "label" => $this->__("Item Name"),
                "class" => "required-entry",
                "required" => true,
                'disabled' => $current->getIsSystem() ? 1 : 0,
                "name" => "name",
            ));

            $fieldset->addField("code", "text", array(
                "label" => $this->__("Code Activator"),
                "name" => "code",
                'disabled' => $current->getIsSystem() ? 1 : 0,
                "note" => "If set, will trigger cost calculation for this item. Usefull for marketing campaigns",
            ));
        }
        if ($current->getIsSystem() != 1) {
            $fieldset->addField("fixed_monthly", "text", array(
                "label" => $this->__("Fixed Monthly Cost"),
                "class" => 'validate-number',
                "name" => "fixed_monthly",
            ));

            $fieldset->addField("fixed", "text", array(
                "label" => $this->__("Fixed per Order"),
                "class" => 'validate-number',
                "name" => "fixed",
            ));

            $fieldset->addField("percent", "text", array(
                "label" => $this->__("Percent per Order"),
                "class" => 'validate-percents',
                "name" => "percent",
            ));

            $fieldset->addField("is_active", "select", array(
                "label" => $this->__("Is Active"),
                'disabled' => $current->getIsSystem() ? 1 : 0,
                "options" => array('1' => $this->__('Yes'), '0' => $this->__('No')),
                "name" => "is_active",
            ));


            $outputFormatDate = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
            $fieldset->addField('start_date', 'date', array(
                'name' => 'start_date',
                'format' => $outputFormatDate,
                'label' => $this->__('Start Date'),
                "required" => true,
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
            ));

            $fieldset->addField('end_date', 'date', array(
                'name' => 'end_date',
                'format' => $outputFormatDate,
                'label' => $this->__('End Date'),
                'image' => $this->getSkinUrl('images/grid-cal.gif'),
            ));
        }

        if ($current) {
            $currentValues = $current->getData();
            $form->addValues($currentValues);
        }

        $fieldset->addField('type_id', 'hidden', array(
            'name' => 'type_id',
            'value' => $type->getTypeId(),
        ));

        return parent::_prepareForm();
    }

}
