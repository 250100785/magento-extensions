<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Items_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('item_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $tid = $this->getRequest()->getParam('tid');

        $collection = Mage::getModel('stella/items')->getCollection();
        $collection->addFieldToFilter('type_id', $tid);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('item_id', array(
            'header' => $this->__('ID'),
            'width' => '50px',
            'index' => 'item_id',
        ));

        $this->addColumn('name', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));
        $this->addColumn('code', array(
            'header' => $this->__('Code'),
            'align' => 'left',
            'index' => 'code',
        ));

        $this->addColumn('fixed', array(
            'header' => $this->__('Fixed per Order'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'fixed',
        ));

        $this->addColumn('percent', array(
            'header' => $this->__('Percent per Order'),
            'type' => 'number',
            'index' => 'percent',
        ));

        $this->addColumn('fixed_monthly', array(
            'header' => $this->__('Fixed Monthly Cost'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'fixed_monthly',
        ));

        $this->addColumn('sales_amount', array(
            'header' => $this->__('Sales Amount'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'sales_amount',
        ));

        $this->addColumn('sales_cost', array(
            'header' => $this->__('Sales Cost'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'sales_cost',
        ));

        $this->addColumn('start_date', array(
            'header' => $this->__('Start Date'),
            'type' => 'date',
            'index' => 'start_date',
            'default' => 'N/A',
        ));

        $this->addColumn('end_date', array(
            'header' => $this->__('End Date'),
            'type' => 'date',
            'index' => 'end_date',
            'default' => 'N/A',
        ));

        $this->addColumn('is_active', array(
            'header' => $this->__('Active'),
            'type' => 'options',
            'options' => array('0' => $this->__('No'), '1' => $this->__('Yes')),
            'index' => 'is_active',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
