<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Logs_Customers extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        $this->_controller = 'adminhtml_logs_customers';
        $this->_blockGroup = 'stella';
        $this->_headerText = $this->__('Customers');
        parent::__construct();

        $url = $this->getUrl('*/*/build');
        $this->_updateButton('add', 'onclick', "window.location='$url'");
        $this->_updateButton('add', 'label', $this->__('Background Refresh'));
    }

}
