<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Logs_Customers_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('report_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('stella/reports_customer')->getCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('report_id', array(
            'header' => $this->__('Report'),
            'type' => 'options',
            'options' => Mage::getModel('stella/reports')->getCollection()->toOptionhash(),
            'index' => 'report_id',
        ));
        $this->addColumn('customer_id', array(
            'header' => $this->__('ID'),
            'align' => 'left',
            'width' => '50px',
            'index' => 'customer_id',
        ));

        $this->addColumn('customer_name', array(
            'header' => $this->__('Customer Name'),
            'index' => 'customer_name',
        ));

        $this->addColumn('customer_email', array(
            'header' => $this->__('Customer Email'),
            'index' => 'customer_email',
        ));

        $model = Mage::getModel('stella/equations')->load(1);

        $this->addColumn('equation_1', array(
            'header' => $model->getData('name_1'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_1',
        ));

        $this->addColumn('equation_2', array(
            'header' => $model->getData('name_2'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_2',
        ));

        $this->addColumn('equation_3', array(
            'header' => $model->getData('name_3'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_3',
        ));

        $this->addColumn('equation_average', array(
            'header' => $this->__('Equations Average'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_average',
        ));

        $this->addExportType('*/*/exportcCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportcXml', $this->__('Excel XML'));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/gridc', array('_current' => true));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('customer_id');
        $this->getMassactionBlock()->setFormFieldName('customers');
        $this->getMassactionBlock()->setUseSelectAll(false);

        $this->getMassactionBlock()->addItem('refresh', array(
            'label' => $this->__('refresh'),
            'url' => $this->getUrl('*/stella_customer/massRefresh'),
            'confirm' => $this->__('Are you sure?')
        ));

        return $this;
    }

    protected function _prepareGrid() {
        parent::_prepareGrid();
        $this->getColumn('massaction')->setData('use_index', true);
    }

}
