<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Logs_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('report_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareGrid() {
        parent::_prepareGrid();
        $columns = array(
            'product_amount' => 'SUM(product_amount)',
            'product_cost' => 'SUM(product_cost)',
            'row_cost_services' => 'SUM(row_cost_services)',
            'row_cost' => 'SUM(row_cost)',
            'row_amount' => 'SUM(row_amount)',
            'row_profit' => 'SUM(row_profit)',
        );
        Mage::helper('stella')->getColumnsForTotals($this, $columns);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('stella/logs')
                ->getCollection();

        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $collection->addFieldToFilter('invoice_id', $id);
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        if (Mage::registry('export_records')) {
            foreach ($this->getStellaColumns() as $column) {
                $this->addColumn($column, array(
                    'header' => $column,
                    'index' => $column,
                ));
            }

            return;
        }

        $this->addColumn('product_sku', array(
            'header' => $this->__('SKU'),
            'align' => 'left',
            'index' => 'product_sku',
        ));

        $this->addColumn('product_name', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => 'product_name',
        ));

        $this->addColumn('product_amount', array(
            'header' => $this->__('Price'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => Mage::app()->getBaseCurrencyCode(),
            'index' => 'product_amount',
        ));

        $this->addColumn('product_cost', array(
            'header' => $this->__('Product Cost'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => Mage::app()->getBaseCurrencyCode(),
            'index' => 'product_cost',
        ));

        $this->addColumn('row_cost_services', array(
            'header' => $this->__('Services Cost'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => Mage::app()->getBaseCurrencyCode(),
            'index' => 'row_cost_services',
        ));

        $this->addColumn('row_cost', array(
            'header' => $this->__('Row Cost'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => Mage::app()->getBaseCurrencyCode(),
            'index' => 'row_cost',
        ));

        $this->addColumn('row_amount', array(
            'header' => $this->__('Row Amount'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => Mage::app()->getBaseCurrencyCode(),
            'index' => 'row_amount',
        ));

        $this->addColumn('row_profit', array(
            'header' => $this->__('Row Profit'),
            'align' => 'left',
            'type' => 'currency',
            'currency_code' => Mage::app()->getBaseCurrencyCode(),
            'index' => 'row_profit',
        ));

        $this->addColumn('created_at', array(
            'header' => $this->__('Created At'),
            'width' => '50px',
            'type' => 'date',
            'index' => 'created_at',
        ));

        $this->addExportType('*/*/exportpCsv', $this->__('All Columns - CSV'));
        $this->addExportType('*/*/exportpXml', $this->__('All Columns - Excel XML'));
        $this->addExportType('*/*/exportpsCsv', $this->__('Current Columns - CSV'));
        $this->addExportType('*/*/exportpsXml', $this->__('Current Columns - Excel XML'));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/gridp', array('_current' => true));
    }

    public function getRowUrl($row) {
        return false;
    }

    public function getStellaColumns() {
        return array(
            'log_id',
            'store_id',
            'customer_email',
            'customer_id',
            'order_id',
            'invoice_id',
            'payment_code',
            'shipping_code',
            'shipping_fixed',
            'shpping_percent',
            'shipping_costs',
            'payment_fixed',
            'payment_percent',
            'payment_costs',
            'product_amount',
            'product_qty',
            'product_cost_row',
            'product_cost',
            'product_name',
            'product_sku',
            'row_cost_services',
            'row_cost_goods',
            'row_amount',
            'row_cost',
            'row_profit',
            'order_item_id',
            'invoice_item_id',
            'human_fixed',
            'human_percent',
            'human_costs',
            'marketing_fixed',
            'marketing_percent',
            'marketing_costs',
            'operational_fixed',
            'operational_percent',
            'operational_costs',
            'financial_fixed',
            'financial_percent',
            'financial_costs',
            'goods_fixed',
            'goods_percent',
            'goods_costs',
            'taxes_fixed',
            'taxes_percent',
            'taxes_costs',
            'other_fixed',
            'other_percent',
            'other_costs',
            'invoice_amount',
            'created_at',
            'updated_at'
        );
    }

}
