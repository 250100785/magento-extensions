<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Logs_Invoices_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('report_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareGrid() {
        parent::_prepareGrid();
        $columns = array(
            'base_grand_total' => 'SUM(base_grand_total)',
            'stella_discount' => 'SUM(stella_discount)',
            'stella_amount' => 'SUM(stella_amount)',
            'stella_cost_services' => 'SUM(stella_cost_services)',
            'stella_cost_goods' => 'SUM(stella_cost_goods)',
            'stella_profit' => 'SUM(stella_profit)',
            'stella_taxes' => 'SUM(stella_taxes)',
            'stella_profit_percent' => 'AVG(stella_profit_percent)'
        );

        Mage::helper('stella')->getColumnsForTotals($this, $columns);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('sales/order_invoice')->getCollection()
                ->addFieldToFilter('stella_processed', 1);

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn('entity_id', array(
            'header' => $this->__('Invoice ID'),
            'width' => '50px',
            'index' => 'entity_id',
            'frame_callback' => array($this, 'invoiceIdLink'),
        ));

        $this->addColumn('order_id', array(
            'header' => $this->__('Order ID'),
            'width' => '50px',
            'index' => 'order_id',
            'frame_callback' => array($this, 'orderIdLink'),
        ));

        $this->addColumn('created_at', array(
            'header' => $this->__('Created At'),
            'align' => 'left',
            'type' => 'date',
            'index' => 'created_at',
        ));

        $this->addColumn('stella_customer_id', array(
            'header' => $this->__('Cust. ID'),
            'align' => 'left',
            'width' => '30px',
            'index' => 'stella_customer_id',
            'frame_callback' => array($this, 'customerIdLink'),
        ));

        $fields = array(
            'stella_total',
            'stella_amount',
            'stella_taxes',
            'stella_cost_services',
            'stella_cost_goods',
            'stella_discount',
            'stella_profit',
        );

        foreach ($fields as $field) {

            $title = substr($field, stripos($field, '_') + 1);
            $title = ucwords(str_replace('_', ' ', $title));

            $this->addColumn($field, array(
                'header' => $this->__($title),
                'type' => 'currency',
                'currency_code' => Mage::app()->getBaseCurrencyCode(),
                'index' => $field,
            ));
        }

//        $this->addColumn('stella_profit_percent', array(
//            'header' => $this->__('Profit %'),
//            'type' => 'number',
//            'index' => 'stella_profit_percent',
//        ));

        $this->addColumn('stella_code', array(
            'header' => $this->__('Code'),
            'align' => 'left',
            'index' => 'stella_code',
        ));

        $this->addColumn('stella_first_order', array(
            'header' => $this->__('F. Order'),
            'align' => 'left',
            'width' => '60px',
            'type' => 'options',
            'options' => array('0' => $this->__('No'), '1' => $this->__('Yes')),
            'index' => 'stella_first_order',
        ));

        $this->addColumn('action', array(
            'header' => $this->__('View'),
            'type' => 'action',
            'align' => 'center',
            'system' => true,
            'filter' => false,
            'sortable' => false,
            'actions' => array(array(
                    'url' => $this->getUrl('*/stella_logs/products', array('id' => '$entity_id')),
                    'caption' => $this->__('Items'),
                )),
            'index' => 'type',
            'sortable' => false
        ));
        $this->addExportType('*/*/exportiCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportiXml', $this->__('Excel XML'));
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return false;
    }

    protected function _prepareMassaction() {

        if (Mage::helper('stella')->magna()) {

            $this->setMassactionIdField('entity_id');
            $this->getMassactionBlock()->setFormFieldName('ids');

            $groups = Mage::getModel('magna/segments')->getOptionArray(false);

            $this->getMassactionBlock()->addItem('massManualDelete', array(
                'label' => $this->__('Add To Customer Segment'),
                'url' => $this->getUrl('*/magna_segments/massManualAddInvoice'),
                'confirm' => $this->__('Are you sure?'),
                'additional' => array(
                    'visibility' => array(
                        'name' => 'segment',
                        'type' => 'select',
                        'class' => 'required-entry',
                        'label' => Mage::helper('magna')->__('Segment'),
                        'values' => $groups
                    )
                )
            ));
        }

        return $this;
    }

    public function orderIdLink($value, $row) {
        $url = $this->getUrl('*/sales_order/view', array('order_id' => $row->getData('order_id')));
        return'<a href="' . $url . '">' . $value . '</a>';
    }

    public function invoiceIdLink($value, $row) {
        $url = $this->getUrl('*/sales_invoice/view', array('invoice_id' => $row->getData('entity_id')));
        return'<a href="' . $url . '">' . $value . '</a>';
    }

    public function customerIdLink($value, $row) {
        $url = $this->getUrl('*/customer/edit', array('id' => $row->getData('stella_customer_id')));
        return'<a href="' . $url . '">' . $value . '</a>';
    }

}
