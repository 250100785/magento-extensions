<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Reports_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $current = Mage::registry("current_report");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("stella_form", array("legend" => $this->__("Item Information")));

        $fieldset->addField("name", "text", array(
            "label" => $this->__("Item Name"),
            "required" => true,
            "class" => "required-entry",
            "name" => "name",
        ));

        $values = Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm();
        array_unshift($values, array('value' => '', 'label' => $this->__('--All--')));

        $fieldset->addField("store_id", "select", array(
            "label" => $this->__("Store View"),
            "values" => $values,
            "name" => "store_id",
        ));

        if (Mage::helper('stella')->magna()) {
            $fieldset->addField('segment_id', 'select', array(
                'name' => 'segment_id',
                'label' => $this->__('Customer Segment'),
                'title' => $this->__('Customer Segment'),
                'required' => true,
                'values' => Mage::getSingleton('magna/segments')->getOptionArray(),
            ));
        }

        $outputFormatDate = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('start_date', 'date', array(
            'name' => 'start_date',
            'format' => $outputFormatDate,
            'label' => $this->__('Start Date'),
            "required" => true,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
        ));

        $fieldset->addField('end_date', 'date', array(
            'name' => 'end_date',
            'format' => $outputFormatDate,
            'label' => $this->__('End Date'),
            "required" => true,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
        ));

        $html = '
                <script>
                Event.observe(window, "load",tooglePeriods());
                   function tooglePeriods(){
                        if($F("period")!="0"){
                            $("start_date").removeClassName("required-entry");
                            $("end_date").removeClassName("required-entry");
                            $("start_date").up().up().hide();
                            $("end_date").up().up().hide();
                        }else{
                            $("start_date").addClassName("required-entry");
                            $("end_date").addClassName("required-entry");
                            $("start_date").up().up().show();
                            $("end_date").up().up().show();
                        }
                    }
                </script>
                ';

        $fieldset->addField('period', 'select', array(
            'name' => 'period',
            'onchange' => 'tooglePeriods()',
            'options' => Mage::getModel('stella/reports')->getPeriods(),
            'label' => $this->__('Dynamic Period'),
        ))->setAfterElementHtml($html);

        if ($current) {
            $currentValues = $current->getData();
            $form->addValues($currentValues);
        }

        return parent::_prepareForm();
    }

}
