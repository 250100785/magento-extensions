<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Reports_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('importerGrid');
        $this->setDefaultSort('report_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel('stella/reports')->getCollection();

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('report_id', array(
            'header' => $this->__('ID'),
            'width' => '50px',
            'index' => 'report_id',
        ));

        $this->addColumn('name', array(
            'header' => $this->__('Name'),
            'align' => 'left',
            'index' => 'name',
        ));

        $this->addColumn('store_id', array(
            'header' => $this->__('Store View'),
            'align' => 'left',
            'type' => 'store',
            'default' => '--All--',
            'index' => 'store_id',
        ));

        if (Mage::helper('stella')->magna()) {
            $this->addColumn('segment_id', array(
                'header' => $this->__('Segment'),
                'align' => 'left',
                'default' => '-NA-',
                'index' => 'segment_id',
                'type' => 'options',
                'options' => Mage::getModel('magna/segments')->toFormValues(),
            ));
        }

        $model = Mage::getModel('stella/equations')->load(1);

        $this->addColumn('equation_1', array(
            'header' => $model->getData('name_1'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_1',
        ));

        $this->addColumn('equation_2', array(
            'header' => $model->getData('name_2'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_2',
        ));

        $this->addColumn('equation_3', array(
            'header' => $model->getData('name_3'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_3',
        ));

        $this->addColumn('equation_average', array(
            'header' => $this->__('Equations Average'),
            'type' => 'currency',
            'currency_code' => (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE),
            'index' => 'equation_average',
        ));

        $this->addColumn('start_date', array(
            'header' => $this->__('Start Date'),
            'type' => 'date',
            'index' => 'start_date',
        ));

        $this->addColumn('end_date', array(
            'header' => $this->__('End Date'),
            'type' => 'date',
            'index' => 'end_date',
        ));

        $this->addColumn('period', array(
            'header' => $this->__('Period'),
            'type' => 'options',
            'width' => '120px',
            'options' => Mage::getModel('stella/reports')->getPeriods(),
            'index' => 'period',
        ));

        $this->addColumn('action', array(
            'header' => $this->__('Report'),
            'type' => 'action',
            'width' => '100px',
            'align' => 'center',
            'filter' => false,
            'sortable' => false,
            'actions' => array(array(
                    'url' => $this->getUrl('*/*/refresh', array('id' => '$report_id')),
                    'caption' => $this->__('--Update--'),
                )),
            'index' => 'type',
            'sortable' => false
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl() {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
