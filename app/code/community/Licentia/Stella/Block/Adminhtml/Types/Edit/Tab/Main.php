<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Block_Adminhtml_Types_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
        $current = Mage::registry("current_type");

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("stella_form", array("legend" => $this->__("Types Information")));

        $fieldset->addField("name", "text", array(
            "label" => $this->__("Name"),
            "class" => "required-entry",
            "required" => true,
            "disabled" => $current->getIsSystem() == 1 ? true : false,
            "name" => "name",
        ));

        $fieldset->addField("is_active", "select", array(
            "label" => $this->__("Is Active"),
            "disabled" => $current->getIsSystem() == 1 ? true : false,
            "options" => array('1' => $this->__('Yes'), '0' => $this->__('No')),
            "name" => "is_active",
        ));

        $fieldset->addField("description", "textarea", array(
            "label" => $this->__("Description"),
            "name" => "description",
        ));

        $fieldset->addField("fixed_monthly", "text", array(
            "label" => $this->__("Monthly Cost"),
            "required" => true,
            "class" => 'validate-number',
            "name" => "fixed_monthly",
        ));

        if ($current) {
            $currentValues = $current->getData();
            $form->setValues($currentValues);
        }

        return parent::_prepareForm();
    }

}
