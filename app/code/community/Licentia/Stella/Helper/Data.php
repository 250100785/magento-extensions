<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Helper_Data extends Mage_Core_Helper_Abstract {

    public function scheduleEvent($code, $date = null) {

        if (null === $date) {
            $date = now();
        }

        $cron = Mage::getModel('cron/schedule')->load($code, 'job_code');
        if ($cron->getId() && $cron->getStatus() == 'pending') {
            return false;
        } else {
            $data['status'] = 'pending';
            $data['job_code'] = $code;
            $data['scheduled_at'] = now();
            $data['created_at'] = now();
            $cron->setData($data)->save();
        }

        return true;
    }

    public function getColumnsForTotals($grid, $columns) {

        $totals = Mage::getStoreConfig('stella/config/totals');

        if ($totals == 0) {
            return $grid;
        }

        $collection = $grid->getCollection();

        $select = clone $collection->getSelect();

        foreach ($columns as $key => $column) {
            $select->reset('columns');
            $select->reset('limitcount');
            $select->reset('limitoffset');
            $tmpS = $select->columns(array('total' => new Zend_Db_Expr($column)));
            $columns[$key] = $collection->getResource()->getReadConnection()->fetchOne($tmpS);
        }
        $grid->setTotals(new Varien_Object($columns));
        $grid->setCountTotals(true);
    }

    public function finalCleanEval($data) {
        foreach ($data as $key => $value) {
            $value = strtolower($value);
            $data[$key] = preg_replace('/([a-z]|[^0-9scatrpim+\-*\/\(\). ])/', "", $value);
        }
        return $data;
    }

    public function filterEquations($data, $fields) {

        foreach ($data as $key => $value) {
            if (in_array($key, $fields)) {
                $value = strtolower($value);
                $data[$key] = preg_replace('/[^ 0-9vscatrpim+\-*\/\(\)]/', "", $value);
            }
        }

        return $data;
    }

    public function magna() {
        return (bool) Mage::getConfig()->getModuleConfig('Licentia_Magna');
    }

    public function getDaysBetweenDates($start, $end) {
        $dateStart = new DateTime($start);
        $dateEnd = new DateTime($end);

        return $dateStart->diff($dateEnd);
    }

    public function setStellaCode($code) {
        Mage::getSingleton('customer/session')->setStellaCode($code);
        Mage::register('stella_code', $code);

        return;
    }

    public function getStellaCode() {
        if (Mage::registry('stella_code')) {
            return Mage::registry('stella_code');
        }

        if (Mage::getSingleton('customer/session')->getStellaCode()) {
            return Mage::getSingleton('customer/session')->getStellaCode();
        }

        return null;
    }

    public function getNameForShipmentCode($code) {
        $data = $this->getAllShippingMethods();

        return $data[$code];
    }

    public function getNameForPaymentCode($code) {
        $data = $this->getAllPaymentMethods();

        return $data[$code];
    }

    public function getAllShippingMethods() {
        $methods = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $options = array();
        foreach ($methods as $_ccode => $_carrier) {
            $_methodOptions = array();
            if ($_methods = $_carrier->getAllowedMethods()) {
                foreach ($_methods as $_mcode => $_method) {
                    $_code = $_ccode . '_' . $_mcode;
                    $_methodOptions[] = array('value' => $_code, 'label' => $_method);
                }

                if (!$_title = Mage::getStoreConfig("carriers/$_ccode/title"))
                    $_title = $_ccode;

                $options[] = array('value' => $_methodOptions, 'label' => $_title);
            }
        }

        return $this->toOptionHashFromValue($options);
    }

    public function toOptionHashFromValue($options) {
        $res = array();
        foreach ($options as $val) {
            if (is_array($val['value'])) {
                foreach ($val['value'] as $name) {
                    $name['label'] = isset($name['label']) ? $name['label'] : $name['value'];
                    $res[$name['value']] = $val['label'] . ' / ' . $name['label'];
                }
            } else {
                $val['label'] = isset($val['label']) ? $val['label'] : $val['value'];
                $res[$val['value']] = $val['label'];
            }
        }

        return $res;
    }

    public function getAllPaymentMethods() {
        $options = Mage::helper('payment')->getPaymentMethodList(true, true, true);

        return $this->toOptionHashFromValue($options);
    }

}
