<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Customer extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('stella/customer');
    }

    public function getCustomerInfo($customerId) {
        $customer = $this->load($customerId, 'customer_id');
        return $customer;
    }

    public function save() {

        if ($this->getData('first_order')) {
            $period = Mage::getStoreConfig('stella/reports/t') * 30;

            $date = date('Y-m-d', strtotime('now +' . $period . ' days'));

            $dates = Mage::helper('stella')->getDaysBetweenDates($this->getData('first_order'), $date);
            $this->setData('time_left', $dates->y . ' ' . Mage::helper('stella')->__('Years') . ' ' . $dates->m . Mage::helper('stella')->__('months'));
        }
        return parent::save();
    }

    public function clearCycle() {
        return $this->getResource()->clearCycle();
    }

    public function buildCurrentCycle() {
        return $this->getResource()->buildCurrentCycle();
    }

}
