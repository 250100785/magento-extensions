<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Items extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('stella/items');
    }

    public function getItemsForService($serviceId, $date) {
        return $this->getCollection()
                        ->addFieldToFilter(
                                array('start_date', 'start_date'), array(
                            array('lteq' => $date),
                            array('null' => true)
                        ))
                        ->addFieldToFilter(
                                array('end_date', 'end_date'), array(
                            array('gteq' => $date),
                            array('null' => true)
                        ))
                        ->addFieldToFilter('is_active', 1)
                        ->addFieldToFilter('type_id', $serviceId);
    }

    public function save() {
        if ($this->getCode()) {
            $col = $this->getCollection()
                    ->addFieldToFilter('code', $this->getCode());
            if ($this->getId()) {
                $col->addFieldToFilter('item_id', array('neq' => $this->getId()));
            }
            if ($col->count() != 0) {
                throw new Mage_Core_Exception('Duplicated value for Code');
            }
        }
        if (!$this->getTypeId()) {
            throw new Mage_Core_Exception('Invalid Type ID');
        }

        return parent::save();
    }

    public function getItemByCode(Licentia_Stella_Model_Types $type, $code, $name = '') {

        $code = rtrim(rtrim($code, 0), '.');

        if (strlen($code) == 0) {
            throw new Mage_Core_Exception('Empty Item Code');
        }

        $return = $this->getCollection()
                ->addFieldToFilter('type_id', $type->getId())
                ->addFieldToFilter('code', $code);

        if ($return->count() == 1) {
            return $return->getFirstItem();
        }

        if ($return->count() == 0) {

            if ($type->getId() == 7) {
                if (is_numeric($code)) {
                    $codeName = Mage::helper('tax')->__('Tax') . ' (' . $code . ')';
                } else {
                    $codeName = $code;
                }
            }

            if ($type->getId() == 8) {
                $codeName = $name;
            }

            $newItem = array();
            $newItem['type_id'] = $type->getId();
            $newItem['code'] = $code;
            $newItem['is_active'] = 1;
            $newItem['fixed'] = 0;
            $newItem['is_system'] = 1;
            $newItem['percent'] = 0;
            $newItem['fixed_monthly'] = 0;
            $newItem['name'] = $codeName;
            return Mage::getModel('stella/items')->setData($newItem)->save();
        }

        throw new Mage_Core_Exception('Cost Type Not defined');
    }

}
