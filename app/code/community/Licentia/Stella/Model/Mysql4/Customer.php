<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Mysql4_Customer extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('stella/customer', 'item_id');
    }

    public function clearCycle() {
        $adapter = $this->_getWriteAdapter();
        $data = array();
        $data['amount_cycle'] = 0;
        $data['number_orders_cycle'] = 0;
        $adapter->update($this->getMainTable(), $data);
        return $this;
    }

    public function buildCurrentCycle() {
        $adapter = $this->_getWriteAdapter();

        $custSelect = $adapter->select()
                ->from($this->getTable('sales/order'), array('customer_id'))
                ->where('created_at>=?', date('Y-m-') . '01 00:00:00');

        $custs = $adapter->fetchCol($custSelect);

        $customers = Mage::getModel('customer/customer')
                ->getCollection()
                ->addNameToSelect()
                ->addFieldToFilter('entity_id', array('in' => $custs));

        $data = array();
        foreach ($customers as $customer) {

            $cust['customer_id'] = $customer->getId();
            $cust['customer_name'] = $customer->getName();
            $cust['customer_email'] = $customer->getEmail();
            $cust['store_id'] = $customer->getStoreId();

            $count = $adapter->select()->from($this->getMainTable())->where('customer_id', $customer->getId());
            $total = $adapter->fetchCol($count);

            if (count($total) == 0) {
                Mage::getModel('stella/customer')->setData($cust)->save();
            }
        }

        $table = $this->getTable('sales/order');
        $select = $adapter->select();
        $select->from($table, array('customer_id'));

        $columns = array(
            'amount_cycle' => 'SUM(base_grand_total)',
            'number_orders_cycle' => 'COUNT(*)',
        );
        $select->columns($columns);
        $select->where('state IN (?)', array('processing', 'complete'));
        $select->where('created_at>=?', date('Y-m-') . '01 00:00:00');
        $select->where('created_at<=?', date('Y-m-d H:i:s'));
        $select->group('customer_id');

        $result = $adapter->fetchAll($select);

        foreach ($result as $data) {

            $key = $data['customer_id'];
            unset($data['customer_id']);

            $adapter->update($this->getMainTable(), $data, array('customer_id=?' => $key));
        }
    }

}
