<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Mysql4_Customer_Aggregated extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('stella/customer_aggregated', 'item_id');
    }

    public function getS($model, $customerId = null) {

        return $this->_buildSelect($model, $customerId, array('total' => new Zend_Db_Expr('SUM(order_average)')));
    }

    public function getA($model, $customerId = null) {

        return $this->_buildSelect($model, $customerId, array('total' => new Zend_Db_Expr('SUM(amount)')));
    }

    public function getP($model, $customerId = null) {

        return $this->_buildSelect($model, $customerId, array('total' => new Zend_Db_Expr('SUM(profit) * 100 / SUM(amount)')));
    }

    public function getM($model, $customerId = null) {
        return $this->_buildSelect($model, $customerId, array('total' => new Zend_Db_Expr('SUM(profit)')));
    }

    public function getC($model, $customerId = null) {

        return $this->_buildSelect($model, $customerId, array('total' => new Zend_Db_Expr('SUM(number_orders)')));
    }

    public function getR($model) {

        $storeId = $model->getStoreId();
        $dates = $this->_getDatesForReport($model);
        $start = $dates['start_date'];
        $end = $dates['end_date'];

        $period = Mage::getStoreConfig('stella/reports/period', $storeId);
        if ($period != 'm') {
            $period = 'a';
        }

        $paramName = 'r_' . $start . '_' . $end . '_' . $period;
        $param = Mage::getModel('stella/params')->load($paramName, 'param');
        if ($param->getId()) {
            return $param->getValue();
        }

        $startMonth = date('m', strtotime($start));
        $startYear = date('Y', strtotime($start));
        $endYear = date('Y', strtotime($end));
        $endMonth = date('m', strtotime($end));

        $daysC = Mage::helper('stella')->getDaysBetweenDates($model->getStartDate(), $model->getEndDate());

        $days = $daysC->days;

        $oldStartMonth = date('m', strtotime($model->getStartDate() . " -$days days"));
        $oldStartYear = date('Y', strtotime($model->getStartDate() . " -$days days"));
        $oldEndMonth = date('m', strtotime($model->getEndDate() . " -$days days"));
        $oldEndYear = date('Y', strtotime($model->getEndDate() . " -$days days"));

        $adapter = $this->_getReadAdapter();
        $select = $adapter->select();
        $select->from($this->getMainTable(), array('customer_id'));

        if ($storeId) {
            if (is_array($storeId)) {
                $select->where('store_id IN (?)', $storeId);
            } else {
                $select->where('store_id=?', $storeId);
            }
        }

        $select->distinct(true);
        $select->where('year>=?', $oldStartYear);
        $select->where('year<=?', $oldEndYear);

        if ($period == 'm') {
            $select->where('month>=?', $oldStartMonth);
            $select->where('month<=?', $oldEndMonth);
        }

        $startCustomers = $adapter->fetchCol($select);

        $select->reset('where');
        $select->where('year>=?', $startYear);
        $select->where('year<=?', $endYear);

        if ($period == 'm') {
            $select->where('month>=?', $startMonth);
            $select->where('month<=?', $endMonth);
        }

        if ($storeId) {
            if (is_array($storeId)) {
                $select->where('store_id IN (?)', $storeId);
            } else {
                $select->where('store_id=?', $storeId);
            }
        }

        $resultN = $adapter->fetchCol($select);

        $finalCustomers = array_intersect($startCustomers, $resultN);

        if (count($finalCustomers) == 0 || count($startCustomers) == 0) {
            return 0;
        }

        $percent = count($finalCustomers) * 100 / count($startCustomers);


        $param->setData($paramName, $percent)->save();

        return $percent;
    }

    protected function _getSelect($model, $customerId = null) {
        $dates = $this->_getDatesForReport($model);
        $start = $dates['start_date'];
        $end = $dates['end_date'];

        $storeId = $model->getStoreId();

        $period = Mage::getStoreConfig('stella/reports/period', $storeId);
        if ($period != 'm') {
            $period = 'a';
        }

        $startMonth = date('m', strtotime($start));
        $startYear = date('Y', strtotime($start));
        $endYear = date('Y', strtotime($end));
        $endMonth = date('m', strtotime($end));

        $table = $this->getMainTable();

        $adapter = $this->_getReadAdapter();
        $select = $adapter->select();
        $select->from($table, array());

        if ($customerId) {
            if (is_array($customerId)) {
                $select->where('customer_id IN (?)', $customerId);
            } else {
                $select->where('customer_id=?', $customerId);
            }
        }
        if ($storeId) {
            if (is_array($storeId)) {
                $select->where('store_id IN (?)', $storeId);
            } else {
                $select->where('store_id=?', $storeId);
            }
        }

        $select->group('customer_id');
        $select->where('year>=?', $startYear);
        $select->where('year<=?', $endYear);

        if ($period == 'm') {
            $select->where('month>=?', $startMonth);
            $select->where('month<=?', $endMonth);
        }

        return $select;
    }

    protected function _buildSelect($model, $customerId = null, $columns = array()) {
        $select = $this->_getSelect($model, $customerId);
        $select->columns($columns);
        $adapter = $this->_getReadAdapter();
        $count = $adapter->fetchCol($select);

        $result = array_sum($count) / count($count);

        return $result;
    }

    protected function _getDatesForReport(Licentia_Stella_Model_Reports $model) {

        $return = array();
        if ($model->getPeriod() == 0) {
            $return['start_date'] = $model->getStartDate();
            $return['end_date'] = $model->getEndDate();
        }

        if ($model->getPeriod() == 'cm') {
            $date = Mage::app()->getLocale()->date();
            $return['start_date'] = $date->get('yyyy-MM-') . '01';
            $return['end_date'] = $date->get('yyyy-MM-') . $date->get(Zend_Date::MONTH_DAYS);
        }
        if ($model->getPeriod() == 'pm') {
            $date = Mage::app()->getLocale()->date()->subMonth(1);
            $return['start_date'] = $date->get('yyyy-MM-') . '01';
            $return['end_date'] = $date->get('yyyy-MM-') . $date->get(Zend_Date::MONTH_DAYS);
        }
        if ($model->getPeriod() == 'cy') {
            $date = Mage::app()->getLocale()->date();
            $return['start_date'] = $date->get('yyyy-') . '01-01';
            $return['end_date'] = $date->get('yyyy-') . '12-31';
        }
        if ($model->getPeriod() == 'py') {
            $date = Mage::app()->getLocale()->date()->subYear(1);
            $return['start_date'] = $date->get('yyyy-') . '01-01';
            $return['end_date'] = $date->get('yyyy-') . '12-31';
        }
        if ($model->getPeriod() == 'ytd') {
            $date = Mage::app()->getLocale()->date()->subYear(1);
            $return['start_date'] = $date->get('yyyy-') . '01-01';
            $return['end_date'] = $date->get('yyyy-MM-dd');
        }

        return $return;
    }

}
