<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Mysql4_Params extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('stella/params', 'param_id');
    }

    public function getOrdersNumberForDate($month, $year) {
        $param = 'invoices_number_' . $month . '_' . $year;
        $countModel = Mage::getModel('stella/params')->load($param, 'name');

        if ($countModel->getId()) {
            return $countModel->getValue();
        }

        $daysInMOnth = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        $start = $year . '-' . $month . '-01';
        $end = $year . '-' . $month . '-' . $daysInMOnth;

        $adapter = $this->_getReadAdapter('read');
        $table = $this->getTable('sales/invoiced_aggregated_order');
        $select = $adapter->select();
        $select->from($table, array());
        $select->where('period>=?', $start)->where('period<=?', $end);
        $select->columns(array('total' => new Zend_Db_Expr('SUM(orders_invoiced)')));
        $count = $adapter->fetchOne($select);

        $countModel->setData(array('name' => $param, 'value' => $count))->save();

        return $count;
    }

}
