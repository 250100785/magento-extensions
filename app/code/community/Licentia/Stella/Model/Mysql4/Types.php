<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Mysql4_Types extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('stella/types', 'type_id');
    }

    public function rebuild() {
        $adapter = $this->_getWriteAdapter();

        $adapter->update($this->getTable('sales/invoice'), array('stella_processed' => '0'));
        $adapter->update($this->getTable('sales/invoice_item'), array('stella_processed' => '0'));

        $data = array();
        $data['sales_amount'] = '0';
        $data['sales_cost'] = '0';
        $adapter->update($this->getTable('stella/types'), $data);
        $adapter->update($this->getTable('stella/items'), $data);

        $adapter->delete($this->getTable('stella/params'));
        $adapter->delete($this->getTable('stella/customer'));
        $adapter->delete($this->getTable('stella/customer_aggregated'));
        $adapter->delete($this->getTable('stella/reports_customer'));
        $adapter->delete($this->getTable('stella/logs'));
    }

}
