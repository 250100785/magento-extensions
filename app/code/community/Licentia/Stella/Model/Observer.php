<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Observer {

    public function startConversion($event) {
        $params = $event->getControllerAction()->getRequest()->getParams();

        $possibleParams = array_map('trim', explode(',', Mage::getStoreConfig('stella/config/params')));

        foreach ($possibleParams as $expected) {
            if (array_key_exists($expected, $params)) {
                Mage::helper('stella')->setStellaCode($params[$expected]);
                break;
            }
        }
    }

    public function newOrder($event) {
        $order = $event->getOrder();
        $rate = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingRatesCollection();
        $cost = 0;
        foreach ($rate as $item) {
            if ($item->getCode() == $order->getShippingMethod()) {
                $cost = $item->getCost();
                break;
            }
        }

        $order->setData('stella_shipping_cost', $cost);
        $order->setStellaCode(Mage::helper('stella')->getStellaCode());
        $order->save();
    }

    public function convertInvoice($event) {
        Mage::getModel('stella/types')->convertInvoice($event->getInvoice());
    }

    public function processOrderItem($event) {
        $orderItem = $event->getEvent()->getOrderItem();
        $item = $event->getEvent()->getItem();

        $inclTax = Mage::getStoreConfig('tax/calculation/price_includes_tax', $orderItem->getStoreId());

        $product = Mage::getModel('catalog/product')->setStoreId($orderItem->getStoreId())->load($item->getProductId());

        if ($inclTax) {
            $tmp1 = explode('.', $orderItem->getTaxPercent());
            $tmp = '1.' . str_pad($tmp1[0], 2, 0, STR_PAD_LEFT) . $tmp1[1];
            $price = $product->getPrice() / $tmp;
        } else {
            $price = $product->getPrice();
        }

        $orderItem->setData('stella_product_price', $price);
    }

}
