<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Quote_Address_Rate extends Mage_Sales_Model_Quote_Address_Rate {

    public function importShippingRate(Mage_Shipping_Model_Rate_Result_Abstract $rate) {
        if ($rate instanceof Mage_Shipping_Model_Rate_Result_Error) {
            $this->setCode($rate->getCarrier() . '_error')
                    ->setCarrier($rate->getCarrier())
                    ->setCarrierTitle($rate->getCarrierTitle())
                    ->setErrorMessage($rate->getErrorMessage())
            ;
        } elseif ($rate instanceof Mage_Shipping_Model_Rate_Result_Method) {
            $this->setCode($rate->getCarrier() . '_' . $rate->getMethod())
                    ->setCarrier($rate->getCarrier())
                    ->setCarrierTitle($rate->getCarrierTitle())
                    ->setMethod($rate->getMethod())
                    ->setMethodTitle($rate->getMethodTitle())
                    ->setMethodDescription($rate->getMethodDescription())
                    ->setPrice($rate->getPrice())
                    ->setCost($rate->getCost())
            ;
        }

        return $this;
    }

}
