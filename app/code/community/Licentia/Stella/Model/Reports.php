<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Reports extends Mage_Core_Model_Abstract {

    protected function _construct() {
        $this->_init('stella/reports');
    }

    public function buildCustomerReports() {
        $customers = Mage::getModel('customer/customer')->getCollection()->addNameToSelect();
        foreach ($customers as $customerInfo) {

            $customer = Mage::getModel('stella/customer')->load($customer, 'customer_id');
            if (!$customer || !$customer->getId()) {
                return false;
            }
            $customer->setData('customer_name', $customerInfo->getName());
            $customer->setData('customer_id', $customerInfo->getId());
            $customer->setData('customer_email', $customerInfo->getEmail());
            Mage::getModel('stella/reports')->applyReportsToCustomer($customer);
        }
    }

    public function applyReportsToCustomer($customer) {

        if (is_numeric($customer)) {
            $customer = Mage::getModel('stella/customer')->load($customer, 'customer_id');
        }

        if (!$customer || !$customer->getId()) {
            return false;
        }

        $reports = Mage::getModel('stella/reports')->getCollection();

        foreach ($reports as $report) {

            $c = Mage::getModel('stella/reports_customer')->getCollection()
                    ->addFieldToFilter('report_id', $report->getId())
                    ->addFieldToFilter('customer_id', $customer->getCustomerId());

            if ($c->count() == 1) {
                $model = $c->getFirstItem();
            } else {
                $model = Mage::getModel('stella/reports_customer');
                $model->setData(array('report_id' => $report->getId(), 'customer_id' => $customer->getCustomerId()));
            }
            $model->addData($report->getData());
            $model->setData('customer_email', $customer->getData('customer_email'));
            $model->setData('customer_name', $customer->getData('customer_name'));
            $this->buildReport($model, $model->getCustomerId());
        }

        Mage::getModel('stella/customer')->load($customer->getCustomerId(), 'customer_id')->setData('report_update', now())->save();
    }

    public function buildReport($model, $customerId = null) {

        $equations = Mage::getModel('stella/equations')->load(1);

        $data = $equations->getdata('equation_1') . $equations->getdata('equation_2') . $equations->getdata('equation_3');

        if ($model->getSegmentId() && Mage::helper('stella')->magna()) {
            $customerIdColletion = Mage::getModel('magna/segments_list')->getCollection()
                    ->addFieldToFilter('segment_id', $model->getSegmentId());
            if ($customerId) {
                $customerIdColletion->addFieldToFilter('customer_id', $customerId);
            }
            $customerId = $customerIdColletion->getAllIds('customer_id');

            if (count($customerId) == 0) {
                return;
            }
        }

        $replace = array();
        if (stripos($data, 'a') !== false) {
            $a = Mage::getResourceModel('stella/customer_aggregated')->getA($model, $customerId);
            $replace['a'] = (float) $a;
        }
        if (stripos($data, 's') !== false) {
            $s = Mage::getResourceModel('stella/customer_aggregated')->getS($model, $customerId);
            $replace['s'] = (float) $s;
        }
        if (stripos($data, 'c') !== false) {
            $c = Mage::getResourceModel('stella/customer_aggregated')->getC($model, $customerId);
            $replace['c'] = (float) $c;
        }
        if (stripos($data, 'r') !== false) {
            $r = Mage::getResourceModel('stella/customer_aggregated')->getR($model, $customerId);
            $replace['r'] = (float) $r;
        }
        if (stripos($data, 'p') !== false) {
            $p = Mage::getResourceModel('stella/customer_aggregated')->getP($model, $customerId);
            $replace['p'] = (float) $p;
        }
        if (stripos($data, 'm') !== false) {
            $m = Mage::getResourceModel('stella/customer_aggregated')->getM($model, $customerId);
            $replace['m'] = (float) $m;
        }
        if (stripos($data, 'i') !== false) {
            $i = Mage::getStoreConfig('stella/reports/i', $model->getStoreId());
            $replace['i'] = (float) $i;
        }
        if (stripos($data, 't') !== false) {
            $t = Mage::getStoreConfig('stella/reports/t', $model->getStoreId());
            $replace['t'] = (float) $t;
        }

        $data = Mage::helper('stella')->filterEquations($equations->getData(), array('equation_1', 'equation_2', 'equation_3'));
        $data = str_replace(array_keys($replace), $replace, $data);
        $data = Mage::helper('stella')->finalCleanEval($data);

        if (strlen($data['equation_1']) > 0) {
            $result1 = eval('return ' . $data['equation_1'] . ';');
            $model->setData('equation_1', $result1);
            $result[] = $result1;
        }
        if (strlen($data['equation_2']) > 0) {
            $result2 = eval('return ' . $data['equation_2'] . ';');
            $model->setData('equation_2', $result2);
            $result[] = $result2;
        }
        if (strlen($data['equation_3']) > 0) {
            $result3 = eval('return ' . $data['equation_3'] . ';');
            $model->setData('equation_3', $result3);
            $result[] = $result3;
        }
        $model->setData('equation_average', array_sum($result) / count($result));
        $model->save();
    }

    public function save() {

        $this->setData('equation_1', strtolower($this->getData('equation_1')));
        $this->setData('equation_2', strtolower($this->getData('equation_2')));
        $this->setData('equation_3', strtolower($this->getData('equation_3')));

        if ($this->getData('period') != '0') {
            $this->setData('start_date', null);
            $this->setData('end_date', null);
        }

        return parent::save();
    }

    public function getPeriods() {
        $options = array();
        $options['0'] = Mage::helper('stella')->__('Start and End Date');
        $options['cm'] = Mage::helper('stella')->__('Current Month');
        $options['pm'] = Mage::helper('stella')->__('Previous Month');
        $options['cy'] = Mage::helper('stella')->__('Current Year');
        $options['py'] = Mage::helper('stella')->__('Previous Year');
        $options['ytd'] = Mage::helper('stella')->__('Year to Date');

        return $options;
    }

}
