<?php

/**
 * Licentia Stella -
 *
 * NOTICE OF LICENSE
 * This source file is subject to the license you can find in your order downloadable links
 *
 * @title      Advanced Email and SMS Marketing Automation
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2014 Licentia - http://licentia.pt
 * @license    Check your order files for more info
 */
class Licentia_Stella_Model_Types extends Mage_Core_Model_Abstract {

    const MYSQL_DATE = 'yyyy-MM-dd';
    const MYSQL_DATETIME = 'yyyy-MM-dd HH:mm:ss';

    protected function _construct() {
        $this->_init('stella/types');
    }

    public function buildCycleData(Mage_Sales_Model_Order_Invoice $invoice) {

        $order = $invoice->getOrder();

        if ($order->getInvoiceCollection()->getFirstItem()->getId() == $invoice->getId()) {
            $newOrder = 1;
        } else {
            $newOrder = 0;
        }

        if ($order->getCustomerId()) {
            $customerId = $order->getCustomerId();
            $customer = Mage::getModel('stella/customer')->load($customerId, 'customer_id');
            $customer->setData('customer_name', $order->getCustomerName());
            $customer->setData('customer_email', $order->getCustomerEmail());
            $customer->setData('store_id', $order->getStoreId());
            $customer->setData('amount_cycle', $customer->setData('amount_cycle') + $invoice->getBaseGrandTotal());
            $customer->setData('number_orders_cycle', $customer->getData('number_orders_cycle') + $newOrder);
            $customer->save();
        }
    }

    /**
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @return \Licentia_Stella_Model_Types
     */
    public function convertInvoice(Mage_Sales_Model_Order_Invoice $invoice) {
        if (Mage::registry('stella_save_invoice')) {
            return $this;
        }
        Mage::register('stella_save_invoice', true);

        if ($invoice->getStellaProcessed() == 1) {
            return;
        }

        $rate = $invoice->getBaseToGlobalRate();

        $order = $invoice->getOrder();
        $items = $invoice->getAllItems();

        if ($order->getInvoiceCollection()->getFirstItem()->getId() == $invoice->getId()) {
            $newOrder = 1;
        } else {
            $newOrder = 0;
        }

        $date = $order->getCreatedAt();
        $year = date('Y', strtotime($date));
        $month = date('m', strtotime($date));
        $period = Mage::getStoreConfig('stella/reports/period');

        $stellaAmount = 0;
        $stellaTaxes = 0;
        $stellaDiscount = 0;
        $stellaCostGoods = 0;
        $stellaCostServices = 0;
        $stellaProfit = 0;
        $totalItems = 0;

        foreach ($items as $item) {
            $totalItems += $item->getData('qty');
        }

        $shippingCost = $order->getData('stella_shipping_cost') * $rate;

        $shipping = $invoice->getData('base_shipping_incl_tax') * $rate;

        $extraShippingDiscount = 0;
        if ($shipping < $shippingCost) {
            $extraShippingDiscount = $shippingCost - $shipping;
        }

        $shippingDiscount = $order->getData('base_shipping_discount_amount') * $rate + $extraShippingDiscount;
        $shippingTaxes = $invoice->getData('base_shipping_tax_amount') * $rate + $invoice->getData('base_shipping_hidden_tax_amnt') * $rate;

        if (!$newOrder) {
            $shippingTaxes = 0;
            $shippingDiscount = 0;
        }

        foreach ($items as $item) {

            if ($item->getStellaProcessed() == 1) {
                return;
            }

            $orderItem = Mage::getModel('sales/order_item')->load($item->getOrderItemId());

            $global = $item->getData('qty') * $orderItem->getData('stella_product_price') * $rate;
            $costGoods = $item->getData('qty') * $this->getProductCost($invoice, $item);

            if ($global == 0) {
                $global = $item->getData('base_row_total') * $rate;
            }

            $item->setData('stella_amount', $item->getData('base_row_total') * $rate);
            $item->setData('stella_taxes', ($item->getData('base_tax_amount') * $rate) + $shippingTaxes + ($item->getData('base_hidden_tax_amount') * $rate) + ($item->getData('base_weee_tax_applied_row_amount') * $rate));
            $item->setData('stella_total', $item->getData('stella_amount') + $item->getData('stella_taxes'));
            $item->setData('stella_discount', ($item->getData('base_discount_amount') * $rate) + $shippingDiscount + ($global - $item->getData('stella_amount')));

            $serviceCosts = $this->getServiceCostsForItem($invoice, $item, $costGoods, $shippingTaxes, $newOrder, $orderItem);

            $item->setData('stella_cost_goods', $costGoods);
            $item->setData('stella_cost_services', $serviceCosts);
            $item->setData('stella_profit', $item->getData('stella_amount') - $item->getData('stella_cost_goods') - $item->getData('stella_cost_services'));
            $item->setData('stella_profit_percent', $item->getData('stella_profit') * 100 / $item->getData('stella_amount'));
            $item->setData('stella_processed', 1);
            $item->save();

            $stellaAmount += $item->getData('stella_amount');
            $stellaTaxes += $item->getData('stella_taxes');
            $stellaDiscount += $item->getData('stella_discount');
            $stellaCostGoods += $item->getData('stella_cost_goods');
            $stellaCostServices += $item->getData('stella_cost_services');
            $stellaProfit += $item->getData('stella_profit');
        }

        $invoice->setData('stella_total', $stellaAmount + $stellaTaxes);
        $invoice->setData('stella_amount', $stellaAmount);
        $invoice->setData('stella_taxes', $stellaTaxes);
        $invoice->setData('stella_discount', $stellaDiscount);
        $invoice->setData('stella_cost_goods', $stellaCostGoods);
        $invoice->setData('stella_cost_services', $stellaCostServices);
        $invoice->setData('stella_profit', $stellaProfit);
        $invoice->setData('stella_profit_percent', $stellaProfit * 100 / $stellaAmount);
        $invoice->setData('stella_processed', 1);
        $invoice->setData('stella_customer_id', $order->getCustomerId());
        $invoice->setData('stella_code', $order->getStellaCode());


        $ordersWidthDiscount = ($stellaDiscount > 0 && $newOrder) ? 1 : 0;

        $customerFirstOrder = 0;
        if ($order->getCustomerId()) {
            $customerId = $order->getCustomerId();
            $customer = Mage::getModel('stella/customer')->load($customerId, 'customer_id');
            $customer->setData('customer_name', $order->getCustomerName());
            $customer->setData('customer_email', $order->getCustomerEmail());
            $customer->setData('customer_id', $customerId);
            $customer->setData('store_id', $order->getStoreId());
            $customer->setData('amount', $customer->getData('amount') + $stellaAmount);
            $customer->setData('taxes', $customer->getData('taxes') + $stellaTaxes);
            $customer->setData('discount', $customer->getData('discount') + $stellaDiscount);
            $customer->setData('cost_goods', $customer->getData('cost_goods') + $stellaCostGoods);
            $customer->setData('cost_services', $customer->getData('cost_services') + $stellaCostServices);
            $customer->setData('profit', $customer->getData('profit') + $stellaProfit);
            $customer->setData('number_orders', $customer->getData('number_orders') + $newOrder);
            $customer->setData('number_orders_discount', $customer->getData('number_orders_discount') + $ordersWidthDiscount);

            if (!$customer->getFirstOrder()) {
                $firstOrderDate = $this->getCustomerFirstOrder($customerId);
                if (!$firstOrderDate) {
                    $firstOrderDate = $order->getCreatedAt();
                }
                $customer->setData('first_order', $firstOrderDate);
                if ($newOrder) {
                    $customerFirstOrder = 1;
                }
            }

            $orderDays = Mage::helper('stella')->getDaysBetweenDates($customer->getFirstOrder(), $order->getCreatedAt());
            $orderDaysAverage = $orderDays->days / $customer->getNumberOrders();

            $customer->setData('order_average', $customer->getData('amount') / $customer->getData('number_orders'));
            $customer->setData('last_order', $order->getCreatedAt());
            $customer->setData('order_average_days', $orderDaysAverage);
            $customer->save();

            $aggregatedCollection = Mage::getModel('stella/customer_aggregated')
                    ->getCollection()
                    ->addFieldToFilter('customer_id', $customerId)
                    ->addFieldToFilter('year', $year);

            if ($period == 'm') {
                $aggregatedCollection->addFieldToFilter('month', $month);
            }

            if ($aggregatedCollection->count() == 1) {
                $aggregated = $aggregatedCollection->getFirstItem();
            } else {
                $aggregated = Mage::getModel('stella/customer_aggregated');
            }

            $aggregated->setData('customer_id', $customerId);
            $aggregated->setData('customer_name', $order->getCustomerName());
            $aggregated->setData('customer_email', $order->getCustomerEmail());
            $aggregated->setData('month', $month);
            $aggregated->setData('year', $year);
            $aggregated->setData('store_id', $order->getStoreId());
            $aggregated->setData('amount', $aggregated->getData('amount') + $stellaAmount);
            $aggregated->setData('taxes', $aggregated->getData('taxes') + $stellaTaxes);
            $aggregated->setData('discount', $aggregated->getData('discount') + $stellaDiscount);
            $aggregated->setData('cost_goods', $aggregated->getData('cost_goods') + $stellaCostGoods);
            $aggregated->setData('cost_services', $aggregated->getData('cost_services') + $stellaCostServices);
            $aggregated->setData('profit', $aggregated->getData('profit') + $stellaProfit);
            $aggregated->setData('number_orders', $aggregated->getData('number_orders') + $newOrder);
            $aggregated->setData('number_orders_discount', $aggregated->getData('number_orders_discount') + $ordersWidthDiscount);

            if (!$aggregated->getFirstOrder()) {
                $aggregated->setData('first_order', $firstOrderDate);
            }
            $aggregated->setData('order_average', $aggregated->getData('amount') / $aggregated->getData('number_orders'));
            $aggregated->setData('last_order', $order->getCreatedAt());
            $aggregated->setData('order_average_days', $orderDaysAverage);
            $aggregated->save();
        }


        $invoice->setData('stella_first_order', $customerFirstOrder);
        $invoice->save();


        Mage::getModel('stella/reports')->applyReportsToCustomer($customer);
    }

    /**
     *
     * @param Mage_Sales_Model_Order_Invoice $invoice
     * @param Mage_Sales_Model_Order_Invoice_Item $invoiceItem
     * @param type $goods
     * @param type $shippingTaxes
     * @param type $newOrder
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @return type
     */
    public function getServiceCostsForItem(Mage_Sales_Model_Order_Invoice $invoice, Mage_Sales_Model_Order_Invoice_Item $invoiceItem, $goods, $shippingTaxes, $newOrder, Mage_Sales_Model_Order_Item $orderItem, $invoicesNumberReal = null) {

        $order = $invoice->getOrder();
        $date = $order->getCreatedAt();
        $servicesCosts = array();
        $otherCosts = array();
        $rate = $invoice->getBaseToGlobalRate();

        $rowTotal = ($invoiceItem->getData('base_row_total_incl_tax') * $rate) + ($invoiceItem->getData('base_weee_tax_applied_row_amount') * $rate);

        $shippingCode = $order->getShippingMethod();
        $paymentCode = $order->getPayment()->getMethodInstance()->getCode();

        $services = $this->getCollection()
                ->addFieldToFilter('is_active', 1);

        if (null !== $invoicesNumberReal) {
            $predictedOrders = $invoicesNumberReal;
        } else {
            $predictedOrders = $this->getOrdersNumberForDate($invoice);
        }
        $cost = 0;
        foreach ($services as $service) {
            $fixed = 0;
            $percent = 0;

            if ($service->getId() == 7) {
                $TaxTmp = $this->processTaxesForService($service, $order, $shippingTaxes, $invoiceItem, $orderItem, $rowTotal);

                $fixed += $TaxTmp;
                $cost += $TaxTmp;
            }
            if ($service->getId() == 2 && $order->getSetllaShippingCost() && $newOrder) {
                $cost += $order->getSetllaShippingCost();
            }
            if ($service->getId() == 2 && $newOrder) {
                $cost += $order->getData('base_shipping_amount') * $rate;
            }
            if ($service->getId() == 8) {

                $TaxTmp = $this->processGoodsForService($service, $goods, $invoiceItem, $rowTotal);

                $fixed += $goods;
            }

            $cost += $service->getData('fixed_monthly') / $predictedOrders;
            $fixed += $service->getData('fixed_monthly') / $predictedOrders;

            $items = Mage::getModel('stella/items')->getItemsForService($service->getId(), $date);

            foreach ($items as $item) {

                if ($item->getTypeId() == 1 && $paymentCode != $item->getCode()) {
                    continue;
                } elseif ($item->getTypeId() == 2 && $shippingCode != $item->getCode()) {
                    continue;
                } elseif (strlen($item->getCode() > 0) && $order->getStellaCode() != $item->getCode()) {
                    continue;
                }

                if ($item->getIsSystem()) {
                    continue;
                }

                if ($item->getFixedMonthly() > 0) {
                    $fixed += $item->getFixedMonthly() / $predictedOrders;

                    $cost += $item->getFixedMonthly() / $predictedOrders;
                }

                if ($item->getFixed() > 0) {
                    $fixed += $item->getFixed();

                    $cost += $item->getFixed();
                }

                if ($item->getPercent() > 0) {
                    $percent += $item->getPercent() * $rowTotal / 100;

                    $cost += $item->getPercent() * $rowTotal / 100;
                }

                $item->setData('sales_amount', $item->getData('sales_amount') + $rowTotal);
                $item->setData('sales_cost', $item->getData('sales_cost') + $fixed + $percent);
                $item->save();
            }

            $servicesCosts[$service->getId()] = array('fixed' => $fixed, 'percent' => $percent);

            if ($service->getId() > 8) {
                $otherCosts['fixed'] += $fixed;
                $otherCosts['percent'] += $percent;
            }

            $service->setData('sales_amount', $service->getData('sales_amount') + $rowTotal);
            $service->setData('sales_cost', $service->getData('sales_cost') + $fixed + $percent);
            $service->save();
        }

        $year = date('Y', strtotime($date));
        $month = date('m', strtotime($date));

        $productCost = $this->getProductCost($invoice, $invoiceItem);

        $data = array();
        $data['year'] = $year;
        $data['month'] = $month;
        $data['customer_id'] = $order->getCustomerId();
        $data['customer_email'] = $order->getCustomerEmail();
        $data['store_id'] = $invoice->getStoreId();
        $data['invoice_id'] = $invoice->getId();
        $data['order_id'] = $order->getId();
        $data['stella_code'] = $order->getStellaCode();
        $data['payment_code'] = $paymentCode;
        $data['shipping_code'] = $shippingCode;
        $data['product_amount'] = $rowTotal / $invoiceItem->getQty();
        $data['product_cost'] = $productCost;
        $data['product_name'] = $invoiceItem->getName();
        $data['product_sku'] = $invoiceItem->getSku();
        $data['product_qty'] = $invoiceItem->getQty();
        $data['order_item_id'] = $invoiceItem->getOrderItemId();
        $data['invoice_item_id'] = $invoiceItem->getId();
        $data['invoice_amount'] = $invoice->getBaseGrandTotal() * $rate;
        $data['payment_fixed'] = $servicesCosts[1]['fixed'];
        $data['payment_percent'] = $servicesCosts[1]['percent'];
        $data['payment_costs'] = $data['payment_fixed'] + $data['payment_percent'];
        $data['shipping_fixed'] = $servicesCosts[2]['fixed'];
        $data['shipping_percent'] = $servicesCosts[2]['percent'];
        $data['shipping_costs'] = $data['shipping_fixed'] + $data['shipping_percent'];
        $data['marketing_fixed'] = $servicesCosts[3]['fixed'];
        $data['marketing_percent'] = $servicesCosts[3]['percent'];
        $data['marketing_costs'] = $data['marketing_fixed'] + $data['marketing_percent'];
        $data['human_fixed'] = $servicesCosts[4]['fixed'];
        $data['human_percent'] = $servicesCosts[4]['percent'];
        $data['human_costs'] = $data['human_fixed'] + $data['human_percent'];
        $data['operational_fixed'] = $servicesCosts[5]['fixed'];
        $data['operational_percent'] = $servicesCosts[5]['percent'];
        $data['operational_costs'] = $data['operational_fixed'] + $data['operational_percent'];
        $data['financial_fixed'] = $servicesCosts[6]['fixed'];
        $data['financial_percent'] = $servicesCosts[6]['percent'];
        $data['financial_costs'] = $data['financial_fixed'] + $data['financial_percent'];
        $data['taxes_fixed'] = $servicesCosts[7]['fixed'];
        $data['taxes_percent'] = $servicesCosts[7]['percent'];
        $data['taxes_costs'] = $data['taxes_fixed'] + $data['taxes_percent'];
        $data['goods_fixed'] = $servicesCosts[8]['fixed'];
        $data['goods_percent'] = $servicesCosts[8]['percent'];
        $data['goods_costs'] = $data['goods_fixed'] + $data['goods_percent'];
        $data['other_fixed'] = $otherCosts['fixed'];
        $data['other_percent'] = $otherCosts['percent'];
        $data['other_costs'] = $data['other_fixed'] + $data['other_percent'];
        $data['row_amount'] = $rowTotal;
        $data['row_cost_goods'] = $productCost * $invoiceItem->getQty();
        $data['row_cost_services'] = $cost;
        $data['row_cost'] = $data['row_cost_services'] + $data['row_cost_goods'] + $data['taxes_costs'];
        $data['row_profit'] = $data['row_amount'] - $data['row_cost'];
        $data['created_at'] = $invoice->getCreatedAt();
        $data['updated_at'] = $invoice->getCreatedAt();

        Mage::getModel('stella/logs')->setData($data)->save();

        return $cost;
    }

    /**
     *
     * @param  Mage_Sales_Model_Order_Invoice      $invoice
     * @param  Mage_Sales_Model_Order_Invoice_Item $invoiceItem
     * @return int
     */
    public function getProductCost(Mage_Sales_Model_Order_Invoice $invoice, Mage_Sales_Model_Order_Invoice_Item $invoiceItem) {
        if (!$invoiceItem->getBaseCost()) {
            $productCost = Mage::getStoreConfig('stella/config/product_cost', $invoice->getStoreId());

            if ($productCost > 0) {
                $taxPC = '1.' . str_pad($productCost, 2, 0, STR_PAD_LEFT);
                $productCost = $invoiceItem->getBasePrice() / $taxPC;
            }
        } else {
            $productCost = $invoiceItem->getBaseCost();
        }

        return $productCost;
    }

    public function getOrdersNumberForDate(Mage_Sales_Model_Order_Invoice $invoice) {
        $month = date('m', strtotime($invoice->getCreatedAt()));
        $year = date('Y', strtotime($invoice->getCreatedAt()));
        $count = Mage::getResourceModel('stella/params')->getOrdersNumberForDate($month, $year);

        return $count;
    }

    /**
     *
     * @return type
     * @throws Mage_Core_Exception
     */
    public function delete() {
        $type = $this->load($this->getId());

        if ($type->getIsSystem()) {
            throw new Mage_Core_Exception('You can not delete a system Cost Type');
        }

        return parent::delete();
    }

    /**
     *
     * @param type $customerId
     * @return null
     */
    public function getCustomerFirstOrder($customerId) {
        $order = Mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToSelect('created_at')
                ->addAttributeToFilter('customer_id', $customerId)
                ->setOrder('created_at', 'ASC')
                ->setPageSize(1);

        if ($order->count() == 1) {
            return $order->getFirstItem()->getData('created_at');
        }

        return null;
    }

    /**
     *
     * @param Mage_Sales_Model_Order $order
     * @return type
     */
    public function getShippingTax(Mage_Sales_Model_Order $order) {
        $taxInfo = $order->getFullTaxInfo();
        foreach ($taxInfo as $tax) {
            $percent = '1.' . str_pad($tax['percent'], 2, 0, STR_PAD_LEFT);
            if (round($percent * $order->getBaseShippingAmount(), 2) == round($order->getBaseShippingInclTax(), 2)) {
                return $tax;
            }
        }
        return array('percent' => 0);
    }

    /**
     *
     * @param type $service
     * @param Mage_Sales_Model_Order $order
     * @param type $shippingTaxes
     * @param Mage_Sales_Model_Order_Invoice_Item $invoiceItem
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @param type $rowTotal
     * @return type
     */
    public function processTaxesForService($service, Mage_Sales_Model_Order $order, $shippingTaxes, Mage_Sales_Model_Order_Invoice_Item $invoiceItem, Mage_Sales_Model_Order_Item $orderItem, $rowTotal) {
        $taxAmount = 0;
        if ($shippingTaxes) {
            $taxItem = $this->getShippingTax($order);
            $percent = $taxItem['percent'];
            $taxesAmount = $shippingTaxes;
            $taxAmount += $taxesAmount;

            $exists = Mage::getModel('stella/items')->getItemByCode($service, $percent);
            $exists->setData('sales_cost', $exists->getData('sales_cost') + $taxesAmount);
            $exists->setData('sales_amount', $exists->getData('sales_amount') + $rowTotal);
            $exists->save();
        }

        if ($invoiceItem->getBaseTaxAmount() > 0) {
            $percent = $orderItem->getTaxPercent();
            $taxAmount += $invoiceItem->getBaseTaxAmount();
            $exists = Mage::getModel('stella/items')->getItemByCode($service, $percent);
            $exists->setData('sales_cost', $exists->getData('sales_cost') + ($invoiceItem->getBaseTaxAmount() * $rate));
            $exists->setData('sales_amount', $exists->getData('sales_amount') + $rowTotal);
            $exists->save();
        }

        if ($invoiceItem->getData('base_weee_tax_applied_row_amnt') > 0) {
            $taxAmount += $invoiceItem->getData('base_weee_tax_applied_row_amnt');
            $exists = Mage::getModel('stella/items')->getItemByCode($service, 'FPT');
            $exists->setData('sales_cost', $exists->getData('sales_cost') + ($invoiceItem->getData('base_weee_tax_applied_row_amnt') * $rate));
            $exists->setData('sales_amount', $exists->getData('sales_amount') + $rowTotal);
            $exists->save();
        }

        if ($invoiceItem->getData('base_hidden_tax_amount') > 0) {
            $taxAmount += $invoiceItem->getData('base_hidden_tax_amount');
            $exists = Mage::getModel('stella/items')->getItemByCode($service, 'hidden');
            $exists->setData('sales_cost', $exists->getData('sales_cost') + ($invoiceItem->getData('base_hidden_tax_amount') * $rate));
            $exists->setData('sales_amount', $exists->getData('sales_amount') + $rowTotal);
            $exists->save();
        }

        return $taxAmount;
    }

    /**
     *
     * @param type $service
     * @param Mage_Sales_Model_Order $order
     * @param type $shippingTaxes
     * @param Mage_Sales_Model_Order_Invoice_Item $invoiceItem
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @param type $rowTotal
     * @return type
     */
    public function processGoodsForService($service, $goods, Mage_Sales_Model_Order_Invoice_Item $invoiceIteml, $rowTotal) {

        $exists = Mage::getModel('stella/items')->getItemByCode($service, $invoiceIteml->getSku(), $invoiceIteml->getName());
        $exists->setData('sales_cost', $exists->getData('sales_cost') + $goods);
        $exists->setData('sales_amount', $exists->getData('sales_amount') + $rowTotal);
        $exists->save();
    }

    public function build($force = false) {
        $count = Mage::getStoreConfig('stella/config/count');
        $day = Mage::getStoreConfig('stella/config/day');

        if (date('j') != $day && $force === false) {
            return;
        }

        $date = Mage::app()->getLocale()->date()->subMonth(1);

        $dateEnd = $date->get('yyyy-MM-') . $date->get(Zend_Date::MONTH_DAYS) . ' 23:59:59';

        $invoices = Mage::getModel('sales/order_invoice')->getCollection()
                ->addFieldToFilter('stella_processed', 0)
                ->addFieldToFilter('created_at', array('lteq' => $dateEnd))
                ->setOrder('created_at', 'ASC')
                ->setPageSize($count);

        foreach ($invoices as $invoice) {
            Mage::unregister('stella_save_invoice');
            $this->convertInvoice($invoice);
        }

        if ($invoices->count() == $count) {
            Mage::helper('stella')->scheduleEvent('stella_build_data_code', date('Y-m-d H:i:s', strtotime('now +10 minutes')));
        }
    }

    public function buildCode() {
        return $this->build(true);
    }

    public function rebuild() {
        Mage::helper('stella')->scheduleEvent('stella_build_data_code');
        return $this->getResource()->rebuild();
    }

}
