<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Adminhtml_Stella_CustomerController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('stella/customer');
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Customers'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_customer'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function clearAction() {
        try {
            Mage::getModel('stella/customer')->clearCycle();
            Mage::getModel('stella/customer')->buildCurrentCycle();
            $this->_getSession()->addSuccess($this->__('Cycle Built'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirectReferer();
    }

}
