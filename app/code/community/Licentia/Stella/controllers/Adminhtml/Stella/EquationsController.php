<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Adminhtml_Stella_EquationsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('stella/equations');

        return $this;
    }

    public function indexAction() {
        $type = Mage::getModel('stella/equations')->load(1);
        if (!$type->getId()) {
            $type->setData(array('id' => 1))->save();
        }

        Mage::register('current_equation', $type);

        $this->_title($this->__('Stella'))->_title($this->__('Equations'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_equations'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Items'));

        $model = Mage::getModel('stella/equations');
        $model->load(1);

        // set entered data if was error when we do save
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        Mage::register('current_equation', $model);

        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_equations_edit'))
                ->_addLeft($this->getLayout()->createBlock('stella/adminhtml_equations_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost();
            $model = Mage::getModel('stella/equations');
            $model->setId(1);

            $data = Mage::helper('stella')->filterEquations($data, array('equation_1', 'equation_2', 'equation_3'));

            try {
                $model->addData($data);
                $this->_getSession()->setFormData($model->getData());
                $model->save();

                $this->_getSession()->addSuccess($this->__('Equations have been saved.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id));

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id));

                return;
            }
        }
        $this->_redirect('*/*/');
    }

}
