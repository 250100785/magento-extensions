<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Adminhtml_Stella_ItemsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('stella/items');

        return $this;
    }

    public function indexAction() {
        $tid = $this->getRequest()->getParam('tid');
        $type = Mage::getModel('stella/types')->load($tid);
        if (!$type->getId()) {
            $this->_getSession()->addError($this->__('No item type defined'));
            $this->_redirect('*/stella_types');

            return;
        }

        Mage::register('current_type', $type);

        $this->_title($this->__('Stella'))->_title($this->__('Items'))->_title($type->getName());
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_items'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Items'));

        $id = $this->getRequest()->getParam('id');
        $tid = $this->getRequest()->getParam('tid');
        $model = Mage::getModel('stella/items');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('This item no longer exists.'));
                $this->_redirect('*/*');

                return;
            }

            $tid = $model->getTypeId();
        }

        $type = Mage::getModel('stella/types')->load($tid);
        if (!$type->getId()) {
            $this->_getSession()->addError($this->__('No item type defined'));
            $this->_redirect('*/stella_types');

            return;
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Item'));

        // set entered data if was error when we do save
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        Mage::register('current_item', $model);
        Mage::register('current_type', $type);

        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_items_edit'))
                ->_addLeft($this->getLayout()->createBlock('stella/adminhtml_items_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($this->getRequest()->isPost()) {
            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getPost();
            $model = Mage::getModel('stella/items');

            if ($id) {
                $model->setId($id);
            }

            $data = $this->_filterDates($data, array('start_date', 'end_date'));
            $tid = $data['type_id'];

            try {
                $model->addData($data);
                $this->_getSession()->setFormData($model->getData());

                $type = Mage::getModel('stella/types')->load($tid);
                if (!$type->getId()) {
                    throw new Mage_core_Exception('No item type defined');
                }

                if ($type->getId() == 1) {
                    $model->setData('name', Mage::helper('stella')->getNameForPaymentCode($data['code']));
                }
                if ($type->getId() == 2) {
                    $model->setData('name', Mage::helper('stella')->getNameForShipmentCode($data['code']));
                }
                $model->save();

                $this->_getSession()->addSuccess($this->__('The Item has been saved.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tid' => $tid, 'tab_id' => $this->getRequest()->getParam('tab_id')));

                    return;
                }
                $this->_redirect('*/*/', array('tid' => $tid));

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id, 'tid' => $tid));

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $id));

                return;
            }
        }
        $this->_redirect('*/*/', array('tid' => $tid));
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id')) {

            $id = $this->getRequest()->getParam('id');

            try {
                $model = Mage::getModel('stella/items');
                $model->setId($id)->delete();

                $this->_getSession()->addSuccess($this->__('Item was successfully deleted'));

                $this->_redirect('*/*/index');
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/index');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

}
