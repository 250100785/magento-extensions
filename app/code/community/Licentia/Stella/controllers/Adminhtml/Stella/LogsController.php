<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Adminhtml_Stella_LogsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('stella/logs');

        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Logs'))->_title($this->__('Invoices'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_logs_invoices'));
        $this->renderLayout();
    }

    public function productsAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Logs'))->_title($this->__('Products'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_logs'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function gridpAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportiCsvAction() {
        $fileName = 'invoices.csv';
        $content = $this->getLayout()->createBlock('stella/adminhtml_logs_invoices_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportiXmlAction() {
        $fileName = 'invoices.xml';
        $content = $this->getLayout()->createBlock('stella/adminhtml_logs_invoices_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportpCsvAction() {
        Mage::register('export_records', true);
        $fileName = 'products_detail.csv';
        $content = $this->getLayout()->createBlock('stella/adminhtml_logs_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportpXmlAction() {
        Mage::register('export_records', true);
        $fileName = 'products_detail.xml';
        $content = $this->getLayout()->createBlock('stella/adminhtml_logs_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportpsCsvAction() {
        $fileName = 'products_simple.csv';
        $content = $this->getLayout()->createBlock('stella/adminhtml_logs_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportpsXmlAction() {
        $fileName = 'products_simple.xml';
        $content = $this->getLayout()->createBlock('stella/adminhtml_logs_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

}
