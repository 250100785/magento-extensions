<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Adminhtml_Stella_ReportsController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('stella/reports');

        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Reports'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_reports'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function refreshAction() {

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('stella/reports');

        $model->load($id);
        if (!$model->getId()) {
            $this->_getSession()->addError($this->__('Report not found.'));
            $this->_redirectReferer();
            return;
        }

        Mage::getModel('stella/reports')->buildReport($model);

        $this->_redirectReferer();
    }

    public function editAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Reports'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('stella/reports');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('This Report no longer exists.'));
                $this->_redirect('*/*');

                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Report'));

        // set entered data if was error when we do save
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        Mage::register('current_report', $model);

        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_reports_edit'))
                ->_addLeft($this->getLayout()->createBlock('stella/adminhtml_reports_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function updatecustomerAction() {

        $id = $this->getRequest()->getParam('id');

        try {
            Mage::getModel('stella/reports')->applyReportsToCustomer($id);
            $this->_getSession()->addSuccess($this->__('Reports Updated'));
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
            Mage::logException($e);
        }
        $this->_redirectReferer();
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getPost();
            $model = Mage::getModel('stella/reports');

            if ($id) {
                $model->setId($id);
            }
            $model->addData($data);
            $this->_getSession()->setFormData($model->getData());

            try {
                $model->save();

                $this->_getSession()->addSuccess($this->__('The Report has been saved.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id')) {
            $id = $this->getRequest()->getParam('id');

            try {
                $model = Mage::getModel('stella/reports');
                $model->setId($id)->delete();

                $this->_getSession()->addSuccess($this->__('Type was successfully deleted'));

                $this->_redirect('*/*/index');
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/index');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function buildAction() {

        $cron = Mage::helper('stella')->scheduleEvent('stella_build_reports');
        if (!$cron) {
            $this->_getSession()->addError($this->__('Please wait until previous cron ends'));
        } else {
            $this->_getSession()->addSuccess($this->__('Report will be built next time your cron runs'));
        }
        $this->_redirectReferer();
    }

    public function gridcAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Export customer grid to CSV format
     */
    public function exportcCsvAction() {
        $fileName = 'customers.csv';
        $content = $this->getLayout()->createBlock('stella/adminhtml_reports_customers_grid')
                ->getCsvFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Export customer grid to XML format
     */
    public function exportcXmlAction() {
        $fileName = 'customers.xml';
        $content = $this->getLayout()->createBlock('stella/adminhtml_reports_customers_grid')
                ->getExcelFile();

        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function customersAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Reports'))->_title($this->__('Customers'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_reports_customers'));
        $this->renderLayout();
    }

    public function massRefreshAction() {
        $customers = $this->getRequest()->getParam('customers');
        $changesNum = 0;

        if (!is_array($customers)) {
            $this->_getSession()->addError($this->__('Please select subscriber(s).'));
        } else {
            $customers = array_unique($customers);
            try {
                foreach ($customers as $customer) {
                    Mage::getModel('stella/reports')->applyReportsToCustomer($customer);
                    $changesNum++;
                }
                $this->_getSession()->addSuccess($this->__('Total of %d record(s) were updated.', $changesNum));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirectReferer();
    }

}
