<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
class Licentia_Stella_Adminhtml_Stella_TypesController extends Mage_Adminhtml_Controller_action {

    protected function _initAction() {
        $this->loadLayout()
                ->_setActiveMenu('stella/types');

        return $this;
    }

    public function indexAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Types'));
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_types'));
        $this->renderLayout();
    }

    public function gridAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__('Stella'))->_title($this->__('Types'));

        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('stella/types');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError($this->__('This type no longer exists.'));
                $this->_redirect('*/*');

                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Type'));

        // set entered data if was error when we do save
        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->addData($data);
        }

        Mage::register('current_type', $model);

        $this->_initAction();

        $this->_addContent($this->getLayout()->createBlock('stella/adminhtml_types_edit'))
                ->_addLeft($this->getLayout()->createBlock('stella/adminhtml_types_edit_tabs'));

        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $data = $this->getRequest()->getPost();
            $model = Mage::getModel('stella/types');

            if ($id) {
                $model->setId($id);
            }
            $model->addData($data);
            $this->_getSession()->setFormData($model->getData());

            try {
                $model->save();

                $this->_getSession()->addSuccess($this->__('The Type has been saved.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId(), 'tab_id' => $this->getRequest()->getParam('tab_id')));

                    return;
                }
                $this->_redirect('*/*/');

                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_getSession()->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam('id')) {
            $id = $this->getRequest()->getParam('id');

            try {
                $model = Mage::getModel('stella/types');
                $model->setId($id)->delete();

                $this->_getSession()->addSuccess($this->__('Type was successfully deleted'));

                $this->_redirect('*/*/index');
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                $this->_getSession()->addError($this->__('An error occurred. Please review the log and try again.'));
                Mage::logException($e);
                $this->_redirect('*/*/index');
            }
        } else {
            $this->_redirect('*/*/');
        }
    }

    public function rebuildAction() {

        try {
            Mage::getModel('stella/types')->rebuild();
            $this->_getSession()->addSuccess($this->__('Rebuilt in progress'));
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirectReferer();
    }

}
