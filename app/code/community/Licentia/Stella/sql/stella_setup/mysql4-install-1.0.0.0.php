<?php

/**
 * Licentia Stella - Customer Management
 *
 * NOTICE OF LICENSE
 * This source file is subject to the Creative Commons Attribution-NonCommercial 4.0 International
 * It is available through the world-wide-web at this URL:
 * http://creativecommons.org/licenses/by-nc/4.0/
 *
 * @title      Customer Management
 * @category   Marketing
 * @package    Licentia
 * @author     Bento Vilas Boas <bento@licentia.pt>
 * @copyright  Copyright (c) 2012 Licentia - http://licentia.pt
 * @license    Creative Commons Attribution-NonCommercial 4.0 International
 */
$installer = $this;
$installer->startSetup();

$installer->run("
-- ----------------------------
--  Table structure for `stella_costs_logs`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_costs_logs')}`;
CREATE TABLE `{$this->getTable('stella_costs_logs')}` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `stella_code` varchar(255) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `payment_code` varchar(255) DEFAULT NULL,
  `shipping_code` varchar(255) DEFAULT NULL,
  `shipping_fixed` decimal(12,4) DEFAULT NULL,
  `shpping_percent` decimal(12,4) DEFAULT NULL,
  `shipping_costs` decimal(12,4) DEFAULT NULL,
  `payment_fixed` decimal(12,4) DEFAULT NULL,
  `payment_percent` decimal(12,4) DEFAULT NULL,
  `payment_costs` decimal(12,4) DEFAULT NULL,
  `product_amount` decimal(12,4) DEFAULT NULL,
  `product_qty` decimal(12,4) DEFAULT NULL,
  `product_cost_row` decimal(12,4) DEFAULT NULL,
  `product_cost` decimal(12,4) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_sku` varchar(255) DEFAULT NULL,
  `discount` decimal(12,4) DEFAULT NULL,
  `row_cost_services` decimal(12,4) DEFAULT NULL,
  `row_cost_goods` decimal(12,4) DEFAULT NULL,
  `row_amount` decimal(12,4) DEFAULT NULL,
  `row_cost` decimal(12,4) DEFAULT NULL,
  `row_profit` decimal(12,4) DEFAULT NULL,
  `order_item_id` int(11) DEFAULT NULL,
  `invoice_item_id` int(11) DEFAULT NULL,
  `human_fixed` decimal(12,4) DEFAULT NULL,
  `human_percent` decimal(12,4) DEFAULT NULL,
  `human_costs` decimal(12,4) DEFAULT NULL,
  `marketing_fixed` decimal(12,4) DEFAULT NULL,
  `marketing_percent` decimal(12,4) DEFAULT NULL,
  `marketing_costs` decimal(12,4) DEFAULT NULL,
  `operational_fixed` decimal(12,4) DEFAULT NULL,
  `operational_percent` decimal(12,4) DEFAULT NULL,
  `operational_costs` decimal(12,4) DEFAULT NULL,
  `financial_fixed` decimal(12,4) DEFAULT NULL,
  `financial_percent` decimal(12,4) DEFAULT NULL,
  `financial_costs` decimal(12,4) DEFAULT NULL,
  `goods_fixed` decimal(12,4) DEFAULT NULL,
  `goods_percent` decimal(12,4) DEFAULT NULL,
  `goods_costs` decimal(12,4) DEFAULT NULL,
  `taxes_fixed` decimal(12,4) DEFAULT NULL,
  `taxes_percent` decimal(12,4) DEFAULT NULL,
  `taxes_costs` decimal(12,4) DEFAULT NULL,
  `other_fixed` decimal(12,4) DEFAULT NULL,
  `other_percent` decimal(12,4) DEFAULT NULL,
  `other_costs` decimal(12,4) DEFAULT NULL,
  `invoice_amount` decimal(12,4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `product_sku` (`product_sku`),
  KEY `product_name` (`product_name`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `FK_STELLA_LOGS_CID` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Items cost log';

-- ----------------------------
--  Table structure for `stella_costs_types`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_costs_types')}`;
CREATE TABLE `{$this->getTable('stella_costs_types')}` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `is_system` enum('0','1') DEFAULT '0',
  `description` varchar(255) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `sales_cost` decimal(12,4) DEFAULT NULL,
  `sales_amount` decimal(12,4) DEFAULT NULL,
  `fixed_monthly` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Costs Types';

-- ----------------------------
--  Table structure for `stella_costs_types_item`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_costs_types_item')}`;
CREATE TABLE `{$this->getTable('stella_costs_types_item')}` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `fixed` decimal(12,4) DEFAULT NULL,
  `percent` smallint(5) DEFAULT NULL,
  `fixed_monthly` decimal(12,4) DEFAULT NULL,
  `is_active` enum('0','1') DEFAULT '1',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `sales_amount` decimal(12,4) DEFAULT NULL,
  `sales_cost` decimal(12,4) DEFAULT NULL,
  `is_system` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `type_id` (`type_id`),
  KEY `code` (`code`),
  CONSTRAINT `FK_STELLA_ITEMS_TID` FOREIGN KEY (`type_id`) REFERENCES `{$this->getTable('stella_costs_types')}` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Cost Types Items';

-- ----------------------------
--  Table structure for `stella_customer`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_customer')}`;
CREATE TABLE `{$this->getTable('stella_customer')}` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `discount` decimal(12,4) DEFAULT NULL,
  `amount` decimal(12,4) DEFAULT NULL,
  `cost_services` decimal(12,4) DEFAULT NULL,
  `cost_goods` decimal(12,4) DEFAULT NULL,
  `profit` decimal(12,4) DEFAULT NULL,
  `taxes` decimal(12,4) DEFAULT NULL,
  `number_orders` int(11) DEFAULT NULL,
  `first_order` datetime DEFAULT NULL,
  `last_order` datetime DEFAULT NULL,
  `order_average_days` smallint(5) DEFAULT NULL,
  `order_average` decimal(12,4) DEFAULT NULL,
  `number_orders_discount` smallint(6) DEFAULT NULL,
  `amount_cycle` decimal(12,4) DEFAULT NULL,
  `number_orders_cycle` int(11) DEFAULT NULL,
  `report_update` datetime DEFAULT NULL,
  `time_left` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `FK_STELLA_CUST_CID` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Customer Quick Stats';

-- ----------------------------
--  Table structure for `stella_customer_aggregated`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_customer_aggregated')}`;
CREATE TABLE `{$this->getTable('stella_customer_aggregated')}` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(11) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `year` smallint(4) DEFAULT NULL,
  `month` smallint(2) DEFAULT NULL,
  `discount` decimal(12,4) DEFAULT NULL,
  `amount` decimal(12,4) DEFAULT NULL,
  `cost_services` decimal(12,4) DEFAULT NULL,
  `cost_goods` decimal(12,4) DEFAULT NULL,
  `profit` decimal(12,4) DEFAULT NULL,
  `taxes` decimal(12,4) DEFAULT NULL,
  `number_orders` int(11) DEFAULT NULL,
  `first_order` datetime DEFAULT NULL,
  `last_order` datetime DEFAULT NULL,
  `order_average_days` smallint(5) DEFAULT NULL,
  `order_average` decimal(12,4) DEFAULT NULL,
  `number_orders_discount` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  UNIQUE KEY `IDX_UQ_CUSTOMER_MONTH_YEAR` (`customer_id`,`year`,`month`),
  CONSTRAINT `FK_STELLA_CUST_AGGR_CID` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Stella - Customer Stats Aggregated ';

-- ----------------------------
--  Table structure for `stella_equations`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_equations')}`;
CREATE TABLE `{$this->getTable('stella_equations')}` (
  `equation_id` int(11) NOT NULL AUTO_INCREMENT,
  `name_1` varchar(255) DEFAULT NULL,
  `equation_1` varchar(255) DEFAULT NULL,
  `name_2` varchar(255) DEFAULT NULL,
  `equation_2` varchar(255) DEFAULT NULL,
  `name_3` varchar(255) DEFAULT NULL,
  `equation_3` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`equation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - LTV Equations';

-- ----------------------------
--  Table structure for `stella_params`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_params')}`;
CREATE TABLE `{$this->getTable('stella_params')}` (
  `param_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`param_id`),
  KEY `IDX_PARAMS_NAME` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Params for various situations';

-- ----------------------------
--  Table structure for `stella_reports`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_reports')}`;
CREATE TABLE `{$this->getTable('stella_reports')}` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `equation_1` decimal(22,4) DEFAULT NULL,
  `equation_2` decimal(22,4) DEFAULT NULL,
  `equation_3` decimal(22,4) DEFAULT NULL,
  `equation_average` decimal(12,4) DEFAULT NULL,
  `period` varchar(255) DEFAULT NULL,
  `build` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`report_id`),
  KEY `segment_id` (`segment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Reports List';

-- ----------------------------
--  Table structure for `stella_reports_customer`
-- ----------------------------
DROP TABLE IF EXISTS `{$this->getTable('stella_reports_customer')}`;
CREATE TABLE `{$this->getTable('stella_reports_customer')}` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) DEFAULT NULL,
  `customer_email` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `customer_id` int(11) unsigned DEFAULT NULL,
  `equation_1` decimal(22,4) DEFAULT NULL,
  `equation_2` decimal(22,4) DEFAULT NULL,
  `equation_3` decimal(22,4) DEFAULT NULL,
  `equation_average` decimal(12,4) DEFAULT NULL,
  PRIMARY KEY (`item_id`),
  KEY `customer_id` (`customer_id`),
  KEY `customer_id_2` (`customer_id`),
  CONSTRAINT `FK_STELLA_REPOR_CUST_CID` FOREIGN KEY (`customer_id`) REFERENCES `{$this->getTable('customer_entity')}` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stella - Customers LTV Reports ';

");


$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_total` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_amount` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_taxes` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_discount` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_cost_goods` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_cost_services` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_profit` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_profit_percent` smallint(6) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice_item')}` ADD COLUMN `stella_processed` enum('0','1') DEFAULT '0'");

$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_total` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_amount` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_taxes` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_discount` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_cost_goods` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_cost_services` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_profit` decimal(12,4) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_profit_percent` smallint(6);");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_processed` enum('0','1') DEFAULT '0';");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_code` varchar(255) DEFAULT NULL;");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_first_order` enum('0','1') DEFAULT '0';");
$installer->run("ALTER TABLE `{$this->getTable('sales_flat_invoice')}` ADD COLUMN `stella_customer_id` int(10) unsigned DEFAULT NULL;");

$installer->run("ALTER TABLE `{$this->getTable('sales_flat_quote_shipping_rate')}` ADD COLUMN `cost` decimal(12,4) DEFAULT NULL");

$installer->endSetup();
