<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category   Zend
 * @package    Zend_Http_UserAgent
 * @subpackage UserAgent
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Lists of User Agent chains for testing :
 *
 * - http://www.useragentstring.com/layout/useragentstring.php
 * - http://user-agent-string.info/list-of-ua
 * - http://www.user-agents.org/allagents.xml
 * - http://en.wikipedia.org/wiki/List_of_user_agents_for_mobile_phones
 * - http://www.mobilemultimedia.be/fr/
 *
 * @category   Zend
 * @package    Zend_Http_UserAgent
 * @subpackage UserAgent
 * @copyright  Copyright (c) 2005-2010 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Zend_Http_UserAgent implements Serializable
{
    /**
     * 'desktop' by default if the sequence return false for each item or is empty
     */
    const DEFAULT_IDENTIFICATION_SEQUENCE = 'mobile,desktop';

    /**
     * Default persitent storage adapter : Session or NonPersitent
     */
    const DEFAULT_PERSISTENT_STORAGE_ADAPTER = 'Session';

    /**
     * 'desktop' by default if the sequence return false for each item
     */
    const DEFAULT_BROWSER_TYPE = 'desktop';

    /**
     * Default User Agent chain to prevent empty value 
     */
    const DEFAULT_HTTP_USER_AGENT = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)';

    /**
     * Default Http Accept param to prevent empty value 
     */
    const DEFAULT_HTTP_ACCEPT = "application/xhtml+xml";

    /**
     * Default markup language 
     */
    const DEFAULT_MARKUP_LANGUAGE = "xhtml";

    /**
     * Browser type
     *
     * @var string
     */
    protected $_browserType;

    /**
     * Browser type class
     *
     * Map of browser types to classes.
     *
     * @var array
     */
    protected $_browserTypeClass = array();

    /**
     * Array to store config
     *
     * Default values are provided to ensure specific keys are present at 
     * instantiation.
     * 
     * @var array
     */
    protected $_config = array(
        'identification_sequence' => self::DEFAULT_IDENTIFICATION_SEQUENCE,
        'storage'                 => array(
            'adapter'             => self::DEFAULT_PERSISTENT_STORAGE_ADAPTER,
        ),
    );

    /**
     * Identified device
     *
     * @var Zend_Http_UserAgent_Device
     */
    protected $_device;

    /**
     * Whether or not this instance is immutable.
     *
     * If true, none of the following may be modified:
     * - $_server
     * - $_browserType
     * - User-Agent (defined in $_server)
     * - HTTP Accept value (defined in $_server)
     * - $_storage
     * 
     * @var bool
     */
    protected $_immutable = false;

    /**
     * Plugin loaders
     * @var array
     */
    protected $_loaders = array();

    /**
     * Valid plugin loader types
     * @var array
     */
    protected $_loaderTypes = array('storage', 'device');

    /**
     * Trace of items matched to identify the browser type
     *
     * @var array
     */
    protected $_matchLog = array();

    /**
     * Server variable
     * 
     * @var array
     */
    protected $_server;

    /**
     * Persistent storage handler
     *
     * @var Zend_Http_UserAgent_Storage
     */
    protected $_storage;

    /**
     * Constructor
     * 
     * @param  null|array|Zend_Config|ArrayAccess $options 
     * @return void
     */
    public function __construct($options = null)
    {
        if (null !== $options) {
            $this->setOptions($options);
        }
    }

    /**
     * Serialized rep