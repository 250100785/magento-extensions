SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `crypta_attributes`
-- ----------------------------
DROP TABLE IF EXISTS `crypta_attributes`;
CREATE TABLE `crypta_attributes` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `attribute_code` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_CRYPTA_ATTRIBUTES_ROLEID` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Crypta - ACL Product Attributes';

-- ----------------------------
--  Table structure for `crypta_tabs`
-- ----------------------------
DROP TABLE IF EXISTS `crypta_tabs`;
CREATE TABLE `crypta_tabs` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `tab` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `FK_CRYPTA_TABS_ROLEID` FOREIGN KEY (`role_id`) REFERENCES `admin_role` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Crypta - ACL Product Tabs';

SET FOREIGN_KEY_CHECKS = 1;
