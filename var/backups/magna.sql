SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `magna_customers`
-- ----------------------------
DROP TABLE IF EXISTS `magna_customers`;
CREATE TABLE `magna_customers` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_id` int(11) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `priority` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `email` (`email`),
  KEY `segment_id` (`segment_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `FK_MAGNA_CUST_CUSTID` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_CUST_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1142 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `magna_evolutions`
-- ----------------------------
DROP TABLE IF EXISTS `magna_evolutions`;
CREATE TABLE `magna_evolutions` (
  `evolution_id` int(11) NOT NULL AUTO_INCREMENT,
  `segment_id` int(11) DEFAULT NULL,
  `records` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `change` int(11) DEFAULT NULL,
  PRIMARY KEY (`evolution_id`),
  KEY `segment_id` (`segment_id`),
  CONSTRAINT `FK_MAGNA_EVO_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=250 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `magna_groups`
-- ----------------------------
DROP TABLE IF EXISTS `magna_groups`;
CREATE TABLE `magna_groups` (
  `change_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `change_date` date DEFAULT NULL,
  `new_group` smallint(5) unsigned DEFAULT NULL,
  `previous_group` smallint(5) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `operation` enum('added','reverted') DEFAULT NULL,
  PRIMARY KEY (`change_id`),
  KEY `new_group` (`new_group`),
  KEY `previous_group` (`previous_group`),
  KEY `segment_id` (`segment_id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `FK_MAGNA_GROUP_CUSTOMER` FOREIGN KEY (`customer_id`) REFERENCES `customer_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_GROUP_NEW` FOREIGN KEY (`new_group`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_GROUP_PREVIOUS` FOREIGN KEY (`previous_group`) REFERENCES `customer_group` (`customer_group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_GROUP_SEGMENT` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Magna - Segments Groups Changes';

-- ----------------------------
--  Table structure for `magna_segments`
-- ----------------------------
DROP TABLE IF EXISTS `magna_segments`;
CREATE TABLE `magna_segments` (
  `segment_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `conditions_serialized` text,
  `is_active` enum('0','1') DEFAULT '1',
  `records` int(11) DEFAULT NULL,
  `description` text,
  `run` varchar(255) DEFAULT NULL,
  `cron` enum('0','d','w','m') DEFAULT '0',
  `cron_last_run` date DEFAULT NULL,
  `build` tinyint(2) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  `notify_user` int(11) DEFAULT NULL,
  `priority` smallint(5) DEFAULT NULL,
  PRIMARY KEY (`segment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `magna_segments_access_blocks`
-- ----------------------------
DROP TABLE IF EXISTS `magna_segments_access_blocks`;
CREATE TABLE `magna_segments_access_blocks` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` smallint(6) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `segment_id` (`segment_id`),
  KEY `block_id` (`block_id`) USING BTREE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_BLOCK_BLOCKID` FOREIGN KEY (`block_id`) REFERENCES `cms_block` (`block_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_BLOCK_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `magna_segments_access_pages`
-- ----------------------------
DROP TABLE IF EXISTS `magna_segments_access_pages`;
CREATE TABLE `magna_segments_access_pages` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` smallint(6) DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `segment_id` (`segment_id`),
  KEY `page_id` (`page_id`) USING BTREE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PAG_PAGEID` FOREIGN KEY (`page_id`) REFERENCES `cms_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PAG_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `magna_segments_access_products`
-- ----------------------------
DROP TABLE IF EXISTS `magna_segments_access_products`;
CREATE TABLE `magna_segments_access_products` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`record_id`),
  KEY `segment_id` (`segment_id`),
  KEY `product_id` (`product_id`) USING BTREE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PROD_PRODID` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_SEGS_ACCESS_PROD_SEGID` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `magna_segments_prices`
-- ----------------------------
DROP TABLE IF EXISTS `magna_segments_prices`;
CREATE TABLE `magna_segments_prices` (
  `price_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `website_id` smallint(5) unsigned DEFAULT NULL,
  `price` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`price_id`),
  KEY `product_id` (`product_id`),
  KEY `segment_id` (`segment_id`),
  KEY `website_id` (`website_id`),
  CONSTRAINT `magna_segments_prices_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `magna_segments_prices_ibfk_2` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
--  Table structure for `magna_segments_prices_idx`
-- ----------------------------
DROP TABLE IF EXISTS `magna_segments_prices_idx`;
CREATE TABLE `magna_segments_prices_idx` (
  `index_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned DEFAULT NULL,
  `segment_id` int(11) DEFAULT NULL,
  `website_id` smallint(5) unsigned DEFAULT NULL,
  `price` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`index_id`),
  KEY `product_id` (`product_id`),
  KEY `segment_id` (`segment_id`),
  KEY `website_id` (`website_id`),
  CONSTRAINT `FK_MAGNA_IDX_PROD` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_IDX_SEG` FOREIGN KEY (`segment_id`) REFERENCES `magna_segments` (`segment_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_MAGNA_IDX_WS` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

SET FOREIGN_KEY_CHECKS = 1;
